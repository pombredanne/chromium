// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ash/system/tray/tray_constants.h"

#include "third_party/skia/include/core/SkColor.h"

namespace ash {

const int kTrayPaddingBetweenItems = 8;
const int kTrayPopupAutoCloseDelayInSeconds = 2;
extern const int kTrayPopupPaddingHorizontal = 18;
const int kTrayPopupPaddingBetweenItems = 5;

const SkColor kBackgroundColor = SK_ColorWHITE;
const SkColor kHoverBackgroundColor = SkColorSetRGB(0xfb, 0xfc, 0xfb);

const int kTrayPopupWidth = 300;

}  // namespace ash
