// This file should almost always be empty. Normally Chromium test expectations
// are only put here temporarily, and moved to
// src/third_party/WebKit/LayoutTests/platform/chromium/test_expectations.txt
// where they will remain until they are rebaselined.
//
//
// If you are landing a Chromium CL that would break webkit layout tests,
// please follow these steps:
//
// 1. Add expected failures to the bottom of this file, and run your CL through
// various *_layout trybots.  Confirm that the trybot results are green (any
// failures are masked by the expectations you added to this file).
//
// 2. BEFORE landing your Chromium CL, land a WebKit CL adding those same
// expected failures to
// src/third_party/WebKit/LayoutTests/platform/chromium/test_expectations.txt .
//
// 3. AFTER you have landed the WebKit test_expectations CL, you can land your
// Chromium CL.  Be sure to include your expected failures in this file, so
// that other tip-of-tree Chromium developers will not be disturbed by your
// change.
//
//
// Every time Chromium's "WebKit Gardener" rolls the WebKit DEPS within
// Chromium, he will delete ALL expectations within this file.  (By then, they
// will be redundant, because you already landed those same expectations in
// src/third_party/WebKit/LayoutTests/platform/chromium/test_expectations.txt .
// Right?)
//
// EVERYTHING BELOW THIS LINE WILL BE DELETED AT EVERY WEBKIT DEPS ROLL
BUGYANGGUO LINUX X86_64 : fast/repaint/moving-shadow-on-container.html = TEXT
BUGYANGGUO LINUX X86_64 : fast/repaint/moving-shadow-on-path.html = TEXT
BUGYANGGUO LINUX X86_64 : svg/css/stars-with-shadow.html = TEXT
BUGYANGGUO MAC : fast/repaint/moving-shadow-on-container.html = TEXT
BUGYANGGUO MAC : fast/repaint/moving-shadow-on-path.html = TEXT
BUGYANGGUO MAC : svg/css/stars-with-shadow.html = TEXT
BUGYANGGUO WIN : fast/repaint/moving-shadow-on-container.html = TEXT
BUGYANGGUO WIN : fast/repaint/moving-shadow-on-path.html = TEXT
BUGYANGGUO WIN : svg/css/stars-with-shadow.html = TEXT

BUGCR117799 : fast/backgrounds/repeat/negative-offset-repeat-transformed.html = IMAGE

// rebaseline when skia 3398 lands
BUGEPOGER : platform/chromium/virtual/gpu/fast/canvas/quadraticCurveTo.xml = IMAGE

BUGCR118411 : fast/repaint/focus-ring.html = IMAGE
BUGCR118411 : fast/repaint/list-marker.html = IMAGE
BUGCR118411 : svg/as-background-image/background-image-preserveaspectRatio-support.html = IMAGE
BUGCR118411 : svg/as-image/img-preserveAspectRatio-support-1.html = IMAGE
BUGCR118411 : svg/hixie/perf/001.xml = IMAGE
BUGCR118411 : svg/hixie/perf/002.xml = IMAGE
BUGCR118411 : svg/hixie/perf/007.xml = IMAGE

