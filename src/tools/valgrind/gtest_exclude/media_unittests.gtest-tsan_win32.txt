# Fails, probably due to the suppressed data races. See http://crbug.com/93932
PipelineIntegrationTest.BasicPlayback

# This test fails reliably in tsan bots after r119048, exclude it for
# now. See  http://crbug.com/109875
PipelineIntegrationTest.SeekWhilePlaying
