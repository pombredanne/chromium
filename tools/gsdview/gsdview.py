# Copyright (c) 2008-2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import datetime
import os
import re
import urllib
import xml.sax
from google.appengine.api import urlfetch
from google.appengine.ext import db
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app


GSD_BASE_URL = 'http://commondatastorage.googleapis.com'


class UsageRecord(db.Model):
  """Entity for storing information about historical access of objects."""
  path = db.StringProperty()
  count = db.IntegerProperty()
  first_access = db.DateTimeProperty()
  last_access = db.DateTimeProperty()


def FixupDate(dt):
  """Convert a date from Google Storage format to human readable form.

  Args:
    dt: a string containing a date.
  Returns:
    A human readable date.
  """
  dt = datetime.datetime(*map(int, re.split('[^\d]', dt)[:-1]))
  dt = dt.strftime('%d-%b-%Y %H:%M')
  return dt


def FixupSize(sz):
  """Convert a size string in bytes to human readable form.

  Args:
    sz: a size string in bytes
  Returns:
    A human readable size in bytes/K/M/G.
  """
  sz = int(sz)
  if sz < 1000:
    sz = str(sz)
  elif sz < 1000000:
    sz = str(int(sz / 100) / 10.0) + 'K'
  elif sz < 1000000000:
    sz = str(int(sz / 100000) / 10.0) + 'M'
  else:
    sz = str(int(sz / 100000000) / 10.0) + 'G'
  return sz


class StorageWalker(xml.sax.handler.ContentHandler):
  """Handler for walking xml returned by Google Storage.

  The input XML looks like this for files:
    <Contents>
      <Key>toolchain/1758/naclsdk_arm-trusted.tgz</Key>
      <LastModified>2010-05-27T21:43:07.713Z</LastModified>
      <ETag>"856eb4f10ea406234d5e95ce02f6bed6"</ETag>
      <Size>62082359</Size>
      <StorageClass>STANDARD</StorageClass>
    </Contents>

  And this for directories:
    <CommonPrefixes>
      <Prefix>toolchain/1758/</Prefix>
    </CommonPrefixes>
  """

  def startDocument(self):
    self.items = []
    self.current_item = {}
    self.truncated = False

  def startElement(self, name, attrs):
    self.inside_truncated = False
    self.focus_field = None
    if name in ['Contents', 'CommonPrefixes']:
      self.current_item = {}
    if name in ['IsTruncated']:
      self.inside_truncated = True
    elif name in ['Key', 'LastModified', 'Size', 'Prefix']:
      self.focus_field = name

  def endElement(self, name):
    if name == 'Contents':
      self.items.append({
          'name': self.current_item['Key'].split('/')[-1],
          'size': FixupSize(self.current_item['Size']),
          'date': FixupDate(self.current_item['LastModified']),
          'link': '/' + self.bucket + '/' + self.current_item['Key'],
          'path': self.current_item['Key'],
          'isdir': False,
      })
    elif name == 'CommonPrefixes':
      self.items.append({
          'name': self.current_item['Prefix'].split('/')[-2] + '/',
          'size': '',
          'date': '',
          'link': '/' + self.bucket + '/' + self.current_item['Prefix'],
          'path': self.current_item['Prefix'],
          'isdir': True,
      })
    self.focus_field = None
    self.inside_truncated = False

  def characters(self, contents):
    if self.focus_field:
      self.current_item[self.focus_field] = contents
    if self.inside_truncated:
      self.truncated = contents == 'true'


def FileOrder(a, b):
  """List directories first, then alphabetize."""
  if a['isdir'] and not b['isdir']: return -1
  if not a['isdir'] and b['isdir']: return 1
  return cmp(a['name'], b['name'])


def GetListing(path, marker):
  """Given a path, lookup a directory listing on Google Storage.

  Args:
    path: Path consisting of bucket followed by / separated path.
    marker: Item to start from (for directories with >1000 items).
  Returns:
    A list of dictionaries containing information on each file.
  """
  path_parts = path[1:].split('/')
  bucket = path_parts[0]
  obj = '/'.join(path_parts[1:])
  url = GSD_BASE_URL + '/%s/?delimiter=/&prefix=%s&marker=%s' % (
      bucket, obj, urllib.quote_plus(marker))
  result = urlfetch.fetch(url, deadline=10)
  assert result.status_code == 200
  h = StorageWalker()
  h.bucket = bucket
  xml.sax.parseString(result.content, h)
  items = sorted(h.items, cmp=FileOrder)
  if h.truncated:
    items.append({
        'name': 'More...',
        'size': '',
        'date': '',
        'link': ('/' + bucket + '/' + obj + '?marker=' +
                 urllib.quote_plus(items[-1]['path'][:-1] + '@')),
        'isdir': True,
    })
  return items


class UploadHandler(webapp.RequestHandler):
  """Generate an anonymous upload form."""

  def get(self):
    path = self.request.path
    template_values = {
        'bucket': path[len('/upload/'):],
    }
    path = os.path.join(os.path.dirname(__file__), 'upload.html')
    self.response.out.write(template.render(path, template_values))


class PathHandler(webapp.RequestHandler):
  """Handle requests for individual directories / files."""

  def get(self):
    path = self.request.path
    marker = self.request.get('marker', '')
    if path.endswith('/'):
      template_values = {
          'path': path[:-1],
          'parent': '/'.join(path[:-1].split('/')[:-1]) + '/',
          'items': GetListing(path, marker),
      }
      tpath = os.path.join(os.path.dirname(__file__), 'directory.html')
      self.response.out.write(template.render(tpath, template_values))
    else:
      q = db.GqlQuery('SELECT * FROM UsageRecord '
                      'WHERE path = :1 ', path)
      ur = q.fetch(1)
      if ur:
        ur = ur[0]
      else:
        ur = UsageRecord()
        ur.count = 0
        ur.first_access = datetime.datetime.utcnow()
        ur.path = path
      ur.count += 1
      ur.last_access = datetime.datetime.utcnow()
      ur.put()
      # Redirect to storage server.
      self.redirect(GSD_BASE_URL + path)


APPLICATION = webapp.WSGIApplication([
    ('/upload/[a-z0-9][a-z0-9-]+[a-z0-9]', UploadHandler),
    ('/.*', PathHandler),
], debug=False)


def main():
  run_wsgi_app(APPLICATION)


if __name__ == '__main__':
  main()
