#!/usr/bin/python
# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.


import io
import os
import re
import signal
import stat
import sys
import tempfile
import urllib


DEPOT_GIT_DIR = 'git_bin'
DEPOT_GIT_FILE = '%s.zip' % DEPOT_GIT_DIR
# The Git scripts call these exes directly, so we keep them in our distribution.
LIBEXEC_EXEMPT_LIST = [
  'git-clone.exe',
  'git-fetch.exe',
  'git-merge-base.exe',
  'git-merge-file.exe',
  'git-merge-index.exe',
  'git-merge-ours.exe',
  'git-merge-recursive.exe',
  'git-merge-subtree.exe',
  'git-merge-tree.exe',
  'git-merge.exe',
  'git-rev-parse.exe',
  'git-unpack-file.exe',
]
PORTABLE_GIT_FILE = 'portablegit.7z'
PORTABLE_GIT_URL = (
    'http://msysgit.googlecode.com/files/PortableGit-1.7.4-preview20110204.7z')
SCRIPTDIR = os.path.dirname(os.path.abspath(__file__))
WIN32PAD_FILE = 'win32pad.zip'
WIN32PAD_URL = 'http://www.gena01.com/win32pad/win32pad_1_5_10_4.zip'


class FlushFile(io.TextIOWrapper):
    def __init__(self, f):
        self.f = f
    def write(self, x):
        self.f.write(x)
        self.f.flush()
sys.stdout = FlushFile(sys.stdout)


def GetFileSize(filename):
  return os.stat(filename)[stat.ST_SIZE]


def main():
  tempdir = tempfile.mkdtemp(dir='/tmp')

  def RemoveTempdir(tempdir):
    if not re.match(r'^/tmp/tmp.*', tempdir):
      return
    os.system('rm -rf %s' % tempdir)

  def handler(signum, frame):
    RemoveTempdir(tempdir)
  signal.signal(signal.SIGINT, handler)

  if os.path.exists('%s/%s' % (SCRIPTDIR, DEPOT_GIT_FILE)):
    print 'ERROR: %s/%s already exists, remove to continue' % (
        SCRIPTDIR, DEPOT_GIT_FILE)
    return 1

  os.chdir(tempdir)
  os.mkdir(DEPOT_GIT_DIR)
  os.chdir(DEPOT_GIT_DIR)

  if os.path.exists(PORTABLE_GIT_FILE):
    print 'ERROR: %s already exists, remove to continue' % PORTABLE_GIT_FILE
    return 1
  print 'Downloading Git from %s... ' % PORTABLE_GIT_URL,
  urllib.urlretrieve(PORTABLE_GIT_URL, PORTABLE_GIT_FILE)
  print 'done'

  # Exact the original portable git archive.
  print 'Extracting archive...'
  os.system('p7zip -d %s' % PORTABLE_GIT_FILE)

  # Remove unused executables.
  os.chdir('libexec/git-core/')
  gitexe_size = GetFileSize('git.exe')
  gitfiles = os.listdir('./')
  for gitfile in gitfiles:
    # Skip processing for files that begin with something other than git- or
    # end with something other than .exe.
    if not (gitfile.startswith('git-') and gitfile.endswith('.exe')):
      continue
    # Skip any git files in the LIBEXEC_EXEMPT_LIST.  The Git scripts call into
    # these exes directly.
    if gitfile in LIBEXEC_EXEMPT_LIST:
      continue
    if GetFileSize(gitfile) == gitexe_size:
      os.system('rm %s' % gitfile)

  # Move back to <tmp>/git_bin/.
  os.chdir('../../')

  # Place extra files into the archive directory.
  os.system('cp %s/files/MANIFEST.chromium ./' % SCRIPTDIR)
  os.system('cp %s/files/git.bat ./' % SCRIPTDIR)
  os.system('cp %s/files/git.cmd ./cmd/' % SCRIPTDIR)
  os.system('cp %s/files/gitk.bat ./' % SCRIPTDIR)
  os.system('cp %s/files/gitk.cmd ./cmd/' % SCRIPTDIR)
  os.system('cp %s/files/ssh_config ./' % SCRIPTDIR)
  os.system('cp %s/files/ssh.bat ./' % SCRIPTDIR)
  os.system('cp %s/files/ssh.cmd ./cmd/' % SCRIPTDIR)
  os.system('cp %s/files/ssh-keygen.bat ./' % SCRIPTDIR)
  os.system('cp %s/files/ssh-keygen.cmd ./cmd/' % SCRIPTDIR)

  # Set up win32pad.
  os.mkdir('win32pad')
  os.chdir('win32pad')
  print 'Downloading Win32pad from %s... ' % WIN32PAD_URL,
  urllib.urlretrieve(WIN32PAD_URL, WIN32PAD_FILE)
  print 'done'
  os.system('unzip %s' % WIN32PAD_FILE)
  os.system('rm %s' % WIN32PAD_FILE)
  os.system('cp %s/files/README.win32pad ./' % SCRIPTDIR)
  os.chdir('..')

  # Create git_bin.zip archive.
  os.chdir('..')
  os.system('zip -r %s %s' % (DEPOT_GIT_FILE, DEPOT_GIT_DIR))

  # Set the archive aside.
  os.chdir(SCRIPTDIR)
  os.system('mv %s/%s ./' % (tempdir, DEPOT_GIT_FILE))
  RemoveTempdir(tempdir)

  return 0


if __name__ == '__main__':
  sys.exit(main())
