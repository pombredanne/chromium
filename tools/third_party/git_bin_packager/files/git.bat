@echo off
setlocal
if DEFINED EDITOR goto rungit
set EDITOR=win32pad
set PATH=%~dp0git_bin\win32pad;%PATH%

:rungit
set PATH=%~dp0git_bin\cmd;%PATH%
"%~dp0git_bin\cmd\git.cmd" %*
