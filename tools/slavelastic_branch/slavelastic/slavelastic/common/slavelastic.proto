// Copyright (c) 2010 Marc-Antoine Ruel

// Requires protoc 2.3 but lucid includes 2.2.
//option py_abstract_services = false;

message Register {
  enum Type {
    NONE = 0;
    STRING = 1;
    PROCESS = 2; // string_value
    EXCEPTION = 3; // string_value
    REGISTERS = 4; // An array/dict of registers
    STREAM = 5; // uint64 under stream_id
  }
  
  enum Pipe {
    STDOUT = 0;
    STDERR = 1;
  }

  optional string name = 1;
  optional Type type = 2 [default = NONE];
  optional string string_value = 3;
  repeated Register registers = 4;
  optional uint64 stream_id = 5;
  optional Pipe pipe = 6 [default = STDOUT];  
}

message ExchangeRegisterData {
  required Register original = 1;
  required Register new = 2;
}

message ExecuteData {
  repeated string args = 1;
  optional string cwd = 2;
  // Use 'a=foo' format.
  repeated string env = 3;
  optional bool shell = 4;
}

message Name {
  optional string name = 1;
}

// Absolutely minimal implementation. Everything can be done with just that.
service SlaveServiceCore {
  // Registers
  // Returns all the variables, pipes and processes.
  rpc GetRegister(Register) returns(Register);

  // Sets a variable. If the type is NONE, deletes the variable. This also
  // kills processes and close their handle.
  rpc SetRegister(Register) returns(Register);

  // Exchange a variable only if original was already set to the same value.
  // Useful in multi-master configurations. Returns the original value.
  rpc InterlockedExchangeRegister(ExchangeRegisterData) returns(Register);

  // Start a process and returns a process handle and its pipes.
  rpc Execute(ExecuteData) returns(Register);

  // Reads a file or stdout from a process.
  rpc Read(Register) returns(Register);

  // Writes a file or to a process's stdin.
  rpc Write(Register) returns(Register);

  // Slave life-time.
  rpc Quit(Register) returns(Register);
}

// Adds frequently used utility functions.
service SlaveServiceOpt {
  // Creates a directory.
  rpc CreateDirectory(Name) returns(Register);

  // Delete file or directory, recursively or terminate a process with a signal.
  rpc Delete(Register) returns(Register);
  
  // Change Directory
  rpc ChangeDirectory(Name) returns(Register);
  
  // Unzips a file
  rpc Unzip(Register) returns (Register);
  
  // Query the status of a Stream
  rpc QueryStream(Register) returns (Register);
}
