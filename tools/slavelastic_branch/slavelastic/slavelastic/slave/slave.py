#!/usr/bin/python
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""The dumb slave."""

__version__ = '0.1'

from collections import deque
import logging
import optparse
import os
import platform
import shutil
import socket
import subprocess
import sys
import threading
import zipfile


ROOT_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from pbrpc.rpc_server import RpcContext, ListenTcpChannel, TcpChannel
from pbrpc.rpc_server import sync_call
from slavelastic.common.reg import Reg
from slavelastic.common.slavelastic_pb2 import Register, SlaveServiceCore
from slavelastic.common.slavelastic_pb2 import SlaveServiceOpt
from slavelastic.slave import http


class SlaveServiceImpl(SlaveServiceCore, SlaveServiceOpt):
  def __init__(self, context, options):
   # TODO(maruel): We shouldn't use Register internally.
    self.registers = {}
    def Set(name, obj):
      self.registers[name] = Reg(obj, name)
    Set('version', __version__)
    Set('name', options.name)
    Set('coordinators', None)
    Set('inbound_port', options.port)
    Set('reconnect_timeout', 10*60)
    Set('reconnect_wait', '1')
    Set('hostname', socket.getfqdn())
    # TODO(maruel): Make it less 'python'
    Set('platform', sys.platform)
    Set('bitness', platform.machine())
    Set('cwd', os.getcwd())
    Set('lock', 'Unlocked')
    self.processes = {}
    self.context = context
    # TODO(hinoka): Maybe make this a priority Queue?
    self.streams = deque(maxlen=7)
    self.active_streams = {}
    self.closed_processes = deque(maxlen=5)
  
  def update_register(self, name, value):
    if name not in self.registers:
      return
    self.registers[name] = Reg(value, name)

  def tearDown(self):
    """On normal shutdown, kill remaining processes."""
    for k, v in self.processes.iteritems():
      v.kill()
      del self.registers[k]
    self.processes = {}
  
  def _new_send_thread(self, fd, stream, name):
    def _sender(fd, stream, name):
      status = [stream.stream_id, [name, 'Running']]
      self.streams.appendleft(status)
      logging.debug('_sender thread started, waiting for accept')
      stream.accept()
      logging.debug('_sender Connected accepted, stream established')
      if not fd:
        # TODO(hinoka): Change close() to remove the stream from the
        # listening pool so that this conditional can be placed
        # before accept()
        stream.close()
        return
      while True:
        # Note: Pipes only flush every 4096 bytes
        buf = fd.read(1024)
        if not buf:
          break
        stream.send(buf)
      logging.debug('_sender Stream finished, closing stream')
      stream.close()
      fd.close()
      status[1][1] = 'Closed'
    t = threading.Thread(target=_sender, args=[fd, stream, name], name=name)
    t.daemon = True
    return t
  
  def _new_recv_thread(self, fd, stream, name):
    def _receiver(fd, stream, name):
      status = [stream.stream_id, [name, 'Running']]
      self.streams.appendleft(status)
      stream.accept()
      while True:
        buf = stream.recv()
        if not buf:
          break
        fd.write(buf)
      stream.close()
      fd.close()
      status[1][1] = 'Closed'
    t = threading.Thread(target=_receiver, args=[fd, stream, name], name=name)
    t.daemon = True
    return t

  ## SlaveServiceCore
  @sync_call
  def GetRegister(self, request):
    # TODO(maruel): Support regexp or something to return a subset?
    if not request.name:
      # Return them all
      return Register(type=Register.REGISTERS,
          registers=self.registers.values())
    elif request.name in self.registers:
      return self.registers[request.name]
    else:
      return Reg(None)

  @sync_call
  def SetRegister(self, request):
    # TODO(maruel): Some are protected and some have side-effects. cwd == change
    # cwd.
    if request.type == Register.NONE:
      # TODO(maruel): If a PROCESS, actual accordingly.
      if request.name in self.registers:
        del self.registers[request.name]
    else:
      self.registers[request.name] = request
    return Reg('ok')

  @sync_call
  def InterlockedExchangeRegister(self, request):
    if request.original.name != request.new.name:
      return Reg(None)
    name = request.original.name
    response = self.registers[name]
    if self.registers[name] == request.original:
      self.registers[name] = request.new
      # Switched states, might be a good time to do some cleanup
      self.clean_up()
    return response

  @sync_call
  def Execute(self, request):
    env = None
    if request.env:
      env = os.environ.copy()
      for item in request.env:
        i = str(item).split('=', 1)
        env[i[0]] = i[1]
    shell = sys.platform == 'win32'
    if request.HasField('shell'):
      shell = request.shell
    cwd = None
    if request.HasField('cwd'):
      cwd = request.cwd
    proc = subprocess.Popen(request.args, cwd=cwd, env=env,
        stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE,
        shell=shell, universal_newlines=True)
    name = '%s:%s' % (request.args[0], proc.pid)
    self.processes[name] = proc
    response = Register(type=Register.PROCESS, name=name)
    self.registers[name] = response
    # TODO(maruel): Callback/push data has it arrives. For that, it needs to use
    # select.select(). Since we're in python, we could use twisted.
    return response

  @sync_call
  def Read(self, request):
    """Reads from stdout/stderr, or from a file.  See Write() for params."""
    stream = self.context.new_stream()
    stream_id = stream.listen()
    name = request.name
    if request.type == Register.PROCESS:
      if request.pipe == Register.STDOUT:
        fd = self.processes[request.name].stdout
        name += '-stdout'
      else:
        fd = self.processes[request.name].stderr
        name += '-stderr'
    elif request.name:
      fd = open(os.path.expandvars(request.name), 'rb')
      name += '-read'
    else:
      return Reg(None)
    send_thread = self._new_send_thread(fd, stream, name)
    send_thread.start()
    return Reg(stream_id, request.name, stream=True)

  @sync_call
  def Write(self, request):
    """Write to either a file or a processes' stdin.
    args:
        (process): Register(name=process, type=PROCESS)
        (file): Register(name=filename)
    returns:
        Register(stream_id=stream_id, name=filename, type=STREAM)"""
    stream = self.context.new_stream()
    stream_id = stream.listen()
    name = request.name
    if request.type == Register.PROCESS:
      fd = self.processes[request.name].stdin
      name += '-stdin'
    elif request.name:
      fd = open(os.path.expandvars(request.name), 'wb')
      name += '-write'
    else:
      return Reg(None)
    recv_thread = self._new_recv_thread(fd, stream, name)
    recv_thread.start()
    return Reg(stream_id, request.name, stream=True)

  @sync_call
  def Quit(self, request):
    self.tearDown()
    sys.exit(1)

  ## SlaveServiceOpt
  @sync_call
  def Delete(self, request):
    if request.type == Register.STRING:
      expanded = os.path.expandvars(request.string_value)
      if os.path.isfile(expanded):
        logging.debug('Deleting file "%s"' % expanded)
        os.remove(expanded)
      elif os.path.isdir(expanded):
        # TODO(maruel): Add the mega code to remove read-only files on Windows
        # and ownership takeover.
        logging.debug('Deleting directory "%s"' % expanded)
        shutil.rmtree(expanded)
      else:
        logging.debug('Can\'t delete "%s"' % expanded)
        return Reg(None)
      return Reg('ok')
    elif request.type == Register.PROCESS:
      self.processes[request.name].kill()
      response = Reg(self.processes[request.name].returncode)
      del self.processes[request.name]
      del self.registers[request.name]
      return response
    else:
      return Reg(None)

  @sync_call
  def ChangeDirectory(self, request):
    expanded_dir = os.path.expandvars(request.name)
    if os.path.isdir(expanded_dir):
      os.chdir(expanded_dir)
      self.registers['cwd'] = Reg(os.getcwd(), 'cwd')
      return Reg('ok')
    return Reg(None)  # Directory does not exist
  
  @sync_call
  def CreateDirectory(self, request):
    expanded_dir = os.path.expandvars(request.name)
    if not os.path.isdir(expanded_dir):
      os.makedirs(expanded_dir)
    return Reg('ok')
  
  @sync_call
  def Unzip(self, request):
    """Args:
      request.name: zipfile name
      request.string_value: dest path, defaults to CWD"""
    expanded = os.path.expandvars(request.name)
    try:
      temp_zip = zipfile.ZipFile(expanded, 'r')
      if request.string_value:
        expanded = os.path.expandvars(request.string_value)
      temp_zip.extractall(request.string_value)
    finally:
      temp_zip.close()
    return Reg('ok')
    
  @sync_call
  def QueryStream(self, request):
    """Args:
      request.stream_id: stream_id to query
    Returns:
      response.string_value: { 'Running', 'Closed', 'Error', 'Invalid' }
      (Error means does not exist, could be closed and deleted)
      (Invalid means it lacks a stream_id)
    """
    if not request.type == Register.STREAM:
      return Reg('Invalid')
    streams_dict = dict(self.streams)
    if request.stream_id in streams_dict:
      name, status = streams_dict[request.stream_id]
      return Reg(status, name)
    else:
      return Reg('Error')
  
  def clean_up(self):
    """Cleans out finished processes"""
    to_delete = []
    for key, proc in self.processes.iteritems():
      status = proc.poll()
      if status:
        self.closed_processes.appendleft([key, status])
        to_delete.append(key)
    for key in to_delete:
      del self.processes[key]
      del self.registers[key]
  
    
def main(argv):
  parser = optparse.OptionParser()
  parser.add_option('--host')
  parser.add_option('-p', '--port', default=9000, type=int)
  parser.add_option('-v', '--verbose', action='store_true')
  parser.add_option('-n', '--name', default='Unnamed Slave', type=str)
  options, args = parser.parse_args()
  level = None
  if options.verbose:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  if options.host:
    channel = TcpChannel(options.host, options.port)
  else:
    channel = ListenTcpChannel('', options.port)
    print 'Listening on port %d' % channel.port
  context = RpcContext(channel)
  impl = SlaveServiceImpl(context, options)
  context.register_impl(impl, SlaveServiceCore)
  context.register_impl(impl, SlaveServiceOpt)
  http_thread = threading.Thread(target=http._run_http, args=[impl])
  http_thread.daemon = True
  http_thread.start()
  context.run()


if __name__ == '__main__':
  sys.exit(main(sys.argv))