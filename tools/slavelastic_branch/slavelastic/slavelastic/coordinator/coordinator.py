#!/usr/bin/python
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Coordinator.

Sorry, it's just a place holder. Come back again!

It is at the same time a service and a client.

Should use twisted...
"""

__version__ = '0.1'

import logging
import optparse
import os
import sys

ROOT_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from pbrpc.rpc_server import RpcContext, RpcServiceStub, TcpChannel
from slavelastic.common.slavelastic_pb2 import Register, SlaveServiceCore_Stub
from slavelastic.common.slavelastic_pb2 import SlaveServiceOpt_Stub
from slavelastic.common.reg import Reg
from slavelastic.slave.slave import Slave


class Coordinator(Slave):
  def __init__(self):
    Slave.__init__(self)
    pass

  def tbr(self):
    # TODO(maruel): We shouldn't use Register internally.
    self.registers = {}
    def Set(name, obj):
      self.registers[name] = Reg(obj, name)
    Set('version', __version__)
    Set('name', None)
    Set('status_servers', None)
    Set('logic_servers', None)
    # TODO(maruel): Get from options.
    Set('inbound_port', None)
    Set('connected_slaves', None)
    Set('reconnect_timeout', 10*60)
    Set('reconnect_wait', '1')
    Set('hostname', socket.getfqdn())
    # TODO(maruel): Make it less 'python'
    Set('platform', sys.platform)
    Set('bitness', platform.machine())
    Set('cwd', os.getcwd())
    # Could the coordinator run processes too? It does make sense. In that
    # respect, a Coordinator is a superset of a Slave and the object hierarchy
    # should reflect that fact.
    self.processes = {}


# TODO(maruel): Reuse most of the class hierarchy of buildbot.
# Do it make sense to have protobufs for BuildStep's commands? JSON is probably
# better in that case.
class Bot(object):
  """a Client instance."""

class Build(object):
  """a series of BuildStep."""

class BuildStep(object):
  """a logical series of commands (and conditions) to send to the client."""

class BuildRequest(object):
  """a pending to be generated Build with the properties."""

class Builder(object):
  """a logical item that dispatch BuildRequest to a Slave."""
  # Ask Logic server for what to execute, in fact a BuildFactory.

class Slave(object):
  """a logical instance on a slave, connected to one or many Builder."""

class Change(object):
  properties = {}

class BuildFactory(object):
  """Ask a logic server to generate a Build object."""


def callback(request, response):
  print "Got %s" % response.name


def Do(service, request):
  print "Request: %s" % str(request)
  #response = service.Do(request, callback=callback)
  response = service.Do(request, timeout=10000)
  print "Got %s" % str(response)


def main(argv):
  parser = optparse.OptionParser()
  parser.add_option('--config', default='coordinator.cfg',
                    help='Configuration file')
  parser.add_option('--port', default=9000, type=int)
  parser.add_option('--host', default='127.0.0.1')
  parser.add_option('-v', '--verbose', action='store_true')
  options, args = parser.parse_args()
  level = None
  if options.verbose:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  coordinator = Coordinator()
  # The coordinator will have multiple build in flight.
  # The Build are triggered by an event.
  # Events can be:
  # - Change source notification
  # - BuildStep finished
  # - Build finished
  # - Whatever in StatusPush.
  #
  # It's the Logic Server that triggers the Builds?
  #
  context = RpcContext(ListenTcpChannel(options.host, options.port))
  service = RpcServiceStub(context, None, SlaveServiceCore_Stub,
      SlaveServiceOpt_Stub)
  r = service.GetRegister(Reg(None, 'reg1'))
  print "Got %s" % r
  r = service.SetRegister(Reg('foo', 'reg1'))
  print "Got %s" % r
  r = service.GetRegister(Reg(None))
  print "Got %s" % r
  loop(coordinator)
  return 0


if __name__ == '__main__':
  sys.exit(main(sys.argv))
