#!/usr/bin/python
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Distribute test jobs based on manifest files."""

import fnmatch
import json
import logging
import optparse
import os
import sys
import tempfile
import time
import threading
import zipfile

ROOT_DIR = os.path.realpath(os.path.join(__file__, '..', '..', '..'))
sys.path.append(ROOT_DIR)

from slavelastic.client.client import Client
from slavelastic.client.slave_finder import SlaveFinder
import test_handler


class Distribute(object):
  def __init__(self):
    self.timer = {}  # Dict to store how long it takes to do things
    self.timer['zip'] = None  # Time to zip all files
    self.timer['find_slaves'] = None  # Time to find slaves
    self.timer['start_slaves'] = None  # Time to start slave handlers
    self.timer['all'] = None  # Time from start to finish
    self.tempdir = tempfile.mkdtemp()  # For the zip files
    self.zipped_files = None
  
  @staticmethod  
  def _locate(pattern, root=os.curdir):
    """Locate all files matching supplied filename pattern in and below
    supplied root directory."""
    for path, dirs, files in os.walk(os.path.abspath(root)):
      for filename in fnmatch.filter(files, pattern):
        yield os.path.join(path, filename)
  
  @staticmethod
  def get_json(manifest_name):
    """Given a manifset filename like 'chrome.win32', it opens
    the file and returns the parsed json dict"""
    manifest = os.path.abspath(manifest_name)
    with open(manifest, 'r') as fd:
      man = json.load(fd)
    # TODO(hinoka): Check if all required fields are present, throw otherwise
    man.setdefault('shard_index', 1)
    man.setdefault('shard_count', 1)
    man.setdefault('properties')
    man.setdefault('output')
    # Get slave platform from the manifest filename
    # It's always testname.platform.manifest
    man['platform'] = manifest_name.split('.')[-2]
    # Get the right os.path handler for the slave platform
    man['slave_os_path'] = Client.os_path(man['platform'])
    return man
  
  @staticmethod
  def _path_join(start_path, end_path, slave_os_path):
    """joins start_path and end_path.  end_path is client-type path
    while start_path is slave-type path"""
    end_path_items = os.path.split(end_path)
    for item in end_path_items:
      start_path = slave_os_path.join(start_path, item)
    return start_path
  
  def get_zipped(self, manifest, manifest_path, zip_file='zipfile'):
    """Returns a list of (zipfile.zip, output_path), one for each DEP.
    This assumes all dep files are in the same folder."""
    manifest['input_path'] = os.path.realpath(os.path.join(manifest_path,
        manifest['input_path']))
    zip_file = os.path.join(self.tempdir, zip_file + '.zip')
    output_zip = zipfile.ZipFile(zip_file, 'w',
        compression=zipfile.ZIP_DEFLATED)
    timer = time.time()
    if 'inputs' not in manifest:
      return []
    root = manifest['input_path']
    result = [(os.path.abspath(zip_file), manifest['output_path'])]
    for item in manifest['inputs']:
      cur_dir = os.path.join(root, os.path.dirname(item))
      item = os.path.basename(item)
      for subitem in self._locate(item, cur_dir):
        subitem_rel = os.path.relpath(subitem, root)
        output_zip.write(subitem, subitem_rel, zipfile.ZIP_DEFLATED)
    output_zip.close()
    if 'deps' in manifest:
      for dep in manifest['deps']:
        man = self.get_json(os.path.join(manifest_path, dep))
        result += self.get_zipped(man, manifest_path, dep)
    self.timer['zip'] = (time.time() - timer)
    return result
  
  @staticmethod
  def find_slaves(slave_count, slave_file):
    """Gets list of slaves, query until we get enough free slaves.
    Slaves receive two commands: hold and lock.  Hold will hold that
    slave for up to 20 seconds, and lock will hold that slave indefinitely.
    Slave must be held by the same client that locked it.
      
      Returns: List of slaves, might be less than slave_count if busy."""
    slave_finder = SlaveFinder(slave_file=slave_file)
    results = []
    while True:
      if len(results) == slave_count:
        break
      slave = slave_finder.pop()
      if not slave:
        break
      slave_host, slave_port = slave
      client = Client(slave_host, slave_port)
      if not client.is_connected():
        sys.stderr.write('Connection to %s:%d failed\n' \
                         % (slave_host, slave_port))
        continue
      if client.lock():
        logging.info('Connected to %s:%d' % (slave_host, slave_port))
        print 'Connected to %s:%d' % (slave_host, slave_port)
        results.append(client)
      else:
        sys.stderr.write('Connection to %s:%d failed - Locked\n' \
                         % (slave_host, slave_port))
    return results
  
  @staticmethod
  def _stream_reader(tempbuffer, out_pipe):
    """Prints a buffered tream to an output pipe (eg. stdout)"""
    while True:
      # Note we're reading from a StreamBuffer object, this blocks
      buf = tempbuffer.read(1024)
      if not buf:
        break
      out_pipe.write(buf)
  
  def _run_zipper(self, man, manifest_path, manifest_name):
    """Runs the zipper in a separate thread so we don't have to wait for this
    to finish before we find that there aren't any slaves around."""
    def _run_zipper_helper(*args):
      self.zipped_files = self.get_zipped(*args)
    t = threading.Thread(target=_run_zipper_helper, args=[man,
        manifest_path, manifest_name])
    t.start()
    return t

  def run(self, manifest, shard_count=None):
    """Opens up the manifest file and runs tests on slaves"""
    # Parse manifest file (eg. base_unit_tests.win32.manifest).
    overall_timer = time.time()
    man = self.get_json(manifest)
    manifest_path = os.path.dirname(os.path.abspath(manifest))
    manifest_name = os.path.basename(manifest)
    # Locate all input files, including dependencies.
    zipper_thread = self._run_zipper(man, manifest_path, manifest_name)
    if not shard_count:
      shard_count = int(man['shard_count'])
    properties = man['properties']
    output_path = man['output_path']
    slave_os_path = man['slave_os_path']
    outputs = man['outputs']
    cleanup = man['pre_cleanup']  # Folder(s) to delete before we run tests
    slaves = []
    try:
      # Attempt to acquire the requested number of slaves
      timer = time.time()
      print 'finding slaves...'
      slaves = self.find_slaves(shard_count, os.path.join(manifest_path,
          'slaves.json'))
      print 'done finding slaves'
      self.timer['find_slaves'] = (time.time() - timer)
      if not slaves:  # No slaves found
        print 'No slaves found'
        return False
      zipper_thread.join()  # Thread dies if we didn't find any slaves
      shard_count = len(slaves)  # Might not have found enough slaves
      test_handlers = []
      # Start a new thread to handle each shard, store in test_handlers
      timer = time.time()
      for shard_index in range(0, shard_count):
        handler = test_handler.types[man['type']](
            slave=slaves[shard_index], cleanup=cleanup,
            slave_os_path=slave_os_path, output_path=output_path, 
            zipped_files=self.zipped_files, outputs=outputs,
            properties=properties, shard_index=shard_index,
            shard_count=shard_count)
        handler.run()
        test_handlers.append(handler)
      self.timer['start_slaves'] = (time.time() - timer)
      i = 0
      for handler in test_handlers:
        # Prints both streams out to their respective pipes (stdout/stderr)
        stdout_thread = threading.Thread(target=self._stream_reader,
            name='stdout_reader%d' % i,
            args=[handler.get_stdout(), sys.stdout])
        stderr_thread = threading.Thread(target=self._stream_reader,
            name='stderr_reader%d' % i,
            args=[handler.get_stderr(), sys.stderr])
        stdout_thread.start()
        stderr_thread.start()
        stdout_thread.join()
        stderr_thread.join()
        handler.join()
        i += 1
    except Exception:
      print 'exception raised in Distribute.run()'
      raise
    finally:  # Make sure we unlock all slaves if we run into an exception
      if slaves:
        for slave in slaves:
          slave.unlock()
      self.timer['all'] = (time.time() - overall_timer)
    print 'Time Elapsed:'
    for key, value in self.timer.iteritems():
      print '%s:\t %f' % (key, value)


def main(argv):
  parser = optparse.OptionParser('%prog [options] manifest_file')
  parser.add_option('-c', '--shard_count', dest='shard_count', default=None,
                    help='Number of shards')
  parser.add_option('-i', '--input_path', dest='input_path', default='..')
  (options, args) = parser.parse_args()
  shard_count = options.shard_count
  if len(args) != 1:
    parser.print_usage()
    return  
  manifest = args[0]
  Distribute().run(manifest, shard_count)

if __name__ == '__main__':
  main(sys.argv)