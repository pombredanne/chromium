import os
import sys
import time
import threading

ROOT_DIR = os.path.join('..', '..', os.path.abspath(__file__))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from slavelastic.client.stream_buffer import StreamBuffer


class BaseHandler(object):
  """Should be common with all handlers.  Also manifests like 'chrome.win32'
  is defined as a base since it doesn't do anything, and should throw an
  error if you try to run it."""
  # slave is actually a Client object referring to a slave
  def __init__(self, slave, zipped_files, properties, shard_index, shard_count):
    self.slave = slave
    self.zipped_files = zipped_files
    self.properties = properties
    self.shard_index = shard_index
    self.shard_count = shard_count

  def run(self):
    raise NotImplementedError
  
  def get_stdout(self):
    raise NotImplementedError
  
  def get_stderr(self):
    raise NotImplementedError


class GtestHandler(BaseHandler):
  """Gtests, order of operations
  1. Upload all files to remote server, keep the same filepath
  2. Run whats in properties['test_name'] with --gtest_catch_exceptions along
  with shard_count/shard_index as the env 'GTEST_TOTAL_SHARDS' and
  'GTEST_SHARD_INDEX'
  """
  def __init__(self, slave, cleanup, slave_os_path, output_path, zipped_files,
        outputs, properties, shard_index, shard_count):
    # List of (slave_files, client_path) to retrieve
    self.outputs = outputs
    # The therad running the test, will be filled when run() is called
    self.test_thread = None
    # Directory to run everything
    self.cwd = properties['cwd']
    # os.path specific to the slave platform
    self.slave_os_path = slave_os_path
    # Path + Name of the unittest.exe file to run relative to cwd
    self.test_name = properties.setdefault('test_name')
    if not self.test_name:
      raise NameError('test_name specifier not found')
    # Buffers for all stdio.
    self.stdout_buffer = StreamBuffer()
    self.stderr_buffer = StreamBuffer()
    # Directory to upload files to
    self.output_path = output_path
    self.stdin = None
    BaseHandler.__init__(self, slave, zipped_files,
                         properties, shard_index, shard_count)
  
  @staticmethod
  def _stream_buffer(stream, out_buffer):
    """Thread used to read data coming in from a stream into a buffer."""
    while True:
      buf = stream.recv(128)
      if not buf:
        break
      out_buffer.write(buf)
    out_buffer.close()
  
  def _run_thread(self):
    """The actual thread that sends the execute command to the remote slave.
    This thread spends most of its life asleep"""
    try:
      self.uploader()
      self.slave.cd(self.cwd)
      test_name = self.properties['test_name']
      args = [test_name, '--gtest_catch_exceptions']
      env = {'GTEST_TOTAL_SHARDS' : self.shard_count,
             'GTEST_SHARD_INDEX' : self.shard_index}
      proc = self.slave.run(args, env=env)
      stdout_thread = threading.Thread(target=self._stream_buffer,
          name='stdout_buffer', args=[proc.stdout, self.stdout_buffer])
      stderr_thread = threading.Thread(target=self._stream_buffer,
          name='stderr_buffer', args=[proc.stderr, self.stderr_buffer])
      self.stdin = proc
      stdout_thread.start()
      stderr_thread.start()
      stdout_thread.join()
      stderr_thread.join()
      # We're done, lets get them [sic] files
      self.get_files()
    except Exception:
      sys.stderr.write('Caught exception while running on shard %d\n' \
                       % self.shard_index)
      raise
    finally:
      self.cleanup()
  
  def join(self):
    self.test_thread.join()
  
  def get_stdout(self):
    return self.stdout_buffer
  
  def get_stderr(self):
    return self.stderr_buffer
  
  def write(self, data):
    """Sends data to the slave's stdin"""
    assert self.stdin.is_open, 'stdin is not open'
    self.stdin.send(data)
    
  def get_files(self):
    """Get the files from output directory, they're written as a list of
    (remote files, local folder) tuples."""
    lcwd = os.getcwd()
    rcwd = self.slave.cwd()
    for remote_file, local_path in self.outputs:
      expanded = os.path.expandvars(local_path)
      expanded = os.path.join(expanded, 'Shard_%2d' % self.shard_index)
      if not os.path.isdir(expanded):
        os.makedirs(expanded)
      os.chdir(expanded)
      remote_path = self.slave_os_path.dirname(remote_file)
      remote_base = self.slave_os_path.basename(remote_file)
      # Different arguments used for windows and linux/OSX
      if self.slave_os_path.__name__ == 'posixpath':
        args = ['find', '.', '-name', remote_base]
      elif self.slave_os_path.__name__ == 'ntpath':
        args = ['FOR /R . %%v IN (%s) DO @dir %%v /b /s' % remote_base]
      self.slave.cd(remote_path)
      find_proc = self.slave.run(args)
      result = ''
      while True:
        buf = find_proc.stdout.recv(128)
        if not buf:
          break
        result += buf
      for file in result.split('\n'):
        if file == 'File Not Found' or not file:
          continue
        self.slave.get(file)
    os.chdir(lcwd)
    self.slave.cd(rcwd)

  def uploader(self):
    """Called before _run_thread to send all required files to the slave."""
    t1 = time.time()
    print 'Uploading files....'
    for local_zip, dest_path in self.zipped_files:
      print '%s -> %s' % (local_zip, dest_path)
      # Store CWD so we can restore back to original state
      rcwd = self.slave.cwd()
      cwd = os.getcwd()
      os.chdir(os.path.dirname(local_zip))
      zipname = os.path.basename(local_zip)
      # Just incase remote DIR doesn't exist
      self.slave.mkdir(dest_path)
      self.slave.cd(dest_path)
      self.slave.put(zipname)
      self.slave.unzip(zipname)
      self.slave.rm(zipname)
      self.slave.cd(rcwd)
      os.chdir(cwd)
    print 'Time Elapsed: %f' % (time.time() - t1 )
  
  def cleanup(self):
    """All post-execution commands go here.  Eg. delete files, reboot"""
    # TODO(hinoka): Decide if we want to delete everything or not.
    pass
  
  def deleter(self):
    original_dir = self.slave_os_path.dirname(self.output_path)
    basename = self.slave_os_path.basename(self.output_path)
    self.slave.cd(original_dir)
    self.slave.rm(basename)
  
  def is_finished(self):
    if not self.test_thread:
      return None
    return not self.test_thread.is_alive()
  
  def run(self):
    """Starts a new thread to run the test"""
    self.test_thread = threading.Thread(name='Test_handler-%d' \
        % self.shard_index, target=self._run_thread)
    self.test_thread.start()


types = {
  'gtest' : GtestHandler,
  'base' : BaseHandler
}