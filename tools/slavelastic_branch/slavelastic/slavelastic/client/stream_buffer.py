"""Wrapper for Buffer.  User for buffering streams.  The read() function
in this class will wait if there is no data in the buffer"""

import os
import sys
import threading

ROOT_DIR = os.path.join('..', '..', os.path.abspath(__file__))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from pbrpc.buffer import Buffer

class StreamBuffer(object):
  def __init__(self):
    self.buffer = Buffer()
    # Cleared when no data, set when there is data
    self.event = threading.Event()
    self.stream_lock = threading.Lock()
    self.is_open = True
  
  def read(self, maxlen):
    if self.is_open:
      self.event.wait()
    with self.stream_lock:
      buf = self.buffer.read(maxlen)
      self.event.clear()
      return buf
  
  def write(self, data):
    with self.stream_lock:
      self.buffer.write(data)
      self.event.set()
  
  def close(self):
    with self.stream_lock:
      self.is_open = False
      self.event.set()
  
  def __len__(self):
    return len(self.buffer)