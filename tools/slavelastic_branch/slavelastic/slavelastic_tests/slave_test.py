#!/usr/bin/python
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

import logging
import os
import re
import signal
import subprocess
import sys
import time
import unittest

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from pbrpc.rpc_server import TcpChannel, RpcContext, RpcServiceStub
from slavelastic.common.reg import Reg
from slavelastic.common.slavelastic_pb2 import ExecuteData, Name, Register
from slavelastic.common.slavelastic_pb2 import SlaveServiceCore_Stub
from slavelastic.common.slavelastic_pb2 import SlaveServiceOpt_Stub
from slavelastic.common import checksum


def add_kill():
  """Add kill() method to subprocess.Popen for python <2.6"""
  if getattr(subprocess.Popen, 'kill', None):
    return
  if sys.platform == 'win32':
    def kill_win(process):
      import win32process
      return win32process.TerminateProcess(process._handle, -1)
    subprocess.Popen.kill = kill_win
  else:
    def kill_nix(process):
      import signal
      return os.kill(process.pid, signal.SIGKILL)
    subprocess.Popen.kill = kill_nix


class testSlave(unittest.TestCase):
  def setUp(self):
    #self.pipe = os.tmpfile()
    env = os.environ.copy()
    env['PYTHONUNBUFFERED'] = '1'
    self.path = os.path.dirname(os.path.abspath(__file__))
    slave_path = os.path.join(ROOT_DIR, 'slavelastic', 'slave', 'slave.py')
    cmd = [sys.executable, slave_path, '--port', '0']
    if '-vv' in sys.argv:
      cmd.append('-v')
    self.slave_proc = subprocess.Popen(cmd,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=env)
    first_line = self.slave_proc.stdout.readline()
    socket_number = int(re.match(r'^Listening on port (\d+)',
                                first_line).group(1))
    self.channel = TcpChannel('localhost', socket_number)
    self.context = RpcContext(self.channel)
    self.service = RpcServiceStub(self.context, None,
        SlaveServiceOpt_Stub, SlaveServiceCore_Stub)
  
  def _create_file(self, filename='temp', size=20000, str_='ABCDEFGHIJK\n'):
    with open(filename, 'wb') as fd:
      for i in range(0, size):
        fd.write(str_)

  def tearDown(self):
    self.slave_proc.kill()

  def testBasicRegister(self):
    r = self.service.GetRegister(Reg(None))
    self.assertEquals(type(r), Register)
    self.assertEquals(len(r.registers), 11)
    self.assertEquals(Reg(None),
        self.service.GetRegister(Reg(None, 'reg1')))
    self.assertEquals(Reg('ok'),
        self.service.SetRegister(Reg('foo', 'reg1')))
    self.assertEquals(Reg('foo', 'reg1'),
        self.service.GetRegister(Reg(None, 'reg1')))
    r = self.service.GetRegister(Reg(None))
    self.assertEquals(len(r.registers), 12)

  def testExecute(self):
    path = os.path.join(self.path, 'test_data')
    if sys.platform == 'win32':
      cmd = ['cmd.exe', '/c', 'dir', '/b']
    else:
      cmd = ['ls']
    # Do something else on Windows...
    e = ExecuteData(args=cmd, cwd=path)
    r = self.service.Execute(e)
    self.assertTrue(isinstance(r, Register))
    r1 = self.service.Read(r)
    stream_id = r1.stream_id
    stream = self.context.new_stream()
    stream.connect(stream_id)
    r = stream.recv()  # Will work iff data is less than 4KB
    self.assertEquals(['dummy_file'],
                      sorted(r.splitlines()))
    # The format is argv[0]:pid
    self.assertTrue(r1.name.startswith(cmd[0] + ':'))

  def testInvalidType(self):
    try:
      r = self.service.Execute(Reg(None))
      self.fail()
    except Exception, e:
      pass

  def testCreateDirectoryAndDelete(self):
    path = os.path.join(self.path, '_trial')
    if os.path.exists(path):
      os.rmdir(path)
    r = self.service.CreateDirectory(Name(name=path))
    self.assertEquals(Reg('ok'), r)
    self.assertTrue(os.path.isdir(path))
    r = self.service.Delete(Reg(path))
    self.assertEquals(Reg('ok'), r)
    self.assertFalse(os.path.isdir(path))
  
  def testSendFile(self):
    self._create_file()
    assert 'temp' in os.listdir(os.getcwd()), 'problem creating test file'
    r = self.service.Write(Reg(None, 'destfile'))
    stream_id = r.stream_id
    self.assertNotEqual(r, Reg(None), 'Slave Write() threw cookie^H an error')
    self.assertEqual('destfile', r.name, '%s' % r)
    stream = self.context.new_stream()
    stream.connect(stream_id)
    with open('temp', 'rb') as fd:
      while True:
        buf = fd.read(4096)
        if not buf:
          break
        stream.send(buf)
      stream.close()
    ls = os.listdir(os.getcwd())
    self.assertTrue('destfile' in ls)
    temphash = checksum.calculate('temp')
    desthash = checksum.calculate('destfile')
    self.assertEquals(temphash, desthash, 'Checksum mismatch')
    os.remove('temp')
    os.remove('destfile')
  
  def testGetFile(self):
    self._create_file(str_='qwertyuiop\r\n')
    r = self.service.Read(Reg(None, 'temp'))
    self.assertEqual(self.service.QueryStream(r).string_value, 'Running')
    stream_id = r.stream_id
    self.assertNotEqual(r, Reg(None), 'Slave Read() threw an bad fit')
    self.assertEqual('temp', r.name, '%s' % r)
    stream = self.context.new_stream()
    stream.connect(stream_id)
    with open('tempgetfile', 'wb') as fd:
      while True:
        buf = stream.recv()
        if not buf:
          break
        fd.write(buf)
    self.assertTrue('tempgetfile' in os.listdir(os.getcwd()))
    hash1 = checksum.calculate('temp')
    hash2 = checksum.calculate('tempgetfile')
    self.assertEqual(hash1, hash2, 'Checksum mismatch')
    os.remove('temp')
    os.remove('tempgetfile')

if __name__ == '__main__':
  add_kill()
  level = None
  if '-v' in sys.argv:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  unittest.main()
