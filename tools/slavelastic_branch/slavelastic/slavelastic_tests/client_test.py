#!/usr/bin/python
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Tests the Client wrapper for slavelastic"""

import logging
import os
import re
import signal
import subprocess
import sys
import unittest

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from slavelastic.client.client import Client

def add_kill():
  """Add kill() method to subprocess.Popen for python <2.6"""
  if getattr(subprocess.Popen, 'kill', None):
    return
  if sys.platform == 'win32':
    def kill_win(process):
      import win32process
      return win32process.TerminateProcess(process._handle, -1)
    subprocess.Popen.kill = kill_win
  else:
    def kill_nix(process):
      import signal
      return os.kill(process.pid, signal.SIGKILL)
    subprocess.Popen.kill = kill_nix


class testClient(unittest.TestCase):
  def setUp(self):
    env = os.environ.copy()
    self.orgdir = os.getcwd()
    env['PYTHONUNBUFFERED'] = '1'
    self.path = os.path.dirname(os.path.abspath(__file__))
    slave_path = os.path.join(ROOT_DIR, 'slavelastic', 'slave', 'slave.py')
    cmd = [sys.executable, slave_path, '--port', '0']
    if '-vv' in sys.argv:
      cmd.append('-v')
    self.slave_proc = subprocess.Popen(cmd,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=env)
    first_line = self.slave_proc.stdout.readline()
    self.socket_number = int(re.match(r'^Listening on port (\d+)',
                                first_line).group(1))
    self.client = Client('localhost', self.socket_number)
  
  def testGetPlatform(self):
    r = self.client.get_platform()
    self.assertEquals(r, sys.platform)
  
  def testCwd(self):
    r = self.client.cwd()
    self.assertEquals(r, os.getcwd())
    
  def testListDirectory(self):
    self.client.cd('client_test')
    self.assertEquals(self.client.ls(), ['newfile.txt'])
    self.client.cd('..')
  
  def testCDAndCat(self):
    cwd = self.client.cd('client_test')
    self.assertEquals(cwd, self.client.cwd())
    self.assertEquals(cwd, os.path.join(self.path, 'client_test'))
    r = self.client.cat('newfile.txt')
    self.assertEquals(r, 'here are some words')
    cwd = self.client.cd('..')
    self.assertEquals(cwd, os.getcwd())
  
  def testGetSmallTextFile(self):
    ls = os.listdir(os.getcwd())
    if 'newfile.txt' in ls:
      os.remove(os.path.join(os.getcwd(), 'newfile.txt'))
    self.client.cd('client_test')
    self.client.get('newfile.txt')
    ls = os.listdir(os.getcwd())
    self.assertTrue('newfile.txt' in ls)
    f = open('newfile.txt', 'rb')
    self.assertEquals('here are some words', f.read())
    f.close()
    os.remove(os.path.join(os.getcwd(), 'newfile.txt'))
  
  def testMakeDirectoryAndDelete(self):
    self.client.cd('client_test')
    self.client.mkdir('new_dir')
    self.assertTrue('new_dir' in self.client.ls())
    self.assertTrue(self.client.rm('new_dir'))
    self.assertTrue(not 'new_dir' in self.client.ls())
    self.client.cd('..')
  
  def testSendSmallTextFile(self):
    os.chdir('client_test')
    self.client.put('newfile.txt', 'newfile_dest.txt')
    ls = self.client.ls()
    self.assertTrue('newfile_dest.txt' in ls)
    self.assertEqual(self.client.cat('newfile_dest.txt'), 'here are some words')
    self.assertTrue(self.client.rm('newfile_dest.txt'))
    os.chdir(os.path.dirname(os.getcwd()))
  
  def testLock(self):
    self.assertTrue(self.client.lock())
    self.assertTrue(self.client.unlock())
    
  def testLockTwoClients(self):
    client2 = Client('localhost', self.socket_number)
    self.assertTrue(self.client.lock())
    self.assertFalse(client2.unlock())
    self.assertFalse(self.client.lock())
    self.assertTrue(self.client.unlock())
    self.assertTrue(client2.lock())
    self.assertTrue(client2.unlock())
    
  
  def tearDown(self):
    self.slave_proc.kill()


if __name__ == '__main__':
  add_kill()
  level = None
  if '-v' in sys.argv:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  unittest.main()  