#!/usr/bin/python
# coding: utf-8
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Awesome code to support pipelined, out-of-order, bidirectional multi-session
flaky-network-resistant not-object-oriented RPC protocol based on Protobuf.
Supports both pipes and sockets.

This code intentionally doesn't provide access control since it was not verified
to robustness. The recommended way to provide access control is to ssh tunnel
between the two host with autossh and use non-shell credentials.
"""

__author__ = 'maruel@google.com'
__version__ = '0.1'

import datetime
import logging
import os
import random
import sys
import threading

try:
  from pbrpc import pbrpc_pb2
except ImportError:
  sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
  from pbrpc import pbrpc_pb2

from pbrpc.errors import *
from pbrpc.channel import exception_to_string, parse_pb
from pbrpc.channel import ListenTcpChannel, TcpChannel
from pbrpc.buffer import Buffer
from pbrpc.stream import Stream


def sync_call(function):
  """Function decorator to make a method implementation a normal synchronous
  call."""
  def hook(self, session, argument, done):
    logging.info('In %s(%s)' % (function.__name__, argument))
    response = None
    try:
      try:
        response = function(self, argument)
      except Exception, e:
        logging.exception('%s() threw! %s' % (function.__name__,
            exception_to_string(e)))
        raise e
    finally:
      # Make sure to send a response if possible.
      logging.info('Returning %s' % str(response).rstrip())
      done(response)
  return hook


class RpcServiceStub(object):
  """Client-side proxy for one or multiple service.

  If a context supports multiple parallel sessions, session_id must be
  specified. session_id is an opaque key that is used to disambiguate multiple
  sessions.

  Can be called from arbitrary threads.
  """
  def __init__(self, context, session_id, *service_stub_classes):
    self._context = context
    if self._context.channel.is_multi_connection() and session_id is None:
      raise TypeError('Need to specify a session for a multi-session RPC '
                      'context')
    if not isinstance(session_id, (None.__class__, int, long)):
      raise TypeError('Need to specify a session or None')
    if not service_stub_classes:
      raise TypeError('Specify at least one stub service')
    self._service_stubs = []
    for service_stub_class in service_stub_classes:
      service_stub = service_stub_class(self._context)
      self._service_stubs.append(service_stub)
      # Go through service_stub methods and add a wrapper function to
      # this object that will call the method
      for method in service_stub.GetDescriptor().methods:
        if method.name in self.__dict__:
          raise ConflictingMethod('Conflicting method name %s' %
                                  method.name)
        # Add service methods to the this object. Must use a lamba to bind
        # the default parameters right now. If def is used instead,
        # service_stub.GetDescriptor().methods[-1] is always used which is
        # wrong.
        rpc = (lambda argument, timeout=None, callback=None, method=method,
                      session_id=session_id:
                   self._call(method, argument, timeout, callback, session_id))
        rpc.__doc__ = ('Automatically generated function from '
                        '%(module)s.%(class)s.%(method)s(%(arg_type)s) and '
                        'returns %(res_type)s.') % {
              'method': method.name,
              'class': service_stub_class.GetDescriptor().name,
              'module': service_stub_class.__module__,
              'arg_type': 'None',
              'res_type': 'None',
            }
        self.__dict__[method.name] = rpc

  def _call(self, method, argument, timeout, callback, session_id):
    """Sends the RPC call to channel and registers to get a response back to
    call callback."""
    return self._context.send_method_call(method, argument, timeout, callback,
                                          session_id)


class _PbRpcServiceImpl(pbrpc_pb2.PbRpcService_Stub):
  """This interface is always available. It implemented the needed functionality
  to reconnected lost connections."""
  def __init__(self, context):
    self._context = context

  @sync_call
  def GetRegisteredServices(self, argument):
    return pbrpc_pb2.Strings(
        strings=[s[1].GetDescriptor().name
                 for s in self._context.services_impl.itervalues()])

  @sync_call
  def SwitchSession(self, argument):
    if len(arguments.strings) != 1:
      raise TypeError('Invalid argument')
    self._context.channel.switch_session(arguments.strings[0])
    return pbrpc_pb2.Strings()

  @sync_call
  def GetVersion(self, _):
    return pbrpc_pb2.Strings(strings=[__version__])


def _reply(channel, connection_id, status, call_id, more=None):
  """Utility function to reply an RPC response, with payload defined in the
  callback. Is thread-safe."""
  new_packet = pbrpc_pb2.RpcPacket(status=status, call_id=call_id)
  # If status OK, we expect payload. We don't otherwise.
  if bool(status == pbrpc_pb2.RpcPacket.OK) is bool(more):
    # TODO(maruel): Assert
    pass
  if status != pbrpc_pb2.RpcPacket.OK:
    logging.warn('Got failure reply: %s' % status)
  if more:
    new_packet.payload = more
  channel.write_packet(connection_id, new_packet.SerializeToString())


class RpcContext(object):
  """Supports pipelined out-of-order bidirectional RPC calls.

  Is stateful with the pending calls.
  Manages the RpcPacket reading thread.
  Each 'session' can be a multiplexed virtual channel in a pipe or a different
  socket connection to the same port for example."""

  class _PendingCallback(object):
    def __init__(self, call_id, callback, service, method, result_type,
                 expiration, state):
      self.call_id = call_id
      self.callback = callback
      self.service = service
      self.method = method
      self.result_type = result_type
      self.expiration = expiration
      # Circular dependency, make sure to set it to None before dumping the
      # object.
      self.state = state

  class _StatefulSession(object):
    def __init__(self, session_id):
      self.session_id = session_id
      self.default_session_id = session_id
      self.connection_id = None
      # On connection tear down, how long we keep the pending calls on
      # artificial life support.
      self.expiration = None
      # { call_id: _PendingCallback }
      # call id's are unique per session.
      self.pending_callbacks = {}

  def __init__(self, channel, default_timeout=60*60):
    # For debugging purposes.
    self.main_thread = threading.currentThread()
    # Two dict of the same _StatefulSession to list pending calls. Having two
    # dicts helps indexing.
    self._session_ids = {}
    self._connection_ids = {}
    # A channel can have multiple sessions for listening channels.
    self.channel = channel
    # A dict of { name: (impl, interface stub) }
    self.services_impl = {}
    # Default timeout for both connection tear down and pending callbacks.
    self.default_timeout = default_timeout
    # An optimisation to not search all the sessions everytime.
    self._next_expiration = None
    self._loop_started = False
    self.register_impl(_PbRpcServiceImpl(self))
    # For streams
    self._listening_streams = set()  # Set of stream_ids
    self._streams = {}  # key: stream_id, value: Stream objects
  
  def new_stream(self):
    return Stream(self)

  def register_impl(self, impl, service_stub=None, partial=False,
                    late_bind=False):
    """If |partial|, don't throw an exception even if the service is not
    completely implemented.

    Use late_bind if you know what you are doing, to register a service after
    communication has started."""
    if not service_stub:
      service_stub = impl.__class__.__bases__[0]
    descriptor = service_stub.GetDescriptor()
    name = descriptor.full_name
    # Sanity check.
    if not partial:
      for method in service_stub.GetDescriptor().methods:
        if (getattr(impl.__class__, method.name) ==
            getattr(service_stub, method.name)):
          raise IncompleteStubImpl('%s.%s() is not implemented in %s' %
                                  (service_stub.__name__, method.name,
                                    impl.__class__.__name__))
    if self._loop_started and not late_bind:
      raise PbRpcException('Registering a service after running the message '
                           'loop may cause race-conditions.')
    logging.info('(%s) for interface %s' % (impl.__class__.__name__, name))
    self.services_impl[name] = (impl, service_stub)

  def send_method_call(self, method, argument, timeout, callback, session_id):
    """Sends a RpcPacket to a remote host to call a RPC method. If callback is
    not specified, it is a synchronous call, meaning that an inner loop is
    started. The inner loop is run as self.run(). If you want to use another
    loop function, like in twisted, just replace the member function."""
    if session_id is None:
      if not self.channel.is_multi_connection():
        session_id = 0
      else:
        raise TypeError('Must bind the method call to a session first.')
    service_name = method.containing_service.full_name
    method_name = method.name
    result_type = None
    logging.info('(%s.%s(%s))' % (service_name, method_name,
                                  method.input_type.full_name))
    # In theory we'd want to use
    # method.containing_server.GetRequestClass(method)() but that construct an
    # object and only it's type is needed here. So use undocumented API.
    if not isinstance(argument, method.input_type._concrete_class):
      raise ValueError('Invalid argument type %s for signature %s.%s(%s)' % (
                       argument.__class__.__name__,
                       service_name, method_name, method.input_type.name))
    payload = argument.SerializeToString()
    # In theory we'd want to use Service.GetResponseClass() but it's not logical
    # to construct the reply object in advance.
    result_type = method.output_type._concrete_class
    return self.send_method_call_manual(service_name, method_name, payload,
        result_type, timeout, callback, session_id)

  def send_method_call_manual(self, service_name, method_name, payload,
      result_type, timeout, callback, session_id):
    """Calls a descriptor-less service. Can be used to call arbitrary method
    without a Stub object. The right Message object should still be encoded."""
    # Cheat a bit. Use this occasion to process the backlog.
    self.run_pending()
    state = self._session_ids.get(session_id, None)
    if not state:
      raise PbRpcException('Invalid session id %s' % session_id)
    self.channel.assert_connection_id(state.connection_id)
    new_call_id = self._gen_random(state.pending_callbacks)
    packet = pbrpc_pb2.RpcPacket(service=service_name, method=method_name,
                                 payload=payload, call_id=new_call_id)
    class _InnerCookie(BaseException):
      def __init__(self, result):
        self.result = result
    def wait_for_result(res, exception):
      # This will quick jump up to the original call.
      if exception:
        raise exception
      raise _InnerCookie(res)
    expiration = None
    if timeout is None:
      timeout = self.default_timeout
    if timeout and timeout > 0:
      now = datetime.datetime.utcnow()
      expiration = now + datetime.timedelta(seconds=timeout)
      # Look if this expiration is earlier than the current earliest expiration.
      if not self._next_expiration or self._next_expiration > expiration:
        self._next_expiration = expiration
        logging.debug('New expiration in %s. %s' %
            (self._next_expiration - now, self._next_expiration))
    # If timeout <= 0, don't even queue to wait for the reply.
    if not timeout or timeout > 0:
      if not callback:
        callback = wait_for_result
      state.pending_callbacks[packet.call_id] = (
          self._PendingCallback(packet.call_id, callback, service_name,
                                method_name, result_type, expiration, state))
    self.channel.write_packet(state.connection_id, packet.SerializeToString())
    if callback == wait_for_result:
      # No callback was specified, run an inner loop.
      try:
        self.run()
      except _InnerCookie, e:
        return e.result
    elif callback:
      return packet.call_id
    else:
      # timeout <= 0
      return None

  def process_method_call(self, timeout=10):
    """Either calls a registered callback or calls a registered service
    method. Call this method repeatedly. Returns True if it processed a message,
    None otherwise."""
    # It's now a bit too late to register an interface.
    self._loop_started = True
    if self._next_expiration:
      new_timeout = (self._next_expiration -
          datetime.datetime.utcnow()).microseconds / 1000000.
      timeout = min(new_timeout, timeout)
    logging.debug('timeout=%s' % timeout)
    # Clamp
    timeout = min(10, max(0, timeout))
    connection_packets = self.channel.pop_packets(timeout)
    if not connection_packets:
      return None
    for connection_packet in connection_packets:
      connection_id, packet = connection_packet
      # Find back the state which contains the session_id.
      state = self._connection_ids.get(connection_id, None)
      if packet is False:
        self._on_connection_lost(state, connection_id)
      elif packet is True:
        self._on_new_connection(state, connection_id)
      elif packet.stream_id:  # Stream packet
        self._process_stream(state, connection_id, packet)
      else:
        if not packet.service:
          # A reply.
          self._call_callback(state, packet)
        else:
          # A method call.
          self._call_method(state, packet)
    return True
  
  def _process_stream(self, state, connection_id, packet):
    """Only a CONNECT packet is expected to land here.  Once a CONNECT
    packet is received, the connection_id is removed from the list of
    connected clients, and is passed to the Stream() object."""
    if not state:
      logging.error('Internal inconsistency with %s' % connection_id)
      return
    # Context doesn't need to know about sockets used for Streams
    del self.channel._connections[connection_id]
    del self._connection_ids[connection_id]
    stream = self._streams[packet.stream_id]
    stream.new_connection(connection_id)

  def _on_connection_lost(self, state, connection_id):
    if not state:
      # We already forgot about this connection_id.
      logging.error('Internal inconsistency with %s' % connection_id)
      return
    state.connection_id = None
    expiration = (datetime.datetime.utcnow() +
        datetime.timedelta(seconds=self.default_timeout))
    if not state.expiration or expiration < state.expiration:
      state.expiration = state.expiration
    # Now only reachable by session_id. Use
    # pbrpc_pb2.PbRpcServer.SwitchSession() to reconnect to it before it
    # expires.
    del self._connection_ids[connection_id]

  def _on_new_connection(self, state, connection_id):
    if state:
      # We already knew about this connection.
      logging.error('Internal inconsistency with %s' % connection_id)
      return
    # Generate a new session_id.
    if self.channel.is_multi_connection():
      state = self._StatefulSession(self._gen_random(
          self._session_ids.iterkeys()))
    else:
      state = self._session_ids.setdefault(0, self._StatefulSession(0))
    # Reuse the stateful session.
    state.connection_id = connection_id
    state.expiration = None
    self._session_ids[state.session_id] = state
    self._connection_ids[connection_id] = state

  @staticmethod
  def _gen_random(not_in):
    """Generates a random 63 bits value not in not_in."""
    while True:
      random_value = random.getrandbits(63)
      if not random_value in not_in:
        return random_value

  def _call_callback(self, state, packet):
    """Calls a method call's callback, the reply to a remote method call."""
    # Note: do not trap exceptions.
    if not packet.call_id in state.pending_callbacks:
      logging.warning('Unexpected packet id %s! Discarting. Note this may '
          'happen if a call has timed out.' % packet.call_id)
      return None
    # Unregisters the callback.
    logging.info('(%s)' % state.session_id)
    pending_callback = state.pending_callbacks.pop(packet.call_id)
    pending_callback.state = None
    argument = None
    if packet.status == pbrpc_pb2.RpcPacket.SERVICE_NOT_FOUND:
      # TODO(maruel): Need more data in the original session.
      exception = PbRpcServiceNotFound(pending_callback.service,
                                       pending_callback.method)
    elif packet.status == pbrpc_pb2.RpcPacket.METHOD_NOT_FOUND:
      exception = PbRpcMethodNotFound(pending_callback.service,
                                      pending_callback.method)
    elif packet.status == pbrpc_pb2.RpcPacket.EXCEPTION:
      exception = PbRpcRemoteException()
    else:
      argument = parse_pb(packet.payload, pending_callback.result_type())
      exception = None
    # May throw.
    return pending_callback.callback(argument, exception)

  def _call_method(self, state, packet):
    """A new method call."""
    # Must not throw.
    service = self.services_impl.get(packet.service, None)
    connection_id = state.connection_id
    if service is None:
      # Unknown service
      return _reply(self.channel, connection_id,
                    pbrpc_pb2.RpcPacket.SERVICE_NOT_FOUND, packet.call_id)
    method = service[1].GetDescriptor().FindMethodByName(packet.method)
    if method is None:
      # Newer version of the service maybe?
      return _reply(self.channel, connection_id,
                    pbrpc_pb2.RpcPacket.METHOD_NOT_FOUND, packet.call_id)
    if (getattr(service[0].__class__, method.name) ==
        getattr(service[1], method.name)):
      # Method is deliberately not implemented.
      return _reply(self.channel, connection_id,
                    pbrpc_pb2.RpcPacket.METHOD_NOT_FOUND, packet.call_id)
    argument = parse_pb(packet.payload, service[1].GetRequestClass(method)())
    if not argument:
      return _reply(self.channel, connection_id,
                    pbrpc_pb2.RpcPacket.INVALID_PAYLOAD, packet.call_id)
    logging.info('(%s) %s.%s(%s x)' % (state.session_id,
        service[0].__class__.__name__, method.name,
        argument.__class__.__name__))
    # Save channel so the callback never access 'self' or 'packet'.
    # TODO(maruel): Tear off this closure?
    channel = self.channel
    call_id = packet.call_id
    packet = None
    try:
      def callback(reply):
        # Warning: Can be called from an arbitrary thread.
        status = pbrpc_pb2.RpcPacket.EXCEPTION
        data = None
        try:
          if reply:
            data = reply.SerializeToString()
            status = pbrpc_pb2.RpcPacket.OK
        finally:
          _reply(channel, connection_id, status, call_id, data)
      # Call the function! Ignore return code. Use the unbounded class to make
      # sure the right Service instance is used when an implementation class
      # implements multiple service stub.
      # TODO(maruel): Nullify the callback on session expiration.
      return service[1].CallMethod(service[0], method, connection_id, argument,
                                   callback)
    except NotImplementedError, e:
      # Trap the exception to send a reply but don't swallow it.
      logging.exception('%s' % exception_to_string(e))
      # Convert NotImplementedError to METHOD_NOT_FOUND and swallow it.
      _reply(self.channel, connection_id, pbrpc_pb2.RpcPacket.METHOD_NOT_FOUND,
             call_id, exception_to_string(e))
      raise e
    except Exception, e:
      logging.exception('%s' % exception_to_string(e))
      _reply(self.channel, connection_id, pbrpc_pb2.RpcPacket.EXCEPTION,
             call_id, exception_to_string(e))
      raise e

  def call_expireds(self):
    """Call expired callbacks and remove them from expected RPC replies."""
    now = datetime.datetime.utcnow()
    if not self._next_expiration or now < self._next_expiration:
      if self._next_expiration:
        logging.debug('Next expiration in %s, %s' % (
          (self._next_expiration - now), self._next_expiration))
      return

    # Since callback may throw and we want to let it surface, we need to go the
    # slow route.
    while True:
      expired = []
      now = datetime.datetime.utcnow()
      for state in self._session_ids.itervalues():
        expired += [pc for pc in state.pending_callbacks.itervalues()
                    if pc.expiration and pc.expiration < now]
      if not expired:
        logging.debug('No more expired callback found.')
        return
      expired.sort(key=lambda x: x.expiration)
      for pending_callback in expired:
        # Since this method can be called recursively, don't assume the
        # callbacks are still there.
        if not pending_callback.state:
          logging.warn('Callback state disapeared')
          continue
        if not pending_callback.state.session_id in self._session_ids:
          logging.warn('Session %s disapeared' % state.session_id)
          continue
        if (not pending_callback.call_id in
                pending_callback.state.pending_callbacks):
          logging.warn('Callback disapeared from state')
          continue
        # Disconnect the callback.
        del pending_callback.state.pending_callbacks[pending_callback.call_id]
        pending_callback.state = None
        logging.warn('Calling timeout %s late' %
            (datetime.datetime.utcnow() - pending_callback.expiration))
        # May throw.
        pending_callback.callback(None,
            PbRpcCallExpired(pending_callback.expiration))
      # It's now time to updated _next_expiration.
      self._next_expiration = None
      now = datetime.datetime.utcnow()
      for state in self._session_ids.itervalues():
        for pending_call in state.pending_callbacks.itervalues():
          if (pending_call.expiration and
              ((not self._next_expiration or
                pending_call.expiration < self._next_expiration))):
            self._next_expiration = pending_call.expiration
      if self._next_expiration:
        logging.debug('New expiration in %s, %s' %
            ((self._next_expiration - now),
             self._next_expiration))
      # TODO(maruel): Process expired sessions.

  def run(self):
    """Runs the message loop. Note: it does not trap exceptions."""
    while True:
      self.call_expireds()
      self.process_method_call()

  def run_pending(self, timeout=0):
    """Runs the message loop without waiting. Returns as soon as the pending
    callbacks is empty. Note: it does not trap exceptions."""
    while True:
      self.call_expireds()
      if not self.process_method_call(timeout):
        break


def main(argv):
  module = sys.modules[__name__]
  def print_doc(obj, indent):
    print(indent + '%s:' % obj.__name__)
    lines = [i.strip() for i in obj.__doc__.splitlines()]
    while lines[-1] == '':
      lines.pop()
    lines.append('')
    print('\n'.join([indent + '  ' + i for i in lines]))

  def print_item(parent, item, indent=''):
    if item.startswith('_'):
      return
    obj = getattr(parent, item, None)
    if not obj or not obj.__doc__ or '__package__' in dir(obj):
      return
    print_doc(obj, indent)

  if len(argv) > 1:
    for i in argv[1:]:
      parent = module.__dict__[i]
      print_doc(parent, '')
      for item in dir(parent):
        print_item(parent, item, '  ')
  else:
    print('Usage: %s [item]\n  for help on a specific item\n' % argv[0])
    print('Author: ' + __author__)
    print('Version: ' + __version__ + '\n')
    print(module.__doc__)
    for item in dir(module):
      print_item(module, item)


if __name__ == '__main__':
  sys.exit(main(sys.argv))
