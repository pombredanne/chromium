# coding: utf-8
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

import logging
import os
import socket
import struct
import sys
import threading


from pbrpc.errors import *
from pbrpc.channel import exception_to_string, parse_pb
from pbrpc.channel import ListenTcpChannel, TcpChannel
from pbrpc.pbrpc_pb2 import RpcPacket


class Stream(object):
  """Interface for creating and using virtual streams between sessions.
  This is here so we can switch between different stream implementations.
  Calls are blocking.  This is okay for the slave side since this is
  intended to not run in the main thread.
  
  The intended order of operations is as follows:
  1. Client --> Slave: RPC call for slave to listen() to new stream
  2. Client <-- Slave: RPC call returns stream id, Slave is waiting
  3. Client --> Slave: connects to slave, send rpcpacket w/ stream_id
  4. Client <-- Slave: ACK rpc_packet, connection established
  5. Client <-> Slave: Transfer
  6. Client <-> Slave: TCP Connection closed, is_open set to False
  """
  
  def __init__(self, context, use_socket=True):
    self._context = context
    self.stream_lock = threading.Lock()
    self.is_open = False
    self.socket = None
    self.stream_id = None  # only valid if is_open is true
    self.listening_event = threading.Event()
  
  def recv(self, maxlen=4096):
    """Receives maxlen bytes from socket.  Return None if EOF"""
    with self.stream_lock:
      assert self.is_open, 'connection is not open'
      # TODO(hinoka): Move this to channel, so it would be more like
      # buf = self._context.channel.recv(connection_id)
      buf = self.socket.recv(maxlen)
    if not buf:
      self.close()
      return None
    return buf
  
  def send(self, data):
    assert self.is_open, 'Connection not open'
    with self.stream_lock:
      return self.socket.send(data)
  
  def close(self):
    with self.stream_lock:
      if not self.is_open:
          return
      self.is_open = False
      del self._context._streams[self.stream_id]
      self._context._listening_streams.discard(self.stream_id)  # if listening
      self.stream_id = None
      self.socket.close()
  
  def new_connection(self, connection_id):
    """This is called by context when a RpcPacket with the matching stream_id
    (found in context._listening_streams) is received.  Should only be called
    by the slave side.  Unblocks accept()"""
    assert self.stream_id in self._context._listening_streams
    self.socket = connection_id
    self.listening_event.set()
  
  def _send_packet(self, packet):
    """Sends an RPC packet, usually for handsake"""
    data = packet.SerializeToString()
    header = struct.pack('!I', len(data))
    self.socket.send(header + data)
  
  def _send_ack(self):
    """Called by accept(), sends an ACK packet from the slave to client"""
    packet = RpcPacket(status=RpcPacket.ACK, stream_id=self.stream_id)
    self._send_packet(packet)
  
  def _send_syn(self):
    """Called by connect().  sends a CONNECT packet from client to slave"""
    packet = RpcPacket()
    packet.status = RpcPacket.CONNECT
    packet.stream_id = self.stream_id
    self._send_packet(packet)
  
  def listen(self):
    """Gets a stream_id from context.  Returns the stream_id it listens to."""
    with self.stream_lock:
      stream_id = self._context._gen_random(self._context._streams)
      self._context._streams[stream_id] = self
      self._context._listening_streams.add(stream_id)
      self.stream_id = stream_id
      return stream_id
    
  def accept(self):
    """Waits until a connection request is received, and opens the stream.
    This is a blocking call.  Call new_connection to unblock.  Sends an
    ACK after connection is received."""
    assert self.stream_id in self._context._listening_streams
    self.listening_event.wait(10)
    with self.stream_lock:
      if not self.socket:
        raise IOError, 'Listen connection timed out, did my client die?'
      self.is_open = True
      self.listening_event.clear()
      self._context._listening_streams.remove(self.stream_id)
      self._send_ack()
      self.socket.setblocking(1)
  
  def connect(self, stream_id, timeout=None):
    """Only works if context is single connectioned, and sends a connection
    request to the host using the stream_id"""
    stream_id = int(stream_id)
    self._context._streams[stream_id] = self
    sock, sa = self._context.channel.get_port(False)
    sock.connect(sa)
    with self.stream_lock:
      self.stream_id = stream_id
      self.socket = sock
      self._send_syn()
      # Wait for ack
      sock.settimeout(timeout)
      try:
        buf = sock.recv(4)
      except socket.timeout, why:
        logging.error('Connection attempt timed out: %s' % why)
        return
      header = struct.unpack('!I', buf)[0]
      try:
        buf = sock.recv(header)
      except socket.timeout, why:
        logging.error('Connection attempt timed out: %s' % why)
        return
      packet = RpcPacket()
      assert len(buf) == header, '%d != %d' % ((len(buf)), header)
      packet.ParseFromString(buf)
      assert int(packet.stream_id) == self.stream_id, '%d' % packet.stream_id
      assert packet.status == RpcPacket.ACK, 'Expect ACK, got %s' % str(packet)
      logging.info('Connected to %s with id %d', (sa, stream_id))
      self.is_open = True