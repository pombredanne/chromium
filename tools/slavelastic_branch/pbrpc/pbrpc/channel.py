# coding: utf-8
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

import cStringIO
from collections import deque
import logging
import Queue
import select
import socket
import struct
import threading

from pbrpc import pbrpc_pb2
from pbrpc.buffer import Buffer
from pbrpc.errors import PbRpcException, PbRpcIOException


def parse_pb(byte_stream, result):
  """Basic protobuf handling."""
  result.ParseFromString(byte_stream)
  if not result.IsInitialized():
    logging.warning('%s is missing required fields' % result_class.__name__)
    return None
  return result


def exception_to_string(e):
  """On non-English platforms, especially on Windows, the exception may contain
  locale-encoded strings, just ignore non-ascii characters."""
  return unicode(e).encode('ASCII', 'replace')


class Connection(object):
  """Connection data associated with each open socket.  At the time of writing
  only the read_buffer is being used."""
  def __init__(self):
    self.read_buffer = Buffer()
    self.write_buffer = Buffer()


class IBaseChannel(object):
  """Not an interface in the Zope sense, just a marker to signal the subclass is
  safe to be used as a channel. These functions are the only one to be used by
  clients, RpcContext."""
  # TODO(maruel): The TCP part should really just use twisted.internet.tcp,
  # things would be much simpler and it would add support for TLS and others.
  # TODO(maruel): Also look at twisted.internet._threadedselect. With their nice
  # naming convention, I missed it at first.
  def read_packet(self, connection_id):
    raise NotImplementedError()

  def write_packet(self, connection_id, data):
    """Writes an encoded packet. Can be called from arbitrary thread."""
    raise NotImplementedError()

  def assert_connection_id(self, connection_id):
    """Raises PbRpcException if connection_id is not valid."""
    raise PbRpcException('Invalid connection id %s' % connection_id)

  def pop_packets(self, timeout):
    """Wait for a valid packet until the timeout.

    Can return one of:
      - None if queue is empty after timeout
      - (connection_id, False) when a connection torn down
      - (connection_id, True) when a new connection is created
      - (connection_id, packet) on received RpcPacket"""
    raise NotImplementedError()

  def is_multi_connection(self):
    """Returns if this channel type supports multiple concurrent connections."""
    raise NotImplementedError()


class BaseChannel(IBaseChannel):
  """The layer format is [ 4 bytes packet length ] [ packet bytes ]
  so it is limited to 4gb packets.

  Warning: this class can potentially be accessed from multiple threads."""
  def __init__(self, timeout):
    # If not None, can reconnect in case of failure.
    self.timeout = timeout
    self.lock = threading.Lock()
    self.max_length = 100*1024*1024
    # Used to message-pass the packets from the read thread.
    self.read_packet_queue = Queue.Queue(maxsize=1000)
    # Used to buffer messages when the connection is down.
    self.write_packet_queue = Queue.Queue(maxsize=1000)
    self.read_thread = None
    self.main_thread = threading.currentThread()
    if self.timeout is None:
      assert not self.is_multi_connection()
    # Don't start the thread in case the subclass wants to do more
    # configuration.

  def read_packet(self, connection_id):
    """Reads an encoded packet. Almost never throws."""
    assert threading.currentThread() == self.read_thread
    logging.debug('(%s)' % connection_id)
    try:
      self.open_channel()
      self.assert_connection_id(connection_id)
      hdr = self.read_bytes(connection_id, 4)
      if not hdr:
        return None
      # socket.ntohl() ?
      length = struct.unpack('!I', hdr)[0]
      if length <= 0 or length >= self.max_length:
        # We need to tear off the connection and start over fresh.
        raise PbRpcException('Invalid size \'%d bytes\'' % length)
      data = self.read_bytes(connection_id, length)
      if not data:
        return None
      logging.debug('(%s) done' % connection_id)
      return data
    except (SystemExit, KeyboardInterrupt):
      self.channel_fault(connection_id)
      raise
    except IOError, e:
      logging.exception('%s' % exception_to_string(e))
      self.channel_fault(connection_id)
      return None
    except Exception, e:
      logging.exception('%s' % exception_to_string(e))
      self.channel_fault(connection_id)
      raise

  def write_packet(self, connection_id, data):
    logging.debug('(%s)' % str(connection_id))
    # TODO(maruel): use socket.htonl() ?
    if len(data) > self.max_length:
      raise PbRpcIOException('Tried to send buffer too large: %d bytes' %
                              length)
    hdr = struct.pack('!I', len(data))
    assert len(hdr) == 4, 'You are on an exotheric platform'
    whole_packet = hdr + data
    try:
      self.open_channel()
      self.assert_connection_id(connection_id)
      result = self.write_bytes(connection_id, whole_packet)
      logging.debug('(%s) done' % connection_id)
      return result
    except IOError, e:
      # TODO(maruel): Other socket errors?
      logging.exception('%s' % exception_to_string(e))
      try:
        self.channel_fault(connection_id)
      finally:
        self.write_packet_queue.put(whole_packet)
      return None
    except Exception, e:
      logging.exception('%s' % exception_to_string(e))
      self.channel_fault(connection_id)
      raise

  def open_channel(self):
    """Can be called from either main_thread or read_thread. Be sure to lock
    accordingly."""
    if self.read_thread:
      if threading.currentThread() == self.read_thread:
        return
      if self.read_thread.isAlive():
        return
      if self.timeout is None:
        # The channel is not recoverable.
        raise PbRpcIOException('Channel cannot be reconnected')
    self.read_thread = threading.Thread(target=self._read_run,
                                        name=self.__class__.__name__ + '.read')
    self.read_thread.daemon = True
    self.read_thread.start()

  def channel_fault(self, connection_id):
    """Called when the channel is desynchronized and thus must be cycled."""
    # See pop_packet().
    self.read_packet_queue.put((connection_id, False))

  def read_bytes(self, connection_id, length):
    """Session is required to be valid. Called from read_thread."""
    raise NotImplementedError()

  def write_bytes(self, connection_id, data):
    """Session is required to be valid. Called from main_thread."""
    raise NotImplementedError()

  def _read_run(self):
    """Reads a RpcPacket at a time and does minimal checking on the packets.

    Runs as read_thread thread."""
    raise NotImplementedError()

  def pop_packets(self, timeout):
    logging.debug('(%s)' % timeout)
    # Make sure the connection is re-created and then restart the thread.
    self.open_channel()
    # If a connection teardown happens, a None object will be poped.
    try:
      result = (self.read_packet_queue.get(True, timeout),)
    except Queue.Empty:
      result = None
    logging.debug('(%s) = %s' % (timeout, result))
    return result


class SingleConnectionMixin(object):
  """Supports only one connection at a time."""
  def assert_connection_id(self, connection_id):
    if connection_id != 0:
      raise PbRpcIOException('Invalid connection id %s' % connection_id)

  def is_multi_connection(self):
    return False

  def _read_run(self):
    try:
      while True:
        byte_stream = self.read_packet(0)
        if not byte_stream:
          self.channel_fault(0)
          return
          raise PbRpcIOException('Failed to read, returned None')
        packet = parse_pb(byte_stream, pbrpc_pb2.RpcPacket())
        if not packet:
          raise PbRpcIOException('Invalid packet, returned None')
        if not packet.call_id:
          raise PbRpcException('Invalid packet. Must have call_id.')
        # Will block if the queue fills up.
        self.read_packet_queue.put((0, packet))
    except Exception, e:
      logging.exception('%s' % exception_to_string(e))
      self.channel_fault(0)
      raise
    finally:
      # Add a None packet in case the main thread is on a blocking read.
      self.read_packet_queue.put(None)


class SocketChannel(BaseChannel):
  def __init__(self, host, port, sock_type, timeout):
    BaseChannel.__init__(self, timeout)
    self.host = host
    self.port = port
    self.sock_type = sock_type
    # Set to True if you have misconfigurations.
    self.ipv4 = True

  def get_port(self, incoming):
    # TODO(maruel): yield
    if self.ipv4:
      try:
        return (socket.socket(socket.AF_INET, self.sock_type),
                (self.host, self.port))
      except socket.error, e:
        logging.exception('Couldn\'t open socket, %s' % exception_to_string(e))
    else:
      # The IPv6 way.
      if incoming:
        flag = socket.AI_PASSIVE
      else:
        flag = 0
      for res in socket.getaddrinfo(self.host, self.port, socket.AF_UNSPEC,
                                    self.sock_type, 0, flag):
        af, socktype, proto, canonname, sa = res
        try:
          return (socket.socket(af, socktype, proto), sa)
        except socket.error, e:
          logging.exception('Couldn\'t open socket, %s' %
              exception_to_string(e))
          continue
    return None

  def connect(self):
    sa = (self.host, self.port)
    try:
      sock, sa = self.get_port(False)
      sock.connect(sa)
      logging.info('Connected to %s' % str(sa))
      return sock
    except socket.error, e:
      logging.debug('Failed %s, %s' % (str(sa), exception_to_string(e)))
      sock.close()
      return None

  def bind(self):
    sa = (self.host, self.port)
    try:
      # TODO(maruel): Bind on *all* interfaces available.
      sock, sa = self.get_port(True)
      sock.bind(sa)
      # Update the host and port.
      sa = sock.getsockname()
      self.host = sa[0]
      self.port = sa[1]
      logging.info('Bind to %s' % str(sa))
      return sock
    except socket.error, e:
      logging.exception('Failed %s, %s' % (str(sa), exception_to_string(e)))
      sock.close()
    return None


class TcpChannel(SingleConnectionMixin, SocketChannel):
  """Creates a TCP connection to a remote host."""
  def __init__(self, host, port, timeout=10*60):
    SocketChannel.__init__(self, host, port, socket.SOCK_STREAM, timeout)
    self.sock = None
    # TODO(hinoka): This async loop thing will probably have to go
    self.open_channel()

  def open_channel(self):
    # TODO(maruel): This call is blocking and would make more sense to execute
    # it in the read thread.
    self.lock.acquire()
    try:
      if not self.sock:
        self.sock = self.connect()
        if not self.sock:
          raise PbRpcIOException(
              'Could not open socket to %s:%d' % (self.host, self.port))
        # See pop_packet().
        self.read_packet_queue.put((0, True))
    finally:
      self.lock.release()
    # Starts the thread.
    SocketChannel.open_channel(self)

  def read_bytes(self, connection_id, length):
    """Reads exactly length bytes."""
    items = ''
    try:
      while len(items) != length:
        items += self.sock.recv(length - len(items))
    except socket.error:
      return None
    return items

  def write_bytes(self, connection_id, data):
    logging.debug("Sending %d bytes" % len(data))
    self.sock.sendall(data)

  def channel_fault(self, connection_id):
    self.lock.acquire()
    try:
      if self.sock:
        self.sock.close()
        self.sock = None
    finally:
      self.lock.release()
    SocketChannel.channel_fault(self, connection_id)


class ListenSocketChannel(SocketChannel):
  """Listens to multiple connections."""
  def __init__(self, host, port, sock_type, timeout):
    SocketChannel.__init__(self, host, port, sock_type, timeout)
    self._connections = {}
    # Binds to the first available interface.
    self.sock = self.bind()
    if not self.sock:
      raise PbRpcIOException('Could not open socket')
    # Start listening.
    self.sock.listen(5)

  def open_channel(self):
    pass

  def is_multi_connection(self):
    return True

  def read_bytes(self, connection_id, length):
    """Reads exactly length bytes."""
    assert threading.currentThread() == self.read_thread
    items = None
    len_buf = None
    self.lock.acquire()
    try:
      # TODO(maruel): Inefficient slice.
      buf = self._connections[connection_id][0]
      len_buf = len(buf)
      if len_buf >= length:
        items = self._connections[connection_id][0][0:length]
        self._connections[connection_id][0] = (
           self._connections[connection_id][0][length:])
    finally:
      self.lock.release()
    if not items:
      logging.debug('No payload; request %d, had %d' % (length, len_buf))
    return items

  def write_bytes(self, connection_id, data):
    if False:
      self.lock.acquire()
      try:
        self._connections[connection_id][1] += data
      finally:
        self.lock.release()
    else:
      # TODO(maruel): Write in the secondary thread will give higher throughput.
      # Since it's non-blocking, it could be done in the primary thread too. It
      # would be even faster.
      connection_id.sendall(data)

  def assert_connection_id(self, connection_id):
    self.lock.acquire()
    try:
      if not connection_id in self._connections:
        raise PbRpcIOException(
            'Invalid connection id %s; %s connection(s) open' %
            (connection_id, len(self._connections)))
    finally:
      self.lock.release()

  def pop_packets(self, timeout):
    """Reads socket and return lists of packets, None otherwise"""
    # Check to see if there are any sockets ready for reading. if not, exit
    inputs = [self.sock] + self._connections.keys()
    logging.debug("(Before Select) Inputs: %s" % [s.fileno() for s in inputs])
    r_ready, w_ready, e_ready = select.select(inputs, [], [], timeout)
    logging.debug("Socket Ready: %s" % [s.fileno() for s in r_ready])
    if not r_ready:
      return []
    results = []
    for sock in r_ready:
      if sock == self.sock:
        # Handles new connection. Puts it in an uninitialized pool
        connection_id, address = self.sock.accept()
        logging.info('Connected by %s, socket %s' % (address,
            connection_id.fileno()))
        connection_id.setblocking(0)
        self._connections[connection_id] = Connection()
        results.append((connection_id, True))
      else:
        # Reads socket data into the buffer
        conn = self._connections[sock]
        try:
          # Read as much as we can into buffer, 4096 is kind of arbitrary
          buf = sock.recv(4096)
        except socket.error, e:
          logging.debug("Socket read unhappy: %s. Killing connection" % str(e))
          self.channel_fault(sock)
          continue
        if not buf:
          # Socket ready, no data => Connection is done
          self.channel_fault(sock)
          continue
        conn.read_buffer.write(buf)
        while True:
          # Read_packet should only pop a packet out of the read buffer if and
          # only if the entire packet has been read into buffer.  Otherwise it
          # leaves the incomplete packet in buffer and returns an empty list.
          packet = self.read_packet(conn)
          if packet is None:
            break
          results.append((sock, parse_pb(packet, pbrpc_pb2.RpcPacket())))
    return results

  # TODO(hinoka): Generalize this and move it to SocketChannel?
  def read_packet(self, conn):
    """Scans the read buffer from conn.read_buffer and return first packet"""
    # Check header, get expected packet length.
    header = conn.read_buffer.peek(4)
    if not header:
      return None
    length = struct.unpack('!I', header)[0]
    # Check buffer length before reading
    if len(conn.read_buffer) < (length + 4):
      logging.info("Packet incomplete, returning None")
      return None
    data = conn.read_buffer.peek(length, 4)
    logging.info("Packet read successful, size: %d" % length)
    conn.read_buffer.skip(length + 4)
    return data

  def channel_fault(self, connection_id):
    self.lock.acquire()
    try:
      # Keep the lock while closing the sockets.
      if connection_id is None:
        logging.info('Destroying all sockets')
        for s in self._connections:
          s.close()
          SocketChannel.channel_fault(self, s)
        # Discard any buffered input.
        self._connections = {}
        # Close listening socket.
        if self.sock:
          self.sock.close()
          self.sock = None
      else:
        assert connection_id != self.sock
        logging.info('Destroying connection %s' % connection_id)
        # Discard any buffered input.
        del self._connections[connection_id]
        connection_id.close()
        SocketChannel.channel_fault(self, connection_id)
    finally:
      self.lock.release()


class ListenTcpChannel(ListenSocketChannel):
  """Listens to connections, is not usable until a connection is made.

  Then, the multiplexing? A context needs to be used for each call. Use the
  call_id to keep the right channel."""
  def __init__(self, host, port, timeout=10*60):
    ListenSocketChannel.__init__(self, host, port, socket.SOCK_STREAM, timeout)
