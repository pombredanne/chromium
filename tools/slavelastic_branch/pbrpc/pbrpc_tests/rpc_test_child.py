#!/usr/bin/python
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

import logging
import os
import optparse
import sys
import threading

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from pbrpc import pbrpc_pb2
from pbrpc import rpc_server
from pbrpc_tests import pbrpc_test_pb2


class PbRpcTest(pbrpc_test_pb2.TestPbRpcService_Stub):
  @rpc_server.sync_call
  def ThrowsException1(self, _):
    raise Exception('foo')

  def ThrowsException2(self, ignored1, ignored2):
    pass

  def NeverCallback(self, session, argument, done):
    pass


class Partial(pbrpc_test_pb2.TestPartial_Stub):
  def DelayedEcho(self, session, argument, done):
    threading.Timer(0.5, done, (argument,)).start()


class TestQuit(pbrpc_test_pb2.TestQuit_Stub):
  @rpc_server.sync_call
  def Quit(self, _):
    # Special code, see rpc_test.testRpcBase.tearDown().
    sys.exit(8)


def child_process_main(options):
  logging.warn(' '.join(sys.argv))
  if options.socket:
    channel = rpc_server.TcpChannel('localhost', options.socket)
  else:
    channel = rpc_server.ListenTcpChannel('localhost', 0)
    # This must be the first thing printed.
    print 'Listening on port %d' % channel.port
  context = rpc_server.RpcContext(channel)
  if options.register_impl:
    context.register_impl(PbRpcTest(context))
    context.register_impl(Partial(context), partial=True)
  context.register_impl(TestQuit(context))
  for call in options.call:
    # --call format is Service.Method:result.
    service, method = call.split('.', 1)
    expected = None
    if ':' in method:
      method, expected = method.split(':', 1)
    # Here's how to make a manual function call without a stub.
    result = context.send_method_call_manual(service, method,
        pbrpc_pb2.Strings().SerializeToString(), pbrpc_pb2.Strings,
        None, None, 0)
    if expected:
      assert len(result.strings) == 1, ('%s returned %s' % (call,
          result.strings))
      assert expected == result.strings[0], ('%s returned instead: %s' % (
        call, str(result)))
    # TODO(maruel): Check the return value?
  # Here's an example on how to swallow all the exceptions generated. Exception
  # will be resurfaced outside context.run() so the caller can observe them and
  # restart the loop if wanted. In that case, we let two exception types filter
  # through.
  while True:
    try:
      context.run()
    except (SystemExit, KeyboardInterrupt):
      raise
    except Exception:
      pass


def main():
  parser = optparse.OptionParser()
  parser.add_option('--socket', type='int')
  parser.add_option('--register_impl', action='store_true')
  parser.add_option('--call', action='append', default=[])
  parser.add_option('-v', action='count')
  parser.add_option('--verbose', action='count')
  options, args = parser.parse_args()
  level = logging.FATAL
  if options.verbose:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  child_process_main(options)


if __name__ == '__main__':
  main()
