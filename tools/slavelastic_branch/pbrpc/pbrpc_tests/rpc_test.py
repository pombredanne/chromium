#!/usr/bin/python
# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

import logging
import os
import re
import subprocess
import sys
import time
import unittest

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from pbrpc.buffer import Buffer
from pbrpc import pbrpc_pb2
from pbrpc import rpc_server
from pbrpc_tests import pbrpc_test_pb2


def add_kill():
  """Add kill() method to subprocess.Popen for python <2.6"""
  if getattr(subprocess.Popen, 'kill', None):
    return
  if sys.platform == 'win32':
    def kill_win(process):
      import win32process
      return win32process.TerminateProcess(process._handle, -1)
    subprocess.Popen.kill = kill_win
  else:
    def kill_nix(process):
      import signal
      return os.kill(process.pid, signal.SIGKILL)
    subprocess.Popen.kill = kill_nix


class testRpcBase(unittest.TestCase):
  def setUp(self):
    unittest.TestCase.setUp(self)
    self.processes = []
    self.services = []
    self.connections = []
    self.sessions = []
    self.channel = None
    self.context = None
    self.env = os.environ.copy()
    self.env['PYTHONUNBUFFERED'] = '1'
    self.base_cmd = [sys.executable, os.path.join(
          os.path.dirname(os.path.abspath(__file__)), 'rpc_test_child.py')]
    if '-vv' in sys.argv:
      self.base_cmd.append('--verbose')
    self.do_connect()

  def tearDown(self):
    logging.debug('%d services, %d processes' % (len(self.services),
                                                 len(self.processes)))
    if (self.result.errors or self.result.failures or
        len(self.services) != len(self.processes)):
      logging.fatal('Force killing child processes')
      for p in self.processes:
        p.kill()
    else:
      for s in self.services:
        s.Quit(pbrpc_pb2.Strings(), timeout=-1)
      for p in self.processes:
        p.wait()
        # Special code, see rpc_child_test.TestQuit.Quit()
        self.assertEquals(8, p.returncode)
    self.processes = []
    self.services = []
    self.connections = []
    self.sessions = []
    self.channel = None
    self.context = None
    logging.debug('done')

  def run(self, result=None):
    if result is None:
      result = self.defaultTestResult()
    self.result = result
    return unittest.TestCase.run(self, result)

  def do_connect(self, extra=[]):
    self.call(extra)
    self.post_call()

  def call(self, extra=[]):
    pass

  def post_call(self):
    pass

  def args(self):
    return self.base_cmd


class testRpcTcpBase(testRpcBase):
  def call(self, extra=[]):
    self.processes.append(subprocess.Popen(self.args() + extra,
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, env=self.env))
    first_line = self.processes[-1].stdout.readline()
    socket_number = int(re.match(r'^Listening on port (\d+)',
                                 first_line).group(1))
    self.channel = rpc_server.TcpChannel('localhost', socket_number)
    self.context = rpc_server.RpcContext(self.channel)

  def post_call(self):
    # Default stub to send RPC call.
    self.services.append(rpc_server.RpcServiceStub(self.context, None,
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))


class testRpcTcpBare(testRpcTcpBase):
  def testGetVersion(self):
    self.assertEquals(self.services[0].GetVersion(pbrpc_pb2.Strings()),
        pbrpc_pb2.Strings(strings=[rpc_server.__version__]))

  def testGetRegisteredServices(self):
    self.assertEquals(self.services[0].GetRegisteredServices(
        pbrpc_pb2.Strings()),
        pbrpc_pb2.Strings(strings=['PbRpcService', 'TestQuit']))
  
  def testBuffer(self):
    b = Buffer()
    b.write('abc')
    b.write('def')
    self.assertEquals(len(b), 6)
    self.assertEquals(b.peek(), 'abcdef')
    self.assertEquals(b.peek(5), 'abcde')
    self.assertEquals(b.peek(4, 1), 'bcde')
    b.skip(1)
    self.assertEquals(b.peek(), 'bcdef')
    self.assertEquals(b.peek(10, 1), 'cdef')
    self.assertEquals(b.peek(2, 1), 'cd')
    b.skip(None)
    self.assertEquals(b.peek(), '')


class testRpcTcpImpl(testRpcTcpBase):
  def args(self):
    return self.base_cmd + ['--register_impl']

  def post_call(self):
    # Stub to send RPC call.
    self.services.append(rpc_server.RpcServiceStub(self.context, None,
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestPbRpcService_Stub,
        pbrpc_test_pb2.TestNotImplemented_Stub, pbrpc_test_pb2.TestPartial_Stub,
        pbrpc_test_pb2.TestQuit_Stub))

  def testNoService(self):
    try:
      self.services[0].Inexistent(pbrpc_pb2.Strings())
      self.fail()
    except rpc_server.PbRpcServiceNotFound, e:
      self.assertEquals(e.service, 'TestNotImplemented')
      self.assertEquals(e.method, 'Inexistent')

  def testNoMethod(self):
    try:
      self.services[0].Inexistent2(pbrpc_pb2.Strings())
      self.fail()
    except rpc_server.PbRpcMethodNotFound, e:
      self.assertEquals(e.service, 'TestPartial')
      self.assertEquals(e.method, 'Inexistent2')

  def testThrow1(self):
    try:
      self.services[0].ThrowsException1(pbrpc_pb2.Strings())
      self.fail()
    except rpc_server.PbRpcRemoteException:
      pass

  def testThrow2(self):
    try:
      self.services[0].ThrowsException2(pbrpc_pb2.Strings())
      self.fail()
    except rpc_server.PbRpcRemoteException:
      pass

  def testNeverCallback(self):
    try:
      self.services[0].NeverCallback(pbrpc_pb2.Strings(), timeout=2)
      self.fail()
    except rpc_server.PbRpcCallExpired:
      pass

  def testDelayedEcho(self):
    obj = pbrpc_pb2.Strings(strings=['foo'])
    result = []
    def do(value, exception):
      result.append(value)
      self.assertEquals(exception, None)
    self.services[0].DelayedEcho(obj, callback=do)
    while not result:
      self.context.call_expireds()
      self.context.process_method_call()
    self.assertEquals(list(result[0].strings), ['foo'])


class testRpcTcpReverseBase(testRpcBase):
  def call(self, extra=[]):
    # Channel must be initialized *before* starting the child process
    if not self.channel:
      self.channel = rpc_server.ListenTcpChannel('localhost', 0)
    # Don't start the context before the process. It's not necessary.
    current_sessions = self.get_sessions()
    self.processes.append(subprocess.Popen(self.args() + extra, env=self.env))
    if not self.context:
      self.context = rpc_server.RpcContext(self.channel)
      self.register_impl()
    self.sessions.append(self.wait_for_new_session(current_sessions))

  def args(self):
    return self.base_cmd + ['--socket', str(self.channel.port)]

  def register_impl(self):
    pass

  def get_sessions(self):
    if not self.context:
      return []
    return self.context._session_ids.keys()

  def wait_for_new_session(self, sessions):
    logging.info('')
    while True:
      for session_id in self.context._session_ids:
        if not session_id in sessions:
          logging.info('Found new session %s' % session_id)
          return session_id
      self.context.run_pending(0.01)


class testRpcTcpReverse(testRpcTcpReverseBase):
  def testGetVersion(self):
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[0],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))

    self.assertEquals(self.services[0].GetVersion(pbrpc_pb2.Strings()),
        pbrpc_pb2.Strings(strings=[rpc_server.__version__]))

  def testGetRegisteredServices(self):
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[0],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    self.assertEquals(
        list(self.services[0].GetRegisteredServices(
                 pbrpc_pb2.Strings()).strings),
        ['PbRpcService', 'TestQuit'])

  def test3ConsecutiveConnections(self):
    def HaveFun(reg):
      try:
        rpc_server.RpcServiceStub(self.context, None,
            pbrpc_pb2.PbRpcService_Stub)
        self.fail()
      except TypeError:
        pass
      service = rpc_server.RpcServiceStub(self.context, self.sessions[0],
          pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub)
      self.services.append(service)
      if reg:
        self.assertEquals(
            list(service.GetRegisteredServices(pbrpc_pb2.Strings()).strings),
            ['PbRpcService', 'TestPbRpcService', 'TestPartial', 'TestQuit'])
      else:
        self.assertEquals(
            list(service.GetRegisteredServices(pbrpc_pb2.Strings()).strings),
            ['PbRpcService', 'TestQuit'])
      self.tearDown()

    HaveFun(False)
    self.call(['--register_impl'])
    HaveFun(True)
    self.call()
    HaveFun(False)

  def test3SimultaneousConnections(self):
    # Start a 2nd and 3rd children with different services.
    self.call(['--register_impl'])
    self.call()
    try:
      rpc_server.RpcServiceStub(self.context, None, pbrpc_pb2.PbRpcService_Stub)
      self.fail()
    except TypeError:
      pass
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[0],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    self.assertEquals(
        list(self.services[0].GetRegisteredServices(
                 pbrpc_pb2.Strings()).strings),
        ['PbRpcService', 'TestQuit'])
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[1],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    self.assertEquals(
        list(self.services[1].GetRegisteredServices(
                 pbrpc_pb2.Strings()).strings),
        ['PbRpcService', 'TestPbRpcService', 'TestPartial', 'TestQuit'])
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[2],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    self.assertEquals(
        list(self.services[2].GetRegisteredServices(
                 pbrpc_pb2.Strings()).strings),
        ['PbRpcService', 'TestQuit'])

  def testStatefulReconnection(self):
    # Open a connection, forcibly turn it down and make it reconnect. Check if
    # the session is still alive.
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[0],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    # Hack the socket to close it.


class testRpcTcpReverseCall(testRpcTcpReverseBase):
  def setUp(self):
    self.unlock = False
    testRpcTcpReverseBase.setUp(self)

  def register_impl(self):
    # To wait for a valid callback
    class PbRpcTest(pbrpc_test_pb2.TestPbRpcService_Stub):
      def ThrowsException1(self2, controller, argument, callback):
        self.unlock = True
        callback(pbrpc_pb2.Strings(strings=['yay']))
    self.context.register_impl(PbRpcTest(self.context), partial=True)

  def args(self):
    # --call format is Service.Method:result.
    return testRpcTcpReverseBase.args(self) + [
        '--call', 'PbRpcService.GetVersion:' + rpc_server.__version__,
        '--call', 'TestPbRpcService.ThrowsException1:yay']

  def testReverseGetVersion(self):
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[0],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    while not self.unlock:
      self.context.call_expireds()
      self.context.process_method_call()

  def testBothWayGetVersion(self):
    self.services.append(rpc_server.RpcServiceStub(self.context,
        self.sessions[0],
        pbrpc_pb2.PbRpcService_Stub, pbrpc_test_pb2.TestQuit_Stub))
    # Also call in the other way.
    self.assertEquals(self.services[0].GetVersion(pbrpc_pb2.Strings()),
        pbrpc_pb2.Strings(strings=[rpc_server.__version__]))
    while not self.unlock:
      self.context.call_expireds()
      self.context.process_method_call()


def main():
  add_kill()
  level = logging.FATAL
  if '-v' in sys.argv:
    level = logging.DEBUG
  module_name = os.path.basename(sys.modules[__name__].__file__)[:-3]
  logging.basicConfig(level=level,
      format=module_name + ':%(module)s(%(lineno)d) %(funcName)s:%(message)s')
  unittest.main()

if __name__ == '__main__':
  main()
