#!/usr/bin/env python
# Copyright (c) 2010 Marc-Antoine Ruel

import os
import subprocess


def call(cmd):
  cmd = cmd.split(' ')
  subprocess.check_call(cmd)


os.chdir(os.path.dirname(os.path.abspath(__file__)))

print('Generating pbrpc protobuf')
call('protoc --python_out=pbrpc pbrpc/pbrpc/pbrpc.proto -Ipbrpc')
call('protoc --python_out=pbrpc pbrpc/pbrpc_tests/pbrpc_test.proto -Ipbrpc')

print('Generating slavelastic protobuf')
call('protoc --python_out=slavelastic '
     'slavelastic/slavelastic/common/slavelastic.proto -Islavelastic')
