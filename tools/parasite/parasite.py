#!/usr/bin/python
# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Use the trybots as a poorman's speculative build.

It's a pain to maintain separate builders for low volume projects like gyp.
Instead, just keep the trybots working and reuse them as an agent poor for
tree status.
"""

import bsddb
import datetime
import json
import os
import pprint
import re
import socket
import subprocess
import sys
import time
import urllib
import urllib2


PARASITE_CONFIG = 'parasite.conf'
SVN_JUNK_DIR = 'parasite_svn_junk'
MAX_HISTORY = 4
MAX_BUILDING = 3


class JsonDB(object):
  """Wrapper around db that assumes all elements stored are json."""
  def __init__(self, filename, mode='c'):
    self.db = bsddb.hashopen(filename, mode)

  def __setitem__(self, key, value):
    rkey = json.dumps(key, sort_keys=True)
    self.db[rkey] = json.dumps(value, sort_keys=True)
    self.db.sync()

  def __getitem__(self, key):
    return self.get(key)

  def __delitem__(self, key):
    rkey = json.dumps(key, sort_keys=True)
    del self.db[rkey]

  def get(self, key, alt=None):
    value = self.db.get(json.dumps(key, sort_keys=True))
    if value is None:
      return alt
    try:
      return json.loads(value)
    except [TypeError, ValueError]:
      sys.stderr.write('WARNING: key %s is corrupt\n' % key)
      return alt


def GetLatestRev(url):
  """Get the latest revision of a svn url

  Args:
    url: svn url.
  Returns:
    Latest revision.
  """
  p = subprocess.Popen(['svn', 'log', '-l', '1', '-q', url],
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  (p_stdout, p_stderr) = p.communicate()
  if p.returncode != 0:
    return -1
  m = re.search('r([0-9]+) ', p_stdout)
  if not m or not m.group(1):
    return -1
  return int(m.group(1))


def GclTry(name, tryserver_svn_url, revision, bot, email):
  """Trigger a tryjob.

  Args:
    name: string identifying the tryjob.
    tryserver_svn_url: svn url for the tryserver.
    revision: revision to trigger.
    bot: bot type to trigger.
    email: email address to use.
  """
  # Create empty checkout.
  cmd = [
      'svn', 'co', '--depth', 'empty',
      'http://src.chromium.org/svn/trunk', SVN_JUNK_DIR,
  ]
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  p.communicate()
  if p.returncode:
    return False
  # Create empty change list.
  cmd = ['gcl', 'change', 'foo']
  env = os.environ.copy()
  env['EDITOR'] = 'touch'
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                       cwd=SVN_JUNK_DIR, env=env)
  p.communicate()
  if p.returncode:
    return False
  # Kick off tryjob.
  cmd = [
      'trychange.py',
      '--name', name,
      '--use_svn',
      '-S', tryserver_svn_url,
      '-r', revision,
      '--patchlevel', '1',
      '--root', 'src',
      '--email', email,
      '--diff', '/dev/null',
      '-b', bot,
  ]
  p = subprocess.Popen(cmd, cwd=SVN_JUNK_DIR,
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  (p_stdout, p_stderr) = p.communicate()
  if p.returncode != 0:
    print p_stdout, p_stderr
    return False
  return True


def QueryBots(url):
  """Query a json buildbot url.

  Args:
    url: url to queury.
  Returns:
    Decoded json from url.
  """
  return json.loads(urllib2.urlopen(url).read())


def SetTreeStatus(url, message):
  """Change the tree status.

  Args:
    url: base url for tree status e.g. https://chromium-status.appspot.com
    message: message to set.
  """
  username = 'buildbot@chromium.org'
  password = open('.status_password', 'r').read().strip()
  params = urllib.urlencode({
      'message': message,
      'username': username,
      'password': password,
  })
  f = urllib2.urlopen(url + '/status', params)
  f.read()


def GetTreeStatus(url):
  """Get the current tree status.

  Args:
    url: base url for tree status e.g. https://chromium-status.appspot.com
  Return:
    Current tree status json, decoded.
  """
  return json.loads(urllib2.urlopen(
      url + '/current?format=json').read())


def UpdateLatest(project, db):
  """For one project update its status in the database.

  Args:
    project: project config dictionary.
    db: database object.
  """
  # Get latest rev.
  latest_rev = GetLatestRev(project['main_svn'])
  # Get latest rev in db.q
  key = {'project': project['name'], 'field': 'latest'}
  db_latest_rev = db.get(key, 0)
  # Done if nothing is new.
  if latest_rev <= db_latest_rev:
    return
  # Decide how far back in time to go.
  rev = db_latest_rev + 1
  if latest_rev - rev > MAX_HISTORY:
    rev = latest_rev - MAX_HISTORY
  # Add all missing revisions to needed.
  needed = db.get('builds_needed', [])
  for i in range(rev, latest_rev + 1):
    builders = project.get('builders', [])
    for builder in builders:
      needed.append({
          'project': project,
          'revision': i,
          'builder': builder,
          })
  db['builds_needed'] = needed
  # Advance marker.
  db[key] = latest_rev


def TriggerBuilds(db):
  """Trigger needed builds.

  Args:
    db: database object.
  """
  triggered = db.get('builds_triggered', [])
  needed = db.get('builds_needed', [])
  while len(triggered) < MAX_BUILDING and len(needed) > 0:
    # Pull out one.
    build = needed[-1]
    needed = needed[:-1]
    # Pluck out project.
    project = build['project']
    # Pick a random name.
    name = 'parasite_%s_%.7f' % (project['name'], time.time())
    build['name'] = name
    # Kick off try.
    GclTry(name=name,
           tryserver_svn_url=project['tryserver_write'],
           revision=str(build['revision']),
           bot=build['builder'],
           email='bradnelson@google.com')
    # Add to triggered list.
    triggered.append(build)
    # Update database.
    db['builds_triggered'] = triggered
    db['builds_needed'] = needed


def WatchForTriggered(db):
  """Check for relevant build's we've triggered.

  Args:
    db: database object.
  """
  triggered = db.get('builds_triggered', [])
  # Find all the sources to poll.
  sources = set()
  for build in triggered:
    if build.get('id') is None:
      project = build['project']
      sources.add('%s/json/builders/%s' % (
          project['tryserver_read'], build['builder']))
  # Tag the ones with IDs that we can.
  for source in sources:
    data = QueryBots(source)
    for bbuild in data.get('currentBuilds', []):
      bdata = QueryBots('%s/builds/%s' % (source, bbuild))
      for build in triggered:
        #if bdata.get('sourceStamp', {}).get(
        #    'job_name', '') == build.get('name'):
        if bdata.get('reason', '') == build.get('name'):
          build['id'] = bbuild
  # Update the database.
  db['builds_triggered'] = triggered


def WatchForFinish(db):
  """Check if any triggered builds have finished.

  Args:
    db: database object.
  """
  triggered = db.get('builds_triggered', [])
  for build in triggered:
    if build.get('id') is None:
      continue
    project = build['project']
    url = '%s/json/builders/%s/builds/%s' % (
        project['tryserver_read'], build['builder'], build['id'])
    bdata = QueryBots(url)
    build['state'] = bdata
    # Update history.
    key = {'project': project['name'], 'revision': build['revision']}
    history = db.get(key, {})
    history[build['name']] = build
    db[key] = history
  # Drop finished ones.
  triggered = [t for t in triggered
               if not t.get('state', {}).get('text', None)]
  # Update the database.
  db['builds_triggered'] = triggered


def ProjectSummary(project, db, fh):
  """Write out a project summary page.

  Args:
    project: project config dictionary.
    db: database object.
    fh: destination file handle to write to.
  """
  fh.write('<html>\n')
  fh.write('<head>\n')
  fh.write('<title>%s status</title>\n' % project['name'])
  fh.write('<style type="text/css">\n')
  fh.write('table, th, td {\n')
  fh.write('  font-size: 24px;\n')
  fh.write('}\n')
  fh.write('a { color: blue; }\n')
  fh.write('a.build { color: black; text-decoration:none; }\n')
  fh.write('</style>\n')
  fh.write('</head>\n')
  fh.write('<body>\n')
  fh.write('<iframe width="100%%" height="44" frameborder="0" scrolling="no" '
           'src="%s/current"></iframe>' % project['tree_status'])
  fh.write('<a href="%s" target="_blank">tree status</a>\n' % (
      project['tree_status']))
  fh.write('<div style="text-align: right;">Generated: ' +
           datetime.datetime.now().isoformat() +
           '</div>')
  fh.write('<table align="center">\n')
  fh.write('<tr><th>Revision</th><th>Results</th></tr>\n')
  key = {'project': project['name'], 'field': 'latest'}
  db_latest_rev = db.get(key, 0)
  min_rev = db_latest_rev - MAX_HISTORY
  if min_rev < 1: rev = 1
  for rev in reversed(range(min_rev, db_latest_rev + 1)):
    results = db.get({'project': project['name'], 'revision': rev})
    if not results:
      continue
    result_pairs = [(results[r]['builder'], r) for r in results.keys()]
    result_pairs = sorted(result_pairs)
    fh.write('<tr><td><a href="%s">%d</a></td><td>' % (
        project['source_url'] % rev, rev))
    for name in [r[1] for r in result_pairs]:
      build = results[name]
      builder = build['builder']
      text = ' '.join(build['state'].get('text', []))
      link = '%s/builders/%s/builds/%d' % (
          build['project']['tryserver_read'], build['builder'], build['id'])
      if text == 'build successful':
        color = '#8FDF5F'
      elif text == '':
        color = '#FFFC6C'
      else:
        color = '#E98080'
      fh.write('<span style="background-color:%s">'
               '<a href="%s" class="build" target="_blank">%s</a></span> ' % (
          color, link, builder))
    fh.write('</td></tr>\n')
  fh.write('</table>\n')
  fh.write('</body>\n')
  fh.write('</html>\n')


def UpdateOutput(config, db):
  """Update output for all projects.

  Args:
    config: configuration dictionary.
    db: database object.
  """
  for project in config.get('projects', []):
    filename = os.path.expanduser(project['output_path'])
    filename_tmp = filename + '.tmp'
    fh = open(filename_tmp, 'w')
    ProjectSummary(project, db, fh)
    fh.close()
    os.chmod(filename_tmp, 0644)
    os.rename(filename_tmp, filename)


def UpdateTreeGate(config, db):
  """Update tree gate based on project status.

  Args:
    config: configuration dictionary.
    db: database object.
  """
  for project in config.get('projects', []):
    # Get status of latest rev.
    key = {'project': project['name'], 'field': 'latest'}
    db_latest_rev = db.get(key, 0)
    results = db.get({'project': project['name'],
                      'revision': db_latest_rev}, [])
    result_good = True
    for name in results:
      build = results[name]
      text = ' '.join(build['state'].get('text', []))
      if text != 'build successful' and text != '':
        result_good = False
    result_complete = len(results) == len(project['builders'])
    # Get current tree status.
    current_status = GetTreeStatus(project['tree_status'])
    is_open = current_status.get('general_state', 'err') == 'open'

    # Close if should be closed.
    if is_open and not result_good:
      message = 'closed by parasite at r%d' % db_latest_rev
      SetTreeStatus(project['tree_status'], message)

    # Open if should be open.
    if not is_open and result_good and result_complete:
      message = 'opened by parasite at r%d' % db_latest_rev
      SetTreeStatus(project['tree_status'], message)


def MonitorOnce(config, db):
  """Make one update monitoring pass.

  Args:
    config: configuration dictionary.
    db: database object.
  """
  for project in config.get('projects', []):
    UpdateLatest(project, db)
  TriggerBuilds(db)
  WatchForTriggered(db)
  WatchForFinish(db)
  UpdateOutput(config, db)
  UpdateTreeGate(config, db)


def main():
  # Only have one instance running.
  try:
    s = socket.socket()
    s.bind((socket.gethostname(), 12345))
  except:
    # Port in use, already running.
    sys.exit(0)

  # Load config file.
  try:
    config = json.loads(open(PARASITE_CONFIG, 'r').read())
  except:
    raise
    sys.stderr.write('ERROR: UNABLE TO LOAD "%s"\n' % PARASITE_CONFIG)
    sys.exit(1)
  # Open state database.
  db = JsonDB('parasite_db')
  # Run once.
  try:
    MonitorOnce(config, db)
  except Exception, e:
    print str(e)


if __name__ == '__main__':
  main()
