#!/usr/bin/python
# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import io
import json
import sys
import urllib
import urllib2


class flushfile(io.TextIOWrapper):
  def __init__(self, f):
    self.f = f
  def write(self, x):
    self.f.write(x)
    self.f.flush()

sys.stdout = flushfile(sys.stdout)


# Get the list of bots.
builders_url = 'http://build.chromium.org/p/chromium/json/builders'
build_url = 'http://build.chromium.org/p/chromium/json/builders/%s/builds/%d'

builders_req = urllib2.urlopen(builders_url)
builders_data = builders_req.read()
builders_json = json.loads(builders_data)
builders_list = builders_json.keys()
builders_list.sort()

for builder in builders_list:
  # Grab the latest 10 builds
  builders_json[builder]['cachedBuilds'].reverse()
  builds = builders_json[builder]['cachedBuilds'][0:10]
  max_time = -1
  min_time = -1
  total_time = 0
  total_count = 0
  for build in builds:
    # Fetch the build info
    url = build_url % (urllib.quote(builder), build)
    build_req = urllib2.urlopen(url)
    build_data = build_req.read()
    build_json = json.loads(build_data)
    
    if build_json['currentStep'] is not None:
      # Build still running, ignoring this build.
      continue
    if 'successful' not in build_json['text']:
      # Build failed, ignore this build. 
      continue

    # Get complete cycle time for this build.
    cycle_time = int(build_json['times'][1]) - int(build_json['times'][0])

    # Update max and min times.
    if max_time == -1 or max_time < cycle_time:
      max_time = cycle_time
    if min_time == -1 or min_time > cycle_time:
      min_time = cycle_time
   
    total_time += cycle_time
    total_count += 1

  average_time = 0
  if total_count != 0:
    average_time = total_time / total_count

  print '%s, %d, %d, %d, %d' % (builder, average_time, max_time, min_time,
                                total_count)
