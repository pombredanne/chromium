USAGE

  Copy these files into the directory where the log_scraper scripts write their
  output.  Presumably this should be a directory reachable via the web. There
  are different types of reports, use the one you need. You may need to rename
  the report name to "report.html".

  Point your browser at report.html and behold.
  

BROWSER COMPAT

  This application uses the <canvas> tag to render the graph of the performance
  data, so you will need a capable browser to view it.  Firefox 1.5 and above
  should work.  Safari with WebKit tip-of-tree also appears to mostly work.
