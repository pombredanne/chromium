var Config = {
  // test title:
  'title': "page-cycler test",

  // link to the source code for the test:
  'source': "http://chrome-svn/viewvc/chrome-internal/trunk/data/page_cycler/moz/start.html?revision=HEAD",

  // link to svn repository viewer; two revision numbers will be appended in
  // the form "123:456".
  'changeLinkPrefix': "http://build.chromium.org/f/chromium/perf/dashboard/ui/changelog.html?url=/trunk/src&mode=html&range=",

  // builder name:
  'builder': "chrome Release - Full",

  // builder link:
  'builderLink': "http://build.chromium.org/buildbot/waterfall/waterfall?builder=chrome%20Release%20-%20Full"
};
