OVERVIEW

  This directory contains code for building a dashboard for tracking perf test
  results over a sequence of chrome builds. 


CONTENTS

  ui/
    - contains the HTML and JS used to construct the UI of the performance
      dashboard.

  log_scraper has been deprecated over log parsers running on buildbot.

