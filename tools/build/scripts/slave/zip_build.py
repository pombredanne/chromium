#!/usr/bin/env python
# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

""" Creates a zip file in the staging dir with the result of a compile.
    It can be sent to other machines for testing.
"""

import glob
import optparse
import os
import re
import shutil
import stat
import sys
import tempfile

from common import chromium_utils
from slave import slave_utils

class StagingError(Exception): pass

WARNING_EXIT_CODE = 88


def GetRecentBuildsByBuildNumber(zip_list, zip_base, zip_ext):
  # Build an ordered list of build numbers we have zip files for.
  regexp = re.compile(zip_base + '_([0-9]+)(_old)?' + zip_ext)
  build_list = []
  for x in zip_list:
    regexp_match = regexp.match(os.path.basename(x))
    if regexp_match:
      build_list.append(int(regexp_match.group(1)))
  # Since we match both ###.zip and ###_old.zip, bounce through a set and back
  # to a list to get an order list of build numbers.
  build_list = list(set(build_list))
  build_list.sort()
  # Only keep the last 10 number (that means we could have 20 due to _old files
  # if someone forced a respin of every single one)
  saved_build_list = build_list[-10:]
  ordered_asc_by_build_number_list = []
  for saved_build in saved_build_list:
    recent_name = zip_base + ('_%d' % saved_build) + zip_ext
    ordered_asc_by_build_number_list.append(recent_name)
    ordered_asc_by_build_number_list.append(
        recent_name.replace(zip_ext, '_old' + zip_ext))
  return ordered_asc_by_build_number_list


def GetRecentBuildsByModificationTime(zip_list):
  """Return the 10 most recent builds by modification time."""
  # Get the modification times for all of the entries in zip_list.
  mtimes_to_files = {}
  for zip_file in zip_list:
    mtime = int(os.stat(zip_file).st_mtime)
    mtimes_to_files.setdefault(mtime, [])
    mtimes_to_files[mtime].append(zip_file)
  # Order all files in our list by modification time.
  mtimes_to_files_keys = mtimes_to_files.keys()
  mtimes_to_files_keys.sort()
  ordered_asc_by_mtime_list = []
  for key in mtimes_to_files_keys:
    ordered_asc_by_mtime_list.extend(mtimes_to_files[key])
  # Return the most recent 10 builds.
  return ordered_asc_by_mtime_list[-10:]


def GetRealBuildDirectory(build_dir, target, factory_properties):
  """Return the build directory."""
  if chromium_utils.IsWindows():
    return os.path.join(build_dir, target)

  if chromium_utils.IsLinux():
    return os.path.join(os.path.dirname(build_dir), 'out', target)

  if chromium_utils.IsMac():
    is_make_or_ninja = (factory_properties.get("gclient_env", {})
        .get('GYP_GENERATORS', '') in ('ninja', 'make'))
    if is_make_or_ninja:
      return os.path.join(os.path.dirname(build_dir), 'out', target)
    return os.path.join(os.path.dirname(build_dir), 'xcodebuild', target)

  raise NotImplementedError('%s is not supported.' % sys.platform)


def ShouldPackageFile(filename, options):
  """Returns true if the file should be a part of the resulting archive."""

  if chromium_utils.IsWindows() and options.target is 'Release':
    # Special case for chrome. Add back all the chrome*.pdb files to the list.
    # Also add browser_test*.pdb, ui_tests.pdb and ui_tests.pdb.
    # TODO(nsylvain): This should really be defined somewhere else.
    expression = (r"^(chrome_dll|chrome_exe"
    #             r"|browser_test.+|unit_tests"
    #             r"|chrome_frame_.*tests"
                  r")\.pdb$")
    if re.match(expression, filename):
      return True

  file_filter = '$NO_FILTER^'
  if chromium_utils.IsWindows():
    # Remove all .ilk/.7z and maybe PDB files
    include_pdbs = options.factory_properties.get('package_pdb_files', False)
    if include_pdbs:
      file_filter = '^.+\.(ilk|7z)$'
    else:
      file_filter = '^.+\.(ilk|pdb|7z)$'
  elif chromium_utils.IsMac():
    # The static libs are just built as intermediate targets, and we don't
    # need to pull the dSYMs over to the testers.
    file_filter = '^.+\.(a|dSYM)$'
  elif chromium_utils.IsLinux():
    # object files, archives, and gcc (make build) dependency info.
    file_filter = '^.+\.(o|a|d)$'

  if re.match(file_filter, filename):
    return False

  # Skip files that the testers don't care about. Mostly directories.
  things_to_skip = []
  if chromium_utils.IsWindows():
    # Remove obj or lib dir entries
    things_to_skip = [ 'obj', 'lib', 'cfinstaller_archive', 'installer_archive']
  elif chromium_utils.IsMac():
    things_to_skip = [
      # We don't need the arm bits v8 builds.
      'd8_arm', 'v8_shell_arm',
      # pdfsqueeze is a build helper, no need to copy it to testers.
      'pdfsqueeze',
      # The inspector copies its resources into a resources folder in the build
      # output, but we only need the copy that ends up within the Chrome bundle.
      'resources',
      # We copy the framework into the app bundle, we don't need the second
      # copy outside the app.
      # TODO(mark): Since r28431, the copy in the build directory is actually
      # used by tests.  Putting two copies in the .zip isn't great, so maybe
      # we can find another workaround.
      # 'Chromium Framework.framework',
      # 'Google Chrome Framework.framework',
      # We copy the Helper into the app bundle, we don't need the second
      # copy outside the app.
      'Chromium Helper.app',
      'Google Chrome Helper.app',
      '.deps', 'obj', 'obj.host', 'obj.target',
    ]
  elif chromium_utils.IsLinux():
    things_to_skip = [
      # intermediate build directories (full of .o, .d, etc.).
      'appcache', 'glue', 'googleurl', 'lib', 'lib.host', 'obj', 'obj.host',
      'obj.target', 'src', '.deps',
      # scons build cruft
      '.sconsign.dblite',
      # build helper, not needed on testers
      'mksnapshot',
    ]

  if filename in things_to_skip:
    return False

  return True


def WriteRevisionFile(path, build_revision):
  """Writes a file containing revision number to given path.
  Replaces the target file in place."""
  try:
    # Script only works on python 2.6
    # pylint: disable=E1123
    tmp_revision_file = tempfile.NamedTemporaryFile(
        mode='w', dir=os.path.dirname(path),
        delete=False)
    tmp_revision_file.write('%d' % build_revision)
    tmp_revision_file.close()
    shutil.move(tmp_revision_file.name, path)
    chromium_utils.MakeWorldReadable(path)
  except IOError:
    print 'Writing to revision file %s failed ' % path


def MakeUnversionedArchive(build_dir, staging_dir, build_revision,
                           zip_file_list):
  """Creates an unversioned full build archive.
  Returns the path of the created archive."""
  zip_file_name = 'full-build-%s' % chromium_utils.PlatformName()
  (zip_dir, zip_file) = chromium_utils.MakeZip(staging_dir,
                                               zip_file_name,
                                               zip_file_list,
                                               build_dir,
                                               raise_error=True)
  chromium_utils.RemoveDirectory(zip_dir)
  if not os.path.exists(zip_file):
    raise StagingError('Failed to make zip package %s' % zip_file)
  chromium_utils.MakeWorldReadable(zip_file)

  # Report the size of the zip file to help catch when it gets too big and
  # can cause bot failures from timeouts during downloads to testers.
  zip_size = os.stat(zip_file)[stat.ST_SIZE]
  print 'Zip file is %ld bytes' % zip_size

  return zip_file


def _MakeVersionedArchive(zip_file, file_suffix):
  """Takes a file name, e.g. /foo/bar.zip and an extra suffix, e.g. _baz,
  and copies the file to /foo/bar_baz.zip."""
  zip_template = os.path.basename(zip_file)
  zip_base, zip_ext = os.path.splitext(zip_template)
  # Create a versioned copy of the file.
  versioned_file = zip_file.replace(zip_ext, file_suffix + zip_ext)
  if os.path.exists(versioned_file):
    # This file already exists. Maybe we are doing a clobber build at the same
    # revision. We can move this file away.
    old_file = versioned_file.replace(zip_ext, '_old' + zip_ext)
    chromium_utils.MoveFile(versioned_file, old_file)
  shutil.copyfile(zip_file, versioned_file)
  chromium_utils.MakeWorldReadable(versioned_file)
  print 'Created versioned archive', versioned_file
  return (zip_base, zip_ext)


def MakeVersionedArchive(zip_file, build_revision):
  """Ensures that a versioned archive exists corresponding
  to given unversioned archive."""
  return _MakeVersionedArchive(zip_file, '_%d' % build_revision)


def MakeWebKitVersionedArchive(zip_file, cr_revision, wk_revision):
  """Ensures that a versioned archive exists corresponding
  to given unversioned archive."""
  return _MakeVersionedArchive(zip_file, '_wk%d_%d' %
                                         (wk_revision, cr_revision))


def PruneOldArchives(staging_dir, zip_base, zip_ext):
  """Removes old archives so that we don't exceed disk space."""
  zip_list = glob.glob(os.path.join(staging_dir, zip_base + '_*' + zip_ext))
  saved_zip_list = GetRecentBuildsByBuildNumber(zip_list, zip_base, zip_ext)
  saved_mtime_list = GetRecentBuildsByModificationTime(zip_list)

  # Prune zip files not matched by the whitelists above.
  for zip_file in zip_list:
    if zip_file not in saved_zip_list and zip_file not in saved_mtime_list:
      print 'Pruning zip %s.' % zip_file
      chromium_utils.RemoveFile(staging_dir, zip_file)


def archive(options, args):
  src_dir = os.path.abspath(options.src_dir)
  build_dir = GetRealBuildDirectory(options.build_dir, options.target,
                                    options.factory_properties)

  staging_dir = slave_utils.GetStagingDir(src_dir)
  build_revision = slave_utils.SubversionRevision(src_dir)
  chromium_utils.MakeParentDirectoriesWorldReadable(staging_dir)

  webkit_dir, webkit_revision = None, None
  if options.webkit_dir:
    webkit_dir = os.path.join(src_dir, options.webkit_dir)
    webkit_revision = slave_utils.SubversionRevision(webkit_dir)

  print 'Full Staging in %s' % build_dir

  # Build the list of files to archive.
  zip_file_list = [f for f in os.listdir(build_dir)
                   if ShouldPackageFile(f, options)]
  if options.include_files is not None:
    zip_file_list.extend([f for f in os.listdir(build_dir)
                          if f in options.include_files])

  zip_file = MakeUnversionedArchive(build_dir, staging_dir,
                                    build_revision, zip_file_list)
  if not webkit_revision:
    (zip_base, zip_ext) = MakeVersionedArchive(zip_file, build_revision)
  else:
    (zip_base, zip_ext) = MakeWebKitVersionedArchive(
        zip_file, build_revision, webkit_revision)
  PruneOldArchives(staging_dir, zip_base, zip_ext)

  # Update the latest revision file in the staging directory
  # to allow testers to figure out the latest packaged revision
  # without downloading tarballs.
  latest_revision_path = os.path.join(
      staging_dir, chromium_utils.FULL_BUILD_REVISION_FILENAME)
  WriteRevisionFile(latest_revision_path, build_revision)

  return 0


def main(argv):
  option_parser = optparse.OptionParser()
  option_parser.add_option('', '--target', default='Release',
      help='build target to archive (Debug or Release)')
  option_parser.add_option('', '--src-dir', default='src',
                           help='path to the top-level sources directory')
  option_parser.add_option('', '--build-dir', default='chrome',
                           help='path to main build directory (the parent of '
                                'the Release or Debug directory)')
  option_parser.add_option('', '--include-files', default=None,
                           help='files that should be included in the'
                                'zip, regardless of any exclusion patterns')
  option_parser.add_option('', '--webkit-dir', default=None,
                           help='webkit directory path, relative to --src-dir')
  option_parser.add_option('--factory-properties', action='callback',
                           callback=chromium_utils.convert_json, type='string',
                           nargs=1, default={},
                           help='factory properties in JSON format')

  options, args = option_parser.parse_args(argv)
  return archive(options, args)

if '__main__' == __name__:
  sys.exit(main(sys.argv))
