# -*- python -*-
# ex: set syntax=python:

# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# This is the buildmaster config file for the 'chromium' bot. It must
# be installed as 'master.cfg' in your buildmaster's base directory
# (although the filename can be changed with the --basedir option to
# 'mktap buildbot master').

# It has one job: define a dictionary named BuildmasterConfig. This
# dictionary has a variety of keys to control different aspects of the
# buildmaster. They are documented in docs/config.xhtml .

# This file follows this naming convention:
# Factories: f_cr_[dbg/rel]_[type]
# Builders:  b_chromium_[dbg/rel]_[os]_[type]
# BuildDir:  chromium-[dbg/rel]-[os]-[type]
#
# os = xp/vista/linux/mac
# type = perf

from buildbot.changes import svnpoller
from buildbot.scheduler import Scheduler
from buildbot.scheduler import Triggerable

from master import build_utils
from master import chromium_notifier
from master import master_config
from master import master_utils
from master import slaves_list
from master.factory import chromium_factory

import config

ActiveMaster = config.Master.ChromiumPerf

# Enable MAIL_NOTIFIER in production to send perf sheriff and cmp alerts.
MAIL_NOTIFIER = ActiveMaster.is_production_host

# This is the dictionary that the buildmaster pays attention to. We also use
# a shorter alias to save typing.
c = BuildmasterConfig = {}

# 'slavePortnum' defines the TCP port to listen on. This must match the value
# configured into the buildslaves (with their --master option)
c['slavePortnum'] = ActiveMaster.slave_port

# Disable compression for the stdio files.
c['logCompressionLimit'] = False

# Load the list of slaves.
slaves = slaves_list.SlavesList('slaves.cfg', 'ChromiumPerf')

config.DatabaseSetup(c, require_dbconfig=ActiveMaster.is_production_host)

####### CHANGESOURCES

# the 'change_source' list tells the buildmaster how it should find out about
# source code changes. Any class which implements IChangeSource can be added
# to this list: there are several in buildbot/changes/*.py to choose from.
def ChromeTreeFileSplitter(path):
  """split_file for the 'src' project in the trunk."""

  # Exclude .DEPS.git from triggering builds on chrome.
  if path == 'src/.DEPS.git':
    return None

  # List of projects we are interested in. The project names must exactly
  # match paths in the Subversion repository, relative to the 'path' URL
  # argument. build_utils.SplitPath() will use them as branch names to
  # kick off the Schedulers for different projects.
  projects = ['src']
  return build_utils.SplitPath(projects, path)

# Polls config.Master.trunk_url for changes
chromium_rev = 'http://src.chromium.org/viewvc/chrome?view=rev&revision=%s'
trunk_poller = svnpoller.SVNPoller(svnurl=config.Master.trunk_url,
                                   split_file=ChromeTreeFileSplitter,
                                   pollinterval=10,
                                   revlinktmpl=chromium_rev)

c['change_source'] = [trunk_poller]


####### SCHEDULERS

## configure the Schedulers

# Scheduler to trigger slaves that depend on the release build.
s_chromium_winrel_builder = Scheduler(name='chromium_winrel_builder',
                                      branch='src',
                                      treeStableTimer=60,
                                      builderNames=['Win Builder'])

s_chromium_winrel_trigger = Triggerable('winrel',
                                        ['XP Perf (1)',
                                         'XP Perf (2)',
                                         'XP Interactive Perf',
                                         'XP Perf (jank)',
                                         'Vista Perf (1)',
                                         'Vista Perf (2)',
                                         'Vista Interactive Perf',
                                         'Vista Perf (jank)',
                                        ])

# Scheduler to trigger slaves that depend on the debug build.
s_chromium_windbg_builder = Scheduler(name='chromium_windbg_builder',
                                      branch='src',
                                      treeStableTimer=60,
                                      builderNames=['Win Builder (dbg)'])

s_chromium_windbg_trigger = Triggerable('windbg',
                                        ['XP Perf (dbg)',
                                         'Vista Perf (dbg)'
                                        ])

# Scheduler to trigger slaves that depend on the mac release build.
s_chromium_macrel_builder = Scheduler(name='chromium_macrel_builder',
                                      branch='src',
                                      treeStableTimer=60,
                                      builderNames=['Mac Builder'])

s_chromium_macrel_trigger = Triggerable('macrel',
                                        ['Mac10.5 Perf(1)',
                                         'Mac10.5 Perf(2)',
                                         'Mac10.5 Perf(3)',
                                         'Mac10.6 Perf(1)',
                                         'Mac10.6 Perf(2)',
                                         'Mac10.6 Perf(3)',
                                        ])

# Scheduler to trigger slaves that depend on the linux release build.
s_chromium_linux_rel_builder = Scheduler(name='chromium_linux_rel_builder',
                                         branch='src',
                                         treeStableTimer=60,
                                         builderNames=['Linux Builder'])

s_chromium_linux_rel_trigger = Triggerable('linuxrel',
                                           ['Linux Perf (1)',
                                            'Linux Perf (2)',
                                            'Linux Perf (lowmem)',
                                           ])

c['schedulers'] = [s_chromium_winrel_builder,
                   s_chromium_winrel_trigger,
                   s_chromium_windbg_builder,
                   s_chromium_windbg_trigger,
                   s_chromium_macrel_builder,
                   s_chromium_macrel_trigger,
                   s_chromium_linux_rel_builder,
                   s_chromium_linux_rel_trigger]

####### BUILDERS

# buildbot/process/factory.py provides several BuildFactory classes you can
# start with, which implement build processes for common targets (GNU
# autoconf projects, CPAN perl modules, etc). The factory.BuildFactory is the
# base class, and is configured with a series of BuildSteps. When the build
# is run, the appropriate buildslave is told to execute each Step in turn.

# the first BuildStep is typically responsible for obtaining a copy of the
# sources. There are source-obtaining Steps in buildbot/process/step.py for
# CVS, SVN, and others.

builders = []

# ----------------------------------------------------------------------------
# FACTORIES

m_win = chromium_factory.ChromiumFactory('src/build', 'win32')
m_linux = chromium_factory.ChromiumFactory('src/build', 'linux2')
m_mac = chromium_factory.ChromiumFactory('src/build', 'darwin')

# Some shortcut to simplify the code below.
F_WIN = m_win.ChromiumFactory
F_LINUX = m_linux.ChromiumFactory
F_MAC = m_mac.ChromiumFactory

chromium_rel_archive = 'http://master.chromium.org:8800/b/build/slave' \
                       '/chromium-rel-builder/chrome_staging/' \
                       'full-build-win32.zip'
chromium_dbg_archive = master_config.GetArchiveUrl('ChromiumPerf',
    'Win Builder (dbg)',
    'chromium-dbg-builder',
    'win32')
chromium_rel_mac_archive = master_config.GetArchiveUrl('ChromiumPerf',
    'Mac Builder',
    'chromium-rel-mac-builder',
    'mac')
chromium_rel_linux_archive = master_config.GetArchiveUrl('ChromiumPerf',
    'Linux Builder',
    'chromium-rel-linux',
    'linux')

# The identifier of the factory is the build configuration. If two factories
# are using the same build configuration, they should have the same identifier.

# BuilderTesters using a custom build configuration.
f_cr_rel_builder = F_WIN(slave_type='Builder',
                         project='all.sln;chromium_builder_perf',
                         factory_properties={'trigger': 'winrel'})

f_cr_dbg_builder = F_WIN(target='Debug', slave_type='Builder',
                         project='all.sln;chromium_builder_perf',
                         factory_properties={
                             'gclient_env': {'GYP_DEFINES': 'fastbuild=1'},
                             'trigger': 'windbg'})

f_cr_dbg_perf =  F_WIN(target='Debug', slave_type='Tester',
                       build_url=chromium_dbg_archive,
                       tests=['plugin', 'page_cycler_morejs',
                              'page_cycler_database', 'page_cycler_indexeddb',
                              'startup'],
                       factory_properties={'process_dumps': True,
                                           'halt_on_missing_build': True,
                                           'start_crash_handler': True,})

f_cr_dbg_perf_vista = F_WIN(target='Debug', slave_type='Tester',
                            build_url=chromium_dbg_archive,
                            tests=['page_cycler_moz', 'page_cycler_morejs',
                                   'page_cycler_database',
                                   'page_cycler_indexeddb', 'startup'],
                            factory_properties={'process_dumps': True,
                                                'halt_on_missing_build': True,
                                                'start_crash_handler': True,})

f_cr_rel_perf_xp_1 = F_WIN(slave_type='Tester',
                           build_url=chromium_rel_archive,
                           tests=['page_cycler_moz', 'page_cycler_morejs',
                                  'page_cycler_intl1', 'page_cycler_intl2', 
                                  'page_cycler_dhtml', 'page_cycler_database',
                                  'page_cycler_indexeddb', 'dom_perf',
                                  'page_cycler_moz-http',
                                  'page_cycler_bloat-http'],
                           factory_properties={
                               'show_perf_results': True,
                               'expectations': True,
                               'halt_on_missing_build': True,
                               'perf_id': 'chromium-rel-xp-dual',
                               'process_dumps': True,
                               'start_crash_handler': True,})

f_cr_rel_perf_xp_2 = F_WIN(slave_type='Tester',
                           build_url=chromium_rel_archive,
                           tests=['dromaeo', 'plugin', 'memory'],
                           factory_properties={
                               'show_perf_results': True,
                               'expectations': True,
                               'halt_on_missing_build': True,
                               'perf_id': 'chromium-rel-xp-dual',
                               'process_dumps': True,
                               'start_crash_handler': True,})

f_cr_rel_perf_xp_int = F_WIN(slave_type='Tester',
                             build_url=chromium_rel_archive,
                             tests=['sunspider', 'v8_benchmark', 'startup',
                                    'tab_switching', 'frame_rate'],
                             factory_properties={
                                 'show_perf_results': True,
                                 'expectations': True,
                                 'halt_on_missing_build': True,
                                 'perf_id': 'chromium-rel-xp-dual',
                                 'process_dumps': True,
                                 'start_crash_handler': True,})

f_cr_rel_perf_xp_jank = F_WIN(slave_type='Tester',
                              build_url=chromium_rel_archive,
                              # startup tests should be re-enabled after they
                              # no longer crash this system.
                              tests=['page_cycler_moz', 'page_cycler_morejs',
                                     'page_cycler_intl1', 'page_cycler_intl2', 
                                     'page_cycler_dhtml', 
                                     'page_cycler_database', 
                                     'page_cycler_indexeddb',
                                     'page_cycler_moz-http',
                                     'page_cycler_bloat-http',
                                     'sunspider', 'v8_benchmark', 'dom_perf',
                                     'dromaeo'],
                              factory_properties={
                                'show_perf_results': True,
                                'expectations': True,
                                'halt_on_missing_build': True,
                                'perf_id': 'chromium-rel-xp-single',
                                'process_dumps': True,
                                'start_crash_handler': True,})

# Why don't we run 'page_cycler_*-http' on vista?
f_cr_rel_perf_vista_1 = F_WIN(slave_type='Tester',
                              build_url=chromium_rel_archive,
                              tests=['page_cycler_moz', 'page_cycler_morejs',
                                     'page_cycler_intl1', 'page_cycler_intl2', 
                                     'page_cycler_dhtml', 
                                     'page_cycler_database', 
                                     'page_cycler_indexeddb', 'dom_perf'],
                              factory_properties={
                                  'show_perf_results': True,
                                  'expectations': True,
                                  'halt_on_missing_build': True,
                                  'perf_id': 'chromium-rel-vista-dual',
                                  'process_dumps': True,
                                  'start_crash_handler': True,})

f_cr_rel_perf_vista_2 = F_WIN(slave_type='Tester',
                              build_url=chromium_rel_archive,
                              tests=['dromaeo', 'plugin', 'memory', 'sync'],
                              factory_properties={
                                  'show_perf_results': True,
                                  'expectations': True,
                                  'halt_on_missing_build': True,
                                  'perf_id': 'chromium-rel-vista-dual',
                                  'process_dumps': True,
                                  'start_crash_handler': True,})

f_cr_rel_perf_vista_int = F_WIN(slave_type='Tester',
                                build_url=chromium_rel_archive,
                                tests=['memory', 'sunspider', 'v8_benchmark',
                                       'startup', 'tab_switching',
                                       'frame_rate'],
                                factory_properties={
                                    'show_perf_results': True,
                                    'expectations': True,
                                    'halt_on_missing_build': True,
                                    'perf_id': 'chromium-rel-vista-dual',
                                    'process_dumps': True,
                                    'start_crash_handler': True,})

# TODO: run 'page_cycler_*-http' on vista.
f_cr_rel_perf_vista_jank = F_WIN(slave_type='Tester',
                                 build_url=chromium_rel_archive,
                                 # dom_perf tests should be re-enabled after
                                 # they no longer crash this system.
                                 tests=['page_cycler_moz', 'page_cycler_morejs',
                                        'page_cycler_intl1', 
                                        'page_cycler_intl2', 
                                        'page_cycler_dhtml', 
                                        'page_cycler_database', 
                                        'page_cycler_indexeddb', 
                                        'startup', 'sunspider', 'v8_benchmark',
                                        'dromaeo'],
                                 factory_properties={
                                   'show_perf_results': True,
                                   'expectations': True,
                                   'halt_on_missing_build': True,
                                   'perf_id': 'chromium-rel-vista-single',
                                   'process_dumps': True,
                                   'start_crash_handler': True,})

f_cr_rel_linux_builder = F_LINUX(target='Release',
                                 slave_type='Builder',
                                 options=['--', 'chromium_builder_perf'],
                                 factory_properties={
                                   'trigger': 'linuxrel'})

f_cr_rel_linux_1 = F_LINUX(slave_type='Tester',
                                 build_url=chromium_rel_linux_archive,
                                 tests=['page_cycler_moz', 
                                        'page_cycler_morejs',
                                        'page_cycler_intl1', 
                                        'page_cycler_intl2', 
                                        'page_cycler_dhtml', 
                                        'page_cycler_database', 
                                        'page_cycler_indexeddb', 'startup',
                                        'page_cycler_moz-http',
                                        'page_cycler_bloat-http'],
                                 factory_properties={
                                     'show_perf_results': True,
                                     'expectations': True,
                                     'halt_on_missing_build': True,
                                     'perf_id': 'linux-release'})

f_cr_rel_linux_2 = F_LINUX(slave_type='Tester',
                                 build_url=chromium_rel_linux_archive,
                                 tests=['dom_perf', 'memory', 'sunspider',
                                        'v8_benchmark', 'dromaeo',
                                        'frame_rate', 'dom_checker', 'sync'],
                                 factory_properties={
                                     'show_perf_results': True,
                                     'expectations': True,
                                     'halt_on_missing_build': True,
                                     'perf_id': 'linux-release'})

f_cr_rel_linux_lowmem = F_LINUX(
    slave_type='Tester',
    build_url=chromium_rel_linux_archive,
    tests=['page_cycler_moz', 'page_cycler_morejs', 'page_cycler_intl1',
           'page_cycler_intl2', 'page_cycler_dhtml', 'page_cycler_database',
           'page_cycler_indexeddb', 'startup', 'dom_perf', 'tab_switching',
           'memory', 'sunspider', 'v8_benchmark', 'page_cycler_moz-http',
           'page_cycler_bloat-http', 'frame_rate', 'sync'],
    factory_properties={
        'show_perf_results': True,
        'expectations': True,
        'halt_on_missing_build': True,
        'perf_id': 'linux-release-lowmem'})

# BuilderTesters using a custom build configuration.

f_cr_rel_mac_builder = F_MAC(
    slave_type='Builder',
    options=['--compiler=goma-clang', '--', '-target', 'chromium_builder_perf'],
    factory_properties={
        'gclient_env': {'GYP_DEFINES': 'use_skia=1'},
        'trigger': 'macrel',
    })

# Helper since the 10.5 vs 10.6 differences are just for ids.
def RelMacPerfTester(perf_id, tests):
  return F_MAC(slave_type='Tester',
               build_url=chromium_rel_mac_archive,
               tests=tests,
               factory_properties={'show_perf_results': True,
                                   'expectations': True,
                                   'halt_on_missing_build': True,
                                   'perf_id': perf_id})

# TODO: XP/Vista Perf also runs: 'reliability', 'dom_checker'
# Tests are split across machines to get all the perf data as quickly as
# possible.
f_cr_rel_mac5_perf_1 = RelMacPerfTester('chromium-rel-mac5',
                                        ['page_cycler_moz',
                                         'page_cycler_morejs',
                                         'page_cycler_intl1', 
                                         'page_cycler_intl2', 
                                         'page_cycler_dhtml', 
                                         'page_cycler_database', 
                                         'page_cycler_indexeddb', 
                                         'page_cycler_moz-http',
                                         'page_cycler_bloat-http'])

f_cr_rel_mac5_perf_2 = RelMacPerfTester('chromium-rel-mac5',
                                        ['startup', 'tab_switching', 'memory',
                                         'v8_benchmark', 'sunspider',
                                         'dom_perf', 'dom_checker'])

f_cr_rel_mac5_perf_3 = RelMacPerfTester('chromium-rel-mac5',
                                        ['dromaeo', 'plugin', 'frame_rate',
                                         'mach_ports'])

# Same setup as 10.5, just using 10.6 naming.
f_cr_rel_mac6_perf_1 = RelMacPerfTester('chromium-rel-mac6',
                                        ['page_cycler_moz', 
                                         'page_cycler_morejs',
                                         'page_cycler_intl1', 
                                         'page_cycler_intl2', 
                                         'page_cycler_dhtml', 
                                         'page_cycler_database', 
                                         'page_cycler_indexeddb', 
                                         'page_cycler_moz-http',
                                         'page_cycler_bloat-http'])

f_cr_rel_mac6_perf_2 = RelMacPerfTester('chromium-rel-mac6',
                                        ['startup', 'tab_switching', 'memory',
                                         'v8_benchmark', 'sunspider',
                                         'dom_perf', 'dom_checker'])

f_cr_rel_mac6_perf_3 = RelMacPerfTester('chromium-rel-mac6',
                                        ['dromaeo', 'plugin', 'frame_rate',
                                         'sync', 'mach_ports'])

# ----------------------------------------------------------------------------
# BUILDER DEFINITIONS

# The 'builders' list defines the Builders. Each one is configured with a
# dictionary, using the following keys:
#  name (required): the name used to describe this bilder
#  builddir (required): which subdirectory to run the builder in
#  factory (required): a BuildFactory to define how the build is run
#  periodicBuildTime (optional): if set, force a build every N seconds
#  category (optional): it is not used in the normal 'buildbot' meaning. It is
#                       used by gatekeeper to determine which steps it should
#                       look for to close the tree.
#

b_chromium_dbg_win_builder = {'name': 'Win Builder (dbg)',
  'builddir': 'chromium-dbg-builder',
  'factory': f_cr_dbg_builder,
  'category': '2windows debug|builders_compile|builders_windows',
}

b_chromium_rel_win_builder = {'name': 'Win Builder',
  'builddir': 'chromium-rel-builder',
  'factory': f_cr_rel_builder,
  'category': '1windows release|builders_compile|builders_windows',
}

b_chromium_dbg_xp_perf = {'name': 'XP Perf (dbg)',
  'builddir': 'chromium-dbg-xp-perf',
  'factory': f_cr_dbg_perf,
  'category': '2windows debug|builder_testers|builders_windows',
  # http://crbug.com/72101  Work around hanging chrome.exe.
  'auto_reboot': True,
}

b_chromium_dbg_vista_perf = {'name': 'Vista Perf (dbg)',
  'builddir': 'chromium-dbg-vista-perf',
  'factory': f_cr_dbg_perf_vista,
  'category': '2windows debug|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_xp_perf_1 = {'name': 'XP Perf (1)',
  'builddir': 'chromium-rel-xp-perf-1',
  'factory': f_cr_rel_perf_xp_1,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_xp_perf_2 = {'name': 'XP Perf (2)',
  'builddir': 'chromium-rel-xp-perf-2',
  'factory': f_cr_rel_perf_xp_2,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_xp_interactive_perf = {'name': 'XP Interactive Perf',
  'builddir': 'chromium-rel-xp-int-perf',
  'factory': f_cr_rel_perf_xp_int,
  'category': '1windows release|builder_testers|builders_windows',
  # Work around http://crbug.com/69940 on this bot by enabling auto_reboot.
  'auto_reboot': True,
}

b_chromium_rel_xp_perf_jank = {'name': 'XP Perf (jank)',
  'builddir': 'chromium-rel-xp-perf-single',
  'factory': f_cr_rel_perf_xp_jank,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_vista_perf_1 = {'name': 'Vista Perf (1)',
  'builddir': 'chromium-rel-vista-perf-1',
  'factory': f_cr_rel_perf_vista_1,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_vista_perf_2 = {'name': 'Vista Perf (2)',
  'builddir': 'chromium-rel-vista-perf-2',
  'factory': f_cr_rel_perf_vista_2,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_vista_perf_int = {'name': 'Vista Interactive Perf',
  'builddir': 'chromium-rel-vista-perf-int',
  'factory': f_cr_rel_perf_vista_int,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_vista_perf_jank = {'name': 'Vista Perf (jank)',
  'builddir': 'chromium-rel-vista-perf-single',
  'factory': f_cr_rel_perf_vista_jank,
  'category': '1windows release|builder_testers|builders_windows',
  'auto_reboot': True,
}

b_chromium_rel_linux_builder = {'name': 'Linux Builder',
  'builddir': 'chromium-rel-linux',
  'factory': f_cr_rel_linux_builder,
  'category': '4linux|builders_compile|builder_testers',
}

b_chromium_rel_linux_1 = {'name': 'Linux Perf (1)',
  'builddir': 'chromium-rel-linux-1',
  'factory': f_cr_rel_linux_1,
  'category': '4linux|builders_compile|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_linux_2 = {'name': 'Linux Perf (2)',
  'builddir': 'chromium-rel-linux-2',
  'factory': f_cr_rel_linux_2,
  'category': '4linux|builders_compile|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_linux_lowmem = {'name': 'Linux Perf (lowmem)',
  'builddir': 'chromium-rel-linux-lowmem',
  'factory': f_cr_rel_linux_lowmem,
  'category': '4linux|builders_compile|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_mac_builder = {'name': 'Mac Builder',
  'builddir': 'chromium-rel-mac-builder',
  'factory': f_cr_rel_mac_builder,
  'category': '3mac|builders_compile',
}

b_chromium_rel_mac5_perf_1 = {'name': 'Mac10.5 Perf(1)',
  'builddir': 'chromium-rel-mac5-perf',
  'factory': f_cr_rel_mac5_perf_1,
  'category': '3mac|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_mac5_perf_2 = {'name': 'Mac10.5 Perf(2)',
  'builddir': 'chromium-rel-mac5-perf-2',
  'factory': f_cr_rel_mac5_perf_2,
  'category': '3mac|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_mac5_perf_3 = {'name': 'Mac10.5 Perf(3)',
  'builddir': 'chromium-rel-mac5-perf-3',
  'factory': f_cr_rel_mac5_perf_3,
  'category': '3mac|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_mac6_perf_1 = {'name': 'Mac10.6 Perf(1)',
  'builddir': 'chromium-rel-mac6-perf',
  'factory': f_cr_rel_mac6_perf_1,
  'category': '3mac|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_mac6_perf_2 = {'name': 'Mac10.6 Perf(2)',
  'builddir': 'chromium-rel-mac6-perf-2',
  'factory': f_cr_rel_mac6_perf_2,
  'category': '3mac|builder_testers',
  'auto_reboot': True,
}

b_chromium_rel_mac6_perf_3 = {'name': 'Mac10.6 Perf(3)',
  'builddir': 'chromium-rel-mac6-perf-3',
  'factory': f_cr_rel_mac6_perf_3,
  'category': '3mac|builder_testers',
  'auto_reboot': True,
}

c['builders'] = [
  b_chromium_rel_win_builder,
  b_chromium_rel_xp_perf_1,
  b_chromium_rel_xp_perf_2,
  b_chromium_rel_xp_interactive_perf,
  b_chromium_rel_xp_perf_jank,
  b_chromium_rel_vista_perf_1,
  b_chromium_rel_vista_perf_2,
  b_chromium_rel_vista_perf_int,
  b_chromium_rel_vista_perf_jank,
  b_chromium_dbg_win_builder,
  b_chromium_dbg_xp_perf,
  b_chromium_dbg_vista_perf,
  b_chromium_rel_mac_builder,
  b_chromium_rel_mac5_perf_1,
  b_chromium_rel_mac5_perf_2,
  b_chromium_rel_mac5_perf_3,
  b_chromium_rel_mac6_perf_1,
  b_chromium_rel_mac6_perf_2,
  b_chromium_rel_mac6_perf_3,
  b_chromium_rel_linux_builder,
  b_chromium_rel_linux_1,
  b_chromium_rel_linux_2,
  b_chromium_rel_linux_lowmem,
]


####### BUILDSLAVES

# Associate the slaves to the manual builders. The configuration is in
# slaves.cfg.
for builder in c['builders']:
  builder['slavenames'] = slaves.GetSlavesName(builder=builder['name'])

# The 'slaves' list defines the set of allowable buildslaves. List all the
# slaves registered to a builder. Remove dupes.
c['slaves'] = master_utils.AutoSetupSlaves(c['builders'],
                                           config.Master.GetBotPassword())
master_utils.VerifySetup(c, slaves)


####### STATUS TARGETS

# Adds common status and tools to this master.
master_utils.AutoSetupMaster(c, ActiveMaster,
    public_html='../master.chromium/public_html',
    templates=['../master.chromium/templates'],
    enable_http_status_push=ActiveMaster.is_production_host)

# Add more.

if MAIL_NOTIFIER:
  categories_steps = {
    '': ['update'],
    'builder_testers': [
      # Perf tests
      'dom_perf',
      'memory_test',
      'new_tab_ui_cold_test',
      'new_tab_ui_warm_test',
      'page_cycler_bloat-http',
      'page_cycler_dhtml',
      'page_cycler_intl1',
      'page_cycler_intl2',
      'page_cycler_morejs',
      'page_cycler_moz',
      'page_cycler_moz-http',
      'startup_test',
      'tab_switching_test',
      'frame_rate_test',
      'sync',
     ],
    'builders_windows': [],
    'builders_compile': []
  }
  exclusions = { }
  forgiving_steps = [ ]
  c['status'].append(chromium_notifier.ChromiumNotifier(
      fromaddr=ActiveMaster.from_address,
      categories_steps=categories_steps,
      exclusions=exclusions,
      relayhost=config.Master.smtp,
      status_header='Perf alert on "%(builder)s":\n%(steps)s',
      subject='%(projectName)s %(builder)s %(result)s',
      extraRecipients=['cmp@google.com'],
      lookup=master_utils.FilterDomain(),
      sheriffs=['sheriff_perf'],
      public_html='../master.chromium/public_html',
      forgiving_steps=forgiving_steps,
      use_getname=True))

####### PROJECT IDENTITY

# the 'projectName' string will be used to describe the project that this
# buildbot is working on. For example, it is used as the title of the
# waterfall HTML page. The 'projectURL' string will be used to provide a link
# from buildbot HTML pages to your project's home page.

c['projectName'] = ActiveMaster.project_name
c['projectURL'] = config.Master.project_url

# the 'buildbotURL' string should point to the location where the buildbot's
# internal web server (usually the html.Waterfall page) is visible. This
# typically uses the port number set in the Waterfall 'status' entry, but
# with an externally-visible host name which the buildbot cannot figure out
# without some help.

c['buildbotURL'] = 'http://build.chromium.org/p/chromium.perf/'
