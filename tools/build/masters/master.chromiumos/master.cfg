# -*- python -*-
# ex: set syntax=python:

# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# This is the buildmaster config file for the 'chromeos' bot. It must
# be installed as 'master.cfg' in your buildmaster's base directory
# (although the filename can be changed with the --basedir option to
# 'mktap buildbot master').

# It has one job: define a dictionary named BuildmasterConfig. This
# dictionary has a variety of keys to control different aspects of the
# buildmaster. They are documented in docs/config.xhtml .

from buildbot.changes import svnpoller
from buildbot.changes.pb import PBChangeSource
from buildbot.scheduler import Dependent
from buildbot.scheduler import Periodic
from buildbot.scheduler import Scheduler
from buildbot.scheduler import Triggerable

# These modules come from scripts/master, which must be in the PYTHONPATH.
from master import build_utils
from master import master_utils
from master import slaves_list
from master.factory import chromeos_factory

# These modules come from scripts/common, which must be in the PYTHONPATH.
import config

ActiveMaster = config.Master.ChromiumOS

TREE_GATE_KEEPER = ActiveMaster.is_production_host

# This is the dictionary that the buildmaster pays attention to. We also use
# a shorter alias to save typing.
c = BuildmasterConfig = {}

# This is a dictionary that maps waterfall dashboard names to cbuildbot_configs.
# In order to add a new dict, you must edit this first.
_NAME_CONFIG_DICT = {
  'x86 generic incremental': 'x86-generic-incremental',
  'tegra2 incremental': 'arm-tegra2-incremental',
  'amd64 generic incremental': 'amd64-generic-incremental',
  'x86 generic paladin': 'x86-generic-paladin',
  'tegra2 paladin': 'arm-tegra2-paladin',
  'amd64 generic paladin': 'amd64-generic-paladin',
  'x86 generic full': 'x86-generic-full',
  'arm generic full': 'arm-generic-full',
  'tegra2 full': 'arm-tegra2-full',
  'tegra2 seaboard full': 'arm-tegra2-seaboard-full',
  'amd64 generic full': 'amd64-generic-full',
  'x86 generic nightly chrome PFQ': 'x86-generic-chrome-pre-flight-queue',
  'tegra2 nightly chrome PFQ': 'arm-tegra2-chrome-pre-flight-queue',
  'amd64 generic nightly chrome PFQ': 'amd64-generic-chrome-pre-flight-queue',
  'chromiumos sdk': 'chromiumos-sdk',
  'refresh packages': 'refresh-packages',
  'x86 generic ASAN': 'x86-generic-asan',
}


config.DatabaseSetup(c, require_dbconfig=ActiveMaster.is_production_host)

####### CHANGESOURCES
def ChromeTreeFileSplitter(path):
  """split_file for chrome tree changes; return None for uninteresting paths."""
  # List of projects we are interested in. The project names must exactly
  # match paths in the Subversion repository, relative to the 'path' URL
  # argument. build_utils.SplitPath() will use them as branch names to
  # kick off the Schedulers for different projects.
  projects = ['releases']
  return build_utils.SplitPath(projects, path)


chromium_rev = 'http://src.chromium.org/viewvc/chrome?view=rev&revision=%s'

chrome_trunk_poller = svnpoller.SVNPoller(
    svnurl='svn://svn.chromium.org/chrome/',
    split_file=ChromeTreeFileSplitter,
    pollinterval=300,
    revlinktmpl=chromium_rev)

pb_source = PBChangeSource()

c['change_source'] = [pb_source, chrome_trunk_poller]

####### SCHEDULERS

## configure the Schedulers
# XXX: Changes to builderNames must also be made in:# - slaves.cfg
# - templates/announce.html
# - And down below in the builder definitions as well
# - and you probably need to restart any changed slaves as well as the master
s_paladin = Periodic(
  name='paladin',
  branch='master',
  periodicBuildTimer=5 * 60, # 5 minutes
  builderNames=[
    'x86 generic paladin',
  ])

# Triggerable for paladin slaves.
s_paladin_slaves = Triggerable(
  name='paladin_slaves',
  builderNames=[
     'tegra2 paladin',
     'amd64 generic paladin',
  ])

# Pre-flight scheduler for deps changes of Chromium for Chromium OS.
s_chrome_pre_flight_queue = Scheduler(
  name='chrome_pre_flight_queue',
  branch='releases',
  treeStableTimer=0,
  builderNames=[
     'x86 generic nightly chrome PFQ',
  ])

# Triggerable for chrome-pre-flight-queue-slaves.
s_chrome_pre_flight_queue_slaves = Triggerable(
  name='chrome_pre_flight_queue_slaves',
  builderNames=[
     'tegra2 nightly chrome PFQ',
     'amd64 generic nightly chrome PFQ',
  ])

s_refresh_packages = Periodic(
  name='refresh_pkgs_scheduler',
  branch='master',
  periodicBuildTimer=24 * 60 * 60, # 1 day
  builderNames=[
    'refresh packages',
  ])

# Default scheduler triggers when we see changes.
s_chromeos_default = Scheduler(
  name='chromeos',
  branch='master',
  treeStableTimer=0,
  builderNames=[
     'x86 generic incremental',
     'tegra2 incremental',
     'amd64 generic incremental',
     'x86 generic full',
     'arm generic full',
     'tegra2 full',
     'tegra2 seaboard full',
     'amd64 generic full',
     'chromiumos sdk',
     'x86 generic ASAN',
  ])

c['schedulers'] = [
    s_paladin, s_paladin_slaves,
    s_chrome_pre_flight_queue, s_chrome_pre_flight_queue_slaves,
    s_chromeos_default, s_refresh_packages,
]

BUILDERS = []

# ----------------------------------------------------------------------------
# BUILDER DEFINITIONS

# The 'builders' list defines the Builders. Each one is configured with a
# dictionary, using the following keys:
#  name (required): the name used to describe this bilder
#  slavename (required): which slave to use, must appear in c['slaves']
#  builddir (required): which subdirectory to run the builder in
#  factory (required): a BuildFactory to define how the build is run
#  category (optional): it is not used in the normal 'buildbot' meaning. It is
#                       used by gatekeeper to determine which steps it should
#                       look for to close the tree.
#

# Cros helper functions to build builders and factories.

def GetCBuildbotFactory(config, trigger_name=None):
  """Returns cros buildbot factories."""
  return chromeos_factory.CbuildbotFactory(
      params=config, dry_run=not ActiveMaster.is_production_host,
      crostools_repo=None,
      trigger_name=trigger_name).get_factory()

def AddBuilderDefinition(display_name, closer=True, trigger_name=None,
                         auto_reboot=True):
  """Adds a builder definition given by the args.

  Args:
    display_name: Name displayed on buildbot waterfall.
    closer:  Do we close the tree based on this build's failure.
    trigger_name:  Name of the triggerable scheduler to fire for pfqs/commitq's.
    auto_reboot: Whether to reboot the bot after each run.
  """
  def GetConfig():
    return _NAME_CONFIG_DICT.get(display_name, display_name)

  category = '1release full|info'
  if closer: category = '1release full|closer'
  build_dir = display_name.replace(' ', '-')
  factory = GetCBuildbotFactory(GetConfig(), trigger_name)

  BUILDERS.append({
    'name': display_name,
    'builddir': build_dir,
    'factory': factory,
    'category': category,
    'auto_reboot': auto_reboot,
  })

# Paladin Builders -- exception to closer rule below as they are very important
# to watch.
AddBuilderDefinition('x86 generic paladin', trigger_name='paladin_slaves',
                     closer=False, auto_reboot=False)
AddBuilderDefinition('tegra2 paladin', closer=False, auto_reboot=False)
AddBuilderDefinition('amd64 generic paladin', closer=False, auto_reboot=False)


AddBuilderDefinition('x86 generic incremental', auto_reboot=False)
AddBuilderDefinition('tegra2 incremental', auto_reboot=False)
AddBuilderDefinition('amd64 generic incremental', auto_reboot=False)

# Full Builders
AddBuilderDefinition('x86 generic full')
AddBuilderDefinition('arm generic full')
AddBuilderDefinition('tegra2 full')
AddBuilderDefinition('tegra2 seaboard full')
AddBuilderDefinition('amd64 generic full')

AddBuilderDefinition('chromiumos sdk')

####### Non Closer build defs.

# Chrome Nightly builders.
AddBuilderDefinition('x86 generic nightly chrome PFQ', closer=False,
                     trigger_name='chrome_pre_flight_queue_slaves')
AddBuilderDefinition('tegra2 nightly chrome PFQ', closer=False)
AddBuilderDefinition('amd64 generic nightly chrome PFQ', closer=False)

# Miscellaneous builders.
AddBuilderDefinition('refresh packages', closer=False)
AddBuilderDefinition('x86 generic ASAN', closer=False)


c['builders'] = BUILDERS


####### BUILDSLAVES

# the 'slaves' list defines the set of allowable buildslaves. Each element is a
# tuple of bot-name and bot-password. These correspond to values given to the
# buildslave's mktap invocation.

# First, load the list from slaves.cfg.
slaves = slaves_list.SlavesList('slaves.cfg', 'ChromiumOS')

# Associate the slaves to the builders.
for builder in c['builders']:
  builder['slavenames'] = slaves.GetSlavesName(builder=builder['name'])
  # Don't enable auto_reboot for people testing locally.
  builder.setdefault('auto_reboot', ActiveMaster.is_production_host)

# The 'slaves' list defines the set of allowable buildslaves. List all the
# slaves registered to a builder. Remove dupes.
c['slaves'] = master_utils.AutoSetupSlaves(c['builders'],
                                           config.Master.GetBotPassword())


####### STATUS TARGETS

# Adds common status and tools to this master.
master_utils.AutoSetupMaster(c, ActiveMaster,
    templates=['./templates', '../master.chromium/templates'],
    order_console_by_time=True)

# Add more.

if TREE_GATE_KEEPER:
  from master import gatekeeper
  # This is the list of the builder categories and the corresponding critical
  # steps. If one critical step fails, gatekeeper will close the tree
  # automatically.
  # Note: don't include 'update scripts' since we can't do much about it when
  # it's failing and the tree is still technically fine.
  categories_steps = {
      'closer': [
          'Clear and Clone chromite',
          'Clear and Clone crostools',
          'cbuildbot',
          'cbuildbot_chrome_latest_release',
          'cbuildbot_master',
          'master',
          'gitrevisiondropper',
      ],
      'info': [
      ],
      '': [
      ],
  }
  exclusions = {}
  # TODO: convert GateKeeper to 'use_getname=True'
  # 1) set names for all steps in categories_steps
  # 2) update categories_steps to use names insteads of description
  # 3) change forgiving_steps to:
  #     forgiving_steps = ['update_scripts', 'update', 'archive_build']
  # 4) add 'use_getname=True' to the GateKeeper call
  forgiving_steps = ['update scripts', 'update', 'archived build']
  c['status'].append(gatekeeper.GateKeeper(
      fromaddr=ActiveMaster.from_address,
      categories_steps=categories_steps,
      exclusions=exclusions,
      relayhost=config.Master.smtp,
      subject='buildbot %(result)s in %(projectName)s on %(builder)s, '
              'revision %(revision)s',
      extraRecipients=ActiveMaster.tree_closing_notification_recipients,
      lookup=master_utils.FilterDomain(),
      forgiving_steps=forgiving_steps,
      tree_status_url=ActiveMaster.tree_status_url,
      check_revisions=False))


####### TROOPER NAGGING
if ActiveMaster.is_production_host:
  from master import chromium_notifier
  categories_steps = {
      'closer': [
          'CleanUp', 'LKGMCandidateSync', 'Sync', 'Uprev', 'UploadPrebuilts',
          'Archive', 'PublishUprevChanges'
      ]
  }

  c['status'].append(chromium_notifier.ChromiumNotifier(
      fromaddr=ActiveMaster.from_address,
      categories_steps=categories_steps,
      relayhost=config.Master.smtp,
      status_header='%(steps)s failed on "%(builder)s"',
      subject='buildbot trooper alert on %(builder)s (%(projectName)s)',
      extraRecipients=['troopers@chromium.org'],
      sheriffs=['sheriff'],
      lookup=master_utils.FilterDomain(),
      use_getname=True))


####### PROJECT IDENTITY

# Buildbot master url:
c['buildbotURL'] = 'http://build.chromium.org/buildbot/chromiumos/'
