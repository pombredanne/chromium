# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Find json at all cost."""

import sys
import os

# pylint: disable=F0401,W0611

try:
  import json
except ImportError:
  try:
    import simplejson as json
  except ImportError:
    import find_depot_tools
    sys.path.append(
        os.path.join(find_depot_tools.add_depot_tools_to_path(), 'third_party'))
    import simplejson as json
