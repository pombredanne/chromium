#!/usr/bin/python
# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Pre-commit hook to add OWNERS access controls to an svn repository.

Place this file in the hooks directory (renamed to pre-commit).
It will restrict commits to persons named in the hierarchy of OWNERS files.
Lines with preceded by # are treated as comments.
set noparent prevents further recursion up the hierarchy.
"""

import os
import subprocess
import sys


def RunCommand(cmd):
  """Run a command, returning stdout on retcode 0, '' otherwise."""
  # Eat stderr,
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  p_stdout = p.communicate()[0]
  if p.returncode == 0:
    return p_stdout
  else:
    return ''


def GetTransactionAuthor(repository_path, transaction_id):
  """Determine the author of a transaction."""
  out = RunCommand(['/usr/bin/svnlook', 'author', repository_path,
                    '--transaction', transaction_id])
  if out:
    return out.split('\n')[0]
  else:
    return None


def GetTransactionFiles(repository_path, transaction_id):
  """Get the list of files altered by a transaction."""
  out = RunCommand(['/usr/bin/svnlook', 'changed', repository_path,
                    '--transaction', transaction_id])
  # Strip off 4 leading character from each to get just the path:
  # M   trunk/src/foo
  return [f[4:] for f in out.splitlines()]


def ParseOwners(owners_file_content):
  """Go from OWNERS to (list of owners, noparent?)."""
  owners = []
  no_parent = False
  for line in owners_file_content.splitlines():
    bare_line = line.strip()
    # Skip comments
    if bare_line.startswith('#'):
      continue
    if bare_line == 'set noparent':
      no_parent = True
    elif bare_line:
      owners.append(bare_line)
  return (owners, no_parent)


def ReadOwners(repository_path, subdir):
  """Read in OWNERS from subdir to get (list of owners, noparent?)."""
  return ParseOwners(RunCommand(
      ['/usr/bin/svn', 'cat', 'file://' +
       repository_path + '/' + subdir + '/OWNERS']))


def IsAccessAllowed(repository_path, path, who):
  """Check if a given user can modify a particular path in this repository."""
  # Check each layer of OWNERS, until we hit noparent or the top.
  while path != os.path.dirname(path):
    path = os.path.dirname(path)
    owners, no_parent = ReadOwners(repository_path, path)
    if who in owners:
      return True
    if no_parent:
      return False
  # If we hit the top without finding an OWNERS file, prevent access.
  # Top level OWNERS files should be added out of band with hooks disabled.
  return False


def main(argv):
  repository_path = argv[1]
  transaction_id = argv[2]
  author = GetTransactionAuthor(repository_path, transaction_id)
  files = GetTransactionFiles(repository_path, transaction_id)
  for f in files:
    if not IsAccessAllowed(repository_path, f, author):
      sys.stderr.write(
          'ERROR - OWNERS disallows %s changing %s\n' % (author, f))
      return 1
  return 0


if __name__ == '__main__':
  sys.exit(main(sys.argv))
