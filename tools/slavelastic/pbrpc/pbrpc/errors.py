# Copyright (c) 2010 Marc-Antoine Ruel. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.

class PbRpcException(Exception):
  pass

class PbRpcRemoteException(PbRpcException):
  """An exception occurred server-side."""

class PbRpcIOException(PbRpcException):
  """Transport-layer issue."""

class PbRpcServiceNotFound(PbRpcException, NotImplementedError):
  def __init__(self, *args):
    PbRpcException.__init__(self, *args)
    NotImplementedError.__init__(self)
    self.service = args[0]
    self.method = args[1]

class PbRpcMethodNotFound(PbRpcException, NotImplementedError):
  def __init__(self, *args):
    PbRpcException.__init__(self, *args)
    NotImplementedError.__init__(self)
    self.service = args[0]
    self.method = args[1]

class PbRpcCallExpired(PbRpcException):
  """Hit timeout."""

class IncompleteStubImpl(PbRpcException):
  """A service is not completely implemented."""

class ConflictingMethod(PbRpcException):
  """Two methods from different services have the same name."""
