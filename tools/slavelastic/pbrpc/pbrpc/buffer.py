# coding: utf-8
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""FIFO buffer used to store data that comes in chunks"""

from __future__ import with_statement
import cStringIO
from collections import deque
import logging
import threading


class Buffer(object):
  """Buffer used for read/write buffer.  All methods are synchronized"""
  def __init__(self):
    # This will really just be a list of strings
    self._buffer = deque()
    self._length = 0
    self.lock = threading.Lock()

  def peek(self, size=None, offset=0):
    """Reads the buffer.  Behaves similarly to calling read() on a file stream,
    but does not flush the buffer.  Instead, call skip() to flush the buffer"""
    with self.lock:
      if not self._buffer:
        return ''
      result = cStringIO.StringIO()
      if size is None:
        size = self._length - offset
      assert size >= 0, 'Invalid size'
      assert offset >= 0, 'Invalid offset'
      for s in self._buffer:
        length = len(s)
        if offset > length:
          offset -= length
        elif offset > 0:
          end = size + offset
          if length <= end:
            result.write(s[offset:])
          else:
            result.write(s[offset:end])
            break
          size -= length - offset
          offset = 0
        elif size >= length:
          # Read all of the buffer into result
          result.write(s)
          size -= length
        else:
          result.write(s[:size])
          break
      result_string = result.getvalue()
      result.close()
      return result_string

  def write(self, data):
    """Writes data into buffer. Data should be a string"""
    assert data is not None, 'Data is None?!'
    assert isinstance(data, str), 'Data is not a string'
    if not data:
      # No need to add empty strings to the buffer
      return
    with self.lock:
      self._buffer.append(data)
      self._length += len(data)
      logging.debug('Num of items in buffer: %d' % len(self._buffer))
      logging.debug('Buffer length: %d' % self._length)

  def read(self, size=None):
    buf = self.peek(size)
    self.skip(size)
    return buf

  def __len__(self):
    """Returns buffer size, much faster than peeking."""
    with self.lock:
      return self._length

  def __str__(self):
    """Debugging use, so that printing this object returns a string
    representation of the internal buffer, rather than a python object"""
    with self.lock:
      return str(self._buffer)

  def skip(self, size):
    """Removes bytes from the buffer.  In place so that we can call peek() and
    skip() separately instead of just read() since there may be an incomplete
    packet in the buffer that we want to leave there."""
    with self.lock:
      if size is None or size >= self._length:  # Flush entire buffer.
        self._length = 0
        self._buffer = deque()
        return
      assert size >= 0, 'Invalid size %d' % size
      self._length -= size
      while self._buffer:
        length = len(self._buffer[0])
        if length > size:
          self._buffer[0] = self._buffer[0][size:]
          return
        self._buffer.popleft()
        size -= length
