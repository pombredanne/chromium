# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Top-level presubmit script for pbrpc.

See http://dev.chromium.org/developers/how-tos/depottools/presubmit-scripts for
details on the presubmit API built into depot_tools.
"""

UNIT_TESTS = [
  # Uncomment this as soon as the test is fixed.
  #'pbrpc_tests.rpc_test',
]

def CommonChecks(input_api, output_api):
  output = []
  black_list = list(input_api.DEFAULT_BLACK_LIST) + [r'.*_pb2\.py$']
  output.extend(input_api.canned_checks.RunPylint(
      input_api, output_api, black_list=black_list))
  output.extend(input_api.canned_checks.RunPythonUnitTests(
      input_api,
      output_api,
      UNIT_TESTS))
  return output


def CheckChangeOnUpload(input_api, output_api):
  return CommonChecks(input_api, output_api)


def CheckChangeOnCommit(input_api, output_api):
  return CommonChecks(input_api, output_api)
