#!/usr/bin/env python
# Copyright (c) 2010 Marc-Antoine Ruel

import os
import subprocess
import sys


def test(test):
  cwd = os.getcwd()
  print('%s' % test)
  os.chdir(os.path.dirname(test))
  subprocess.check_call([sys.executable, os.path.basename(test)])
  os.chdir(cwd)


os.chdir(os.path.dirname(os.path.abspath(__file__)))

test(os.path.join('pbrpc', 'pbrpc_tests', 'rpc_test.py'))
test(os.path.join('pbrpc', 'pbrpc_tests', 'stream_test.py'))
test(os.path.join('slavelastic', 'slavelastic_tests', 'client_test.py'))
test(os.path.join('slavelastic', 'slavelastic_tests', 'slave_test.py'))