#!/usr/bin/python
# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Distribute test jobs based on manifest files."""

import os
import sys
import time
import threading

ROOT_DIR = os.path.join('..', '..', os.path.abspath(__file__))
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from slavelastic.client.stream_buffer import StreamBuffer


class BaseHandler(object):
  """Should be common with all handlers.  Also manifests like 'chrome.win32'
  is defined as a base since it doesn't do anything, and should throw an
  error if you try to run it."""
  # slave is actually a Client object referring to a slave
  def __init__(self, slave, zipped_files, properties, shard_index, shard_count):
    self.slave = slave
    self.zipped_files = zipped_files
    self.properties = properties
    self.shard_index = shard_index
    self.shard_count = shard_count

  def run(self):
    raise NotImplementedError

  def get_stdout(self):
    raise NotImplementedError

  def get_stderr(self):
    raise NotImplementedError


class GtestHandler(BaseHandler):
  """Gtests, order of operations
  1. Upload all files to remote server, keep the same filepath
  2. Run whats in properties['test_name'] with
  shard_count/shard_index as the env var 'GTEST_TOTAL_SHARDS' and
  'GTEST_SHARD_INDEX'
  """
  def __init__(self, slave, slave_os_path, output_path, zipped_files, outputs,
               properties, shard_index, shard_count, timer=None,
               return_codes=None, max_uploaders=10):
    # List of (slave_files, client_path) to retrieve
    self.outputs = outputs
    # The therad running the test, will be filled when run() is called
    self.test_thread = None
    # Directory to run everything
    self.cwd = properties['cwd']
    # os.path specific to the slave platform
    self.slave_os_path = slave_os_path
    # Path + Name of the unittest.exe file to run relative to cwd
    self.test_name = properties.setdefault('test_name')
    if not self.test_name:
      raise NameError('test_name specifier not found')
    # Buffers for all stdio.
    self.stdout_buffer = StreamBuffer()
    # Directory to upload files to
    self.output_path = output_path
    self.stdin = None
    self.timer = timer or {}
    self.return_codes = return_codes or []
    super(GtestHandler, self).__init__(
        slave, zipped_files, properties, shard_index, shard_count)

  @staticmethod
  def _stream_buffer(stream, out_buffer):
    """Thread used to read data coming in from a stream into a buffer."""
    while True:
      buf = stream.recv(1024)
      if not buf:
        break
      out_buffer.write(buf)
    out_buffer.close()

  def _run_thread(self):
    """The actual thread that sends the execute command to the remote slave.
    This thread spends most of its life asleep"""
    try:
      self.pre_cleanup()
      self.uploader()
      self.slave.cd(self.cwd)
      test_name = self.properties['test_name']
      args = [test_name]
      env = {'GTEST_TOTAL_SHARDS' : self.shard_count,
             'GTEST_SHARD_INDEX' : self.shard_index}
      # Zips don't support linux file permissions
      if self.slave_os_path.__name__ == 'posixpath':
        self.slave.run(['chmod', '755', test_name])
      t1 = time.time()
      proc = self.slave.run(args, env=env)
      stdout_thread = threading.Thread(target=self._stream_buffer,
          name='stdout_buffer', args=[proc.stdout, self.stdout_buffer])
      self.stdin = proc
      stdout_thread.start()
      stdout_thread.join()
      # Waiting because apperently closing stdio doesn't always mean the
      # process is killed
      proc.wait()
      return_code = proc.poll()
      self.return_codes.append(return_code)
      self.timer['Shard %d run' % self.shard_index] = (time.time() - t1)
      # We're done, lets get them [sic] files
      # TODO(hinoka): This is written, but never tested on more than one shard
      t1 = time.time()
      # If getting files somehow crash distribute.py, comment out this line
      self.get_files()
      self.timer['Shard %d get files' % self.shard_index] = (time.time() - t1)
    except Exception:
      proc.stdin.close()
      sys.stderr.write('Caught exception while running on shard %d\n' \
                       % self.shard_index)
      raise
    finally:
      self.cleanup()

  def join(self):
    self.test_thread.join()

  def get_stdout(self):
    return self.stdout_buffer

  def get_stderr(self):
    return None

  def write(self, data):
    """Sends data to the slave's stdin"""
    assert self.stdin.is_open, 'stdin is not open'
    self.stdin.send(data)

  def get_files(self):
    """Get the files from output directory, they're written as a list of
    (remote files, local folder) tuples."""
    # TODO(hinoka): Switching cwd may not work if changing cwd in one thread
    # means the cwd will change in a different thread (this is the most
    # probable thing that can go wrong in a multi thread setup).  To fix, get
    # rid of all os.chdir() and change the self.slave.get(file) call to
    # self.slave.get(file, dest_path) where dest_path is the full local pathname
    lcwd = os.getcwd()
    rcwd = self.slave.cwd()
    for remote_file, local_path in self.outputs:
      # Resolves things like %temp% or $tmp or %userprofile%
      # Does not resolve home directories (~)
      expanded = os.path.expandvars(local_path)
      expanded = os.path.join(expanded, 'Shard_%2d' % self.shard_index)
      if not os.path.isdir(expanded):
        os.makedirs(expanded)
      os.chdir(expanded)
      remote_path = self.slave_os_path.dirname(remote_file)
      remote_base = self.slave_os_path.basename(remote_file)
      # Different arguments used for windows and linux/OSX for getting a list
      # of files matching an expression
      if self.slave_os_path.__name__ == 'posixpath':
        args = ['find', '.', '-name', remote_base]
      elif self.slave_os_path.__name__ == 'ntpath':
        args = ['FOR /R . %%v IN (%s) DO @dir %%v /b /s' % remote_base]
      self.slave.cd(remote_path)
      find_proc = self.slave.run(args)
      result = ''
      # Get result of query
      while True:
        buf = find_proc.stdout.recv(128)
        if not buf:
          break
        result += buf
      for filename in result.splitlines():
        if filename == 'File Not Found' or not filename:
          continue
        self.slave.get(filename)
    os.chdir(lcwd)
    self.slave.cd(rcwd)

  def uploader(self):
    """Called before _run_thread to send all required files to the slave."""
    t1 = time.time()
    for local_zip, dest_path in self.zipped_files:
      # Store CWD so we can restore back to original state
      rcwd = self.slave.cwd()
      cwd = os.getcwd()
      os.chdir(os.path.dirname(local_zip))
      zipname = os.path.basename(local_zip)
      # Just incase remote DIR doesn't exist
      self.slave.mkdir(dest_path)
      self.slave.cd(dest_path)
      self.slave.put(zipname)
      self.slave.unzip(zipname)
      self.slave.rm(zipname)
      self.slave.cd(rcwd)
      os.chdir(cwd)
    self.timer['Shard %d upload' % self.shard_index] = (time.time() - t1)
    
  def pre_cleanup(self):
    # TODO(hinoka): Delete all files on slaves that matches files in a list
    # Eg. the out/Debug directory
    pass

  def cleanup(self):
    """All post-execution commands go here.  Eg. delete files, reboot"""
    # At the moment there isn't really anything I want to do post execution
    pass

  def deleter(self):
    original_dir = self.slave_os_path.dirname(self.output_path)
    basename = self.slave_os_path.basename(self.output_path)
    self.slave.cd(original_dir)
    self.slave.rm(basename)
  
  # TODO(hinoka): I thought this was useful at first, but it turns out that its
  # not really used.  Could consider getting rid of it
  def is_finished(self):
    if not self.test_thread:
      return None
    return not self.test_thread.is_alive()

  def run(self):
    """Starts a new thread to run the test"""
    self.test_thread = threading.Thread(name='Test_handler-%d' \
        % self.shard_index, target=self._run_thread)
    self.test_thread.start()


types = {
  'gtest' : GtestHandler,
  'base' : BaseHandler
}
