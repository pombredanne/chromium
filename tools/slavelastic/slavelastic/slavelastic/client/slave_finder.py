"""Finds slaves.  Currently reads the list of slaves from slaves.json."""

from __future__ import with_statement
import random

try:
  import json
except ImportError:
  import simplejson as json 


class SlaveFinder(object):
  def __init__(self, slave_file='slaves.json'):
    with open(slave_file, 'rb') as fd:
      self.slaves_file = json.load(fd)
    self.slaves = self.slaves_file['slaves']
    self.start_index = random.randint(1, len(self.slaves)) - 1
  
  def pop(self):
    # TODO(hinoka): Optimize this?
    if not self.slaves:
      return None
    elif len(self.slaves) > self.start_index:
      return self.slaves.pop(self.start_index)
    else:
      return self.slaves.pop(0)