# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Abstraction layer for PbRpc.  This is meant to connect to a PbRpc slave
running slavelastic.  When a Client object is created, it connects to a remote
slave.  Once that happens, calling functions such as put() or ls() will return
values in a pythonic format"""

from __future__ import with_statement  # Python 2.5 compatability
import binascii
import os
import socket
import sys
import threading
import time

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_DIR = os.path.dirname(ROOT_DIR)
sys.path.append(ROOT_DIR)
sys.path.append(os.path.join(os.path.dirname(ROOT_DIR), 'pbrpc'))

from pbrpc.channel import TcpChannel
from pbrpc.rpc_server import RpcContext, RpcServiceStub
from slavelastic.common.reg import Reg
from slavelastic.common.slavelastic_pb2 import ExecuteData, Name
from slavelastic.common.slavelastic_pb2 import ExchangeRegisterData
from slavelastic.common.slavelastic_pb2 import SlaveServiceCore_Stub
from slavelastic.common.slavelastic_pb2 import SlaveServiceOpt_Stub


class SlavePopen(object):
  """Process object to keep track of slave processes.
  Behaves similarly to Popen.  Note that stdio are Stream objects,
  so recv() is used instead of read()."""
  def __init__(self, client, process_name, stdin=None, stdout=None,
               stderr=None, service=None):
    self.client = client
    self.process_name = process_name
    self.stdin = stdin
    self.stdout = stdout
    self.stderr = stderr
    self.pid = process_name.string_value
    self.service = service

  def poll(self):
    response = self.service.GetRegister(self.process_name)
    if response.string_value is None:
      return None
    else:
      return int(response.string_value)

  def wait(self):
    while self.service.GetRegister(self.process_name).string_value is None:
      time.sleep(0.5)

  def communicate(self):
    pass

  def terminate(self):
    pass

  def kill(self):
    pass


class Client(object):
  """Client interface for slavelastic slaves."""

  def __init__(self, host=None, port=None, timeout=None, ping_period=5,
      FILE_BUF_SIZE=4096):
    self.host = host
    self.port = port
    self.host_name = socket.getfqdn()
    self.context = None
    self.channel = None
    self.service = None
    self.call_lock = threading.Lock()
    self.FILE_BUF_SIZE = 4096
    self.key = Reg('Locked:%s:%s' % (self.host_name,
        binascii.hexlify(os.urandom(31))), 'lock')
    if host is not None and port is not None:
      self.connect(host, port)
      if not self.is_connected():
        return
    platform = self.get_platform()
    self.slave_os_path = self.os_path(platform)
    self._keep_alive_thread = threading.Thread(target=self._keep_alive)
    self._keep_alive_thread.daemon = True
    self.timeout = timeout
    # How often to send a keep alive signal
    self.ping_period = int(ping_period)
    if not self.slave_os_path:
      raise

  @staticmethod
  def os_path(platform):
    if platform == 'win32':
      import ntpath
      return ntpath
    elif (platform == 'linux2' or
         platform == 'darwin'):
      import posixpath
      return posixpath
    else:
      return None

  def _keep_alive(self):
    def _ping():
      with self.call_lock:
        response = self.service.KeepAlive(self.key)
      return response == self.key
    while _ping():
      time.sleep(self.ping_period)

  def connect(self, host, port):
    try:
      self.channel = TcpChannel(host, port)
    # Generic since different platforms throw different exceptions
    except Exception:
      self.channel = None
      return
    self.context = RpcContext(self.channel)
    self.service = RpcServiceStub(self.context, None,
        SlaveServiceOpt_Stub, SlaveServiceCore_Stub)

  def is_connected(self):
    return self.channel is not None

  def _query_stream(self, stream_id):
    with self.call_lock:
      return self.service.QueryStream(Reg(stream_id, stream=True)).string_value

  def put(self, src, dest=None):
    dest = dest or os.path.basename(src)
    remote_dir = self.slave_os_path.dirname(dest)
    if remote_dir:
      self.mkdir(remote_dir)
    if os.path.isdir(src):
      self.mkdir(dest)
      return
    with open(src, 'rb') as f:
      with self.call_lock:
        stream_id = self.service.Write(Reg(None, dest)).stream_id
      stream = self.context.new_stream()
      stream.connect(stream_id)
      while True:
        buf = f.read(self.FILE_BUF_SIZE)
        if not buf:
          stream.close()
          break
        stream.send(buf)
    # Busyloop to make sure the remote stream is closed and finished
    # This should not loop too many times (1 time is normal, 3 max)
    while self._query_stream(stream_id) == 'Running':
      pass

  def get(self, src, dest=None):
    dest = dest or self.slave_os_path.basename(src)
    with self.call_lock:
      stream_id = self.service.Read(Reg(None, src)).stream_id
    assert stream_id, 'Invalid stream_id: %s' % str(stream_id)
    stream = self.context.new_stream()
    stream.connect(stream_id)
    with open(dest, 'wb') as f:
      while True:
        buf = stream.recv(self.FILE_BUF_SIZE)
        if not buf:
          stream.close()
          break
        f.write(buf)

  def mkdir(self, target):
    with self.call_lock:
      r = self.service.CreateDirectory(Name(name=target))
    return r == Reg('ok')

  def get_platform(self):
    with self.call_lock:
      r = self.service.GetRegister(Reg(None, 'platform'))
    return r.string_value

  def rm(self, target):
    with self.call_lock:
      r = self.service.Delete(Reg(target, None))
    return r == Reg('ok')

  def _query(self, query):
    with self.call_lock:
      return self.service.GetRegister(Reg(None, query))

  def unzip(self, src, dest=None):
    """Unzips a file remotely"""
    cwd = self.cwd()
    reg = Reg(dest, src)
    with self.call_lock:
      self.service.Unzip(reg)
    self.cd(cwd)

  # TODO(hinoka): Implement these five functions.
  def ps(self):
    """Returns a dict of active processes as (name : proc).
    Usable with get_stdout."""
    pass

  def killall(self, target=None):
    pass

  def kill(self, proc):
    pass

  def zip(self, exp, dest=None):
    """Zip files on slave matching regex exp"""
    pass

  def mv(self, src, dest=None):
    pass

  def lock(self, timeout=None):
    """Returns true if success, False otherwise.  Steals the lock if a timeout
    is specified and the slave has timed out."""
    timeout = timeout or self.timeout
    original = Reg('Unlocked', 'lock')
    if timeout is not None:
      timeout = int(timeout)
      with self.call_lock:
        response = self.service.GetRegister(Reg(None, 'last_command'))
      last_command = int(response.string_value)
      # If this throws, that means someone mucked up the slave
      delta_time = int(time.time()) - last_command
      if delta_time > timeout:
        # Timed out, we want to steal the key
        with self.call_lock:
          original = self.service.GetRegister(Reg(None, 'lock'))
    locker = ExchangeRegisterData(original=original,
        new=self.key)
    with self.call_lock:
      r = self.service.InterlockedExchangeRegister(locker)
    if r == original and not self._keep_alive_thread.is_alive():
      self._keep_alive_thread.start()
    return r == original

  def unlock(self):
    locker = ExchangeRegisterData(original=self.key,
        new=Reg('Unlocked', 'lock'))
    with self.call_lock:
      r = self.service.InterlockedExchangeRegister(locker)
    return r == self.key

  def run(self, args=None, cwd=None, env=None, shell=None):
    """Same format as subprocess.popen. env is a dict. Returns PID object."""
    # Instance of 'ExecuteData' has no 'XX' member
    # pylint: disable=E1101
    exec_data = ExecuteData()
    for item in args:
      exec_data.args.append(item)
    if cwd:
      exec_data.cwd = cwd
    if env:
      for item in env.iteritems():
        exec_data.env.append('%s=%s' % item)
    if shell is not None:
      exec_data.shell = shell
    # Send RPC call here
    with self.call_lock:
      p = self.service.Execute(exec_data)
      # Get all 3 stdio streams
      # TODO(hinoka): Stderr taken out to fix some things, put it back in later
      stdin_sid = self.service.Write(p).stream_id
      stdout_sid = self.service.Read(p).stream_id
      # p.pipe = Register.STDERR
      # stderr_sid = self.service.Read(p).stream_id
    stdin_stream = self.context.new_stream()
    stdin_stream.connect(stdin_sid)
    stdout_stream = self.context.new_stream()
    stdout_stream.connect(stdout_sid)
    # stderr_stream = self.context.new_stream()
    # stderr_stream.connect(stderr_sid)
    # Encapsulate all streams into an object
    proc = SlavePopen(self, p, stdin_stream, stdout_stream, stdout_stream,
        self.service)
    return proc

  def cwd(self):
    with self.call_lock:
      return self.service.GetRegister(Reg(None, 'cwd')).string_value

  def cat(self, target):
    if self.get_platform() == 'win32':
      args = ['type']
    else:
      args = ['cat']
    args.append(target)
    return self._recv_all(self.run(args).stdout)

  def cd(self, target=None):
    """Change working directory"""
    cwd = self.cwd()
    if not target:
      return cwd
    with self.call_lock:
      r = self.service.ChangeDirectory(Name(name=target))
    if r.string_value != 'ok':
      return False
    return self.cwd()

  @staticmethod
  def _recv_all(stream):
    output = ''
    while True:
      buf = stream.recv(4096)
      if not buf:
        break
      output += buf
    return output

  def ls(self, target=None):
    """Returns list of files"""
    platform = self.get_platform()
    if platform == 'win32':
      args = ['cmd.exe', '/c', 'dir', '/b']
    else:
      args = ['ls']
    if target is not None:
      args.append(target)
    return self._recv_all(self.run(args).stdout).rstrip().split('\n')
