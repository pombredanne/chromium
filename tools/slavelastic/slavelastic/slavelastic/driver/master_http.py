# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Lightweight http interface to modify slave list"""

import BaseHTTPServer
import os
import sys
import time

try:
  import json
except ImportError:
  import simplejson as json

def _master_http(json_file):
  """Http server to reside on master to manage slaves.json
  / -> main page
  /del/hostname/port -> delete hostname/port
  /add/hostname/port -> add hostname/port
  /json -> returns slaves.json, use with XHR
  """
  class Handler(BaseHTTPServer.BaseHTTPRequestHandler):
    def send_redirect(self, target):
      self.send_response(200)
      self.send_header('Content-type', 'text/html')
      self.end_headers()
      self.wfile.write('<meta http-equiv="REFRESH" content="0;url='
          '%s">' % target)

    def do_POST(self):
      pass

    def do_HEAD(self):
      self.send_response(200)
      self.send_header('Content-type', 'text/html')
      self.end_headers()

    @staticmethod
    def get_json():
      return json.load(open(json_file, 'rb'))

    @staticmethod
    def put_json(obj):
      json.dump(obj, open(json_file, 'wb'))

    def do_GET(self):
      self.send_response(200)
      obj = self.get_json()
      if self.path.startswith('/del'):
        if len(self.path.split('/')) == 4:
          _, _, hostname, port = self.path.split('/')
          port = int(port)
          print obj['slaves']
          if [hostname, port] in obj['slaves']:
            obj['slaves'].remove([hostname, port])
            self.put_json(obj)
      elif self.path.startswith('/add'):
        if len(self.path.split('/')) == 4:
          _, _, hostname, port = self.path.split('/')
          obj['slaves'].append([hostname, port])
          self.put_json(obj)
        if self.path.startswith('/add?'):
          result = {}
          for pair in self.path[5:].split('&'):
            key, value = pair.split('=')
            result[key] = value
          if 'host' in result and 'port' in result:
            print result
            obj['slaves'].append((result['host'], int(result['port'])))
            self.put_json(obj)
      elif self.path == '/json':
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(open(json_file, 'rb').read())
        return
      elif self.path == '/manifests':
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(filter(lambda x: x.endswith('.manifest'),
            os.listdir(os.path.dirname(json_file)))))
        return
      elif self.path.startswith('/manifests'):
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(open(os.path.join(os.path.dirname(json_file),
            self.path.split('/')[2]), 'r').read())
        return
      else:
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(open('master.html', 'rb').read())
        return
      self.send_redirect('/')
  try:
    print 'Trying to bind to port 80...'
    httpd = BaseHTTPServer.HTTPServer(('', 80), Handler)
  # Try to bind to ports 8000 to 8010
  except Exception:
    for i in range(8000, 8010):
      try:
        print 'Trying to bind to port %d...' % i
        httpd = BaseHTTPServer.HTTPServer(('', i), Handler)
      except Exception:
        continue
      else:
        break
  print 'running server'
  httpd.serve_forever()

def main(json_file):
  print 'running with %s' % json_file
  try:
    _master_http(json_file)
  finally:
    time.sleep(20)

if __name__ == '__main__':
  main(sys.argv[1])
