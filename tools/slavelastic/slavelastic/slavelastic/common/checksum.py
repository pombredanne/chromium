# Copyright (c) 2010 Chromium Authors. See LICENSE for license.
# All rights reserved if the file LICENSE can't be found in this package.
"""Tool used to calculate checksums."""

from __future__ import with_statement
import hashlib


def md5_calc(fd):
  m = hashlib.md5()
  while True:
    buf = fd.read(4096)
    if not buf:
      break
    m.update(buf)
  return m.hexdigest()


def crc_calc(fd):
  raise NotImplementedError


checksum_functions = {
  'MD5':md5_calc,
  'CRC':crc_calc
}


def calculate(filename, hash_='MD5'):
  assert hash_ in checksum_functions
  with open(filename, 'rb') as fd:
    return checksum_functions[hash_](fd)
