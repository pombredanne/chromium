#!/usr/bin/python
# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import optparse
import os
import sys
from bugdroid import Bugdroid

def _get_change_info(revision):
  """ Create content information for Issue Tracking System. """
  get_content_info = ['svn', 'log', '-v', '-r', revision]
  content = os.popen(' '.join(get_content_info)).read()
  return content

def main():
  parser = optparse.OptionParser()
  parser.add_option('-c', '--change-string-path', dest='file_path',
                    help='file path to a text file that contains the change '
                          'info.  this is for debugging only.', metavar='FILE')
  parser.add_option('-a', '--allowed-projects', action='append', 
                    default=['chromium', 'chromium-os', 'gyp',
                             'chrome-os-partner'], dest='trackers',
                    help='issue trackers that can be updated')
  parser.add_option('-e', '--default-project', dest='default_tracker',
                    help='the issue tracker to update if no project name is '
                         'given in the BUG= line', default='chromium')
  parser.add_option('-r', '--repo-path', dest='repo_path',
                    help='the svn repository path')
  parser.add_option('-n', '--revision', dest='revision',
                    help='the svn revision number created by the commit')
  parser.add_option('-p', '--password-file', dest='password_file_path',
                    help='file path to a text file that contains the password'
                         'for the account.', metavar='FILE')
  (options, args) = parser.parse_args()

  if not options.password_file_path:
    parent = os.path.dirname(os.path.abspath(__file__))
    password_path = os.path.join(parent, '.bugdroid_password')
    password = None
    if os.path.exists(password_path):
      password = open(password_path, 'r').readline().strip()
  else:
    if os.path.exists(options.password_file_path):
      password = open(options.password_file_path, 'r').readline().strip()
    else:
      parser.error('Password file path is invalid')

  bugger = Bugdroid('bugdroid1@chromium.org', password)
  bugger.login()

  if not options.file_path:
    if not options.revision:
      print 'No revision number available.'
      return 1
    content = 'The following revision refers to this bug:\n'
    content += '    http://src.chromium.org/viewvc/chrome?view=rev&revision=' +\
      options.revision + '\n\n'
    content += _get_change_info(options.revision)
  else:
    if not os.path.isfile(options.file_path):
      print 'The path given is invalid'
      return 1
    f = open(options.file_path, 'rU')
    content = str(f.read())
  if not bugger.process_all_bugs(content, options.trackers,
                                 options.default_tracker):
    print 'No bug ID was found.'

  return 0


if __name__ == '__main__':
  main()
