#!/usr/bin/python2.5
# Copyright 2010 Google Inc.
# All Rights Reserved.

"""AppEngine scripts to proxy build.chromium.org.

   This app is a proxy on top of build.chromium.org. It redirects all request
   to the real server and caches the results for 60 seconds.
"""

__author__ = 'nsylvain (Nicolas Sylvain)'

from google.appengine.ext import webapp
import wsgiref.handlers

from google.appengine.api import urlfetch
from google.appengine.api import memcache

class BuildbotRequest(webapp.RequestHandler):
  def get(self, path):
    # Find the requested path.
    requested_path = self.request.path
    if self.request.query_string:
      requested_path += '?' + self.request.query_string

    # See if it's in the cache.
    content = memcache.get(requested_path)
    type = memcache.get(requested_path + '__TYPE__')

    if not content or not type:
      # This is not in the cache. We need to fetch it.
      url = "http://build.chromium.org/" + requested_path
      result = urlfetch.fetch(url, deadline=10)
      if result.status_code == 200:
        # Add the content to the cache.
        content = result.content.replace('build.chromium.org', 'chromium-buildbot.appspot.com')
        memcache.add(path, content, 60)

        # Add the content type to the cache
        type = result.headers['Content-Type']
        memcache.add(requested_path + '__TYPE__', type, 60)
      else:
        self.response.out.write('Error %d' % result.status_code)
        return
   
    self.response.headers['Content-Type'] = type
    self.response.out.write(content)

def main():
  """Proxies connections to the buildbot."""

  application = webapp.WSGIApplication([(r'/(.*)', BuildbotRequest)],
                                       debug=True)
  wsgiref.handlers.CGIHandler().run(application)

if __name__ == '__main__':
  main()

