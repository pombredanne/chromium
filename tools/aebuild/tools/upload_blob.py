#!/usr/bin/env python
# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Upload a file to the BlobStore."""

import getpass
import logging
import optparse
import os
import re
import socket
import sys
import time
import urllib
import urllib2
import urlparse
try:
  from hashlib import md5
except ImportError:
  from md5 import new as md5

import utils
utils.fix_sys_path()

from google.appengine.tools.appengine_rpc import HttpRpcServer, \
    HttpRequestToString


def EncodeMultipartFormdata(fields, files,
                            mime_mapper=lambda _: 'application/octet-stream'):
  """Encodes a Multipart form data object.

  Args:
    fields: a sequence (name, value) elements for
      regular form fields.
    files: a sequence of (name, filename, value) elements for data to be
      uploaded as files.
    mime_mapper: function to return the mime type from the filename.
  Returns:
    content_type: for httplib.HTTP instance
    body: for httplib.HTTP instance
  """
  boundary_hash = md5()
  boundary_hash.update('%.7f' % time.time())
  boundary = boundary_hash.hexdigest()
  body_list = []
  for (key, value) in fields:
    body_list.append('--' + boundary)
    body_list.append('Content-Disposition: form-data; name="%s"' % key)
    body_list.append('')
    body_list.append(value)
    body_list.append('--' + boundary)
    body_list.append('')
  for (key, filename, value) in files:
    body_list.append('--' + boundary)
    body_list.append('Content-Disposition: form-data; name="%s"; '
                     'filename="%s"' % (key, filename))
    body_list.append('Content-Type: %s' % mime_mapper(filename))
    body_list.append('')
    body_list.append(value)
    body_list.append('--' + boundary)
    body_list.append('')
  if body_list:
    body_list[-2] += '--'
  body = '\r\n'.join(body_list)
  content_type = 'multipart/form-data; boundary=%s' % boundary
  return content_type, body


class HttpRpcServer2(HttpRpcServer):
  """Fixes HttpRpcServer.Send() for Google AppEngine SDK 1.3.4 and lower with
  real redirects and adds PostData()."""
  def Send(self, request_path, payload="",
           content_type="application/octet-stream",
           timeout=None,
           **kwargs):
    """Same as super.Send() except it raises exceptions more often."""
    logger = logging
    old_timeout = socket.getdefaulttimeout()
    socket.setdefaulttimeout(timeout)
    try:
      tries = 0
      auth_tried = False
      while True:
        tries += 1
        args = dict(kwargs)
        url = "%s://%s%s?%s" % (self.scheme, self.host, request_path,
                                urllib.urlencode(args))
        req = self._CreateRequest(url=url, data=payload)
        req.add_header("Content-Type", content_type)
        req.add_header("X-appcfg-api-version", "1")
        try:
          logger.debug('Sending HTTP request:\n%s' %
                       HttpRequestToString(req, include_data=self.debug_data))
          f = self.opener.open(req)
          response = f.read()
          f.close()
          return response
        except urllib2.HTTPError, e:
          logger.debug("Got http error, this is try #%s" % tries)
          if tries > self.auth_tries:
            raise
          elif e.code == 401:
            if auth_tried:
              raise
            auth_tried = True
            self._Authenticate()
          elif e.code >= 500 and e.code < 600:
            continue
          elif e.code == 302:
            if auth_tried:
              raise
            auth_tried = True
            loc = e.info()["location"]
            logger.debug("Got 302 redirect. Location: %s" % loc)
            if loc.startswith("https://www.google.com/accounts/ServiceLogin"):
              self._Authenticate()
            elif re.match(r"https://www.google.com/a/google.com/ServiceLogin",
                          loc):
              self.account_type = os.getenv("APPENGINE_RPC_HOSTED_LOGIN_TYPE",
                                            "HOSTED_OR_GOOGLE")
              self._Authenticate()
            elif re.match(r"https://www.google.com/a/[a-z0-9.-]+/ServiceLogin",
                          loc):
              self.account_type = os.getenv("APPENGINE_RPC_HOSTED_LOGIN_TYPE",
                                            "HOSTED")
              self._Authenticate()
            elif loc.startswith("http://%s/_ah/login" % (self.host,)):
              self._DevAppServerAuthenticate()
            else:
              raise
          else:
            raise
    finally:
      socket.setdefaulttimeout(old_timeout)

  def PostData(self, url, data, files=None, *args, **kwargs):
    """Posts data to the provided URL.

    Similar to self.Send() except that it uses form-data to send data and the
    blob store redirect is handled correctly.

    Args:
      url: The URL to post to.
      data: A dictionary of key-value pairs to post.
      files: A sequence of (name, filename, value) elements for data to be
          uploaded as files
    """
    files = files or []
    content_type, body = EncodeMultipartFormdata(data.items(), files)
    try:
      return self.Send(url, body, content_type=content_type, *args, **kwargs)
    except urllib2.HTTPError, e:
      if files and e.code == 302:
        # That's expected.
        url = ''.join(urlparse.urlparse(e.info()['location'])[2:])
        # TODO(maruel): Set referer.
        return self.Send(url, payload=None)
      else:
        raise


def send_file(rpc, filename, file_content):
  # Send a request to retrieve the right url.
  url = rpc.Send('/blobs/get_upload_url', payload=None)
  # Remove the http(s) and host name from the URL.
  url = ''.join(urlparse.urlparse(url)[2:])
  return rpc.PostData(url, {}, [(filename, filename,
      file_content)])


def main(argv=None):
  parser = optparse.OptionParser()
  parser.add_option('-e', '--email')
  parser.add_option('-p', '--password',
                    help='file to load the password from')
  parser.add_option('--source', default='upload_blob')
  parser.add_option('-A', '--application',
                    default=utils.get_default_app())
  parser.add_option('-H', '--host')
  parser.add_option('--retries', type=int, default=1,
                    help='Number of retries, default=%default')
  parser.add_option('--disable_cookie_store', default=False,
                    action='store_true')
  parser.add_option('-f', '--file')
  parser.add_option('-n', '--no_https', default=False, action='store_true')
  parser.add_option('-v', '--verbose', default=0, action='count')
  options, args = parser.parse_args(argv)
  if options.verbose == 0:
    logging.basicConfig(level=logging.WARNING)
  elif options.verbose == 1:
    logging.basicConfig(level=logging.INFO)
  else:
    logging.basicConfig(level=logging.DEBUG)

  def auth_func():
    if not options.email:
      options.email = raw_input('Username: ')
    if options.password:
      password = open(options.pasword, 'r').readline().strip()
    else:
      password = getpass.getpass('Password for %s: ' % options.email)
    return options.email, password

  if not options.host:
    options.host = '%s.appspot.com' % options.application

  print('Connecting to %s' % options.host)
  rpc = HttpRpcServer2(options.host, auth_func, None, options.source,
                       save_cookies=not options.disable_cookie_store,
                       auth_tries=options.retries,
                       secure=not options.no_https)
  try:
    if options.file:
      filename = os.path.basename(options.file)
      file_content = open(options.file, 'rb').read()
      print(send_file(rpc, filename, file_content))
    else:
      # payload=None makes a GET, otherwise a POST is made.
      if not args:
        parser.error('Need to pass an url')
      print rpc.Send(args[0], payload=None)
  except urllib2.HTTPError, e:
    print "HTTP %s" % e.code
    return 1
  return 0


if __name__ == '__main__':
  sys.exit(main())
