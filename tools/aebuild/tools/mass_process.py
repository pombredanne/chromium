# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Fixes entities in the data store.

Uses concurrent requests to speed things up and retry on exceptions.

To run this script, use appengine_console.py.
"""

import datetime
import Queue
import sys
import threading
import time

from google.appengine.ext import db

from aebuild import models


def _put(items):
  try:
    if items:
      db.put(items)
  except KeyboardInterrupt:
    # Give up.
    raise
  except:
    # AppEngine and the httplib can throw too many kinds of exceptions to trap
    # them all.
    _put(items[:len(items)/2])
    _put(items[len(items)/2:])


def _delete(items):
  try:
    if items:
      db.delete(items)
  except KeyboardInterrupt:
    raise
  except:
    _delete(items[:len(items)/2])
    _delete(items[len(items)/2:])


class QueryProcessor(object):
  """Resumable iterator over an unlimited number of entities."""
  DEFAULT_FETCH_SIZE = 1000
  DEFAULT_ACTION_SIZE = 50
  DEFAULT_NB_THREADS = 5

  def __init__(self, fetch_size=None, action_size=None, cursor=None,
               nb_threads=None):
    # Performance settings:
    self.fetch_size = fetch_size or self.DEFAULT_FETCH_SIZE
    self.action_size = action_size or self.DEFAULT_ACTION_SIZE
    self.nb_threads = nb_threads or self.DEFAULT_NB_THREADS
    # Resume a query
    self.cursor = cursor
    # Statistics
    self.lock = threading.Lock()
    self.nb_processed = 0
    self.nb_delete_pending = 0
    self.nb_deleted = 0
    self.nb_put_pending = 0
    self.nb_put = 0
    # Inner workings
    self.stop = threading.Event()
    self.delete_queue = []
    self.put_queue = []
    self.worker_queue = Queue.Queue()

  def loop(self, query, process_item):
    """Cursor-based query to work around the default fetch implementation in
    remote_api."""
    self.stop.clear()
    threads = []
    status_thread = None
    try:
      start_time = datetime.datetime.utcnow()
      while True:
        # Fetch a batch.
        if self.cursor:
          query.with_cursor(self.cursor)
        while True:
          try:
            items = query.fetch(self.fetch_size)
            break
          except KeyboardInterrupt:
            raise
          except:
            continue
        if not items:
          break

        if not status_thread:
          # Start the threads late so they don't interfere with login prompt.
          for i in xrange(self.nb_threads):
            threads.append(threading.Thread(target=self._worker,
                                            name='QueryProcessor._worker'))
            threads[-1].daemon = True
            threads[-1].start()
          print(('Fetching %d items at a time, flushing %d, %d worker '
                 'threads') % (
                    self.fetch_size, self.action_size, self.nb_threads))
          print('processed           deleted               put      speed'
                '           duration')
          status_thread = threading.Thread(target=self._status_thread,
                                           name='QueryProcessor._status_thread',
                                           args=(start_time,))
          status_thread.daemon = True
          status_thread.start()

        # Process the batch.
        for item in items:
          process_item(self, item)
          with self.lock:
            self.nb_processed += 1
        self.cursor = query.cursor()

        # Don't queue too much, it's useless.
        while ((self.worker_queue.qsize() * self.action_size) >
                  (3 * self.fetch_size)):
          time.sleep(0.1)

      # We're done, flush the remaining serially.
      _delete(self.delete_queue)
      _put(self.put_queue)
    except:
      print('\n\nStopped with cursor %s\n\n' % self.cursor)
      raise
    finally:
      # Kill the worker threads.
      for i in xrange(len(threads)):
        self.worker_queue.put(('Q', None))
      for thread in threads:
        thread.join()
      if status_thread:
        self.stop.set()
        status_thread.join()
        print('')

  def delete(self, item):
    """Queue an item for deletion in a batch."""
    self.delete_queue.append(item)
    with self.lock:
      self.nb_delete_pending += 1
    if len(self.delete_queue) >= self.action_size:
      items = self.delete_queue[:self.action_size]
      self.delete_queue = self.delete_queue[self.action_size:]
      self.worker_queue.put(('D', items))

  def put(self, item):
    """Queue an item for put (save) in a batch."""
    self.put_queue.append(item)
    with self.lock:
      self.nb_put_pending += 1
    if len(self.put_queue) >= self.action_size:
      items = self.put_queue[:self.action_size]
      self.put_queue = self.put_queue[self.action_size:]
      self.worker_queue.put(('P', items))

  def _status_thread(self, start_time):
    data = ''
    new_data = ''
    while not self.stop.is_set():
      self.stop.wait(0.5)
      with self.lock:
        if self.nb_processed:
          delta = datetime.datetime.utcnow() - start_time
          # Will fail for >1 day jobs.
          # TODO(maruel): calculate instant speed with damping.
          speed = 0
          if delta.seconds:
            speed = self.nb_processed / float(delta.seconds)
          new_data = '%9d   %7d/%7d   %7d/%7d   %4.2d items/sec  %6d secs' % (
                      self.nb_processed,
                      self.nb_deleted, self.nb_delete_pending,
                      self.nb_put, self.nb_put_pending,
                      speed, delta.seconds)
      if new_data != data:
        data = new_data
        sys.stdout.write('\r%s   ' % data)
        sys.stdout.flush()

  def _worker(self):
    """Worker thread that delete or put entity batches."""
    while True:
      cmd, items = self.worker_queue.get()
      if cmd == 'D':
        _delete(items)
        with self.lock:
          self.nb_deleted += len(items)
      elif cmd == 'P':
        _put(items)
        with self.lock:
          self.nb_put += len(items)
      elif cmd == 'Q':
        break
      else:
        raise Exception('Unknown command %s' % cmd)


#### Premade functions.

def unbreak_events(cursor=None):
  """Reset Event.broken = False and delete processed Event."""
  def unbreak_event(context, item):
    if item.processed == None or item.broken == True or item.broken == None:
      item.processed = False
      item.broken = False
      context.put(item)
    elif item.processed == True:
      context.delete(item)

  QueryProcessor(cursor=cursor).loop(db.Query(models.Event), unbreak_event)


def delete(query):
  """Delete all items in the specified query."""
  QueryProcessor().loop(query, lambda context, item: context.delete(item))


def delete_events():
  """Delete all Event entities."""
  print('Deleting all Event')
  delete(db.Query(models.Event, keys_only=True))


def remove_old_data(date=30):
  """Delete everything from XX days old masters. Defaults to 30 days."""
  if isinstance(date, basestring):
    date = datetime.datetime.strptime(date, '%Y-%m-%d')
  elif isinstance(date, int):
    date = datetime.date.today() - datetime.timedelta(date)

  # Do Event first!
  for model in ('Event', 'Build', 'BuildStep', 'Builder',
                'Change', 'Console', 'Project'):
    print('\nDeleting %s prior %s' % (model,
                                      date.strftime('%Y-%m-%d')))
    if model == 'Project':
      delete(db.GqlQuery(
        'SELECT __key__ FROM %s WHERE started < :1' % model, date))
    else:
      delete(db.GqlQuery(
        'SELECT __key__ FROM %s WHERE projectstarted < :1' % model, date))
