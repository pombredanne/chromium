#!/usr/bin/env python
# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Small utilities for aebuild's tools."""

import gzip
import os
import re
import subprocess
import sys
import tarfile
import urllib
import warnings
import zipfile


# You can override if necessary.
BASE_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))


def auto_setup():
  """Download all the necessary dependencies, if possible."""
  # Makes the script's directory the current directory. It makes everything
  # simpler and django expects appengine_config.py to live there.
  os.chdir(BASE_PATH)
  root_path = os.path.dirname(BASE_PATH)
  django_dir = os.path.join(root_path, 'Django-1.1')
  gae_sdk_dir = os.path.join(root_path, 'google_appengine')
  closure_library_dir = os.path.join(root_path, 'closure_library')
  closure_compiler_dir = os.path.join(root_path, 'closure_compiler')

  # Make sure Django 1.1 is available.
  if not os.path.isdir(django_dir):
    print('WARNING: Downloading Django 1.1!\n')
    django_zip_file = os.path.join(root_path, 'django.tar.gz')
    if os.path.isfile(django_zip_file):
      os.remove(django_zip_file)
    urllib.urlretrieve('http://www.djangoproject.com/download/1.1/tarball/',
        django_zip_file)
    tarfile.open(django_zip_file).extractall(root_path)
    os.remove(django_zip_file)

  # Make sure Google AppEngine SDK is available.
  if not os.path.isdir(gae_sdk_dir):
    print('Please visit http://code.google.com/appengine/downloads.html and')
    print('install at %s' % gae_sdk_dir)
    sys.exit(1)

  # Make sure Google Closure Library is available.
  if not os.path.isdir(closure_library_dir):
    print('WARNING: Downloading Closure Library!\n')
    subprocess.check_call(['svn', 'checkout',
        'http://closure-library.googlecode.com/svn/trunk/',
        closure_library_dir])

  # Make sure Google Closure Compiler is available.
  if not os.path.isdir(closure_compiler_dir):
    print('WARNING: Downloading Closure Compiler!\n')
    compiler_zip_file = os.path.join(root_path, 'compiler-lastest.zip')
    if os.path.isfile(compiler_zip_file):
      os.remove(compiler_zip_file)
    urllib.urlretrieve(
        'http://closure-compiler.googlecode.com/files/compiler-latest.zip',
        compiler_zip_file)
    zipfile.ZipFile(compiler_zip_file, 'r').extractall(closure_compiler_dir)
    os.remove(compiler_zip_file)


def get_default_app():
  """Parses app.yaml and returns the application's name."""
  app_yaml = os.path.join(BASE_PATH, 'app.yaml')
  return re.search(r'application: +(.*)', open(app_yaml, 'r').read()).group(1)


def fix_sys_path():
  """Fix sys.path to be able to import GAE SDK and Django."""
  auto_setup()
  root_path = os.path.dirname(BASE_PATH)
  django_dir = os.path.join(root_path, 'Django-1.1')
  gae_sdk_dir = os.path.join(root_path, 'google_appengine')
  if not BASE_PATH in sys.path:
    sys.path.insert(0, BASE_PATH)
  if not django_dir in sys.path:
    sys.path.insert(0, django_dir)
  if not gae_sdk_dir in sys.path:
    # Remove when google/appengine/ext/remote_api/remote_api_stup.py stops
    # importing sha.
    warnings.simplefilter('ignore', DeprecationWarning)

    # Insert at the top in case another "google" mode is in sys.path
    sys.path.insert(0, gae_sdk_dir)


def get_env():
  """Returns a aebuild-friendly environment dictionary to start subprocesses."""
  auto_setup()
  root_path = os.path.dirname(BASE_PATH)
  django_dir = os.path.join(root_path, 'Django-1.1')
  gae_sdk_dir = os.path.join(root_path, 'google_appengine')
  env = os.environ.copy()
  env.setdefault('PYTHONPATH', '')
  env['PYTHONPATH'] = '%s:%s:%s:%s' % (gae_sdk_dir, django_dir, BASE_PATH,
                                       env['PYTHONPATH'])
  return env
