# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""URL mappings for the aebuild package."""

# NOTE: Must import *, since Django looks for things here, e.g. handler500.
from django.conf.urls.defaults import *

urlpatterns = patterns(
    'aebuild.views',
    (r'^$', 'index'),
    (r'^buildstatus$', 'buildstatus'),
    (r'^console$', 'console'),
    (r'^manage$', 'manage'),
    (r'^sheriff.js$', 'sheriff'),
    (r'^status-listener$', 'status_listener'),
    )

urlpatterns += patterns(
    'aebuild.rpc',
    (r'^rpc$', 'rpc'),
    (r'^consoletask$', 'consoletask'),
    )

urlpatterns += patterns(
    'aebuild.console',
    (r'^task/process-console$', 'process_console'),
    (r'^task/process-rev$', 'process_rev'),
    )

urlpatterns += patterns(
    'aebuild.eventqueue',
    (r'^task/process-event$', 'process_event'),
    )

urlpatterns += patterns(
    'aebuild.cron',
    (r'^cron/queue-event$', 'queue_event'),
    )

urlpatterns += patterns(
    'aebuild.cron',
    (r'^cron/cleanup-events$', 'cleanup_events'),
    )

urlpatterns += patterns(
    'aebuild.blobs_views',
    (r'^blobs/get_upload_url/?$', 'get_upload_url'),
    (r'^blobs/upload_form/?$', 'upload_form'),
    (r'^blobs/on_upload/?$', 'on_upload'),
    (r'^blobs/upload_failure/?$', 'upload_failure'),
    (r'^blobs/upload_success/?$', 'upload_success'),
    (r'^blobs/serve/([a-zA-Z_0-9]+)/?$', 'serve'),
    )
