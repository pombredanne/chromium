# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import datetime

from mapreduce import operation as op

import models


def delete_entity(entity):
  yield op.db.Delete(entity)


def delete_old_entity(entity, date):
  try:
    date = datetime.date.today() - datetime.timedelta(int(date))
  except ValueError:
    if isinstance(date, basestring):
      date = datetime.datetime.strptime(date, '%Y-%m-%d')

  if isinstance(entity, models.Project):
    if entity.started < date:
      yield op.db.Delete(entity)
  else:
    if entity.projectstarted < date:
      yield op.db.Delete(entity)
