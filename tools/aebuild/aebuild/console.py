# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from django.http import HttpResponseForbidden, HttpResponseNotFound
from google.appengine.api.labs import taskqueue
from google.appengine.ext import db
from google.appengine.runtime import DeadlineExceededError

import datetime
import models
import random
import time

def GetBuildersDict(projectname):
  builders = models.Builder.all()
  builders.filter('projectname =', projectname)

  empty = {'buildkey': None, 'number': None, 'state': 'notstarted'}
  data = {}
  for builder in builders:
    if not data.get(builder.category):
      data[builder.category] = {}
    data[builder.category][builder.name] = empty
  return data

# Console task that update all the data in the console.
# This is the current work flow:
#  . Create new rows in the console table for all the new changes that came in.
#  . Create a task queue for all pending changes.
def process_console(request):
  # Create main try to be able to get the exception if we run out of time.
  try:
    # Fetch all the new rows.
    changes = models.Change.all()
    changes.filter('parsed =', False)

    for change in changes:
      all_builders = simplejson.dumps(GetBuildersDict(change.projectname))

      # Set the last_check 1 day back, so we don't re-process all the
      # older builds for a new revision.branch
      lastcheck = datetime.datetime.now() - datetime.timedelta(hours=24)
      # Create the new row in the Console table.
      if change.branch is None:
        logging.exception('branch is None in change key %s' % change.key())

      # We need to create a task name that we will use to process this revision
      taskname = 'TaskRev%s-%s' % (change.revision,
                                   int(random.random() * 10000000))

      row = models.Console(revision=change.revision,
                           done=False,
                           projectname=change.projectname,
                           projectstarted=change.projectstarted,
                           branch=change.branch or 'trunk',
                           comments=change.comments,
                           when=change.when,
                           who=change.who,
                           revlink=change.revlink,
                           lastcheck=lastcheck,
                           data=all_builders,
                           datastrict=all_builders,
                           taskname=taskname)
      row.put()
      change.parsed = True
      change.put()

    # List all changes that are not done, and create a task queue for all of.
    # of them. Since there is a maximum of 20 task queues per second, and we
    # run this code every 10 seconds, we won't be done processing the previous
    # tasks if we have more than 200 pending revisions. If it happens, some of
    # the tasks will fail to be queued with 'Task Already Exist'. This is ok,
    # what matters is that they will eventually be running.
    console_changes = models.Console.all()
    console_changes.filter('done = ', False)
    now = datetime.datetime.now()
    for console_change in console_changes:
      # Disable rev processing for now.  It uses task queue API quota quickly
      # and at least some of the rev processing is failing due to unexpected
      # results in the data store.
      #  File ".../aebuild/console.py", line 231, in process_rev
      #      current_revision = int(build.revision)
      #      TypeError: int() argument must be a string or a number, not
      #      'NoneType'
      break

      # If this revision has been idle for more than 24 hours, I doubt we
      # will ever get the result. Let's mark it as done already.
      #if console_change.modified < now - datetime.timedelta(hours=2400):
      #  console_change.done = True
      #  continue

      # Start the task.
      try:
        params = {'key': str(console_change.key())}
        rev_task = taskqueue.Task(name=console_change.taskname, params=params,
                                  url='/task/process-rev')
        rev_task.add('revqueue');
      except taskqueue.TaskAlreadyExistsError:
        # This task is already running or pending, we don't need to add it
        # again.
        pass
      except taskqueue.TombstonedTaskError:
        # This task is now unusable, thanks to TombstonedTaskError in AppEngine.
        # We need to create a new name for it by incrementing the last part of
        # the name.
        taskname = 'TaskRev%s-%s' % (console_change.revision,
                                     int(random.random() * 10000000))
        console_change.taskname = taskname
        console_change.put()

        # Try again. Don't catch the errors, since they won't be expected.
        params = {'key': str(console_change.key())}
        rev_task = taskqueue.Task(name=taskname, params=params,
                                  url='/task/process-rev')
        rev_task.add('revqueue');

  except DeadlineExceededError:
    pass

  return HttpResponse('done')


def IsDataComplete(data):
  if data['state'] in ['success', 'failure', 'warning', 'exception']:
    return True
  return False

def NameFromNumber(number):
  if number is None:
    return "notfinished"
  if number == 0:
    return 'success'
  if number == 1:
    return 'warnings'
  if number == 2:
    return 'failure'
  if number == 3:
    return 'skipped'
  if number == 4:
    return 'exception'

def SetDataFromBuild(data, build):
  data['buildkey'] = str(build.key())
  data['state'] = NameFromNumber(build.results)
  data['number'] = build.number

import logging

# Update a revision.
# NOTE: We always return HTTP status 200 because otherwise appengine will retry
# until it works, and if we sent bad data, it will be broken forever.
def process_rev(request):
  # Put everything in a try. This function should never return an error.
  #try:
    # Get the key for the entry we are suppose to parse.
    logging.info('process_rev: START')
    key = db.Key(request.POST['key'])
    if not key:
      logging.error('process_rev: cannot find key')
      return HttpResponse('cannot find key')

    revision = db.get(key)
    if not revision:
      logging.error('process_rev cannot find revision from key')
      return HttpResponse('cannot find revision from key')

    # Save the current check time
    check_time = datetime.datetime.now()
    modified = False

    revision_data = simplejson.loads(revision.data)
    revision_datastrict = simplejson.loads(revision.datastrict)
    # Update the status for each revision.
    for category in revision_data:
      for builder in revision_data[category]:
        logging.info('process_rev: Working for %s/%s' % (category, builder))
        data = revision_data[category][builder]
        datastrict = revision_datastrict[category][builder]
        # Check the status.
        need_update = not IsDataComplete(data)
        need_update_strict = not IsDataComplete(datastrict)

        if not need_update and not need_update_strict:
          logging.info('process_rev: Already have all the data')
          # Nothing to do!
          continue

        logging.info('Check if we know the build')
        # Check if we know already which build it is.
        if need_update and data.get('buildkey'):
          build = db.get(db.Key(data['buildkey']))
          if build:
            logging.info('process_rev: we have a build')
            if build.modified > revision.lastcheck:
              logging.info('process_rev: and it was modified.')
              modified = True
              SetDataFromBuild(data, build)

        # Do the same, for the strict. (This needs to be optimized).
        if need_update_strict and datastrict.get('buildkey'):
          build = db.get(db.Key(datastrict['buildkey']))
          if build:
            modified = True
            if build.modified > revision.lastcheck:
              SetDataFromBuild(datastrict, build)

        logging.info('Setup the build information')
        # Now, if one of them is not already fixed on a build, we need to
        # fetch all the builds for this builder that have been modified or
        # added since the last time we checked.
        if data.get('buildkey') and datastrict.get('buildkey'):
          # We have all the info we need. continue.
          logging.info('process_rev: now we got all the info we need')
          builds = []
        else:
          builds = models.Build.all()
          builds.filter('projectname =', revision.projectname)
          builds.filter('buildername =', builder)
          builds.filter('modified >', revision.lastcheck)
          builds.order('modified')
          builds.order('number')

        logging.info('Iterate the builds %s %s %s' % (
                     revision.projectname, builder, revision.lastcheck))
        for build in builds:
          logging.info('process_rev: looking at a build %d', build.number)
          logging.info(simplejson.dumps(data))
          # Loose mode first.
          if not data.get('buildkey'):
            try:
              current_revision = int(build.revision)
              change_revision = int(revision.revision)
              logging.info('process_rev: revisions are %s-%s' % (
                           build.revision, revision.revision))
              if current_revision > change_revision:
                # The revision was include in this build
                SetDataFromBuild(data, build)
                modified = True
            except ValueError, TypeError:
              logging.error('process_rev: ERROR')
              pass

          if data.get('buildkey') and datastrict.get('buildkey'):
            # We have all the info we need. break this loop.
            break

          # Try the strict mode.
          # for build_change in build.changeset:
          #   if build_change.revision = revision.revision:
          #     if not data.get('buildkey'):
          #       SetDataFromBuild(data, build)
          #       modified = True
          #     if not datastrict('buildkey'):
          #       SetDataFromBuild(datastrict, build)
          #       modified = True

        revision.data = simplejson.dumps(revision_data)
        revision.datastrict = simplejson.dumps(revision_datastrict)

    # Update the last check time.
    revision.lastcheck = check_time
    if modified:
      revision.modified = datetime.datetime.now()
    # Update the build
    revision.put()
  #except:
  #  return HttpResponse('got exception')

    # TODO(nsylvain): Keep track of the number of builds that are not complete,
    # and if it is zero, then set the 'done' flag for this revision to True.
    return HttpResponse('done')
