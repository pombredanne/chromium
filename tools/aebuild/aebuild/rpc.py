# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""/rpc handler.

Inspired from http://code.google.com/appengine/articles/rpc.html
"""

import datetime
import time

from django.utils import simplejson
from django.http import HttpResponse
import models


class RPCMethods(object):
  """This class contains all the RPC methods that are allowed to be called from
  the web page."""

  @staticmethod
  def UpdateRevs(*args):
    """Retrieves revision data for console view."""
    values = []
    revs = simplejson.loads(args[2])
    for rev in revs:
      matching_revs = models.Console.all()
      matching_revs.filter("revision = ", rev['revision'])
      matching_revs.filter("branch =", args[1])
      matching_revs.filter("modified >",
                           datetime.datetime.fromtimestamp(rev['modified'] + 1))

      results = matching_revs.fetch(1)
      if results:
        p = results[0]
        dict = {}
        dict['revision'] = p.revision
        dict['modified'] = time.mktime(p.modified.timetuple())
        dict['done'] = p.done
        dict['projectname'] = p.projectname
        dict['branch'] = p.branch
        dict['comments'] = p.comments
        dict['who'] = p.who
        dict['revlink'] = p.revlink
        dict['data'] = p.data
        values.append(dict);
    return {'results': values}

  @staticmethod
  def GetRevs(*args):
    """Retrieves revision data for console view."""
    revs = models.Console.all()
    revs.filter("revision >", args[2])
    revs.filter("branch =", args[1])
    results = revs.fetch(2)
    values = []
    for p in results:
      dict = {}
      dict['revision'] = p.revision
      dict['modified'] = time.mktime(p.modified.timetuple())
      dict['done'] = p.done
      dict['projectname'] = p.projectname
      dict['branch'] = p.branch
      dict['comments'] = p.comments
      dict['who'] = p.who
      dict['revlink'] = p.revlink
      dict['data'] = p.data
      values.append(dict);
    return values

  @staticmethod
  def GetBuilders(*args):
    """Returns all the builders, with their category, projects and status."""
    builders = models.Builder.all()
    my_dict = {}
    for builder in builders:
      if not my_dict.get(builder.projectname):
        my_dict[builder.projectname] = {}

      category = builder.category or 'Default'
      category = category.split('|')[0]
      if not my_dict[builder.projectname].get(category):
        my_dict[builder.projectname][category] = []

      builder_dict = {}
      builder_dict['state'] = builder.state or 'None'
      builder_dict['name'] = builder.name
      my_dict[builder.projectname][category].append(builder_dict)
    return simplejson.dumps(my_dict)


def rpc(request, methods=RPCMethods):
  """Main RPC handler. Will dispatch to the right method and return the
  data back to the user in json. This code is taken from the appengine
  manual, and has been converted to django."""
  func = None
  action = request.GET['action']
  if action:
    if action[0] == '_':
      return HttpResponse('restricted action', status=503)
    else:
      func = getattr(methods, action, None)
  if not func:
    return HttpResponse('func not found', status=503)

  # Get the args.
  args = ()
  while True:
    key = 'arg%d' % len(args)
    val = None
    try:
      val = request.GET[key]
    except:
      pass
    if val:
      args += (simplejson.loads(val),)
    else:
      break
  # Call the function
  result = func(*args)
  # Return the result, in json.
  return HttpResponse(simplejson.dumps(result))
