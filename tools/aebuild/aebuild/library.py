# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Django template library for aebuild package."""

import cgi
import logging

from google.appengine.api import memcache
from google.appengine.api import users

import django.template
import django.utils.safestring

import models

register = django.template.Library()


class GetHeadBuildAttrNode(django.template.Node):
  """Renders a headBuild attribute."""

  def __init__(self, headBuilds, builderName, attr):
    self.headBuilds = django.template.Variable(headBuilds)
    self.builderName = django.template.Variable(builderName)
    self.attr = django.template.Variable(attr)

  def render(self, context):
    try:
      headBuilds = self.headBuilds.resolve(context)
      builderName = self.builderName.resolve(context)
      attr = self.attr.resolve(context)
    except django.template.VariableDoesNotExist:
      logging.exception('nothing')
      return ''
    request = context.get('request')
    try:
      return headBuilds[builderName][attr]
    except KeyError:
      return ''


@register.tag
def get_head_build_attr(parser, token):
  """Get a headBuild attribute."""
  try:
    tag_name, headBuilds, builderName, attr = token.split_contents()
  except ValueError:
    raise django.template.TemplateSyntaxError(
      "%r requires exactly three arguments" % token.contents.split()[0])
  return GetHeadBuildAttrNode(headBuilds, builderName, attr)


class GetBuildAttrNode(django.template.Node):
  """Renders a change's build attribute."""

  def __init__(self, change, revisions, builderName, attr):
    self.change = django.template.Variable(change)
    self.revisions = django.template.Variable(revisions)
    self.builderName = django.template.Variable(builderName)
    self.attr = django.template.Variable(attr)

  def render(self, context):
    try:
      change = self.change.resolve(context)
      revisions = self.revisions.resolve(context)
      builderName = self.builderName.resolve(context)
      attr = self.attr.resolve(context)
    except django.template.VariableDoesNotExist:
      logging.exception('nothing')
      return ''
    request = context.get('request')
    try:
      return revisions[change.revision][builderName][attr]
    except KeyError:
      return ''


@register.tag
def get_build_attr(parser, token):
  """Get a change's build attribute."""
  try:
    tag_name, change, revisions, builderName, attr = token.split_contents()
  except ValueError:
    raise django.template.TemplateSyntaxError(
      "%r requires exactly four arguments" % token.contents.split()[0])
  return GetBuildAttrNode(change, revisions, builderName, attr)
