# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import datetime
import logging
import time

from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from django.http import HttpResponseForbidden, HttpResponseNotFound
from google.appengine.api.datastore_errors import NeedIndexError
from google.appengine.api.labs import taskqueue
from google.appengine.ext import db
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError

import models
import views


def updateMember(obj, member_name, new_val):
  """Utility function to update a member variable only if it was modified."""
  if getattr(obj, member_name) != new_val:
    setattr(obj, member_name, new_val)
    return True
  return False


def getOne(query):
  """Returns the youngest item of a query and delete any older item found.

  It may not always give the best result when concurrent tasks generate
  incomplete objects."""
  items = query.fetch(1000)
  if not items:
    return None
  if len(items) == 1:
    return items[0]
  else:
    youngest = items[0]
    for item in items[1:]:
      if item.modified > youngest.modified:
        youngest = item
    items.remove(youngest)
    logging.warning('Deleting %d duplicate %s objects' % (
                        len(items), youngest.__class__.__name__))
    db.delete(items)
    return youngest


def getBuildObj(eventid, projectname, projectstarted, builder_name,
                build_number):
  """Retrieves a models.Build object if it exists.
  Returns a tuple (buildobj, was_created)"""
  buildobj = getOne(models.Build.gql(
      'WHERE projectname = :1 AND projectstarted = :2 '
      'AND buildername = :3 AND number = :4', projectname,
      projectstarted, builder_name, build_number))
  if buildobj:
    return buildobj, False
  buildobj = models.Build(eventid=eventid,
                          projectname=projectname,
                          projectstarted=projectstarted,
                          buildername=builder_name,
                          number=build_number)
  return buildobj, True


def getBuildStepObj(eventid, projectname, projectstarted, builder_name,
                    build_number, step_number, step_name):
  """Retrieves a models.BuildStep object if it exists.
  Returns a tuple (buildstepobj, was_created)"""
  stepobj = getOne(models.BuildStep.gql(
      'WHERE projectname = :1 AND projectstarted = :2 '
      'AND buildername = :3 AND buildnumber = :4 AND name = :5',
      projectname, projectstarted, builder_name, build_number,
      step_name))
  if stepobj:
    return stepobj, False
  stepobj = models.BuildStep(eventid=eventid,
                             projectname=projectname,
                             projectstarted=projectstarted,
                             buildername=builder_name,
                             buildnumber=build_number,
                             number=step_number,
                             name=step_name)
  return stepobj, True


def getBuilderObj(eventid, projectname, projectstarted, buildername, state,
                    basedir, slaves, category):
  """Retrieves a models.Builder object if it exists.
  Returns a tuple (builderobj, was_created)"""
  builderobj = getOne(models.Builder.gql(
      'WHERE projectname = :1 AND projectstarted = :2 AND name = :3',
      projectname, projectstarted, buildername))
  if builderobj:
    return builderobj, False
  builderobj = models.Builder(eventid=eventid,
                              projectname=projectname,
                              projectstarted=projectstarted,
                              name=buildername,
                              state='state',
                              basedir='basedir',
                              slaves=slaves,
                              category=category)
  return builderobj, True


def onChangeAdded(eventid, projectname, projectstarted, change):
  changeobj = getOne(models.Change.gql(
      'WHERE projectname = :1 AND projectstarted = :2 AND number = :3',
      projectname, projectstarted, change['number']))
  if changeobj:
    logging.info('Received a duplicate change event for change %s' %
                 changeobj.number)
    return True
  ts = datetime.datetime.fromtimestamp(change['when'])
  changeobj = models.Change(eventid=eventid,
                            projectname=projectname,
                            projectstarted=projectstarted,
                            number=change['number'],
                            when=ts,
                            parsed=False)
  updateMember(changeobj, 'who', change.get('who'))
  updateMember(changeobj, 'revision', change.get('revision'))
  updateMember(changeobj, 'branch', change.get('branch'))
  updateMember(changeobj, 'comments', change.get('comments'))
  updateMember(changeobj, 'files', '\n'.join(change.get('files', ())))
  updateMember(changeobj, 'revlink', change.get('revlink'))
  changeobj.put()
  return True


def onBuilderAdded(eventid, projectname, projectstarted, builderName, builder):
  builderobj, changed = getBuilderObj(eventid, projectname, projectstarted,
      builderName, builder['state'], builder['basedir'],
      '\n'.join(builder.get('slaves', ())), '')
  changed |= updateMember(builderobj, 'category', builder.get('category'))
  if changed:
    builderobj.put()
  return True


def onBuilderChangedState(eventid, projectname, projectstarted, builderName,
                          state):
  builderobj, changed = getBuilderObj(eventid, projectname, projectstarted,
      builderName, state, '', '', '')
  changed |= updateMember(builderobj, 'state', state)
  if changed:
    builderobj.put()
  return True


def updateStepObj(stepobj, number, step):
  """Updates a models.BuildStep with new data.

  Returns True only if it was modified."""
  ret = False
  if number != -1:
    ret |= updateMember(stepobj, 'number', number)
  ret |= updateMember(stepobj, 'text', '\n'.join(step.get('text', '')))
  ret |= updateMember(stepobj, 'name', step['name'])
  if step.get('times', (None, None))[0]:
    step_started = datetime.datetime.fromtimestamp(step['times'][0])
    ret |= updateMember(stepobj, 'started', step_started)
    ret |= updateMember(stepobj, 'is_started', True)
  if step.get('times', (None, None))[1]:
    step_finished = datetime.datetime.fromtimestamp(
        step['times'][1])
    ret |= updateMember(stepobj, 'finished', step_finished)
    ret |= updateMember(stepobj, 'is_finished', True)
  if stepobj.finished:
    ret |= updateMember(stepobj, 'results',
                        step.get('results', (None, None))[0])
  return ret


def updateBuildObj(buildobj, got_revision, revision):
  """Updates a models.Build with new data.

  Returns True only if it was modified."""
  ret = False
  if got_revision and buildobj.revision != got_revision:
    buildobj.revision = got_revision
    ret = True
  elif revision and not buildobj.revision:
    buildobj.revision = str(revision)
    ret = True
  return ret


def onStepEvent(eventid, projectname, projectstarted, step, properties):
  """Handles stepStarted and stepFinished."""
  properties = dict([(i[0], i[1]) for i in properties])
  builder_name = properties['buildername']
  build_number = properties['buildnumber']
  buildobj, build_obj_changed = getBuildObj(eventid, projectname,
                                            projectstarted, builder_name,
                                            build_number)
  stepobj, step_obj_changed = getBuildStepObj(eventid, projectname,
                                              projectstarted, builder_name,
                                              build_number, -1, step['name'])

  updated_models = []
  revision = properties.get('revision')
  got_revision = properties.get('got_revision')
  if updateBuildObj(buildobj, got_revision, revision) or build_obj_changed:
    updated_models.append(buildobj)
  if updateStepObj(stepobj, -1, step) or step_obj_changed:
    updated_models.append(stepobj)
  if updated_models:
    db.put(updated_models)
  return True


def onBuildEvent(eventid, projectname, projectstarted, build):
  properties = dict([(i[0], i[1]) for i in build['properties']])
  builder_name = properties['buildername']
  build_number = properties['buildnumber']
  buildobj, changed = getBuildObj(eventid, projectname, projectstarted,
                                  builder_name, build_number)
  revision = properties.get('revision')
  got_revision = properties.get('got_revision')
  changed |= updateBuildObj(buildobj, got_revision, revision)
  changed |= updateMember(buildobj, 'reason', build.get('reason'))
  changed |= updateMember(buildobj, 'results', build.get('results'))
  changed |= updateMember(buildobj, 'slave', build.get('slave'))
  changed |= updateMember(buildobj, 'text', '\n'.join(build.get('text', '')))
  if build.get('times', (None, None))[0]:
    changed |= updateMember(buildobj, 'started',
                            datetime.datetime.fromtimestamp(build['times'][0]))
  if build.get('times', (None, None))[1]:
    changed |= updateMember(buildobj, 'finished',
                            datetime.datetime.fromtimestamp(build['times'][1]))

  updated_models = []
  if changed:
    updated_models.append(buildobj)

  squery = models.BuildStep.gql(
      'WHERE projectname = :1 AND projectstarted = :2 '
      'AND buildername = :3 AND buildnumber = :4',
      projectname, projectstarted, builder_name, build_number)
  stepobjs = {}
  MAX_STEPS = 40
  nb = 0
  for s in squery:
    nb += 1
    logging.info('Step %s' % s.name)
    stepobjs[s.name] = s
    if nb > MAX_STEPS:
      logging.warning(('Something is wrong, got more than %d steps for a '
                       'single build, aborting') % MAX_STEPS)
      return False
  for step_number, step in enumerate(build['steps']):
    changed = False
    stepobj = stepobjs.get(step['name'], None)
    if stepobj is None:
      stepobj = models.BuildStep(eventid=eventid,
                                 projectname=projectname,
                                 projectstarted=projectstarted,
                                 buildername=builder_name,
                                 buildnumber=build_number,
                                 number=step_number,
                                 name=step['name'])
      changed = True
    changed |= updateStepObj(stepobj, step_number, step)
    if changed:
      updated_models.append(stepobj)

  if updated_models:
    db.put(updated_models)
  logging.info('Saved %d steps' % len(updated_models))
  return True


SUPPORTED_EVENTS = {
    'changeAdded': onChangeAdded,
    'builderAdded': onBuilderAdded,
    'builderChangedState': onBuilderChangedState,
    'buildStarted': onBuildEvent,
    'buildFinished': onBuildEvent,
    'stepStarted': onStepEvent,
    'stepFinished': onStepEvent,
}


def process_event(request):
  """Event task that parses new event submissions."""
  if views.loadConfig().disable_event_process:
    return HttpResponse('Was disabled')
  first_id = request.POST.get('first_id', None)
  number_items = request.POST.get('number_items', None)
  logging.info('process_event: start: %s-%s' % (first_id, number_items))

  if bool(first_id) != bool(number_items):
    logging.error('Internal inconsistency')
    first_id = None
    number_items = None

  if first_id:
    first_id = int(first_id)
    number_items = int(number_items)

  if (request.META.get('X-AppEngine-TaskRetryCount', None) == 1
      and number_items >= 2):
    # Last execution of this task failed. Split the task in 2 to ensure it has
    # enough time to complete.
    def queue(fi, ni):
      name = 'TaskEvent-%d-%d' % (fi, ni)
      params = {'first_id': fi, 'number_items': ni}
      task = taskqueue.Task(url='/task/process-event', name=name, params=params)
      task.add('eventqueue')
    queue(first_id, number_items / 2)
    queue(first_id + (number_items / 2), number_items - (number_items / 2))
    return HttpResponse('Task split', status=200)

  # Create main try to be able to get the exception if we run out of time.
  processed_events = 0
  nb_looped_events = 0
  failures = []
  # Use eventual consistency for faster access.
  # This means we run a cron job to reprocess events we missed.
  try:
    # Fetch all the new rows.
    logging.info('process_event: fetching events')
    project = None
    if first_id:
      from_key = db.Key.from_path('Event', first_id)
      to_key = db.Key.from_path('Event', first_id + number_items)
      q = models.Event.gql('WHERE __key__ >= :1 AND __key__ < :2',
                           from_key, to_key)
      # Needs consistency.
      rpc = None
    else:
      # It hardly has time to process more than 100 items.
      q = models.Event.gql(
          'WHERE processed = FALSE AND broken = FALSE LIMIT 100')
      number_items = 'as much as we could'
      rpc = db.create_rpc(deadline=10, read_policy=db.EVENTUAL_CONSISTENCY)

    for event in q.run(rpc=rpc):
      nb_looped_events += 1
      # A query can retrieve same entity multiple times so filter out
      # duplicates.
      if event.processed:
        logging.info('Event %s was already processed, skipping' %
            event.key().id())
        continue
      if event.broken:
        logging.info('Event %s was broken, skipping' % event.key().id())
        continue

      logging.info('process_event: processing %d/%s from %s' % (
          event.key().id(), event.type,
          event.timestamp.strftime('%Y-%m-%d %H:%M:%S')))

      projectname = event.projectname
      projectstarted = event.projectstarted
      if (not project or project.name != projectname or
          project.started != projectstarted):
        # Ensure the project is noted in the datastore.
        project = models.Project.gql('WHERE name = :1 AND started = :2',
                                     projectname, projectstarted).get()
        if not project:
          logging.info('New unseen project: %s %s' %
              (projectname, projectstarted))
          project = models.Project(name=projectname, started=projectstarted)
          project.put()

      handler = SUPPORTED_EVENTS.get(event.type, None)
      if handler:
        # Handle specific events.
        # Stupid python doesn't accept unicode as dict key for kwargs.
        payload_data = simplejson.loads(event.payload)
        if isinstance(payload_data, list):
          logging.error('Error parsing event %s: dict expected, list found' %
                        event.key().id())
          continue
        kwargs = dict([(str(k), v)
                       for (k, v)
                       in payload_data.iteritems()])
        try:
          ret = handler(event.id, projectname, projectstarted, **kwargs)
        except NeedIndexError:
          logging.exception('Please update index.yaml for event: %s/%s' % (
              event.key().id(), event.type))
          ret = False
        except ValueError:
          # The entry was invalid and couldn't be parsed, ignore it.
          logging.exception('ValueError exception for event: %s/%s' % (
              event.key().id(), event.type))
          ret = False
        if ret is False:
          failures.append(event.type)
          event.broken = True
          event.put()
        else:
          processed_events += 1
      else:
        ret = True
      if ret:
        # Mark the event as processed even if there is no handler, but not if
        # there was an exception.
        event.processed = True
        event.put()
  except CapabilityDisabledError:
    logging.info('CapabilityDisabledError: read-only datastore')
  except DeadlineExceededError:
    logging.info('DeadlineExceededError')
    if (nb_looped_events >= 1 and processed_events == 0 and event and not
        event.broken and not event.processed):
      logging.warn(('Couldn\'t parse the single event %s in a whole request; '
                    'marking as broken') % event.key().id())
      event.broken = True
      event.put()

  if len(failures) > 0:
    logging.error('%d handler failures: %s' % (len(failures), failures))
  logging.info(('process_event: ran over %s events, processed %s events out of '
                '%s') % (nb_looped_events, processed_events, number_items))

  try:
    console_task = taskqueue.Task(url='/task/process-console')
    console_task.add('consolequeue')
  except DeadlineExceededError:
    # Try again later to continue the list.
    pass
  except taskqueue.TaskAlreadyExistsError:
    # Since we only want to queue the console task from the event processor
    # once every 10s, we can safely ignore all of the failed queue attempts.
    pass
  except taskqueue.TombstonedTaskError:
    # Since we only want to queue the console task from the event processor
    # once every 10s, we can safely ignore all of the failed queue attempts.
    pass

  return HttpResponse('done')
