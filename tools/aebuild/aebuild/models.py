# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""App Engine data model (schema) definition for aebuild package."""

# Python imports
import datetime
import logging
import md5
import os
import re
import time

# AppEngine imports
from django.utils import simplejson
from google.appengine.ext import db
from google.appengine.api import memcache

# Local imports
import settings


CONTEXT_CHOICES = (3, 10, 25, 50, 75, 100)


### GQL query cache ###


_query_cache = {}


def gql(cls, clause, *args, **kwds):
  """Return a query object, from the cache if possible.

  Args:
    cls: a db.Model subclass.
    clause: a query clause, e.g. 'WHERE draft = TRUE'.
    *args, **kwds: positional and keyword arguments to be bound to the query.

  Returns:
    A db.GqlQuery instance corresponding to the query with *args and
    **kwds bound to the query.
  """
  query_string = 'SELECT * FROM %s %s' % (cls.kind(), clause)
  query = _query_cache.get(query_string)
  if query is None:
    _query_cache[query_string] = query = db.GqlQuery(query_string)
  query.bind(*args, **kwds)
  return query


### Config ###


class Config(db.Model):
  allowedhostsjson = db.TextProperty(required=True, indexed=False)
  needsnewpassword = db.BooleanProperty(required=True, indexed=False)
  submitpassword = db.TextProperty(required=True, indexed=False)
  defaultproject = db.StringProperty(required=False, indexed=False)
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)
  disable_event_clean = db.BooleanProperty(default=False, indexed=False)
  disable_event_process = db.BooleanProperty(default=False, indexed=False)
  memcacheduration = db.IntegerProperty(required=True, default=5*60,
                                        indexed=False)

  def __init__(self, *args, **kwargs):
    db.Model.__init__(self, *args, **kwargs)
    try:
      self.allowedhosts = simplejson.loads(kwargs['allowedhostsjson'])
    except:
      self.allowedhosts = []


### Accounts ###

class Account(db.Model):
  """Maps a user or email address to a user-selected nickname, and more.

  The default nickname is generated from the email address by
  stripping the first '@' sign and everything after it.  The email
  should not be empty nor should it start with '@' (AssertionError
  error is raised if either of these happens).

  This also holds a list of ids of starred issues.  The expectation
  that you won't have more than a dozen or so starred issues (a few
  hundred in extreme cases) and the memory used up by a list of
  integers of that size is very modest, so this is an efficient
  solution.  (If someone found a use case for having thousands of
  starred issues we'd have to think of a different approach.)
  """

  user = db.UserProperty(auto_current_user_add=True, required=True)
  email = db.EmailProperty(required=True)  # key == <email>
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)
  is_manager = db.BooleanProperty(default=False)

  # Current user's Account.  Updated by middleware.AddUserToRequestMiddleware.
  current_user_account = None

  lower_email = db.StringProperty()
  xsrf_secret = db.BlobProperty()

  # Note that this doesn't get called when doing multi-entity puts.
  def put(self):
    self.lower_email = str(self.email).lower()
    super(Account, self).put()

  @classmethod
  def get_account_for_user(cls, user):
    """Get the Account for a user, creating a default one if needed."""
    email = user.email()
    assert email
    key = '<%s>' % email
    # Since usually the account already exists, first try getting it
    # without the transaction implied by get_or_insert().
    account = cls.get_by_key_name(key)
    if account is not None:
      return account
    return cls.get_or_insert(key, user=user, email=email)

  @classmethod
  def get_by_key_name(cls, key, **kwds):
    """Override db.Model.get_by_key_name() to use cached value if possible."""
    if not kwds and cls.current_user_account is not None:
      if key == cls.current_user_account.key().name():
        return cls.current_user_account
    return super(Account, cls).get_by_key_name(key, **kwds)

  def get_xsrf_token(self, offset=0):
    """Return an XSRF token for the current user."""
    if not self.xsrf_secret:
      self.xsrf_secret = os.urandom(8)
      self.put()
    m = md5.new(self.xsrf_secret)
    email_str = self.lower_email
    if isinstance(email_str, unicode):
      email_str = email_str.encode('utf-8')
    m.update(self.lower_email)
    when = int(time.time()) // 3600 + offset
    m.update(str(when))
    return m.hexdigest()


### Events, Projects ###


class Event(db.Model):
  id = db.IntegerProperty(required=True, indexed=False)
  type = db.StringProperty(required=True)
  version = db.IntegerProperty(required=True)
  payload = db.TextProperty(required=True)
  projectname = db.StringProperty(required=True)
  projectstarted = db.DateTimeProperty(required=True)
  timestamp = db.DateTimeProperty(required=True, indexed=False)
  # processed is set to TRUE once it is fully converted and it is eventually
  # garbage collected by a cron job.
  processed = db.BooleanProperty(required=True, default=False)
  broken = db.BooleanProperty(required=True, default=False)
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)


class Project(db.Model):
  name = db.StringProperty(required=True)
  started = db.DateTimeProperty(required=True)
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)


### Changes ###


class Change(db.Model):
  eventid = db.IntegerProperty(required=True, indexed=False)
  projectname = db.StringProperty(required=True)
  projectstarted = db.DateTimeProperty(required=True)
  branch = db.StringProperty(indexed=False)
  comments = db.TextProperty()
  files = db.TextProperty()
  number = db.IntegerProperty(required=True)
  revision = db.StringProperty(indexed=False)
  when = db.DateTimeProperty(required=True)
  who = db.StringProperty(indexed=False)
  revlink = db.TextProperty()
  # Was parsed for console display.
  parsed = db.BooleanProperty()
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)


### Builders, Builds, and BuildSteps ###


class Builder(db.Model):
  eventid = db.IntegerProperty(required=True, indexed=False)
  projectname = db.StringProperty(required=True)
  projectstarted = db.DateTimeProperty(required=True)
  name = db.StringProperty(required=True)
  state = db.StringProperty(indexed=False)
  basedir = db.StringProperty(indexed=False)
  slaves = db.TextProperty()
  category = db.TextProperty()
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)


class Build(db.Model):
  eventid = db.IntegerProperty(required=True, indexed=False)
  projectname = db.StringProperty(required=True)
  projectstarted = db.DateTimeProperty(required=True)
  number = db.IntegerProperty(required=True)
  buildername = db.StringProperty(required=True)
  finished = db.DateTimeProperty(indexed=False)
  reason = db.TextProperty()
  results = db.IntegerProperty(indexed=False)
  revision = db.StringProperty()
  slave = db.StringProperty(indexed=False)
  started = db.DateTimeProperty(indexed=False)
  text = db.TextProperty()
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)


class BuildStep(db.Model):
  eventid = db.IntegerProperty(required=True, indexed=False)
  projectname = db.StringProperty(required=True)
  projectstarted = db.DateTimeProperty(required=True)
  buildername = db.StringProperty(required=True)
  buildnumber = db.IntegerProperty(required=True)
  number = db.IntegerProperty(required=True, indexed=False)
  name = db.StringProperty(required=True)
  text = db.TextProperty()
  eta = db.DateTimeProperty(indexed=False)
  results = db.IntegerProperty(indexed=False)
  reason = db.StringProperty(indexed=False)
  started = db.DateTimeProperty(indexed=False)
  finished = db.DateTimeProperty(indexed=False)
  is_started = db.BooleanProperty(default=False, indexed=False)
  is_finished = db.BooleanProperty(default=False, indexed=False)
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)

  def __init__(self, *args, **kwargs):
    db.Model.__init__(self, *args, **kwargs)
    try:
      self.cssClass = "exception"
      if kwargs['started'] and not kwargs['finished']:
        self.cssClass = "running"
      elif kwargs['results'] is None:
        self.cssClass = "notstarted"
      elif kwargs['results'] == settings.SUCCESS:
        self.cssClass = "success"
      elif kwargs['results'] == settings.FAILURE:
        self.cssClass = "failure"
      # TODO: store prevResults so we can set cssClass to warnings
    except KeyError:
      # kwargs missing 'started' in cases
      pass


class Console(db.Model):
  # TODO(maruel): Rename to ConsoleLine or something more explicit.
  projectname = db.StringProperty(required=True)
  projectstarted = db.DateTimeProperty(required=True)
  revision = db.StringProperty(required=True, indexed=False)
  done = db.BooleanProperty(required=True, default=False)
  branch = db.StringProperty(required=True, indexed=False)
  comments = db.TextProperty()
  when = db.DateTimeProperty(indexed=False)
  who = db.StringProperty(indexed=False)
  revlink = db.TextProperty(indexed=False)
  lastcheck = db.DateTimeProperty(required=True, indexed=False)
  data = db.TextProperty(required=True)
  datastrict = db.TextProperty(required=True)
  created = db.DateTimeProperty(auto_now_add=True)
  modified = db.DateTimeProperty(auto_now=True)
