# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Views for aebuild package."""

import datetime
import logging
import md5
import os
import random
import re
import string
import time

from google.appengine.api import memcache
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.runtime import DeadlineExceededError
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.api.labs import taskqueue

# Fix django import
import settings

# Import settings as django_settings to avoid name conflict with settings().
from django.conf import settings as django_settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
import django.template
from django.template import RequestContext
from django.utils import simplejson

import models

# Make sure the tags are registered.
import library

# Add our own template library.
_library_name = __name__.rsplit('.', 1)[0] + '.library'
if not django.template.libraries.get(_library_name, None):
  django.template.add_to_builtins(_library_name)


IS_DEV = os.environ['SERVER_SOFTWARE'].startswith('Dev')  # Development server

# Counter displayed by respond() on every page showing how many requests the
# current incarnation has handled, not counting redirects. Rendered by
# templates/base.html.
counter = 0


### Helper functions ###


def updateCachedConfig(config):
  if not memcache.set("config", config, time=config.memcacheduration):
    logging.error("Memcache set of 'config' failed.")
  return config


def loadConfig():
  config = memcache.get("config")
  if config is not None:
    return config

  config = db.GqlQuery('SELECT * FROM Config').get()
  if config is None:
    config = models.Config(allowedhostsjson='[]',
                           needsnewpassword=True,
                           submitpassword=createPassword())
    config.put()
  updateCachedConfig(config)
  return config


def createPassword(password=None):
  if not password:
    password = ''.join([random.choice(string.letters+string.digits)
                       for x in range(8)])
  m = md5.new()
  m.update(password)
  return m.hexdigest()


def respond(template, request, **kwargs):
  """Helper to render a response, passing standard stuff to the response.

  Args:
    request: The request object.
    template: The template name; '.html' is appended automatically.
    params: A dict giving the template parameters; modified in-place.

  Returns:
    Whatever render_to_response(template, params) returns.

  Raises:
    Whatever render_to_response(template, params) raises.
  """
  global counter
  counter += 1
  mimetype = kwargs.get('mimetype', django_settings.DEFAULT_CONTENT_TYPE)
  params = kwargs.get('params', {})
  params['request'] = request
  params['counter'] = counter
  params['user'] = request.user
  params['is_dev'] = IS_DEV
  params['console_url'] = 'http://%s/console' % request.get_host()
  params['manage_url'] = 'http://%s/manage' % request.get_host()

  # Handle sign in/out data.
  full_path = request.get_full_path().encode('utf-8')
  if request.user is None:
    params['sign_in'] = users.create_login_url(full_path)
  else:
    params['sign_out'] = users.create_logout_url(full_path)
    params['is_manager'] = False
    account = models.Account.current_user_account
    if account is not None:
      params['xsrf_token'] = account.get_xsrf_token()
      params['is_manager'] = account.is_manager

  try:
    return render_to_response(template, params,
                              context_instance=RequestContext(request),
                              mimetype=mimetype)
  except DeadlineExceededError:
    logging.exception('DeadlineExceededError')
    return HttpResponse('DeadlineExceededError', status=503)
  except CapabilityDisabledError, err:
    logging.exception('CapabilityDisabledError: %s', err)
    return HttpResponse('aebuild: App Engine is undergoing maintenance. '
                        'Please try again in a while. ' + str(err),
                        status=503)
  except MemoryError:
    logging.exception('MemoryError')
    return HttpResponse('MemoryError', status=503)
  except AssertionError:
    logging.exception('AssertionError')
    return HttpResponse('AssertionError')


def getResultsClass(results, prevResults, inProgress):
    """Given the current and past results, returns the class that will be used
    by the css to display the right color for a box."""

    if inProgress:
        return "running"

    if results is None:
       return "notstarted"

    if results == settings.SUCCESS:
        return "success"

    if results == settings.FAILURE:
        if not prevResults:
            # This is the bottom box. We don't know if the previous one failed
            # or not. We assume it did not.
            return "failure"

        if prevResults != settings.FAILURE:
            # This is a new failure.
            return "failure"
        else:
            # The previous build also failed.
            return "warnings"

    # Any other results? Like EXCEPTION?
    return "exception"


def getBuildsForChanges(changes, numBuilds, project, categories, builders):
  allBuilds = {}
  revisions = {}
  builderList = {}
  headBuilds = {}

  if not project:
    return (builderList, allBuilds, revisions, headBuilds)

  builderquery = models.Builder.gql('WHERE projectname = :1 '
                                    'AND projectstarted = :2',
                                    project.name, project.started)
  for builder in builderquery:
    if categories and builder.category not in categories:
      continue
    if builders and builder.name not in builders:
      continue

    # We want to display this builder.
    category = builder.category or 'default'
    # Strip the category to keep only the text before the first |.
    # This is a hack to support the chromium usecase where they have
    # multiple categories for each slave. We use only the first one.
    category = category.split('|')[0]
    if category not in builderList:
      builderList[category] = []
    builderList[category].append(builder.name)

  if len(changes) > 0:
    buildquery = models.Build.gql('WHERE projectname = :1 AND revision >= :2',
                                  project.name, changes[-1].revision)
    for build in buildquery:
      # Skip forced builds.
      if not build.revision:
        continue
      if build.buildername not in allBuilds:
        allBuilds[build.buildername] = []
      allBuilds[build.buildername].append(build)

  for builderName in allBuilds:
    allBuilds[builderName].sort(lambda a, b: int(a.revision) > int(b.revision))

  for change in changes:
    if change.revision not in revisions:
      revisions[change.revision] = {}
    for category in builderList.keys():
      for builderName in builderList[category]:
        introducedIn = None
        firstNotIn = None

        if builderName in allBuilds:
          for build in allBuilds[builderName]:
            if int(build.revision) >= int(change.revision):
              introducedIn = build
            else:
              firstNotIn = build
              break

        results = None
        previousResults = None
        if introducedIn:
          results = introducedIn.results
        if firstNotIn:
          previousResults = firstNotIn.results
        isRunning = False
        if introducedIn and not introducedIn.finished:
          isRunning = True
        resultsClass = getResultsClass(results, previousResults, isRunning)

        tag = ''
        url = ''
        if introducedIn:
          builderStrip = builderName.replace(' ', '')
          builderStrip = builderStrip.replace('(', '')
          builderStrip = builderStrip.replace(')', '')
          builderStrip = builderStrip.replace('.', '')
          tag = 'Tag%s%s' % (builderStrip, introducedIn.revision)
          url = '/buildstatus?project=%s&builder=%s&number=%s' % (
              introducedIn.projectname, introducedIn.buildername,
              introducedIn.number)
        revisions[change.revision][builderName] = {'color': resultsClass,
                                                   'title': builderName,
                                                   'url': url,
                                                   'tag': tag}
        if (builderName not in headBuilds or
            headBuilds[builderName]['color'] == 'notstarted'):
          headBuilds[builderName] = revisions[change.revision][builderName]

  return (builderList, allBuilds, revisions, headBuilds)


### Decorators for request handlers ###


def get_required(func):
  """Decorator that returns an error unless request.method == 'GET'."""

  def post_wrapper(request, *args, **kwds):
    if request.method != 'GET':
      return HttpResponse('This requires a GET request.', status=405)
    return func(request, *args, **kwds)

  return post_wrapper


def post_required(func):
  """Decorator that returns an error unless request.method == 'POST'."""

  def post_wrapper(request, *args, **kwds):
    if request.method != 'POST':
      return HttpResponse('This requires a POST request.', status=405)
    return func(request, *args, **kwds)

  return post_wrapper


### Request handlers ###


@get_required
def index(request):
  """/ - Show a list of patches."""
  return HttpResponseRedirect("/console")


@get_required
def buildstatus(request):
  """/buildstatus - Show an individual build's status."""
  project = request.GET.get('project')
  if isinstance(project, list):
    project = project[0]

  builder = request.GET.get('builder')
  if isinstance(builder, list):
    builder = builder[0]

  number = request.GET.get('number')
  if isinstance(number, list):
    number = number[0]
  number = int(number)

  build = db.GqlQuery('SELECT * FROM Build WHERE projectname = :1 AND '
                      'buildername = :2 AND number = :3', project, builder,
                      number).get()

  buildsteps = []
  if build:
    buildstepquery = db.GqlQuery(
        'SELECT * FROM BuildStep WHERE projectname = :1 AND buildername = :2 '
        'AND buildnumber = :3 ORDER BY number DESC', project, builder, number)
    for buildstep in buildstepquery:
      buildsteps.append(buildstep)

  params = {'build': build, 'buildsteps': buildsteps}

  return respond('buildstatus.html', request, params=params)


@get_required
def console(request):
  """/console - Show the standard Buildbot console view."""

  params = {}
  config = loadConfig()

  projectname = request.GET.get('project')
  if isinstance(projectname, list):
    projectname = projectname[0]
  if not projectname and config.defaultproject:
    projectname = config.defaultproject
  project = models.Project.gql('WHERE name = :1 '
                               'ORDER BY started DESC',
                               projectname).get()
  if not project and projectname != config.defaultproject:
    params['error'] = "Project '%s' does not exist, showing '%s' instead." % (
                          projectname, config.defaultproject)
    projectname = config.defaultproject
    project = models.Project.gql('WHERE name = :1 '
                                 'ORDER BY started DESC',
                                 projectname).get()

  categories = request.GET.get('category')
  if not categories:
    categories = []
  if not isinstance(categories, list):
    categories = [categories]

  builders = request.GET.get('builder')
  if not builders:
    builders = []
  if not isinstance(builders, list):
    builders = [builders]

  changes = models.Change.gql('WHERE projectname = :1 ORDER BY when DESC',
                              projectname).fetch(40, 0)

  lastRevision = None
  if len(changes) > 0:
    lastRevision = changes[-1].revision
  (builderList, allBuilds, revisions, headBuilds) = getBuildsForChanges(
      changes, 40, project, categories, builders)

  count = 0
  for category in builderList:
    count += len(builderList[category])

  categorylist = builderList.keys()
  categorylist.sort()
  categories = []
  for key in categorylist:
    # TODO(nsylvain): Another hack to display the category in a pretty
    # way.  If the master owner wants to display the categories in a
    # given order, he/she can prepend a number to it. This number won't
    # be shown.
    pretty = key.lstrip('0123456789')

    # To be able to align the table correctly, we need to know
    # what percentage of space this category will be taking. This is
    # (#Builders in Category) / (#Builders Total) * 100.
    category = {'name': pretty, 'size': (len(builderList[key]) * 100) / count,
                'alt': 'Alt', 'first': '', 'last': '',
                'builders': builderList[key]}
    categories.append(category)
  if len(categories) > 0:
    categories[0]['first'] = 'first'
    categories[-1]['last'] = 'last'

  params['changes'] = changes
  params['categories'] = categories
  params['builderList'] = builderList
  params['allBuilds'] = allBuilds
  params['revisions'] = revisions
  params['headBuilds'] = headBuilds

  return respond('console.html', request, params=params)


def manage(request):
  """/manage - Management area."""

  # If the user is not signed in or not a manager, send them to /.
  account = models.Account.current_user_account
  if not account or not account.is_manager:
    return HttpResponseRedirect("/")

  # Load the config into the page params.
  config = loadConfig()
  params = {'config': config}

  # On get requests, render the manage page.
  if request.method == 'GET':
    return respond('manage.html', request, params=params)

  # This is a post request.
  errors = []

  # Try saving the allowedhosts input.
  config.allowedhostsjson = request.POST.get('allowedhosts')
  try:
    config.allowedhosts = simplejson.loads(config.allowedhostsjson)
    if not isinstance(config.allowedhosts, list):
      errors += ["Allowed hosts should be a list of IP addresses."]
    else:
      for host in config.allowedhosts:
        if not isinstance(host, (str, unicode)):
          errors += ["Allowed hosts should be a list of IP addresses."]
          break
  except:
    errors += ["Supplied allowed hosts is not valid JSON format."]

  # Try saving the defaultproject input.
  config.defaultproject = request.POST.get('defaultproject')
  if len(config.defaultproject) == 0:
    errors += ["Must give a default project."]

  # Handle submitpassword changes.
  submitpassword = request.POST.get('submitpassword')
  submitpassword2 = request.POST.get('submitpassword2')
  if len(submitpassword) > 0 or len(submitpassword2) > 0:
    # Try saving the submitpassword input.
    if submitpassword == submitpassword2:
      config.submitpassword = createPassword(request.POST.get('submitpassword'))
      config.needsnewpassword = False
    else:
      errors += ["Given passwords do not match."]

  # If there was an error, render the manage page.
  if errors:
    params['errors'] = errors
    return respond('manage.html', request, params=params)

  params['message'] = "Config updated."
  config.put()
  updateCachedConfig(config)
  return respond('manage.html', request, params=params)


@get_required
def sheriff(request):
  params = {}
  return respond('sheriff.js', request, params=params,
                 mimetype='application/x-javascript')


@post_required
def status_listener(request):
  """/status_listener - Receive and process a GAE push from Buildbot."""

  # If in production, check the remote IP is allowed to submit.
  config = loadConfig()
  if not IS_DEV and request.META['REMOTE_ADDR'] not in config.allowedhosts:
    return HttpResponse('Unauthorized.', status=401)

  packets = simplejson.loads(request.POST['packets'])
  if IS_DEV:
    logging.exception(packets)

  # First create a batch of consecutive ids to simplify processing later and
  # reduce the number of database accesses.
  ids_batch = db.allocate_ids(db.Key.from_path('Event', 1), len(packets))
  events = []
  # A list of packets should have been submitted, store each event.
  for index in xrange(len(packets)):
    packet = packets[index]
    event_dict = {
        'id': packet['id'],
        'type': packet['event'],
        'version': packet.get('version', 0),
        'projectname': packet['project'],
    }

    # the projectstarted time from the packet, ...
    ts = time.strptime(packet['started'].split('.')[0], '%Y-%m-%d %H:%M:%S')
    event_dict['projectstarted'] = (
        datetime.datetime.fromtimestamp(time.mktime(ts)))

    # the timestamp of this packet, ...
    ts = time.strptime(packet['timestamp'].split('.')[0], '%Y-%m-%d %H:%M:%S')
    event_dict['timestamp'] = datetime.datetime.fromtimestamp(time.mktime(ts))

    # and extract a dump of the packet.
    if 'payload_json' in packet:
      event_dict['payload'] = packet['payload_json']
    elif 'payload' in packet:
      event_dict['payload'] = simplejson.dumps(packet['payload'])
    else:
      return HttpResponse('event payload missing', status=500)

    # Write this event to the datastore.
    item_key = db.Key.from_path('Event', ids_batch[0] + index)
    events.append(models.Event(key=item_key, **event_dict))

  # The events could be saved one at a time but it would incur much more db
  # accesses so use a batch save instead. That means that we could have a
  # partial success or we could hit DeadlineExceeded. If the write fails, an
  # HTTP 500 will be returned. It is expected that the client tries to push all
  # the events again but in smaller batches.
  db.put(events)
  logging.info('Saved %s events' % len(events))

  # Run the process-event task at most every one second. Attempts to requeue
  # it or queue it after it has completed within that second should be ignored.
  try:
    index = 0
    # Split the events in a specified quantity per task to parallelize events
    # processing.
    # TODO(maruel): Adapt it with the load or put the value in the config.
    EVENTS_PER_TASK = 50
    while index < len(events):
      # id() is guaranteed to be unique so TaskAlreadyExistsError cannot be
      # raised.
      first_id = events[index].key().id()
      number_items = min(len(events) - index, EVENTS_PER_TASK)
      name = 'TaskEvent-%d-%d' % (first_id, number_items)
      # Create the task with a subset of the events to process.
      loop_task = taskqueue.Task(url='/task/process-event', name=name,
          params={'first_id': first_id, 'number_items': number_items})
      loop_task.add('eventqueue')
      logging.info('Queued %s' % name)
      index += number_items
  except taskqueue.TombstonedTaskError:
    pass

  return respond('status_listener.html', request, params={})
