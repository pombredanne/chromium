// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('BuildbotModel');

goog.require('goog.net.XhrIo');

goog.require('BuildbotData');
goog.require('BuildbotConfig');

/**
 * The model for the buildbot which provides data and logic functionality
 * to buildbot.
 * @constructor
 */
BuildbotModel = function() {
  /**
   * Information about all the revisions we are showing. We keep them all
   *  because it's easier to re-draw a line then to modify it.
   * @type {BuildbotData}
   * @private
   */
  this.buildbotdata_ = new BuildbotData();

  /**
   * Information about all the builders we are showing. This contains enough
   * information to show the first 3 rows (projects, category, builders static)
   * @type {dictionary}
   * @private
   */
  this.builders_ = {};

  /**
   * Information about the percentage of builders per project and category. This
   * is to be able to display the correctly.
   * @type {Object}
   * @private
   */
  this.buildersCount_ = {};

  /**
   * Determine if we should update the buildbot.
   * @type {boolean}
   * @private
   */
  this.builderStatusChanged_ = false;

  /**
   * Projects we want to display. Currently hardcoded, but it should be chosen
   * by the user and store in a cookie or preference.
   * @type {Array.<string>}
   * @private
   */
  this.projects_ = ['chromium', 'chromium fyi'];

  /**
   * Branch to look at. It should also be specified by the user.
   * We can display only 1 branch at a time.
   * @type {string}
   * @private
   */
  this.branch_ = 'src';

  /**
   * Define if we want to display the strict mode or not. String mode will
   * display only informations that comes explicitly from buildbot itself,
   * without trying to "guess". This is useful for git or other scm that are not
   * revision based.
   * @type {boolean}
   * @private
   */
  this.strict_ = false;

  /**
   * Dictionary of all the builders that have been hidden by the user.
   * @type {Object}
   * @private
   */
  this.mask_ = {};

  /**
   * Last Revision.
   * @type {number}
   * @private
   */
  this.lastRev_ = 0;
};


/**
 * Asynchronously fetch Revisions from App Engine.
 * @param {function(Object)} callback The XHR response.
 */
BuildbotModel.prototype.getRevs = function(callback) {
  this.execute_('GetRevs', callback, 'chromium', 'src', this.lastRev_);
};


/**
 * Asynchronously fetch Builders from App Engine.
 * @param {function(Object)} callback The XHR response.
 */
BuildbotModel.prototype.getBuilders = function(callback) {
  this.execute_('GetBuilders', callback);
};


/**
 * Process the builders coming in from app engine.
 * @param {Object} event The XHR response.
 */
BuildbotModel.prototype.processBuilders = function(event) {
  if (!this.verify_(event)) {
    return;
  }
  
  // this might be confusing. The transfer is using json, but the information
  // contained for this function is also in json format, so we need to parse
  // it twice. (first in getResponseJson, then in JSON.parse).
  var response = event.target.getResponseJson();
  if (!response) {
    return;
  }

  // Update the builder list.
  var builder_data = goog.json.parse(response);
  if (builder_data) {
    this.updateBuilders_(builder_data);
  }
};


/**
 * Build the array of revisions that need to be updated, then send it to
 * to App Engine. The asynchronous result will be an array of updated
 * revision.
 */
BuildbotModel.prototype.updateRevisions = function() {
  var revisions = this.buildbotdata_.getArray();
  var length = revisions.length;
  var revisionArray = [];
  for (var i = 0; i < length; i++) {
    if (!revisions[i].isDone()) {
      // Needs to be updated.
      var revision = {
        'revision': revisions[i].getRevision(),
        'modified': revisions[i].getModified()
      };
      revisionArray.push(revision);

      // Don't send more than 5 revisions at the time, otherwise the call may
      // fail because the request is too big.
      // TODO(nsylvain): Use POST instead of GET in execute_ so we don't have
      // this problem.
      if (revisionArray.length > 5) {
        this.execute_('UpdateRevs', goog.bind(this.processRevisions, this),
                      'chromium', 'src', goog.json.serialize(revisionArray));
        revisionArray = [];
      }
    }

    // Limit the number of revisions shown on the renderer. If the last revision
    // index is not defined yet, -1, then depend on the maxDefaultRows instead.
    var lastIndex = this.getData().getLastRevisionIndex();
    if (lastIndex == -1 && length > BuildbotConfig.MAX_DEFAULT_ROWS) {
      lastIndex = length - BuildbotConfig.MAX_DEFAULT_ROWS;
    }
    revisions[i].setVisible(i >= lastIndex);
  }

  this.execute_('UpdateRevs', goog.bind(this.processRevisions, this),
                 'chromium', 'src', goog.json.serialize(revisionArray));
};


/**
 * Process the revisions coming in from app engine.
 * @param {Object} event The XHR response.
 */
BuildbotModel.prototype.processRevisions = function(event) {
  if (!this.verify_(event)) {
    return;
  }
  
  // Get the json response. Which is an array of updated revisions.
  var response = event.target.getResponseJson();
  
  // Check if we have a valid response, otherwise inform nothing been processed.
  if (!response) {
    return;
  }

  var length = response.length;  
  for (var rev = 0; rev < length ; rev++) {
    // Update lastRev.
    if (this.lastRev_ < response[rev]['revision']) {
      this.lastRev_ = response[rev]['revision'];
    }
    
    this.getData().updateRevision(response[rev]);
  }
};


/**
 * Returns the buildbot data.
 * @return {Object} the build data.
 */
BuildbotModel.prototype.getData = function() {
  return this.buildbotdata_;
};


/**
 * Send a request to the RPC server, and return the response by
 * calling the callback function
 * Inspired from http://code.google.com/appengine/articles/rpc.html
 * @param {string} functionName The function name to call from the RPC service.
 * @param {function(Object)} callback The callback once a XHR result has been
 *                                    returned.
 * @private
 */
BuildbotModel.prototype.execute_ = function(functionName, callback) {
  // Set the action.
  var query = 'action=' + encodeURIComponent(functionName);
  
  // For each extra arguments, we add it to the query.
  for (var i = 0; i < arguments.length-2; i++) {
    var key = 'arg' + i;
    var val = goog.json.serialize(arguments[i+2]);
    query += '&' + key + '=' + encodeURIComponent(val);
  }
  
  // Add the current time to the query. This is an IE cache workaround
  query += '&time=' + new Date().getTime();
  
  // Make the request.
  goog.net.XhrIo.send('/rpc?' + query, callback);
};


/**
 * Verify if a RPC call was successful. Alert if not.
 * @param {Object} event The XHR Event returned back.
 * @return {boolean} True if RPC verified.
 * @private
 */
BuildbotModel.prototype.verify_ = function(event) {
  if (!event.target.isSuccess()) {
    alert('HO NOES! ' +
          event.target.getStatusText() +
          ' (' +
          event.target.getStatus() +
          ')');
    return false;
  }
  return true;
};


/**
 * Invalidate the builders, most likely because a the status of a builder
 * changed, or we added a new one. We will need to repaint the headers.
 * @private
 */
BuildbotModel.prototype.invalidateBuilders_ = function() {
  this.builderStatusChanged_ = true;
};


/**
 * Invalidates all revisions, most likely because a new builder got added and
 * they all need to be repainted.
 * @private
 */
BuildbotModel.prototype.invalidateRevisions_ = function() {
  this.getData().invalidateRevisions();
};

/**
 * Remove all the masked builder from the builders list passed in parameters.
 * @param {Object} builders The builders collection.
 * @private
 */
BuildbotModel.prototype.stripMask_ = function(builders) {
  // TODO(nsylvain): Rewrite me.
  // this code is wrong. so it has been removed.
  // for each builders, check mask. if exists, then remove.
  // var builder_name = this.mask_[project][category][i].name;
  // var index = builders[project][category].indexOf();
  // if (index >= 0) {
  // builders[project][category].splice(index, 1);
  this.simplify_(builders);
};


/**
 * Simplify an object by removing all the empty nodes.
 * @type {Object} obj Nodes.
 * @private
 */
BuildbotModel.prototype.simplify_ = function(obj) {
  if (typeof obj != 'object') {
    logger.severe('Cannot simplify an object of type ' + typeof obj);
    return;
  }

  for (i in obj) {
    // If this is an object, call recursively.
    if (typeof obj[i] == 'object' && !goog.isArray(obj)) {
      this.simplify_(obj[i]);
    }
    
    // Check if this object is now empty.
    var empty = true;
    for (j in obj[i]) {
      empty = false;
      break;
    }
    
    // Delete the child object if it is empty.
    if (empty) {
      delete obj[i];
    }
  }
};


/**
 * Go through the list of all builders, and update the builderCount member.
 * This object contains the number of builders for each category and project.
 * This is used to be able to get this information quickly when we need to know
 * the td.width we need.
 * @private
 */
BuildbotModel.prototype.updateBuilderCount_ = function() {
  // First, we find the number of builders.  
  var buildersCountTotal = 0;
  for (project in this.builders_) {
    this.buildersCount_[project] = {};
    var buildersCountProject = 0;
    var categoryCountProject = 0;
  
    for (category in this.builders_[project]) {
      categoryCountProject += 1;
      this.buildersCount_[project][category] = {};
      var buildersCountCategory = 0;
      
      for (builder in this.builders_[project][category]) {
        buildersCountTotal += 1;
        buildersCountProject += 1;
        buildersCountCategory += 1;
      }
      this.buildersCount_[project][category]['count'] = buildersCountCategory;
    }
    this.buildersCount_[project]['count'] = buildersCountProject;
    this.buildersCount_[project]['count_category'] = categoryCountProject;
  }
  this.buildersCount_['total'] = buildersCountTotal;
  
  // Then we compute the percentage.
  for (project in this.builders_) {
    this.buildersCount_[project]['percentage'] =
        this.buildersCount_[project]['count'] /
        this.buildersCount_['total'] *
        100;

    for (category in this.builders_[project]) {
      this.buildersCount_[project][category]['percentage'] =
         this.buildersCount_[project][category]['count'] /
         this.buildersCount_['total'] * 
         100;
    }
  }
};


/**
 * Update the status of all builders, given a builder object received from
 * appengine.
 * @param {Object} builders The builders collection.
 * @private
 */
BuildbotModel.prototype.updateBuilders_ = function(builders) {
  // Strip all the builders we don't care about.
  this.stripMask_(builders);
  var statusChanged = false;
  var buildersChanged = false;

  for (project in builders) {
    if (!this.builders_[project]) {
      // This project did not exist before.
      buildersChanged = true;
      this.builders_[project] = {};
    }
    
    for (category in builders[project]) {
      if (!this.builders_[project][category]) {
        // This category did not exist before.
        buildersChanged = true;
        this.builders_[project][category] = {};
      }

      var buildersLength = builders[project][category].length;
      for (i = 0; i < buildersLength; i++) {
        var builderName = builders[project][category][i].name;
        if (!this.builders_[project][category][builderName]) {
          // This builder did not exist before.
          buildersChanged = true;
          this.builders_[project][category][builderName] = {};
        }
        
        // Verify the content.
        var b = builders[project][category][i];
        var cb = this.builders_[project][category][builderName];
        if (cb.state != b.state) {
          this.builders_[project][category][builderName].state = b.state;
          statusChanged = true;
        }
        
        if (cb.order != b.order) {
          this.builders_[project][category][builderName].order = b.order;
          buildersChanged = true;
        }
      }
    }
  }
  
  if (buildersChanged) {
    // Something changed, and it will affect the layout, we need to repaint all
    // the revision.
    this.invalidateRevisions_();
    this.invalidateBuilders_();
    this.updateBuilderCount_();
  }
  
  if (statusChanged) {
    this.invalidateBuilders_();
  }
};