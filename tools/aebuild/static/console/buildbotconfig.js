// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('BuildbotConfig');


/**
 * Enable debugger window.
 * TODO(mhm): reset to false for production.
 * @const
 * @type {boolean}
 */
BuildbotConfig.ENABLE_DEBUG = true;

/**
 * Number of ms between ticks for builders.
 * TODO(mhm): reset to 60000 for production.
 * @const
 * @type {number}
 */
BuildbotConfig.BUILDERS_TIMER_INTERVAL = 10000;

/**
 * Number of ms between ticks for revisions.
 * TODO(mhm): reset to 60000 for production.
 * @const
 * @type {number}
 */
BuildbotConfig.REVISIONS_TIMER_INTERVAL = 2000;

/**
 * Max default rows available in the renderer.
 * @const
 * @type {number}
 */
BuildbotConfig.MAX_DEFAULT_ROWS = 10;

/**
 * Max number of revisions to fetch while scrolling.
 * @const
 * @type {number}
 */
BuildbotConfig.MAX_NEW_REVISIONS = 10;
