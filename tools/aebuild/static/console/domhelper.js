// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('DomHelper');

goog.require('goog.dom');

/**
 * Append a table to domObj and return the first TR object.
 * @param {Node} domObj The DOM to append table to.
 * @return {Node} A first TR object.
 */
DomHelper.appendTable = function(domObj) {
  var info = {
    'width': '100%',
    'class': 'ConsoleSubtable'
  };
  var table = goog.dom.createDom('table', info, '');
  var tr = goog.dom.createDom('tr', '', '');
  goog.dom.appendChild(table, tr);
  goog.dom.appendChild(domObj, table);
  return tr;
};


/**
 * Create a new row in the  This will be marked as "commit pending",
 * which mean it wont be displayed until commit() has been called.
 * @param {string} className
 * @param {string} id
 */
DomHelper.TR = function(className, id) {
  var info = {
    'class': className + ' pending commit',
    'id': id + '_pending'
  };
  return goog.dom.createDom('tr', info, '');
};


/**
 * Create a TD.
 * I hate this function.
 * @param {string} className
 * @param {string} id
 * @param {string} width
 * @param {string} colspan
 * @param {string} value
 */
DomHelper.TD = function(className, id, width, colspan, value) {
  var info = {};
  if (className) {
    info['class'] = className;
  }
  
  if (colspan) {
   info['colspan'] = colspan;
  }

  if (id) {
    info['id'] = id;
  }
  
  if (width) {
    info['width'] = width;
  }
  
  return goog.dom.createDom('td', info, value);
};


/**
 * Append a class name to a dom object.
 * @param {Node} domObject The DOM object
 * @param {string} className The class name to append the dom.
 */
DomHelper.appendClass = function(domObject, className) {
  domObject.className += ' ';
  domObject.className += className;
};


/**
 * Add 2 <td> and append them to the dom object in parameter. This is
 * done to simplify a popular codepath.
 * @param {Node} domObj The DOM object to add to.
 */
DomHelper.add2TD = function(domObj) {
  var td1 = DomHelper.TD(null, null, '1%', null);
  var td2 = DomHelper.TD(null, null, '1%', null);
  goog.dom.appendChild(domObj, td1);
  goog.dom.appendChild(domObj, td2);
};


/**
 * Commit to screen all the uncommited rows.
 */
DomHelper.commit = function() {
  var nodeList = goog.dom.$$('tr', 'pending', goog.dom.$('console'));
  for (var i = 0; i < nodeList.length ; i++) {
    // pending commit or pending delete?
    if (nodeList[i].className.indexOf(' commit') != -1) {
      // Pending commit.
      nodeList[i].className =
          nodeList[i].className.replace(' pending commit', '');
      nodeList[i].id = nodeList[i].id.replace('_pending', '');
    } else {
      goog.dom.removeNode(nodeList[i]);
    }
  }
};
