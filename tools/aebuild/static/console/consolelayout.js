// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('ConsoleLayout');

goog.require('goog.dom');

goog.require('Buildbot');

/**
 * Functions to change the layout of the page.
 * @param {Buildbot} the controller.
 * @constructor
 */
ConsoleLayout = function(controller) {
  this.controller_ = controller;
};

/**
 * Enum for Buildbot Layout.
 * @enum {number}
 */
ConsoleLayout.Mode = {
  COLLAPSE: 1,
  EXPAND: 2,
  MERGE: 3,
  UNMERGE: 4,
};

/**
 * Hides all "details" and "comments" section.
 */
ConsoleLayout.prototype.collapse = function() {
    // Hide all Comments sections.
    var comments = goog.dom.$$('.ConsoleComment');
    for(var i = 0; i < comments.length; i++) {
        comments[i].style.display = 'none';
    }

    // Hide all details sections.
    var details = goog.dom.$$('.ConsoleDetails');
    for(var i = 0; i < details.length; i++) {
        details[i].style.display = 'none';
    }

    // Fix the rounding on the Revision box. (Lower right corner must be round)
    var revisions = goog.dom.$$('.ConsoleRevisionRevision');
    for(var i = 0; i < revisions.length; i++) {
      revisions[i].className = revisions[i].className + ' DevRevCollapse';
    }

    // Fix the rounding on the last category box.
    // (Lower left corner must be round)
    var status = goog.dom.$$('.last');
    for(var i = 0; i < status.length; i++) {
        status[i].className = status[i].className + ' DevStatusCollapse';
    }

    // Hide the collapse and the unmerge buttons.
    goog.dom.$('collapse').style.display = 'none';
    goog.dom.$('unmerge').style.display = 'none';

    // Activate the merge and expand buttons.
    goog.dom.$('expand').style.display = 'inline';
    goog.dom.$('merge').style.display = 'inline';
};


/**
 * Expands the view. This is the opposite of "Collapse"
 */
ConsoleLayout.prototype.expand = function() {
    // Make the comments visible.
    var comments = goog.dom.$$('.ConsoleComment');
    for(var i = 0; i < comments.length; i++) {
        comments[i].style.display = '';
    }

    // Make the details visible.
    var details = goog.dom.$$('.ConsoleDetails');
    for(var i = 0; i < details.length; i++) {
        details[i].style.display = '';
    }

    // Remove the round corner (lower right) for the Revision box.
    var revisions = goog.dom.$$('.ConsoleRevisionRevision');
    for(var i = 0; i < revisions.length; i++) {
        revisions[i].className =
            revisions[i].className.replace('DevRevCollapse', '');
    }

    // Remoe the round corner (lower left) for the last category box.
    var status = goog.dom.$$('.DevStatus');
    for(var i = 0; i < status.length; i++) {
        status[i].className = status[i].className.replace('DevStatusCollapse',
                                                          '');
    }

    // Display the "collapse" and "merge" buttons.
    goog.dom.$('collapse').style.display = 'inline';
    goog.dom.$('merge').style.display = 'inline';

    // Remove the "expand" and "unmerge" buttons.
    goog.dom.$('expand').style.display = 'none';
    goog.dom.$('unmerge').style.display = 'none';
};
