// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('Logger');

goog.require('goog.debug.FancyWindow');
goog.require('goog.debug.Logger');

goog.require('BuildbotConfig');

// Add a console to be able to see the debug messages.
var debugWindow = new goog.debug.FancyWindow('main');
var logger = goog.debug.Logger.getLogger('demo');

// Decide whether to enable logging or not.
debugWindow.setEnabled(BuildbotConfig.ENABLE_DEBUG);
if (BuildbotConfig.ENABLE_DEBUG) {
  debugWindow.init();
}

logger.info('Logger initialized.');

