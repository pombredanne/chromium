// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('Buildbot');

goog.require('goog.Timer');
goog.require('goog.ui.Component');
goog.require('goog.ui.Component.Error');

goog.require('BuildbotConfig');
goog.require('BuildbotModel');
goog.require('ConsoleRenderer');
goog.require('ConsoleScroller');
goog.require('ConsoleLayout');
goog.require('Logger');

/**
 * A Buildbot Console UI Component. Only one instance at a time.
 *
 * @param {Renderer} opt_renderer A renderer that the Buildbot will
 *     display.
 * @param {goog.dom.DomHelper=} opt_domHelper DOM helper to use.
 *
 * @extends {goog.ui.Component}
 * @constructor
 */
Buildbot = function(opt_renderer, opt_domHelper) {
  goog.ui.Component.call(this, opt_domHelper);

  /**
   * @type {BuildbotModel}
   * @private
   */
  this.model_ = new BuildbotModel();

  /**
   * @type {ConsoleRenderer}
   * @private
   */
  this.renderer_ = opt_renderer || new ConsoleRenderer(this.model_);

  /**
   * @type {ConsoleScroller}
   * @private
   */
  this.scroller_ = new ConsoleScroller(this);

  /**
   * @type {ConsoleLayout}
   * @private
   */
  this.layout_ = new ConsoleLayout(this);
};
goog.inherits(Buildbot, goog.ui.Component);
goog.addSingletonGetter(Buildbot);


/**
 * Changes Buildbot Layout. A layout changes the way the current console view
 * looks like such as minified version and full version.
 * @param {ConsoleLayout.Mode} mode The console layout mode.
 */
Buildbot.prototype.setLayout = function(mode) {
  switch (mode) {
    case ConsoleLayout.Mode.COLLAPSE:
      this.getLayout().collapse();
      break;
    case ConsoleLayout.Mode.MERGE:
      // TODO(mhm): Implement Merge Layout.
    case ConsoleLayout.Mode.UNMERGE:
      // TODO(mhm): Implement Unmerge Layout.
    default:
      this.getLayout().expand();
  }
};

/**
 * The current layout that Buildbot sis using.
 * @return {ConsoleLayout.Mode}  The Layout mode.
 */
Buildbot.prototype.getLayout = function() {
  return this.layout_;
};

/**
 * Main function that starts all the rpc connections. This should be called
 * from the OnInit.
 */
Buildbot.prototype.start = function() {
  // Start the main loop that will request the new list of builders every
  // X seconds.
  this.getBuilders();

  // Then we start the main loop that will request new build status every X
  // seconds.
  this.getRevs();

  // Start the Scroller routine.
  this.getScroller().start();
};


/**
 * Get more builders from App Engine.
 */
Buildbot.prototype.getBuilders = function() {
  this.getModel().getBuilders(goog.bind(this.getBuildersCallback, this));
};


/**
 * Get more revisions from App Engine.
 */
Buildbot.prototype.getRevs = function() {
  this.getModel().getRevs(goog.bind(this.getRevsCallback, this));
};


/**
 * Returns the renderer used by this Buildbot to render itself or to decorate
 * an existing element.
 * @return {goog.ui.ContainerRenderer} Renderer used by the Buildbot.
 */
Buildbot.prototype.getRenderer = function() {
  return this.renderer_;
};


/**
 * Returns the Model used by this Buildbot to fetch connections from the
 * AppEngine.
 * @return {BuildbotModel} JSON response.
 */
Buildbot.prototype.getModel = function() {
  return this.model_;
};


/**
 * Returns scroller controller.
 * @return {ConsoleScroller} Console Scroller
 */
Buildbot.prototype.getScroller = function() {
  return this.scroller_;
};


/**
 * Registers the given renderer with the container.  Changing renderers after
 * the container has already been rendered or decorated is an error.
 * @param {goog.ui.ContainerRenderer} renderer Renderer used by the container.
 */
Buildbot.prototype.setRenderer = function(renderer) {
  if (this.getElement()) {
    // Too late.
    throw Error(goog.ui.Component.Error.ALREADY_RENDERED);
  }

  this.renderer_ = renderer;
};


/** @inheritDoc */
Buildbot.prototype.createDom = function() {
  this.setElementInternal(this.getRenderer().createDom());
};


/** @inheritDoc */
Buildbot.prototype.decorateInternal = function(element) {
  Buildbot.superClass_.decorateInternal.call(this, element);
  this.getRenderer().decorateDom(element);
};


/** @inheritDoc */
Buildbot.prototype.enterDocument = function() {
  Buildbot.superClass_.enterDocument.call(this);
  this.getRenderer().enterDocument();
};


/** @inheritDoc */
Buildbot.prototype.exitDocument = function() {
  Buildbot.superClass_.exitDocument.call(this);
  this.getRenderer().exitDocument();
};


/**
 * Callback when we receive the current list of builders.
 * @param {Object} event The JSON builders data object.
 */
Buildbot.prototype.getBuildersCallback = function(event) {
  // Process the revisions.
  this.getModel().processBuilders(event);
  //TODO(m0) make sure we use buildbotdata instead.
  if (this.getModel().builderStatusChanged_) {
    this.getRenderer().displayBuilders();
    this.getModel().builderStatusChanged_ = false;
  }
  goog.Timer.callOnce(function() {
    this.getBuilders();
  }, BuildbotConfig.BUILDERS_TIMER_INTERVAL, this);
};


/**
 * Callback when we receive new revisions/builds information.
 * @param {Object} event The JSON revs data object.
 */
Buildbot.prototype.getRevsCallback = function(event) {
  // Process the revisions.
  this.getModel().processRevisions(event);
  this.getModel().updateRevisions();
  this.getRenderer().displayRevisions();
  goog.Timer.callOnce(function() {
    this.getRevs();
  }, BuildbotConfig.REVISIONS_TIMER_INTERVAL, this);
};


/**
 * Get more revisions for this view by fetching more downwards.
 */
Buildbot.prototype.getMoreRevisions = function() {
  // TODO(mhm): Make this smarter.
  var data = this.getModel().getData();
  var revisions = data.getArray();

  // Retrieve the last revision index to show on the renderer. If it's still -1,
  // that means we have to depend on the max rows available.
  var prevLastRevisionIndex = data.getLastRevisionIndex();
  if (prevLastRevisionIndex == -1) {
    prevLastRevisionIndex = revisions.length < BuildbotConfig.MAX_DEFAULT_ROWS ?
        0 : revisions.length - BuildbotConfig.MAX_DEFAULT_ROWS;
  }

  // Go down the stack to see how many revisions we have left.
  var lastRevisionIndex = prevLastRevisionIndex -
      BuildbotConfig.MAX_NEW_REVISIONS;
  if (lastRevisionIndex < 0) {
    lastRevisionIndex = 0;
  }

  // We need to display the revisions that are not visible so it can be added
  // back to the end of the DOM.
  for (var i = prevLastRevisionIndex; i >= lastRevisionIndex; i--) {
    var revision = revisions[i];
    if (!revision.isVisible()) {
      this.getRenderer().displayRevision(revision, false);
    }
  }

  // Increase the max revisions allowed to be viewed.
  data.setLastRevisionIndex(lastRevisionIndex);
  DomHelper.commit();
};


/**
 * Sets the state of the buildbot to active.
 * @param {boolean} val True if active.
 */
Buildbot.prototype.setActive = function(val) {
  this.active_ = status;
};