// Copyright (c) 2010 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

goog.provide('BuildbotData');

goog.require('Revision');

/**
 * The class contains all the informations to be displayed on the page.
 *
 * TODO(nsylvain): Also add the builders data.
 */
BuildbotData = function() {
  /**
   * Array of all the revisions we are showing.
   * @type {Array.<Revision>}
   * @private
   */
  this.revisionArray_ = [];

  /**
   * Last revision index that is aquired via user initiated event, such as
   * Scrolling. We need to keep that index visible at all times since the user
   * requested it.
   * @type {number}
   * @private
   */
  this.lastRevisionIndex_ = -1;
};


/**
 * Find a revision and return it.
 * @param {string} revision The revision to look for.
 * @return {Revision} The matching revision, or null.
 */  
BuildbotData.prototype.findRevision = function(revision) {
  var length = this.revisionArray_.length;
  for (i = 0; i < length; i++) {
    if (this.revisionArray_[i].getRevision() == revision) {
      return this.revisionArray_[i];
    }
  }
  
  return null;
};


/**
 * Update or add a revision to the array.
 * @param {Object} revisionDict The revisions to update or add.
 */  
BuildbotData.prototype.updateRevision = function(revisionDict) {
  var revision = this.findRevision(revisionDict.revision);
  if (!revision) {
    revision = new Revision();
    this.revisionArray_.push(revision);
  }

  revision.fromDict(revisionDict);
};


/**
 * Return the array of all the revisions to show.
 * @return {Array.<Revision>} All the revisions.
 */  
BuildbotData.prototype.getArray = function() {
  return this.revisionArray_;
};


/**
 * Mark all the revisions as invalidated, so they will all be updated on the
 * page.
 */  
BuildbotData.prototype.invalidateRevisions = function() {
  var length = this.revisionArray_.length;
  for (i = 0; i < length; i++) {
    this.revisionArray_[i].invalidate();
  }
};


/**
 * Fetch the last revision allowed to be shown on the renderer.
 * @return {number} The last revision.
 */
BuildbotData.prototype.getLastRevisionIndex = function() {
  return this.lastRevisionIndex_;
};


/**
 * Set the last revision to show on the renderer.
 * @param {number} val The last revision.
 */
BuildbotData.prototype.setLastRevisionIndex = function(val) {
  this.lastRevisionIndex_ = val;
};