# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Parser of gtest (googletest) output."""

import re


# Version number of the parser. Cached results will be re-parsed
# when this changes.
VERSION = 6


# These regular expressions detect start and end of each test case run.
START_RE = re.compile(r'.*\[[\s]*RUN[\s]*\].* ([^\s]+)\.([^\s]+)[\s]*')
END_RE = re.compile(
    r'.*\[[\s]*([^=/()]*)[\s]*\].* (.*)\.(.*) \((\d+) ms\)[\s]*')


# These regular expressions strip out timestamps and process/thread numbers
# etc. from the logs so we can remove more duplicates.
FILTER_3_RE = re.compile(r'\[\d+:\d+:\d+:(.*):(.*)\]')
FILTER_4_RE = re.compile(r'\[\d+:\d+:\d+/\d+:\d+:(.*):(.*)\]')


LOG_LINE_LENGTH_LIMIT = 1000
LOG_LINES_LIMIT = 50
LOG_TRIMMED_MESSAGE = ['\n', '(trimmed by parser)\n', '\n']
LOG_LINE_TRUNCATED_MESSAGE = ' (truncated by parser)'


def trim_long_log(lines):
  for i in range(len(lines)):
    if len(lines[i]) > LOG_LINE_LENGTH_LIMIT:
      lines[i] = lines[i][:LOG_LINE_LENGTH_LIMIT] + LOG_LINE_TRUNCATED_MESSAGE
  if len(lines) < LOG_LINES_LIMIT:
    return lines
  half_length = (LOG_LINES_LIMIT - len(LOG_TRIMMED_MESSAGE)) / 2
  return lines[:half_length] + LOG_TRIMMED_MESSAGE + lines[-half_length:]


def parse(log_contents):
  """
  Return a list of dictionaries, one for each test case, representing
  parsed data we are interested in.
  """
  parsed_data = []
  log_buffer = []
  is_inside_test = False
  test_info = None
  for line in log_contents:
    # Remove the test prefix. It may be different across platforms.
    original_line = line
    line = line.replace('FLAKY_', '').replace('FAILS_', '')

    start_match = None
    match = None
    if '[' in line:
      start_match = START_RE.match(line)
      match = END_RE.match(line)
    if match and not start_match:
      fullname = test_info.group(1).strip() + '.' + test_info.group(2).strip()

      parsed_data.append({'is_successful': 'OK' in match.group(1),
                          'is_crash_or_hang': False,
                          'fullname': fullname.strip(),
                          'test_prefix': test_prefix,
                          'run_time_ms': long(match.group(4).strip()),
                          'log': ''.join(trim_long_log(log_buffer)).strip()})
      log_buffer = []
      is_inside_test = False
      test_info = None
    elif start_match:
      if is_inside_test:
        # Couldn't read the "test end" mark. Probably means a crash or hang.
        fullname = '%s.%s' % (test_info.group(1), test_info.group(2))
        data = {
            'is_successful': False,
            'is_crash_or_hang': True,
            'fullname': fullname,
            'test_prefix': test_prefix,
            'run_time_ms': -1,
            'log': ''.join(trim_long_log(log_buffer)).strip(),
        }
        parsed_data.append(data)
      log_buffer = []
      is_inside_test = True
      test_info = start_match
      test_prefix = ''
      if 'FLAKY_' in original_line:
        test_prefix = 'FLAKY'
      elif 'FAILS_' in original_line:
        test_prefix = 'FAILS'

    elif is_inside_test:
      if '[' in line:
        line = FILTER_3_RE.sub('[\\1:\\2]', line)
        line = FILTER_4_RE.sub('[\\1:\\2]', line)
      log_buffer.append(line)
  if is_inside_test:
    # Couldn't read the "test end" mark. Probably means a crash or hang.
    fullname = '%s.%s' % (test_info.group(1), test_info.group(2))
    data = {
        'is_successful': False,
        'is_crash_or_hang': True,
        'fullname': fullname,
        'test_prefix': test_prefix,
        'run_time_ms': -1,
        'log': ''.join(trim_long_log(log_buffer)).strip(),
    }
    parsed_data.append(data)
  return parsed_data
