# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import appengine_config

import datetime
import logging
import os.path
import pickle
import sys
import urllib

from google.appengine.ext import blobstore, deferred, webapp
from google.appengine.ext.webapp import blobstore_handlers, template
from google.appengine.ext.webapp.util import run_wsgi_app

import app
import gtest_parser


"""When displaying a list of results, how many to display on one page."""
PAGE_SIZE = 100


def _clean_int(value, default):
  """Convert a value to an int, or the default value if conversion fails."""
  try:
    return int(value)
  except (TypeError, ValueError), err:
    return default


class MyRequestHandler(webapp.RequestHandler):
  """Base request handler with this application specific helpers."""

  def _render_template(self, name, values):
    """
    Wrapper for template.render that updates response
    and knows where to look for templates.
    """
    self.response.out.write(template.render(
        os.path.join(os.path.dirname(__file__), 'templates', name),
        values))


class StatusReceiverAction(MyRequestHandler):

  def post(self):
    # This handler should be extremely fast so that buildbot doesn't fail
    # the push and doesn't get stuck on us. Defer all processing to the
    # background.
    try:
      deferred.defer(app.process_status_push, self.request.body, _queue='fast')
    except:
      # For large requests we have to do it now. We can't return HTTP 500
      # because buildbot will try again.
      app.process_status_push(self.request.body)


class FetchBuildersAction(MyRequestHandler):

  def get(self):
    deferred.defer(app.fetch_builders)


class FetchStepsAction(MyRequestHandler):

  def get(self):
    deferred.defer(app.fetch_steps)


class CleanUpBlobsAction(MyRequestHandler):

  def get(self):
    # Only look for at least one week old blobs. Remember that we have eventual
    # consistency, so when checking for orphanness we might miss a witness
    # that given blob is not an orphan and mistakenly delete it.
    creation = datetime.datetime.now() - datetime.timedelta(weeks=1)
    query = blobstore.BlobInfo.all().filter('creation <', creation)
    deferred.defer(app.for_all_entities, query, app.clean_up_blobs)


class UpdateParsedDataAction(MyRequestHandler):

  def get(self):
    query = app.BuildStep.all(keys_only=True)
    query.filter('is_fetched =', True)
    query.filter('is_too_large =', False)
    deferred.defer(app.for_all_entities,
                   query,
                   app.update_parsed_data)


class MainAction(MyRequestHandler):

  def get(self):
    self._render_template('main.html', {})


class GTestQueryAction(MyRequestHandler):

  def get(self):
    gtest_results = []
    cursor = None
    if self.request.get('gtest_query'):
      query = app.GTestResult.all()
      query.filter('fullname =', self.request.get('gtest_query'))
      query.filter('is_successful =', False)
      query.order('-time_finished')
      if self.request.get('cursor'):
        query.with_cursor(start_cursor=self.request.get('cursor'))
      gtest_results = query.fetch(PAGE_SIZE)
      cursor = query.cursor()
    self._render_template('query.html', {
        'gtest_query': self.request.get('gtest_query'),
        'cursor': cursor,
        'gtest_results': gtest_results,
    })


class SuppressionQueryAction(MyRequestHandler):

  def get(self):
    query = app.MemorySuppressionResult.all()
    query.filter('name =', self.request.get('suppression_query'))
    query.order('-time_finished')
    if self.request.get('cursor'):
      query.with_cursor(start_cursor=self.request.get('cursor'))
    suppression_results = query.fetch(PAGE_SIZE)
    self._render_template('suppression_query.html', {
        'suppression_query': self.request.get('suppression_query'),
        'cursor': query.cursor(),
        'suppression_results': suppression_results,
    })


class ListAction(MyRequestHandler):
  """Lists stored build results."""

  def get(self):
    all_steps = app.BuildStep.all().order('-time_finished')
    if self.request.get('buildbot_root'):
      all_steps.filter('buildbot_root =',
                       urllib.unquote(self.request.get('buildbot_root')))
    if self.request.get('step_name'):
      all_steps.filter('step_name =',
                       urllib.unquote(self.request.get('step_name')))
    if self.request.get('status'):
      all_steps.filter('status =', _clean_int(urllib.unquote(
          self.request.get('status')), None))

    if self.request.get('cursor'):
      all_steps.with_cursor(start_cursor=self.request.get('cursor'))

    steps = all_steps.fetch(limit=PAGE_SIZE)
    step_names = app.iterate_large_result(app.StepName.all().order('name'))

    self._render_template('list.html', {
        'buildbot_roots': app.BUILDBOT_ROOTS,
        'step_names': step_names,
        'steps': steps,
        'cursor': all_steps.cursor(),
        'filter_buildbot_root': self.request.get('buildbot_root', ''),
        'filter_step_name': self.request.get('step_name', ''),
        'filter_status': self.request.get('status', ''),
        })


class CrashinessReportAction(MyRequestHandler):
  """Displays summary information about crashy tests."""

  def get(self):
    query = app.GTestResult.all()
    query.filter('is_crash_or_hang =', True)
    query.order('-time_finished')
    if self.request.get('cursor'):
      query.with_cursor(start_cursor=self.request.get('cursor'))
    gtest_results = query.fetch(PAGE_SIZE)
    self._render_template('crashiness_report.html', {
        'gtest_results': gtest_results,
        'cursor': query.cursor(),
    })


class FailsReportAction(MyRequestHandler):
  """Displays summary information about tests marked FAILS."""

  def get(self):
    query = app.GTestResult.all()
    query.filter('test_prefix =', 'FAILS')
    query.filter('is_successful =', True)
    query.order('-time_finished')
    if self.request.get('cursor'):
      query.with_cursor(start_cursor=self.request.get('cursor'))
    gtest_results = query.fetch(PAGE_SIZE)
    self._render_template('fails_report.html', {
        'gtest_results': gtest_results,
        'cursor': query.cursor(),
    })


class ViewRawLogAction(blobstore_handlers.BlobstoreDownloadHandler):
  """Sends selected log file to the user."""

  def get(self, blobkey):
    blob_info = blobstore.BlobInfo.get(urllib.unquote(blobkey))
    if not blob_info:
      self.error(404)
      return
    self.send_blob(blob_info)


application = webapp.WSGIApplication(
  [('/', MainAction),
   ('/gtest_query', GTestQueryAction),
   ('/suppression_query', SuppressionQueryAction),
   ('/list', ListAction),
   ('/crashiness_report', CrashinessReportAction),
   ('/fails_report', FailsReportAction),
   ('/status_receiver', StatusReceiverAction),
   ('/tasks/clean_up_blobs', CleanUpBlobsAction),
   ('/tasks/fetch_builders', FetchBuildersAction),
   ('/tasks/fetch_steps', FetchStepsAction),
   ('/tasks/update_parsed_data', UpdateParsedDataAction),
   ('/viewlog/raw/(.*)', ViewRawLogAction)])


def main():
  run_wsgi_app(application)


if __name__ == '__main__':
  main()
