#!/usr/bin/env python
# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import unittest

import gtest_parser

class TestGTestParser(unittest.TestCase):
  def test_empty(self):
    self.assertEqual([], gtest_parser.parse(''.splitlines(True)))

  def test_pass(self):
    test_log = """[ RUN      ] SimpleTest.TestCase
[       OK ] SimpleTest.TestCase (42 ms)
"""
    self.assertEqual([{
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': 42,
      'test_prefix': '',
      'log': '',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_fail(self):
    test_log = """[ RUN      ] SimpleTest.TestCase
test/test.cc:123: Some Failure
[  FAILED  ] SimpleTest.TestCase (18 ms)
"""
    self.assertEqual([{
      'is_successful': False,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': 18,
      'test_prefix': '',
      'log': 'test/test.cc:123: Some Failure',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_crash(self):
    test_log = """[ RUN      ] SimpleTest.TestCase
"""
    self.assertEqual([{
      'is_successful': False,
      'is_crash_or_hang': True,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': -1,
      'test_prefix': '',
      'log': '',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_crash_log(self):
    test_log = """[ RUN      ] SimpleTest.TestCase
test/test.cc:123: Some Failure
"""
    self.assertEqual([{
      'is_successful': False,
      'is_crash_or_hang': True,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': -1,
      'test_prefix': '',
      'log': 'test/test.cc:123: Some Failure',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_crash_log_not_last(self):
    test_log = """[ RUN      ] SimpleTest.TestCase
test/test.cc:123: Some Failure
Haaaaang! Craaaaaassssshhhhhh!
[ RUN      ] SimpleTest.TestCase2
something
[       OK ] SimpleTest.TestCase2 (9 ms)
"""
    self.assertEqual([{
      'is_successful': False,
      'is_crash_or_hang': True,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': -1,
      'test_prefix': '',
      'log': 'test/test.cc:123: Some Failure\nHaaaaang! Craaaaaassssshhhhhh!',
    }, {
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase2',
      'run_time_ms': 9,
      'test_prefix': '',
      'log': 'something',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_multiple(self):
    test_log = """[ RUN      ] SimpleTest.TestCase1
[       OK ] SimpleTest.TestCase1 (10 ms)
[ RUN      ] SimpleTest.TestCase2
[       OK ] SimpleTest.TestCase2 (9 ms)
[ RUN      ] SimpleTest.TestCase3
[       OK ] SimpleTest.TestCase3 (8 ms)
"""
    self.assertEqual([{
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase1',
      'run_time_ms': 10,
      'test_prefix': '',
      'log': '',
    }, {
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase2',
      'run_time_ms': 9,
      'test_prefix': '',
      'log': '',
    }, {
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase3',
      'run_time_ms': 8,
      'test_prefix': '',
      'log': '',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_multiple_noise(self):
    test_log = """noise-noise
[ RUN      ] SimpleTest.TestCase1
[       OK ] SimpleTest.TestCase1 (10 ms)
[ RUN      ] SimpleTest.TestCase2
[       OK ] SimpleTest.TestCase2 (9 ms)
[ --  -    ] some more noise
garbagegarbage
[ RUN      ] SimpleTest.TestCase3
real log
second line
[       OK ] SimpleTest.TestCase3 (8 ms)
"""
    self.assertEqual([{
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase1',
      'run_time_ms': 10,
      'test_prefix': '',
      'log': '',
    }, {
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase2',
      'run_time_ms': 9,
      'test_prefix': '',
      'log': '',
    }, {
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase3',
      'run_time_ms': 8,
      'test_prefix': '',
      'log': 'real log\nsecond line',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_single_params(self):
    test_log = """[ RUN      ] ExtensionDevToolsBrowserTest.FLAKY_TimelineApi
/b/build/slave/chromium-mac-flaky-builder-dbg/build/src/chrome/browser/extensions/extension_devtools_browsertests.cc:90: Failure
Value of: result
  Actual: false
Expected: true
[  FAILED  ] ExtensionDevToolsBrowserTest.FLAKY_TimelineApi, where TypeParam =  and GetParam() =  (814 ms)"""
    self.assertEqual([{
      'is_successful': False,
      'is_crash_or_hang': False,
      'fullname': 'ExtensionDevToolsBrowserTest.TimelineApi',
      'run_time_ms': 814,
      'test_prefix': 'FLAKY',
      'log': """/b/build/slave/chromium-mac-flaky-builder-dbg/build/src/chrome/browser/extensions/extension_devtools_browsertests.cc:90: Failure
Value of: result
  Actual: false
Expected: true"""
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_line_endings(self):
    test_log = """[ RUN      ] SimpleTest.TestCase\r\n[       OK ] SimpleTest.TestCase (42 ms)\r\n"""
    self.assertEqual([{
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': 42,
      'test_prefix': '',
      'log': '',
    }], gtest_parser.parse(test_log.splitlines(True)))

  def test_truncate(self):
    test_log = '\n'.join(['[ RUN      ] SimpleTest.TestCase'] +
                         ['a' * 5000] + 
                         ['[       OK ] SimpleTest.TestCase (42 ms)'])
    self.assertEqual([{
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': 42,
      'test_prefix': '',
      'log': 'a' * 1000 + ' (truncated by parser)',
    }], gtest_parser.parse(test_log.splitlines(True)))
 
  def test_trim(self):
    test_log = '\n'.join(['[ RUN      ] SimpleTest.TestCase'] +
                         [str(x) for x in range(1000)] + 
                         ['[       OK ] SimpleTest.TestCase (42 ms)'])
    self.assertEqual([{
      'is_successful': True,
      'is_crash_or_hang': False,
      'fullname': 'SimpleTest.TestCase',
      'run_time_ms': 42,
      'test_prefix': '',
      'log': '\n'.join([str(x) for x in range(23)] +
                       ['', '(trimmed by parser)', ''] +
                       [str(x) for x in range(977, 1000)]),
    }], gtest_parser.parse(test_log.splitlines(True)))

if __name__ == '__main__':
  unittest.main()
