Chromebot for distributed crash test.

Summary:

  ChromeBot is a distributed crash detection system.

  Chrome is run on a list of URLs to check for crashes.  A pool of machines is
  used to distribute the workload, each machine might potentially launch
  several instances of Chrome. Crashes detected are symbolicated and saved in
  database.

  Mac chromebot expects chrome to be built with breakpad support, for crash
  detection to work.

  Server Requirements:
   - MySQL database installed
   - MySQLdb installed

  Client Requirements:
   - reliability_tests binary
   - Chrome breakpad symbol files
   - crash_report binary (Mac only)
   - process_dumps_linux.py (Linux only)
   - minidump_stackwalk binary (Linux only)

Modules:

  chromebot.py
    Class Chromebot server and client that does the distributing and
    handling URL jobs.

  results_db.py
    Handles updating database.