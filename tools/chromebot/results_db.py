#!/usr/bin/env python
# Copyright (c) 2012 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Handle Chromebot database related updates.

Run this module with (--setup) once to setup tables.

A brief description on the terminology used here:
  Stack trace: the call stack of all functions when the crash happened.  This
               can be obtained from crash dump processor tool per platform.
  Crash signature: a concatenation of all functions in chrome.dll on the crash
                   stack without the memory address. This is used to determine
                   crash duplicates.
  Full memmory crash dump: contains the entire mapped memory space so the file
                           can be large, around 200MB for Chrome.
  Breakpad minidump: contains current thread's state, name of currently loaded 
                     executable and shared libaries. They are generally less
                     than 1MB.
"""

from cStringIO import StringIO
import logging
import MySQLdb
import optparse
import os
import re

import config


class _ChromebotMySQL(object):
  """MySQL related queries for Chromebot."""

  # Database tables:
  #   builds          Contains information related to each build tested or
  #                   being tested.
  #   crashes         Holds information about each crash.  Each entry is linked
  #                   to an 'id' in 'builds' table by 'build_index'.
  #   crash_dumps     Holds crash dump file contents.
  MYSQL_TABLE_SCHEMA = """
    DROP TABLE IF EXISTS crashes;
    DROP TABLE IF EXISTS crash_dumps;
    DROP TABLE IF EXISTS builds;

    CREATE TABLE builds (
      id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
      build_id VARCHAR(255),
      platform ENUM('win', 'linux', 'mac', 'chromeos'),
      status ENUM('running', 'complete', 'interrupted'),
      crash_count INT DEFAULT 0,
      uploaded_crash_count INT DEFAULT 0,
      total_visited INT DEFAULT 0,
      total_url INT DEFAULT 0,
      completed_time INT DEFAULT 0,
      date TIMESTAMP DEFAULT NOW()
    );

    CREATE TABLE crashes (
      id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
      build_index INT NOT NULL,
      platform ENUM('win', 'linux', 'mac', 'chromeos'),
      url TEXT,
      stack_trace TEXT,
      crash_signature TEXT,
      dump_index INT NOT NULL,
      date TIMESTAMP DEFAULT NOW()
    );

    CREATE TABLE crash_dumps (
      id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
      dump_file_name varchar(250) NOT NULL,
      dump_content longblob,
      date TIMESTAMP DEFAULT NOW()
    );
  """

  def __init__(self):
    self._connection = None
    self._host = config.MySQL.host
    self._port = config.MySQL.port
    self._user = config.MySQL.user
    self._passwd = config.MySQL.GetPassword()
    self._database_name = config.MySQL.database_name
    self._insert_id = None
    self._Connect()

  def Query(self, sql, param=None):
    """Run SQL query.

    Args:
      sql: The query.
      param: Query parameter.

    Returns:
      Result of the query.
    """
    cursor = self._connection.cursor()
    try:
      cursor.execute(sql, param)
      results = cursor.fetchall()
      self._insert_id = cursor.lastrowid
      cursor.close()
      self._connection.commit()
      return results
    except MySQLdb.OperationalError, e:
      if 'MySQL server has gone away' in e[1]:
        self._Connect()
        return self.Query(sql, param)
      else:
        raise

  def GetInsertId(self):
    """Get last row inserted ID.

    Only valid after an insert query on tables with auto increment column.
    """
    return self._insert_id

  def SetupDatabase(self):
    """Drop existing tables and create new tables."""
    # Confirm dropping existing tables.
    user_input = raw_input('Creating new tables will drop the existing one.'
                           '  Continue? (y/n)').strip()
    if user_input != 'y':
      return

    self._Connect()
    self.Query(self.MYSQL_TABLE_SCHEMA)
    self._Disconnect()

  def RemoveOldReports(self):
    """Remove old crash data from database.

    Delete records from 'crashes' and 'crash_dumps' table for builds that are
    older than the specified day in |config.RECORD_TO_KEEP_DAYS|.
    """
    sql = """
        DELETE crashes, crash_dumps FROM crashes LEFT JOIN crash_dumps
        ON crashes.dump_index = crash_dumps.id
        WHERE crashes.build_index IN
        (SELECT id FROM builds
        WHERE DATE_ADD(date, INTERVAL %s DAY) < CURDATE())
    """
    self.Query(sql, (config.RECORD_TO_KEEP_DAYS))

  def _Connect(self):
    self._Disconnect()
    self._connection = MySQLdb.connect(
      host=self._host, port=self._port, user=self._user,
      passwd=self._passwd, db=self._database_name)

  def _Disconnect(self):
    if self._connection:
      self._connection.commit()
      self._connection.close()
      self._connection = None


class CrashReport(object):
  """Handles inserting and updating crash report in database.

  An instance of this class handles all crash reporting related to a
  Chromebot run which is identified by an 'id' in 'builds' table.
  """

  def __init__(self, platform, build_id, total_urls):
    """Initialize CrashReport.

    Old crash reports are removed on initialization.

    Args:
      platform: String with 'win', 'linux', 'mac' or 'chromeos'.
      build_id: Build ID.
    """
    assert platform in (
        'win', 'linux', 'mac', 'chromeos'), 'Unexpected platform %s' % platform
    self._platform = platform
    self._build_id = build_id
    self._num_full_dump_uploaded = 0
    self._report_id = None
    self._db = _ChromebotMySQL()

    self._InitializeBuild()
    self._SetTotalURLs(total_urls)
    self._db.RemoveOldReports()

  def _InitializeBuild(self):
    """Insert a row in 'builds' table and set 'status' as 'running'."""
    sql = """
        INSERT INTO builds(build_id, platform, status)
        VALUES(%s, %s, %s)
    """
    self._db.Query(sql, (self._build_id, self._platform, 'running'))
    self._report_id = self._db.GetInsertId()

  def SetTestStatus(self, status):
    """Set testing status for a build in 'builds' table.

    Args:
      status: String for the status of this build with either 'running',
              'complete' or 'interrupted'.
    """
    assert status in (
        'running', 'complete', 'interrupted'), 'Unexpected status %s' % status
    sql = """
        UPDATE builds
        SET status = %s WHERE id = %s
    """
    self._db.Query(sql, (status, self._report_id))

  def UpdateURLDone(self, num_urls):
    """Update number of URL visited in 'builds' table.

    Args:
      num_urls: Number of URLs.
    """
    sql = """
        UPDATE builds
        SET total_visited = %s
        WHERE id = %s
    """
    self._db.Query(sql, (num_urls, self._report_id))

  def AddCrashCount(self, crash_count):
    """Increment crash count in 'builds' table for this build.

    Args:
      crash_count: Number of crashes.
    """
    sql = """
        UPDATE builds
        SET crash_count = crash_count + %s
        WHERE id = %s
    """
    self._db.Query(sql, (crash_count, self._report_id))

  def UpdateCompleteTime(self, time_s):
    """Update the testing finish time for this build.

    Args:
      time_s: Time in seconds.
    """
    sql = """
        UPDATE builds
        SET completed_time = %s
        WHERE id = %s
    """
    self._db.Query(sql, (time_s, self._report_id))

  def IsFullDumpUploadMaxed(self):
    """Check if number of full crash dump uploaded is maxed."""
    return self._num_full_dump_uploaded >= config.MAX_FULL_DUMP_UPLOAD

  def UploadCrashReport(self, url, full_dump, mini_dump, stack_trace_file):
    """Upload crash report to Chromebot results database.

    Since full dump files are much larger than mini dump files, we will limit
    the number of full dump files uploaded to the database per Chromebot run.
    Crash signatures will be searched against existing ones in the database, and
    checked against crash signature patterns in the known crashes to ignore
    list.  If they exist, mini dump will be uploaded instead.

    This function expects atleast a full or mini dump file.

    Args:
      url: URL that caused the crash.
      full_dump: File of full crash dump.
      mini_dump: File of mini crash dump.
      stack_trace_file: File containing the stack trace from a crash dump.

    Returns:
      True if upload succeeded, False otherwise.
    """
    if not full_dump and not mini_dump:
      logging.error('No crash dump file given.')
      return False
    if full_dump and not os.path.exists(full_dump):
      logging.error('Full crash dump file does not exist.')
      return False
    if mini_dump and not os.path.exists(mini_dump):
      logging.error('Mini crash dump file does not exist.')
      return False

    if stack_trace_file:
      stack_trace = self._CleanStackTrace(stack_trace_file)
      crash_signature = self._GetCrashSignature(stack_trace_file)
    else:
      stack_trace = ''
      crash_signature = ''

    # Determine if full dump or mini dump needs to be uploaded.
    is_full_dump = True
    if (full_dump and self._num_full_dump_uploaded <
        config.MAX_FULL_DUMP_UPLOAD):
      dump_file = full_dump
      if stack_trace:
        if (self._HasStackTraceInDB(crash_signature) or
            self._IsCrashDumpOnIngoredList(crash_signature)):
          dump_file = mini_dump
          is_full_dump = False
    else:
      dump_file = mini_dump
      is_full_dump = False

    dump_content = open(dump_file).read()
    dump_file_name = os.path.basename(dump_file)

    sql = """
        INSERT INTO crash_dumps(dump_file_name, dump_content)
        VALUES(%s, %s);
        INSERT INTO crashes
        (build_index, platform, url, stack_trace, crash_signature, dump_index)
        VALUES(%s, %s, %s, %s, %s, LAST_INSERT_ID())
    """
    self._db.Query(
        sql,
        (dump_file_name, dump_content, self._report_id, self._platform, url,
         stack_trace, crash_signature))

    self._AddUploadedCrashCount(1)
    if is_full_dump:
      self._num_full_dump_uploaded += 1

  def _SetTotalURLs(self, num_urls):
    """Set the total number of URL to be visited for this test.

    Args:
      num_urls: Number of URLs.
    """
    sql = """
        UPDATE builds
        SET total_url = %s
        WHERE id = %s
    """
    self._db.Query(sql, (num_urls, self._report_id))

  def _AddUploadedCrashCount(self, crash_count):
    """Increment uploaded crash count in 'builds' table for this build.

    Args:
      crash_count: Number of crashes.
    """
    sql = """
        UPDATE builds
        SET uploaded_crash_count = uploaded_crash_count + %s
        WHERE id = %s
    """
    self._db.Query(sql, (crash_count, self._report_id))

  def _CleanStackTrace(self, stack_trace):
    """Remove lines with no useful information in report file.

    Returns:
      String content of cleaned report file.
    """
    clean_str = StringIO()
    fhandler = open(stack_trace)
    if self._platform == 'win':
      found = False
      for line in fhandler:
        if 'Closing open log file' in line:
          break
        if found:
          clean_str.write(line)
        elif '0:000>' in line:
          found = True
      return clean_str.getvalue()
    else:
      return fhandler.read()

  def _GetCrashSignature(self, stack_trace):
    """Get stack trace without the addresses.

    Args:
      stack_trace: File of stack trace.

    Returns:
      String containing crash signatures.
    """
    crash_signatures = StringIO()
    fhandler = open(stack_trace)
    for line in fhandler:
      if re.search('!*::', line):
        split = line.split('!')
        if len(split) > 1:
          crash_signatures.write(split[1])
    return crash_signatures.getvalue()

  def _HasStackTraceInDB(self, crash_signature):
    """Search for existing stack trace in 'crashes' table.

    Args:
      crash_signature: String containing crash signatures.

    Returns:
      True if there's a row with matching crash signatures. False otherwise.
    """
    sql = """
        SELECT id FROM crashes
        WHERE build_index = %s AND crash_signature = %s LIMIT 1
    """
    return self._db.Query(
        sql, (self._report_id, crash_signature))

  def _IsCrashDumpOnIngoredList(self, crash_signature):
    """Check if stack trace is on ignored list.

    |crash_signature| is matched against a list crash signature patterns
    provided by a text file.

    Example pattern:
      PREFIX : `anonymous namespace'::onnomemory
    This matches any crash with these functions at the beginning.

    Args:
      crash_signature: String containing crash signatures.

    Returns:
      True if |config.IGNORED_CRASHES| exists and crash signature is on
      the ignored list. False otherwise.
    """
    if (not config.IGNORED_CRASHES or not
        os.path.exists(config.IGNORED_CRASHES)):
      return False
    crash_signatures = []
    count = 0
    for line in crash_signature.splitlines():
      if '::' in line:
        crash_signatures.append(line)
        count += 1
      if count > 5:
        break

    type_list = ['PREFIX', 'SUBSTRING', 'REGEX']

    ignored_crashes = open(config.IGNORED_CRASHES)
    for line in ignored_crashes:
      if '# ' not in line:
        line_split = line.split(' : ')
        if len(line_split) == 2:
          if line_split[0] in type_list:
            type = line_split[0]
          else:
            type = 'PREFIX'
          if type == 'PREFIX':
            for sig in crash_signatures:
              if re.search('^' + line_split[1], sig):
                return True
          elif type == 'SUBSTRING' or type == 'REGEX':
            for sig in crash_signatures:
              if re.search(line_split[1], sig):
                return True
    return False

  def _CleanUp(self):
    """Set Chromebot run status as 'interrupted' if it's not 'complete'.

    This happens when the program doesn't get a chance to call
    SetTestStatus('complete').
    """
    sql = """
        UPDATE builds
        SET status = 'interrupted' WHERE id = %s
        AND status != 'complete'
    """
    self._db.Query(sql, (self._report_id, self._platform))

  def __del__(self):
    self._CleanUp()


def main():
  parser = optparse.OptionParser()
  parser.add_option(
      '-S', '--setup', action='store_true',
      help='Setup Chromebot MySQL database.  Drop existing database.')

  options, args = parser.parse_args()

  if options.setup:
    db = _ChromebotMySQL()
    db.SetupDatabase()
  else:
    parser.print_help()


if __name__ == '__main__':
  main()
