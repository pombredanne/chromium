#!/usr/bin/env python
# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be found
# in the LICENSE file.

import oauth2
import os
import re
import sys
import urllib

BASE_URL = 'https://chromium-status.appspot.com/'

def read_token(file_name):
    try:
        f = open(file_name, 'r')
    except:
        return None
    contents = eval(f.read())
    f.close()
    return contents

def main():
    treestatus_url = BASE_URL

    args = sys.argv
    if len(args) <= 1:
        print 'usage: %s <tree status>'
        return 0
    message = ' '.join(args[1:])

    home_dir = os.getenv('HOME')
    if not home_dir:
        print "No home directory!"
        return 1
    token_string = read_token(home_dir + '/.cr_oauth_token')
    if not isinstance(token_string, str):
        print "No OAuth token found. Please use authorize-treestatus.py first."
        return 1

    consumer = oauth2.Consumer('anonymous', 'anonymous')
    token = oauth2.Token.from_string(token_string)
    client = oauth2.Client(consumer, token)

    # We need to get the "last_status_key" to successfully submit a tree status
    # change.
    (response, content) = client.request(treestatus_url, 'GET')
    if response['status'] != '200':
        print "Invalid response %s." % response['status']
        return 1
    # Just apply a regex, because we're too lazy to parse HTML properly. This
    # matches line boundaries, so it should never be fooled by "clever" tree
    # statuses. (It should be the first match in any case.)
    match = re.search(
        r'<input type="hidden" name="last_status_key" value="([^"]+)">$',
        content,
        re.MULTILINE);
    if not match:
        print "Didn't find last_status_key. Did someone change chromium-status?"
        return 1
    last_status_key = match.group(1)

    print "Setting tree status to: \"%s\"" % message

    # TODO(viettrungluu): Detect status-change collisions and report them to the
    # user.
    parameters = {'message': message,
                  'last_status_key': last_status_key}
    (response, content) = client.request(treestatus_url, method='POST',
                                         body=urllib.urlencode(parameters))
    if response['status'] != '200':
        print "Invalid response %s." % response['status']
        return 1

    print "Done!"
    return 0

if __name__ == '__main__':
    sys.exit(main())
