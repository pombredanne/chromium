# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import appengine_config

import logging
import re

from google.appengine.ext import deferred, webapp
from google.appengine.ext.webapp.mail_handlers import InboundMailHandler
from google.appengine.ext.webapp.util import run_wsgi_app

import app


OFFLINE_REGEX = re.compile(r'buildslave (.*) was lost')


class BuildbotMailHandler(InboundMailHandler):

  def receive(self, mail_message):
    match = OFFLINE_REGEX.search(mail_message.subject)
    if not match:
      parts = [body.decode() for _, body in mail_message.bodies('text/plain')]
      logging.warn("Unmatched e-mail: subject: [%s] parts: %r" %
                       (mail_message.subject, parts))
      return
    deferred.defer(app.process_offline_alert, match.group(1))


application = webapp.WSGIApplication([BuildbotMailHandler.mapping()])


def main():
  run_wsgi_app(application)


if __name__ == '__main__':
  main()
