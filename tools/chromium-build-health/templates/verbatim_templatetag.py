# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from django import template


register = template.Library()


class VerbatimNode(template.Node):

  def __init__(self, text):
    self.text = text

  def render(self, context):
    return self.text


@register.tag
def verbatim(parser, token):
  text = []
  while True:
    token = parser.tokens.pop(0)
    if token.contents == 'endverbatim':
      break
    if token.token_type == template.TOKEN_VAR:
      text.append('{{')
    elif token.token_type == template.TOKEN_BLOCK:
      text.append('{%')
    text.append(token.contents)
    if token.token_type == template.TOKEN_VAR:
      text.append('}}')
    elif token.token_type == template.TOKEN_BLOCK:
      text.append('%}')
  return VerbatimNode(''.join(text))
