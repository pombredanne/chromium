# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

import appengine_config

import datetime
import os.path
import uuid

from google.appengine.api import channel
from google.appengine.ext import deferred, webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app

import app


class MyRequestHandler(webapp.RequestHandler):
  """Base request handler with this application specific helpers."""

  def _render_template(self, name, values):
    """
    Wrapper for template.render that updates response
    and knows where to look for templates.
    """
    self.response.out.write(template.render(
        os.path.join(os.path.dirname(__file__), 'templates', name),
        values))


class ListAction(MyRequestHandler):

  def get(self):
    client_id = str(uuid.uuid4())
    channel_id = channel.create_channel(client_id)
    self._render_template('list.html', {
        'channel': channel_id,
    })


class GetCurrentStateAction(MyRequestHandler):

  def get(self):
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(app.get_current_state())


class ChannelConnectedAction(MyRequestHandler):

  def post(self):
    client_id = self.request.get('from')

    channel_entity = app.Channel(key_name=client_id, client_id=client_id)
    channel_entity.put()

    channel.send_message(client_id, 'connected')


class ChannelDisconnectedAction(MyRequestHandler):

  def post(self):
    client_id = self.request.get('from')

    # Note: it is important to ensure that the entity we delete
    # is a Channel and not some other entity (otherwise it'd be trivial
    # for anyone to delete arbitrary entities).
    channel_entity = app.Channel.get_by_key_name(client_id)
    if channel_entity:
      channel_entity.delete()


class StatusReceiverAction(MyRequestHandler):

  def post(self):
    # This handler should be extremely fast so that buildbot doesn't fail
    # the push and doesn't get stuck on us. Defer all processing to the
    # background.
    deferred.defer(app.process_status_push, self.request.get('packets'))


application = webapp.WSGIApplication(
  [('/', ListAction),
   ('/json/get_current_state', GetCurrentStateAction),
   ('/_ah/channel/connected/', ChannelConnectedAction),
   ('/_ah/channel/disconnected/', ChannelDisconnectedAction),
   ('/status_receiver', StatusReceiverAction)])


def main():
  run_wsgi_app(application)


if __name__ == '__main__':
  main()
