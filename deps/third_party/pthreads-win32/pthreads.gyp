# Copyright (c) 2009 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

{
  'targets': [
    {
      'target_name': 'pthreads',
      'conditions': [
        ['OS!="win"', {
          'type': 'none',
        }, {
          'type': 'shared_library',
          'sources': [
            'files/pthread.c',
            'files/version.rc',
          ],
          'defines': [
            'PTW32_BUILD',
            'PTW32_RC_MSC',
            # 'PTW32_STATIC_LIB',
          ],
          'msvs_settings': {
            'VCLinkerTool': {
              'ProgramDatabaseFile': '$(OutDir)\\pthreads_dll.pdb',
            },
          },
          'link_settings': {
            'libraries': [
              '-lws2_32.lib',
            ]
          },
          'include_dirs': [
            'files',
            'files/include',
          ],
          'direct_dependent_settings': {
            'include_dirs': [
              'files/include',
            ],
            'defines': [
              # 'PTW32_STATIC_LIB',
            ],
          },
        }],
      ],
      # pthreads-win32 is in LGPL license. DO NO DEPEND on this target unless
      # you are absolutely sure what you are doing!
      'target_name': 'pthreads_static',
      'conditions': [
        ['OS!="win"', {
          'type': 'none',
        }, {
          'type': '<(library)',
          'sources': [
            'files/pthread.c',
            'files/version.rc',
          ],
          'defines': [
            'PTW32_BUILD',
            'PTW32_RC_MSC',
            'PTW32_STATIC_LIB',
          ],
          'include_dirs': [
            'files',
            'files/include',
          ],
          'direct_dependent_settings': {
            'include_dirs': [
              'files/include',
            ],
            'defines': [
              'PTW32_STATIC_LIB',
            ],
          },
        }],
      ],
    },
  ],
}
