Name: pthreads-w32
Version: 2.7.0

Description:
This directory contains the source code of the pthreads-w32 library.

Why it's here:
  The sync engine code has a historical pthreads dependency.  Chromium
  bug http://code.google.com/p/chromium/issues/detail?id=19895 tracks
  the removal of the dependency.

  Building FFmpeg on Windows requires pthreads and this library is built
  as a static library for FFmpeg DLL.

Current distribution is available from:
  http://sourceware.org/pthreads-win32/

License information:
  See http://sourceware.org/pthreads-win32/copying.html
  This implementation is free software, distributed under the GNU Lesser 
  General Public License (LGPL).

Local modifications:
  Relocated .h files as follows to limit overinclusion by Chromium code:
   * Moved files/pthread.h into files/include
   * Moved files/sched.h into files/include
  Removed files as follows to reduce the checkout size:
   * Removed src/tests/* to save 540KB.
   * Removed src/manual/* to save 339KB.
   * Removed src/ChangeLog to save 175KB.
   * Removed src/README.CV, which discusses some subtleties of
     the condition variables implementation, to save 85KB.
  Allow clang to parse sched.h and pthread.h:
   * Allow clang to not recognize __declspec.
