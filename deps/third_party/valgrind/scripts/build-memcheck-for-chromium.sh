#!/bin/bash

# Copyright (c) 2009-2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Script to build Memcheck for use with chromium
export THISDIR=`dirname $0`
source $THISDIR/common.sh

# Use a fixed version of valgrind-variant.
MEMCHECK_VV_REV=136

VG_MEMCHECK_DIR="$VG_SRC_DIR/valgrind-memcheck"
checkout_and_patch_valgrind_variant "$MEMCHECK_VV_REV" "$VG_MEMCHECK_DIR"

cd "$VG_MEMCHECK_DIR/valgrind"

build_valgrind_for_available_platforms
