# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

{
  'targets': [
    {
      'target_name': 'libunwind',
      'product_name': 'unwind',
      'type': '<(library)',
      'sources': [
        'include/config.h',
        'include/dwarf_i.h',
        'include/libunwind-x86_64.h',
        'include/libunwind.h',
        'src/dwarf/Lexpr.c',
        'src/dwarf/Lfde.c',
        'src/dwarf/Lfind_proc_info-lsb.c',
        'src/dwarf/Lparser.c',
        'src/dwarf/Lpe.c',
        'src/dwarf/Lstep.c',
        'src/dwarf/global.c',
        'src/elf64.c',
        'src/elf64.h',
        'src/mi/Ldestroy_addr_space.c',
        'src/mi/Ldyn-extract.c',
        'src/mi/Lfind_dynamic_proc_info.c',
        'src/mi/Lget_accessors.c',
        'src/mi/Lget_fpreg.c',
        'src/mi/Lget_proc_info_by_ip.c',
        'src/mi/Lget_proc_name.c',
        'src/mi/Lget_reg.c',
        'src/mi/Lput_dynamic_unwind_info.c',
        'src/mi/Lset_caching_policy.c',
        'src/mi/Lset_fpreg.c',
        'src/mi/Lset_reg.c',
        'src/mi/backtrace.c',
        'src/mi/dyn-cancel.c',
        'src/mi/dyn-info-list.c',
        'src/mi/dyn-register.c',
        'src/mi/flush_cache.c',
        'src/mi/init.c',
        'src/mi/mempool.c',
        'src/mi/strerror.c',
        'src/os-linux.c',
        'src/os-linux.h',
        'src/x86_64/Lcreate_addr_space.c',
        'src/x86_64/Lget_proc_info.c',
        'src/x86_64/Lget_save_loc.c',
        'src/x86_64/Lglobal.c',
        'src/x86_64/Linit.c',
        'src/x86_64/Linit_local.c',
        'src/x86_64/Linit_remote.c',
        'src/x86_64/Lis_signal_frame.c',
        'src/x86_64/Lregs.c',
        'src/x86_64/Lresume.c',
        'src/x86_64/Lstep.c',
        'src/x86_64/getcontext.S',
        'src/x86_64/init.h',
        'src/x86_64/is_fpreg.c',
        'src/x86_64/regname.c',
        'src/x86_64/setcontext.S',
        'src/x86_64/ucontext_i.h',
        'src/x86_64/unwind_i.h',
      ],
      'defines': [
        'HAVE_CONFIG_H',
        '_GNU_SOURCE',
      ],
      'include_dirs': [
        'src',
        'include',
        'include/tdep',
      ],
      'direct_dependent_settings': {
        'include_dirs': [
          'include',  # Clients expect <libunwind.h> to be a system header.
        ],
      },
    },
  ],
}

# Local Variables:
# tab-width:2
# indent-tabs-mode:nil
# End:
# vim: set expandtab tabstop=2 shiftwidth=2:
