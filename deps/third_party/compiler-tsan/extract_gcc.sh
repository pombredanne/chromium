#!/bin/bash

# Copyright (c) 2011 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Extract the gcc compiler into the current directory. Create the gcc-current
# directory symlink which should point to the current gcc version, and the
# librelite_current.so symlink to the current instrumentation plugin version.

THISDIR=`dirname $0`
cd $THISDIR

GCC_VERSION=4.6.1

GCC=gcc-$GCC_VERSION
GCC_TAR=$GCC.tar
GCC_CURRENT=$PWD/gcc-current
LIBTSAN_PREFIX=$PWD/gcc-tsan/lib/libtsan_
LIBTSAN=${LIBTSAN_PREFIX}${GCC_VERSION}.so
LIBTSAN_CURRENT=${LIBTSAN_PREFIX}current.so

echo "Extracting GCC"

# If the tarball is older than the gcc-current symlink, don't extract it.
# We use stat instead of "f1 -ot f2" to check the symlink's last modification
# timestamp.
if [ -e "$GCC_CURRENT" ]
then
  STAT="stat -c \"%Y\""
  TAR_TIMESTAMP=`eval $STAT $GCC_TAR`
  LINK_TIMESTAMP=`eval $STAT $GCC_CURRENT`
  [ "$TAR_TIMESTAMP" -lt "$LINK_TIMESTAMP" ] && echo "Using the existing GCC binaries from $GCC_CURRENT" && exit 0
fi

tar -xf "$GCC_TAR"
(test -e $GCC/bin/gcc && echo "Successfully extracted $GCC") || (echo "Extracting $GCC failed" && exit 1)
[ -L "$GCC_CURRENT" ] && unlink "$GCC_CURRENT"
ln -s "$GCC" "$GCC_CURRENT" || exit 1
[ -L "$LIBTSAN_CURRENT" ] && unlink "$LIBTSAN_CURRENT"
ln -s "$LIBTSAN" "$LIBTSAN_CURRENT" || exit 1
