vars = {
  "chromium_trunk": "http://src.chromium.org/svn/trunk",
  "nixysa_rev": "75",
  # When updating the chromium rev, you must also update the nss and sqlite
  # revs to match the version pulled-in by Chromium's own DEPS in the new rev.
  "chromium_rev": "97669",
  "chromium_breakpad_rev": "97669",
  "chromium_build_mac_rev": "97669",
  "o3d_code_rev": "239",
  "skia_rev": "2128",
  "gyp_rev": "1064",
  "gtest_rev": "560",
  "gmock_rev": "374",
  "gflags_rev": "30",
  "breakpad_rev": "843",
  "v8_rev": "9603",
  "lss_code_rev": "3",
  "google_toolbox_for_mac_rev": "440",
  "sfntly_revision": "54",
}

deps = {
  "o3d/o3d_assets":
    "http://o3d.googlecode.com/svn/trunk/googleclient/o3d_assets@" + Var("o3d_code_rev"),

  "third_party/antlr3":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/antlr3@" + Var("o3d_code_rev"),

  "third_party/cg":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/cg@" + Var("o3d_code_rev"),

  "third_party/fcollada":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/fcollada@" + Var("o3d_code_rev"),

  "third_party/glew":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/glew@" + Var("o3d_code_rev"),

  "third_party/jsdoctoolkit":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/jsdoctoolkit@" + Var("o3d_code_rev"),

  "third_party/libevent":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/libevent@" + Var("o3d_code_rev"),

  "third_party/makeself":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/makeself@" + Var("o3d_code_rev"),

  "third_party/pdiff":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/pdiff@" + Var("o3d_code_rev"),

  "third_party/zip_utils":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/zip_utils@" + Var("o3d_code_rev"),

  "third_party/selenium_rc":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/selenium_rc@" + Var("o3d_code_rev"),

# Stuff that is O3D specific (from a Chrome point of view).

  "o3d/third_party/glu":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/glu@" + Var("o3d_code_rev"),

  "o3d/third_party/libtxc_dxtn":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/libtxc_dxtn@" + Var("o3d_code_rev"),

  "o3d/third_party/vectormath":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/vectormath@" + Var("o3d_code_rev"),

  "o3d/third_party/nixysa":
    "http://nixysa.googlecode.com/svn/trunk/nixysa@" + Var("nixysa_rev"),

  "o3d/third_party/npapi":
    "http://nixysa.googlecode.com/svn/trunk/third_party/npapi@" + Var("nixysa_rev"),

  "o3d/third_party/ply":
    "http://nixysa.googlecode.com/svn/trunk/third_party/ply-3.1@" + Var("nixysa_rev"),

  "o3d/third_party/gflags":
    "http://google-gflags.googlecode.com/svn/trunk@" + Var("gflags_rev"),

# Stuff from the Chromium tree.

  "third_party/harfbuzz":
    Var("chromium_trunk") + "/src/third_party/harfbuzz@" + Var("chromium_rev"),

  "third_party/libevent":
    Var("chromium_trunk") + "/src/third_party/libevent@" + Var("chromium_rev"),

  "third_party/libjpeg":
    Var("chromium_trunk") + "/src/third_party/libjpeg@" + Var("chromium_rev"),

  "third_party/libpng":
    Var("chromium_trunk") + "/src/third_party/libpng@" + Var("chromium_rev"),

  "third_party/modp_b64":
    Var("chromium_trunk") + "/src/third_party/modp_b64@" + Var("chromium_rev"),

  "third_party/sfntly":
    Var("chromium_trunk") + "/src/third_party/sfntly@" + Var("chromium_rev"),

  "chrome/third_party/wtl":
    Var("chromium_trunk") + "/src/third_party/wtl@" + Var("chromium_rev"),

  "third_party/zlib":
    Var("chromium_trunk") + "/src/third_party/zlib@" + Var("chromium_rev"),

  "third_party/skia":
    "http://skia.googlecode.com/svn/trunk@" + Var("skia_rev"),

  "v8":
    "http://v8.googlecode.com/svn/trunk@" + Var("v8_rev"),

  "testing":
    Var("chromium_trunk") + "/src/testing@" + Var("chromium_rev"),

  "skia":
    Var("chromium_trunk") + "/src/skia@" + Var("chromium_rev"),

  "breakpad":
    Var("chromium_trunk") + "/src/breakpad@" + Var("chromium_breakpad_rev"),

  "base":
    Var("chromium_trunk") + "/src/base@" + Var("chromium_rev"),

  "ipc":
    Var("chromium_trunk") + "/src/ipc@" + Var("chromium_rev"),

  "native_client":
    "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/native_client@" + Var("o3d_code_rev"),

  "net":
    Var("chromium_trunk") + "/src/net@" + Var("chromium_rev"),

  "breakpad/src":
    "http://google-breakpad.googlecode.com/svn/trunk/src@" + Var("breakpad_rev"),

  "testing/gtest":
    "http://googletest.googlecode.com/svn/trunk@" + Var("gtest_rev"),

  "testing/gmock":
    "http://googlemock.googlecode.com/svn/trunk@" + Var("gmock_rev"),

  # Dependency of chrome base
  "third_party/nss":
    Var("chromium_trunk") + "/deps/third_party/nss@101569",

  "third_party/icu":
    Var("chromium_trunk") + "/deps/third_party/icu46@101184",

  # Dependency of nss, even though its DEPS fail to mention that.
  "third_party/sqlite":
    Var("chromium_trunk") + "/src/third_party/sqlite@101034",
    
  "third_party/sfntly/src/sfntly":
    "http://sfntly.googlecode.com/svn/trunk/cpp/src/sfntly@" + Var("sfntly_revision"),
    
  # Stuff needed for GYP to run
  "build":
    Var("chromium_trunk") + "/src/build@" + Var("chromium_rev"),

  "build/mac":
    Var("chromium_trunk") + "/src/build/mac@" + Var("chromium_build_mac_rev"),

  "tools/gyp":
    "http://gyp.googlecode.com/svn/trunk@" + Var("gyp_rev"),
}

deps_os = {
  "win": {
    "third_party/wix_2_0_4221":
      "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/wix_2_0_4221@" + Var("o3d_code_rev"),

    "third_party/cygwin":
      "/trunk/deps/third_party/cygwin@66844",

    "third_party/python_26":
      Var("chromium_trunk") + "/tools/third_party/python_26@89111",

    "third_party/pixman":
      "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/pixman@" + Var("o3d_code_rev"),

    "third_party/cairo":
      "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/cairo@" + Var("o3d_code_rev"),
  },
  "mac": {
    "ui/base":
      Var("chromium_trunk") + "/src/ui/base@" + Var("chromium_rev"),

    "ui/gfx":
      Var("chromium_trunk") + "/src/ui/gfx@" + Var("chromium_rev"),

    "third_party/apple_apsl":
      Var("chromium_trunk") + "/src/third_party/apple_apsl@" + Var("chromium_rev"),

    "third_party/mach_override":
      Var("chromium_trunk") + "/src/third_party/mach_override@" + Var("chromium_rev"),

    "third_party/GTM":
      "http://google-toolbox-for-mac.googlecode.com/svn/trunk@" + Var("google_toolbox_for_mac_rev"),

    "third_party/pixman":
      "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/pixman@" + Var("o3d_code_rev"),

    "third_party/pkg-config":
      "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/pkg-config@" + Var("o3d_code_rev"),

    "third_party/cairo":
      "http://o3d.googlecode.com/svn/trunk/googleclient/third_party/cairo@" + Var("o3d_code_rev"),

    "tools/clang":
	  Var("chromium_trunk") + "/src/tools/clang@" + Var("chromium_rev"),

  },
  "unix": {
    # Linux, really.
    "third_party/lss":
      "http://linux-syscall-support.googlecode.com/svn/trunk/lss@" + Var("lss_code_rev"),
    "tools/xdisplaycheck":
      Var("chromium_trunk") + "/src/tools/xdisplaycheck@" + Var("chromium_rev"),
  },
}

hooks = [
  {
    # A change to a .gyp, .gypi, or to GYP itself should run the generator.
    "pattern": "\\.gypi?$|[/\\\\]src[/\\\\]tools[/\\\\]gyp[/\\\\]|[/\\\\]src[/\\\\]o3d[/\\\\]build[/\\\\]gyp_o3d$|MANIFEST$",
    "action": ["python", "o3d/build/gyp_o3d", "o3d/build/o3d.gyp", "o3d/tests/lab/ChangeResolution/change_resolution.gyp"],
  },
]

