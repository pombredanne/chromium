/*
 * Copyright 2011, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


// Implementations of the abstract Texture2D.
// Texture class for handling Cairo Rendering Mode.

#include "core/cross/cairo/texture_cairo.h"

#include <string.h>

#include "core/cross/cairo/renderer_cairo.h"

namespace o3d {

namespace {

Texture::RGBASwizzleIndices g_gl_abgr32f_swizzle_indices = {0, 1, 2, 3};

}  // anonymous namespace.

namespace o2d {

static const cairo_content_t kCairoContentInvalid =
    static_cast<cairo_content_t>(-1);

static cairo_content_t CairoContentFromO3DFormat(
    Texture::Format format) {
  switch (format) {
    case Texture::ARGB8:
      return CAIRO_CONTENT_COLOR_ALPHA;
    case Texture::XRGB8:
      return CAIRO_CONTENT_COLOR;
    default:
      return kCairoContentInvalid;
  }
  // Cairo also supports a pure-alpha surface, but we don't expose that.
}

Texture2DCairo::Texture2DCairo(ServiceLocator* service_locator,
                               cairo_surface_t* surface,
                               unsigned char* backup_buffer,
                               int backup_buffer_pitch,
                               Texture::Format format,
                               int levels,
                               int width,
                               int height,
                               bool enable_render_surfaces)
    : Texture2D(service_locator,
                width,
                height,
                format,
                levels,
                enable_render_surfaces),
      surface_(surface),
      backup_buffer_(backup_buffer),
      backup_buffer_pitch_(backup_buffer_pitch),
      locked_surface_(NULL),
      content_dirty_(false) {
  DLOG(INFO) << "Texture2DCairo Construct";
  DCHECK_NE(format, Texture::UNKNOWN_FORMAT);
}

// Creates a new texture object from scratch.
Texture2DCairo* Texture2DCairo::Create(ServiceLocator* service_locator,
                                       Texture::Format format,
                                       int levels,
                                       int width,
                                       int height,
                                       bool enable_render_surfaces) {
  cairo_content_t content = CairoContentFromO3DFormat(format);
  if (kCairoContentInvalid == content) {
    DLOG(ERROR) << "Texture format " << format << " not supported by Cairo";
    return NULL;
  }

  RendererCairo* renderer = static_cast<RendererCairo*>(
      service_locator->GetService<Renderer>());
  // NOTE(tschmelcher): Strangely, on Windows if COMPOSITING_TO_IMAGE is turned
  // off in RendererCairo then we actually get much better performance if we use
  // an image surface for textures rather than using CreateSimilarSurface(). But
  // the performance of COMPOSITING_TO_IMAGE narrowly takes first place.
  cairo_surface_t* surface = renderer->CreateSimilarSurface(
      content,
      width,
      height);
  if (!surface) {
    DLOG(ERROR) << "Failed to create texture surface";
    return NULL;
  }

  // If creating an ARGB8 texture then we also need a buffer to back-up the data
  // in its original non-premultiplied alpha format to facilitate
  // Lock()/Unlock().
  unsigned char* backup_buffer = NULL;
  int backup_buffer_pitch = 0;
  if (ARGB8 == format) {
    backup_buffer_pitch = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32,
                                                        width);
    size_t size = backup_buffer_pitch * height;
    backup_buffer = new unsigned char[size];
    memset(backup_buffer, 0, size);
  }

  return new Texture2DCairo(service_locator,
                            surface,
                            backup_buffer,
                            backup_buffer_pitch,
                            format,
                            levels,
                            width,
                            height,
                            enable_render_surfaces);
}

// In 2D: is not really used
const Texture::RGBASwizzleIndices& Texture2DCairo::GetABGR32FSwizzleIndices() {
  NOTIMPLEMENTED();
  return g_gl_abgr32f_swizzle_indices;
}

Texture2DCairo::~Texture2DCairo() {
  cairo_surface_destroy(surface_);
  DLOG(INFO) << "Texture2DCairo Destruct";
}

static void Copy32BppPixels(const unsigned char* src_data,
                            int src_pitch,
                            unsigned char* dst_data,
                            int dst_pitch,
                            unsigned width,
                            unsigned height) {
  for (unsigned i = 0; i < height; ++i) {
    memcpy(dst_data, src_data, width * 4);
    src_data += src_pitch;
    dst_data += dst_pitch;
  }
}

static void ConvertToPremultipliedAlphaPixels(
    const unsigned char* src_data,
    int src_pitch,
    unsigned char* dst_data,
    int dst_pitch,
    unsigned width,
    unsigned height) {
  // Cairo supports only premultiplied alpha, but we get images as
  // non-premultiplied alpha, so we have to convert.
  for (unsigned i = 0; i < height; ++i) {
    for (unsigned j = 0; j < width; ++j) {
      // NOTE: This assumes a little-endian architecture (e.g., x86). It
      // works for RGBA or BGRA where alpha is in byte 3.
      // Get alpha.
      uint8 alpha = src_data[3];
      // Convert each colour.
      for (int i = 0; i < 3; i++) {
        dst_data[i] = (src_data[i] * alpha + 128U) / 255U;
      }
      // Copy alpha.
      dst_data[3] = alpha;
      src_data += 4;
      dst_data += 4;
    }
    src_data += src_pitch - static_cast<int>(width) * 4;
    dst_data += dst_pitch - static_cast<int>(width) * 4;
  }
}

static void CopyCairoSurfaceContent(cairo_surface_t* src,
                                    cairo_surface_t* dst) {
  cairo_t* cr = cairo_create(dst);
  cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
  cairo_pattern_t* pattern = cairo_pattern_create_for_surface(src);
  cairo_set_source(cr, pattern);
  cairo_pattern_destroy(pattern);
  cairo_paint(cr);
  cairo_destroy(cr);
}

// Set the image data to the renderer
void Texture2DCairo::SetRect(int level,
                             unsigned dst_left,
                             unsigned dst_top,
                             unsigned src_width,
                             unsigned src_height,
                             const void* src_data_void,
                             int src_pitch) {
  if (0 != level) {
    // Cairo does not support/need mip-maps.
    return;
  }

  unsigned char* src_data =
      reinterpret_cast<unsigned char*>(const_cast<void*>(src_data_void));

  if (IsImageSurface()) {
    // Copy directly to the image surface's data buffer.

    cairo_surface_flush(surface_);

    unsigned char* dst_data = cairo_image_surface_get_data(surface_);

    int dst_pitch = cairo_image_surface_get_stride(surface_);

    dst_data += dst_top * dst_pitch + dst_left * 4;
  
    if (ARGB8 == format()) {
      ConvertToPremultipliedAlphaPixels(src_data,
                                        src_pitch,
                                        dst_data,
                                        dst_pitch,
                                        src_width,
                                        src_height);
    } else {
      Copy32BppPixels(src_data,
                      src_pitch,
                      dst_data,
                      dst_pitch,
                      src_width,
                      src_height);
    }

    cairo_surface_mark_dirty(surface_);
  } else {
    // No way to get a pointer to a data buffer for the surface, so we have to
    // update it using cairo operations.
    cairo_t* cr = cairo_create(surface_);

    // Create a pattern and surface for the paint operation.
    cairo_pattern_t* pattern;
    cairo_surface_t* tmp_surface;
    if (ARGB8 == format()) {
      // Have to convert to premultiplied alpha, so we need to make a temporary
      // image surface and copy to it.
      tmp_surface = cairo_image_surface_create(
          CAIRO_FORMAT_ARGB32,
          src_width,
          src_height);

      cairo_surface_flush(tmp_surface);

      unsigned char* dst_data = cairo_image_surface_get_data(tmp_surface);

      int dst_pitch = cairo_image_surface_get_stride(tmp_surface);

      ConvertToPremultipliedAlphaPixels(src_data,
                                        src_pitch,
                                        dst_data,
                                        dst_pitch,
                                        src_width,
                                        src_height);

      cairo_surface_mark_dirty(tmp_surface);

      pattern = cairo_pattern_create_for_surface(tmp_surface);
    } else {
      // No conversion needed, so we can paint directly from the input buffer.
      // Except Cairo doesn't fully support negative pitches, so if the input
      // has a negative pitch then reinterpret it as a positive-pitch image and
      // define a transformation matrix that mirrors it.
      if (src_pitch >= 0) {
        tmp_surface = cairo_image_surface_create_for_data(
            src_data,
            CAIRO_FORMAT_RGB24,
            src_width,
            src_height,
            src_pitch);
        pattern = cairo_pattern_create_for_surface(tmp_surface);
      } else {
        tmp_surface = cairo_image_surface_create_for_data(
            src_data + src_pitch * (static_cast<int>(src_height) - 1),
            CAIRO_FORMAT_RGB24,
            src_width,
            src_height,
            -src_pitch);
        pattern = cairo_pattern_create_for_surface(tmp_surface);
        cairo_matrix_t matrix;
        cairo_matrix_init(&matrix, 1, 0, 0, -1, 0, src_height);
        cairo_pattern_set_matrix(pattern, &matrix);
      }
    }

    cairo_surface_destroy(tmp_surface);

    // Now paint it to this texture's surface.
    cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
    cairo_rectangle(cr, dst_left, dst_top, src_width, src_height);
    cairo_translate(cr, dst_left, dst_top);
    cairo_set_source(cr, pattern);
    cairo_pattern_destroy(pattern);
    cairo_fill(cr);
    cairo_destroy(cr);
  }

  if (ARGB8 == format()) {
    // Also need to save the original non-premultiplied alpha data to facilitate
    // Lock()/Unlock().
    Copy32BppPixels(src_data,
                    src_pitch,
                    backup_buffer_.get()
                        + dst_top * backup_buffer_pitch_ + dst_left * 4,
                    backup_buffer_pitch_,
                    src_width,
                    src_height);
  }

  set_content_dirty(true);

  TextureUpdated();
}

bool Texture2DCairo::IsImageSurface() const {
  return cairo_surface_get_type(surface_) == CAIRO_SURFACE_TYPE_IMAGE;
}

// Locks the given mipmap level of this texture for direct pointer access.
bool Texture2DCairo::PlatformSpecificLock(
    int level, void** data, int* pitch, Texture::AccessMode mode) {
  if (0 != level) {
    // Cairo does not support/need mip-maps.
    return false;
  }

  if (ARGB8 == format()) {
    // The data in the Cairo surface is premultiplied alpha, but the caller
    // wants non-premultiplied alpha, so we need to return the data we
    // backed up.
    *data = backup_buffer_.get();
    *pitch = backup_buffer_pitch_;
  } else {
    // Need to get the data out of the Cairo surface.
    // NOTE(tschmelcher): We don't actually need to do this if
    // mode == kWriteOnly since the existing content won't be preserved.
    cairo_surface_t* image_surface;
    if (IsImageSurface()) {
      // It's an image surface so we can get a direct pointer to the data.
      image_surface = surface_;
    } else {
      // Else we need to copy the data out into an image surface.
      image_surface = cairo_image_surface_create(
          CAIRO_FORMAT_RGB24,
          width(),
          height());
      CopyCairoSurfaceContent(surface_, image_surface);
      locked_surface_ = image_surface;
    }
    cairo_surface_flush(image_surface);
    *data = cairo_image_surface_get_data(image_surface);
    *pitch = cairo_image_surface_get_stride(image_surface);
  }

  return true;
}

// Unlocks the given mipmap level of this texture for direct pointer access,
// updating the Cairo surface with the modified content.
bool Texture2DCairo::PlatformSpecificUnlock(int level) {
  if (0 != level) {
    // Cairo does not support/need mip-maps.
    return false;
  }

  AccessMode mode = LockedMode(0);
  if (kWriteOnly == mode || kReadWrite == mode) {
    // The buffer may have been modified. Must replace the current surface
    // contents with the buffer contents.
    if (ARGB8 == format()) {
      cairo_surface_t* target_surface;
      if (IsImageSurface()) {
        target_surface = surface_;
      } else {
        target_surface = cairo_image_surface_create(
            CAIRO_FORMAT_ARGB32,
            width(),
            height());
      }

      cairo_surface_flush(target_surface);

      unsigned char* dst_data = cairo_image_surface_get_data(target_surface);

      int dst_pitch = cairo_image_surface_get_stride(target_surface);

      ConvertToPremultipliedAlphaPixels(
          backup_buffer_.get(),
          backup_buffer_pitch_,
          dst_data,
          dst_pitch,
          width(),
          height());

      cairo_surface_mark_dirty(target_surface);

      if (!IsImageSurface()) {
        CopyCairoSurfaceContent(target_surface, surface_);
        cairo_surface_destroy(target_surface);
      }
    } else {
      if (IsImageSurface()) {
        cairo_surface_mark_dirty(surface_);
      } else {
        cairo_surface_mark_dirty(locked_surface_);
        CopyCairoSurfaceContent(locked_surface_, surface_);
      }
    }
  }

  if (ARGB8 != format() && !IsImageSurface()) {
    cairo_surface_destroy(locked_surface_);
    locked_surface_ = NULL;
  }

  return true;
}

// In 2D: is not really used
RenderSurface::Ref Texture2DCairo::PlatformSpecificGetRenderSurface(
    int mip_level) {
  DCHECK_LT(mip_level, levels());
  NOTIMPLEMENTED();
  return RenderSurface::Ref(NULL);
}

// Returns the implementation-specific texture handle for this texture.
void* Texture2DCairo::GetTextureHandle() const {
  NOTIMPLEMENTED();
  return reinterpret_cast<void*>(NULL);
}

}  // namespace o2d

}  // namespace o3d
