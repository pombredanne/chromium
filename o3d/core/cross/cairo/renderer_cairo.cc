/*
 * Copyright 2011, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// A 2D renderer that uses the Cairo library.

#include "core/cross/cairo/renderer_cairo.h"

#if defined(OS_LINUX)
#include <cairo-xlib.h>
#elif defined(OS_MACOSX)
#include <CoreGraphics/CGBitmapContext.h>
#include <cairo-quartz.h>
#elif defined(OS_WIN)
#include <cairo-win32-private.h>
#include <cairo-win32.h>
#endif

#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/cross/types.h"
#include "core/cross/cairo/layer.h"
#include "core/cross/cairo/texture_cairo.h"

#if defined(OS_MACOSX) || defined(OS_WIN)
// As of OS X 10.6.4, the Quartz 2D drawing API has hardware acceleration
// disabled by default, and if you force-enable it then it actually hurts
// performance instead of improving it (really, go google it). It also turns out
// that the performance of the software implementation in Pixman is about 12%
// faster than the OS X software implementation (measured as CPU usage per
// rendered frame), so we do all compositing with Pixman via an image surface
// and only use the OS to paint the final frame to the screen.

// On Windows COMPOSITING_TO_IMAGE is also slightly faster than compositing with
// GDI.
// TODO(tschmelcher): Profile Windows without COMPOSITING_TO_IMAGE and see if we
// can make it faster.
#define COMPOSITING_TO_IMAGE 1
#endif

namespace o3d {

// This is a factory function for creating 2D Renderer objects.
Renderer* Renderer::Create2DRenderer(ServiceLocator* service_locator) {
  return o2d::RendererCairo::CreateDefault(service_locator);
}

namespace o2d {

RendererCairo::RendererCairo(ServiceLocator* service_locator)
    : Renderer(service_locator),
#if defined(OS_LINUX)
      display_(NULL),
      window_(0),
#elif defined(OS_MACOSX)
      mac_cg_context_(0),
      mac_window_(0),
      mac_window_cg_context_(0),
      mac_current_cg_context_(0),
      mac_last_cg_context_(0),
      mac_last_cg_context_was_bitmap_context_(false),
      needed_double_buffer_(true),
#elif defined(OS_WIN)
      hwnd_(NULL),
#endif
      display_surface_(NULL),
      offscreen_surface_(NULL),
      display_context_(NULL),
      offscreen_context_(NULL),
      fullscreen_(false),
      offscreen_surface_outdated_(false) {
  // Don't need to do anything.
}

RendererCairo::~RendererCairo() {
  Destroy();
}

RendererCairo* RendererCairo::CreateDefault(ServiceLocator* service_locator) {
  return new RendererCairo(service_locator);
}

// Released all hardware resources.
void RendererCairo::Destroy() {
  DLOG(INFO) << "To Destroy";

  DestroyOffscreen();
#if defined(OS_LINUX) || defined(OS_WIN)
  DestroyDisplay();
#endif

#if defined(OS_LINUX)
  display_ = NULL;
  window_ = 0;
#elif defined(OS_MACOSX)
  mac_cg_context_ = 0;
  CGContextRelease(mac_last_cg_context_);
  mac_last_cg_context_ = 0;
#elif defined(OS_WIN)
  hwnd_ = NULL;
#endif
}

// Comparison predicate for STL sort.
static bool LayerZValueLessThan(const Layer* first, const Layer* second) {
  return first->z() < second->z();
}

static void AddRectangleRegion(cairo_t* cr, const Region& region) {
  cairo_rectangle(cr,
                  region.x,
                  region.y,
                  region.width,
                  region.height);
}

typedef double (*rounding_fn)(double);

static void RoundRegion(const Region& in_region,
                        rounding_fn round_x1y1,
                        rounding_fn round_x2y2,
                        Region* out_region) {
  out_region->everywhere = in_region.everywhere;
  out_region->x = (*round_x1y1)(in_region.x);
  out_region->y = (*round_x1y1)(in_region.y);
  out_region->width = (*round_x2y2)(in_region.x + in_region.width)
      - out_region->x;
  out_region->height = (*round_x2y2)(in_region.y + in_region.height)
      - out_region->y;
}

static bool IsSurfaceOpaque(cairo_surface_t* surface) {
  return !(cairo_surface_get_content(surface) & CAIRO_CONTENT_ALPHA);
}

bool RendererCairo::IsDisplayOpaque() {
  return IsSurfaceOpaque(display_surface_);
}

static bool IsPatternOpaque(cairo_pattern_t* pattern) {
  cairo_surface_t* surface;
  return CAIRO_STATUS_SUCCESS == cairo_pattern_get_surface(pattern, &surface)
         && IsSurfaceOpaque(surface);
}

static void PrepareFillRule(cairo_t* cr) {
  // Change clip rule for the paint operations.
  cairo_set_fill_rule(cr, CAIRO_FILL_RULE_EVEN_ODD);
}

void RendererCairo::StartPaint(cairo_t* cr,
                               const Layer* layer,
                               LayerList::const_iterator i) {
  // Save the current drawing state.
  cairo_save(cr);

  // Clip the region outside the current Layer.
  if (!layer->everywhere()) {
    // This should really be layer->region(), but there is a bug in cairo with
    // complex unaligned clip regions so we use an aligned one instead. See
    // http://lists.cairographics.org/archives/cairo/2011-March/021827.html
    // This doesn't really matter though because any sensible web app will
    // align everything to pixel boundaries.
    AddRectangleRegion(cr, layer->outer_clip_region());
    cairo_clip(cr);
  }

  // Clip the regions within other Layers that will obscure this one.
  LayerList::const_iterator start_mask_it = i;
  start_mask_it++;
  ClipArea(cr, start_mask_it);
}

void RendererCairo::EndPaint(cairo_t* cr) {
  cairo_restore(cr);
}

static void PreparePattern(cairo_t* cr, const Layer* layer) {
  // Transform the pattern to fit into the Layer's region.
  cairo_translate(cr, layer->x(), layer->y());
  if (layer->scale_x() != 0.0 && layer->scale_y() != 0.0) {
    cairo_scale(cr, layer->scale_x(), layer->scale_y());
  }

  // Set source pattern.
  cairo_set_source(cr, layer->pattern()->pattern());
}

void RendererCairo::PaintOffscreenToDisplay() {
  cairo_set_source_surface(display_context_, offscreen_surface_, 0, 0);
  // Painting to a Win32 window with the SOURCE operator interacts poorly with
  // window resizing, so on Windows we use the default of OVER.
#ifndef OS_WIN
  cairo_set_operator(display_context_, CAIRO_OPERATOR_SOURCE);
#endif

  cairo_paint(display_context_);
}

void RendererCairo::Paint() {
#ifdef OS_MACOSX
  // On OSX we can't persist the display surface across Paint() calls because
  // of issues with unbalanced CGContextSaveGState/RestoreGState calls, so we
  // have to create and destroy the surface and context for every frame.
  CreateDisplay();
#endif

#if !defined(COMPOSITING_TO_IMAGE) && defined(OS_LINUX)
  if (!offscreen_surface_) {
    // Have to call this here because strangely it breaks rendering if we call
    // it during InitPlatformSpecific() on Linux. Possibly the X11 Window
    // underlying the display_surface_ is not fully initialized until sometime
    // after NPP_SetWindow().
    CreateOffscreen();
  }
#endif

  if (!display_context_ || !offscreen_context_) {
    DLOG(INFO) << "No context(s), cannot paint";
    return;
  }

  bool needs_double_buffer;
#ifdef OS_MACOSX
  // If our CGContextRef is a CGBitmapContext then paints to it do not actually
  // go to the screen, so the browser must be drawing from that buffer to the
  // screen after we draw to it. That means we don't need to do our own
  // double-buffering.
  needs_double_buffer = !UsingCGBitmapContext();
  if (needs_double_buffer && !needed_double_buffer_) {
    // We are changing from single-buffering to double-buffering, so the
    // offscreen surface is now outdated because we weren't necessarily updating
    // all areas of it before.
    offscreen_surface_outdated_ = true;
  }
  needed_double_buffer_ = needs_double_buffer;
#else
  // On other platforms we always get to draw straight to the screen, so we
  // always need to double-buffer.
  needs_double_buffer = true;
#endif

  if (!needs_double_buffer) {
    DLOG_EVERY_N(INFO, 30) << "By-passing internal offscreen buffer";
  }

  cairo_save(offscreen_context_);
  if (!needs_double_buffer) cairo_save(display_context_);

  // Clip areas of the offscreen buffer that don't need to be updated.

  // If the offscreen surface is out-of-date then we must redraw everything.
  if (offscreen_surface_outdated_) {
    AddDisplayRegion(offscreen_context_);
  }

  bool needs_sort = false;
  UpdateRegions(&AddRegionToOffscreen, this, &needs_sort);

  // Clip everything outside the regions defined by the above.
  cairo_clip(offscreen_context_);

  // If any visible layer has had its z-value modified, re-sort the list to get
  // the right draw order.
  if (needs_sort) {
    std::sort(layer_list_.begin(), layer_list_.end(), LayerZValueLessThan);
  }

  PrepareFillRule(offscreen_context_);
  if (!needs_double_buffer) PrepareFillRule(display_context_);

  // Core process of painting.
  for (LayerList::const_iterator i = layer_list_.begin();
       i != layer_list_.end(); i++) {
    Layer* layer = *i;
    if (!layer->ShouldPaint()) continue;

    // Paint the pattern to the off-screen surface, or directly to the display
    // surface if possible.
    bool bypass = false;
    switch (layer->paint_operator()) {
      case Layer::BLEND:
        {
          // OVER preserves opacity of the destination, so it can always by-pass
          // the offscreen surface if double buffering is not needed.
          bypass = !needs_double_buffer;
          cairo_t* cr = bypass ? display_context_ : offscreen_context_;
          StartPaint(cr, layer, i);
          PreparePattern(cr, layer);
          cairo_paint(cr);
          EndPaint(cr);
        }
        break;

      case Layer::BLEND_WITH_TRANSPARENCY:
        {
          // OVER preserves opacity of the destination, so it can always by-pass
          // the offscreen surface if double buffering is not needed.
          bypass = !needs_double_buffer;
          cairo_t* cr = bypass ? display_context_ : offscreen_context_;
          StartPaint(cr, layer, i);
          PreparePattern(cr, layer);
          cairo_paint_with_alpha(cr, layer->alpha());
          EndPaint(cr);
        }
        break;

      case Layer::COPY:
        {
          // SOURCE does not preserve opacity of the destination, so we can only
          // by-pass the offscreen surface if double buffering is not needed AND
          // either the display is opaque, the source is opaque, or we can
          // reinterpret the source as opaque (only done for SOLID patterns). We
          // could also reinterpret for surface patterns whose surfaces are
          // image surfaces, but currently there are no COPY operations for
          // transparent surface patterns in practice.
          cairo_pattern_t* pattern = layer->pattern()->pattern();
          bool can_reinterpret =
              CAIRO_PATTERN_TYPE_SOLID == cairo_pattern_get_type(pattern);
          bypass = !needs_double_buffer
                   && (IsDisplayOpaque()
                       || IsPatternOpaque(pattern)
                       || can_reinterpret);
          bool needs_reinterpret = bypass
                                   && !IsDisplayOpaque()
                                   && !IsPatternOpaque(pattern);
          cairo_t* cr = bypass ? display_context_ : offscreen_context_;
          StartPaint(cr, layer, i);
          PreparePattern(cr, layer);
          cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
          if (needs_reinterpret) {
            // Make a new solid pattern that discards the alpha component of the
            // original (if any).
            double r, g, b;
            cairo_status_t status =
                cairo_pattern_get_rgba(pattern, &r, &g, &b, NULL);
            DCHECK_EQ(CAIRO_STATUS_SUCCESS, status);
            cairo_set_source_rgb(cr, r, g, b);
          }
          cairo_paint(cr);
          EndPaint(cr);
        }
        break;

      case Layer::COPY_WITH_FADING:
        // Cairo has no built-in operator for this, but we can emulate one.
        if (layer->alpha() <= 0.0) {
          // Old versions of Cairo have a bug where cairo_paint_with_alpha()
          // doesn't check if the operator is bounded by masks when deciding if
          // a paint with alpha of 0 should be a no-op, so we explicitly
          // translate that case to a paint of opaque black. This also lets us
          // bypass the offscreen buffer in this situation.
          bypass = !needs_double_buffer;
          cairo_t* cr = bypass ? display_context_ : offscreen_context_;
          StartPaint(cr, layer, i);
          PreparePattern(cr, layer);
          cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
          cairo_set_source_rgb(cr, 0, 0, 0);
          cairo_paint(cr);
          EndPaint(cr);
        } else {
          // Using CAIRO_OPERATOR_IN only works for this if the target is
          // opaque, so if it isn't then we need to composite offscreen
          // regardless of the need for double-buffering.
          bypass = !needs_double_buffer && IsDisplayOpaque();
          cairo_t* cr = bypass ? display_context_ : offscreen_context_;
          // If using accelerated X11 then it is actually slightly faster to
          // first use the CLEAR operator and then use the OVER operator, but
          // with all software back-ends it is much faster to use IN since it
          // touches the memory once instead of twice.
          StartPaint(cr, layer, i);
          PreparePattern(cr, layer);
          cairo_set_operator(cr, CAIRO_OPERATOR_IN);
          cairo_paint_with_alpha(cr, layer->alpha());
          EndPaint(cr);
        }
        break;

      default:
        DCHECK(false);
    }

    if (!needs_double_buffer && !bypass) {
      // We are not double-buffering, but this paint had to be done offscreen,
      // so we need to copy the results to the screen.
      StartPaint(display_context_, layer, i);
      PaintOffscreenToDisplay();
      EndPaint(display_context_);
    }
  }

  cairo_restore(offscreen_context_);
  if (!needs_double_buffer) cairo_restore(display_context_);

  offscreen_surface_outdated_ = false;

  if (needs_double_buffer) {
    // Paint everything we composited above to the screen.
    cairo_save(display_context_);
    PaintOffscreenToDisplay();
    cairo_restore(display_context_);
  }

  // Clear dirtiness.
  for (LayerList::const_iterator i = layer_list_.begin();
       i != layer_list_.end(); i++) {
    Layer* layer = *i;

    // Clear pattern/texture content dirtiness on all layers, since it has been
    // aggregated into the layer.
    Pattern* pattern = layer->pattern();
    if (pattern) {
      pattern->set_content_dirty(false);
      Texture2DCairo* texture = pattern->texture();
      if (texture) {
        texture->set_content_dirty(false);
      }
    }

    if (needs_sort) {
      // If we sorted the list, clear the z dirty flag for everyone, since even
      // layers that were not updated will have been sorted into the right
      // order.
      layer->set_z_dirty(false);
    }

    if (!layer->ShouldPaint() && !layer->GetSavedShouldPaint()) {
      // Was not updated, so any layer dirtiness persists.
      continue;
    }

    // Clear layer dirtiness.
    layer->set_region_dirty(false);
    layer->set_content_dirty(false);

    // Save current region and paintability for reference on the next frame.
    layer->SaveShouldPaint();
    layer->SaveOuterClipRegion();
  }

  layer_ghost_list_.clear();

#ifdef OS_MACOSX
  DestroyDisplay();
#endif
}

void RendererCairo::ClipArea(cairo_t* cr,  LayerList::const_iterator it) {
  for (LayerList::const_iterator i = it; i != layer_list_.end(); i++) {
    // Preparing and updating the Layer.
    Layer* layer = *i;
    if (!layer->ShouldClip()) continue;

    if (!layer->everywhere()) {
      AddDisplayRegion(cr);
      AddRectangleRegion(cr, layer->inner_clip_region());
    }
    cairo_clip(cr);
  }
}

void RendererCairo::AddLayer(Layer* layer) {
  layer_list_.push_back(layer);
}

void RendererCairo::RemoveLayer(Layer* layer) {
  LayerList::iterator i = std::find(layer_list_.begin(),
                                    layer_list_.end(),
                                    layer);
  if (layer_list_.end() == i) {
    return;
  }
  layer_list_.erase(i);
  if (layer->GetSavedShouldPaint()) {
    // Need to remember this layer's region so we can redraw it on the next
    // frame.
    layer_ghost_list_.push_back(layer->GetSavedOuterClipRegion());
  }
}

void RendererCairo::CreateDisplay() {
  CreateDisplaySurface();
  CreateDisplayContext();
}

void RendererCairo::DestroyDisplay() {
  DestroyDisplayContext();
  DestroyDisplaySurface();
}

void RendererCairo::CreateOffscreen() {
  CreateOffscreenSurface();
  CreateOffscreenContext();
}

void RendererCairo::DestroyOffscreen() {
  DestroyOffscreenContext();
  DestroyOffscreenSurface();
}

#ifdef OS_MACOSX
bool RendererCairo::UsingCGBitmapContext() {
  // Annoyingly, there is no way to find out if a CGContext is a CGBitmapContext
  // except by calling a CGBitmapContext accessor method and checking if it
  // returns a valid result, but all CGBitmapContext methods log an error to the
  // syslog if called on a non-CGBitmapContext. So to avoid spamming the syslog
  // with spurious errors we need to cache the result of CGBitmapContext
  // detection.
  if (mac_current_cg_context_ != mac_last_cg_context_) {
    // CGContext has changed, so must check the new one. To avoid the (small)
    // chance of incorrectly returning a cached result for a new context that
    // happens to have the same address as the previous one, we hold a reference
    // to the old one so as to force any new ones to be placed at a different
    // point in memory.
    CGContextRelease(mac_last_cg_context_);
    mac_last_cg_context_ = mac_current_cg_context_;
    CGContextRetain(mac_last_cg_context_);
    mac_last_cg_context_was_bitmap_context_ =
        CGBitmapContextGetData(mac_current_cg_context_) != NULL;
  }
  return mac_last_cg_context_was_bitmap_context_;
}
#endif

void RendererCairo::CreateDisplaySurface() {
  DCHECK(!display_surface_);

#if defined(OS_LINUX)
  display_surface_ = cairo_xlib_surface_create(display_,
                                               window_,
                                               XDefaultVisual(display_, 0),
                                               display_width(),
                                               display_height());
#elif defined(OS_MACOSX)
  if (!mac_window_) {
    if (!mac_cg_context_) {
      // Can't create the surface until we get a valid CGContextRef. If we
      // don't have one it may be because we haven't yet gotten one from
      // HandleCocoaEvent() in main_mac.mm.
      DLOG(INFO) << "No CGContextRef, cannot initialize yet";
      return;
    }
    mac_current_cg_context_ = mac_cg_context_;
  } else {
    // For O2D full-screen, we need to get the context from the WindowRef.
    QDBeginCGContext(GetWindowPort(mac_window_),
                     &mac_window_cg_context_);
    Rect port_bounds;
    GetPortBounds(GetWindowPort(mac_window_), &port_bounds);

    // Flip the CG context vertically -- its origin is at the lower left,
    // but QuickDraw's origin is at the upper left.
    CGContextTranslateCTM(mac_window_cg_context_, 0.0,
                          port_bounds.bottom - port_bounds.top);
    CGContextScaleCTM(mac_window_cg_context_, 1.0, -1.0);
    mac_current_cg_context_ = mac_window_cg_context_;
  }
#ifdef COMPOSITING_TO_IMAGE
  // Check if the CGContext is a CGBitmapContext that draws to an image format
  // that Cairo supports. If so, it is more efficient to create a Cairo image
  // surface for the underlying buffer than to use the provided CGContext, since
  // Pixman is faster than Quartz.
  cairo_format_t format = static_cast<cairo_format_t>(-1);
  if (UsingCGBitmapContext()) {
    // It's a bitmap context; figure out if Cairo supports its format. We allow
    // any kind of RGB colorspace to use a Cairo surface, so in principle if the
    // CG colorspace is specially calibrated then the color may appear different
    // with Cairo than with CG, but any differences would be minor and the
    // performance win would more than justify them.
    CGColorSpaceRef colorspace = CGBitmapContextGetColorSpace(
        mac_current_cg_context_);
    CGBitmapInfo info = CGBitmapContextGetBitmapInfo(mac_current_cg_context_);
    if (CGColorSpaceGetModel(colorspace) == kCGColorSpaceModelRGB
        && CGColorSpaceGetNumberOfComponents(colorspace) == 3
        && CGBitmapContextGetBitsPerPixel(mac_current_cg_context_) == 32
        && CGBitmapContextGetBitsPerComponent(mac_current_cg_context_) == 8
        && (info & kCGBitmapFloatComponents) == 0
        && (info & kCGBitmapByteOrderMask) == kCGBitmapByteOrder32Host) {
      CGBitmapInfo alpha = info & kCGBitmapAlphaInfoMask;
      if (kCGImageAlphaPremultipliedFirst == alpha ||
          kCGImageAlphaFirst == alpha) {
        // Since OXD does not support transparency, the alpha values of the
        // output should always be 255, so we can safely treat a
        // non-premultiplied alpha bitmap as if were premultiplied alpha.
        format = CAIRO_FORMAT_ARGB32;
      } else if (kCGImageAlphaNoneSkipFirst == alpha) {
        format = CAIRO_FORMAT_RGB24;
      }
    }
    if (-1 == format) {
      // Unlikely.
      DLOG(WARNING) << "CGBitmapContext type not supported by Cairo";
    }
  }
  if (-1 != format) {
    // It's a bitmap context of a type we support; use an image surface.
    display_surface_ = cairo_image_surface_create_for_data(
        static_cast<unsigned char*>(
            CGBitmapContextGetData(mac_current_cg_context_)),
        format,
        CGBitmapContextGetWidth(mac_current_cg_context_),
        CGBitmapContextGetHeight(mac_current_cg_context_),
        CGBitmapContextGetBytesPerRow(mac_current_cg_context_));
  } else {
    // Not a bitmap context or odd format; use a CG surface.
#endif
    display_surface_ = cairo_quartz_surface_create_for_cg_context(
        mac_current_cg_context_,
        display_width(),
        display_height());
#ifdef COMPOSITING_TO_IMAGE
  }
#endif
#elif defined(OS_WIN)
  HDC hdc = GetDC(hwnd_);

  display_surface_ = cairo_win32_surface_create(hdc);
#endif

  if (CAIRO_STATUS_SUCCESS != cairo_surface_status(display_surface_)) {
    DLOG(ERROR) << "Failed to create display surface";
    DestroyDisplaySurface();
    return;
  }

#ifdef OS_WIN
  // Check the surface to make sure it has the correct clip box.
  cairo_win32_surface_t* cairo_surface =
      reinterpret_cast<cairo_win32_surface_t*>(display_surface_);
  if (cairo_surface->extents.width != display_width() ||
      cairo_surface->extents.height != display_height()) {
    // The clip box doesn't have the right info.  Need to do the following:
    // 1. Update the surface parameters to the right rectangle.
    // 2. Try to update the DC clip region.
    DLOG(WARNING) << "CreateDisplaySurface updates clip box from (0,0,"
                  << cairo_surface->extents.width << ","
                  << cairo_surface->extents.height << ") to (0,0,"
                  << display_width() << "," << display_height() << ").";
    HRGN hRegion = CreateRectRgn(0, 0, display_width(), display_height());
    int result = SelectClipRgn(hdc, hRegion);
    if (result == ERROR) {
      LOG(ERROR) << "CreateDisplaySurface SelectClipRgn returns ERROR";
    } else if (result == NULLREGION) {
      // This should not happen since the ShowWindow is called.
      LOG(ERROR) << "CreateDisplaySurface SelectClipRgn returns NULLREGION";
    }
    DeleteObject(hRegion);
    cairo_surface->extents.width = display_width();
    cairo_surface->extents.height = display_height();
  }

  ReleaseDC(hwnd_, hdc);
#endif
}

void RendererCairo::DestroyDisplaySurface() {
  if (display_surface_ != NULL) {
    cairo_surface_destroy(display_surface_);
    display_surface_ = NULL;
  }
#if defined(OS_MACOSX)
  if (mac_window_) {
    QDEndCGContext(GetWindowPort(mac_window_),
                   &mac_window_cg_context_);
    mac_window_cg_context_ = NULL;
  }
  mac_current_cg_context_ = NULL;
#endif
}

void RendererCairo::CreateOffscreenSurface() {
  DCHECK(!offscreen_surface_);

  offscreen_surface_ = CreateSimilarSurface(CAIRO_CONTENT_COLOR,
                                            display_width(),
                                            display_height());
  if (!offscreen_surface_) {
    DLOG(ERROR) << "Failed to create offscreen surface";
    return;
  }
  // The created surface is initialized to black, so we need to redraw the
  // whole surface on the next frame.
  offscreen_surface_outdated_ = true;

  ClientInfo::Backend backend;
  switch (cairo_surface_get_type(offscreen_surface_)) {
#if defined(COMPOSITING_TO_IMAGE) || defined(OS_WIN) || defined(OS_LINUX)
    // Even when not using COMPOSITING_TO_IMAGE, an image surface is a possible
    // outcome on Windows and Linux because cairo_surface_create_similar() can
    // fall back to an image surface if it detects that it will be better.
    case CAIRO_SURFACE_TYPE_IMAGE:
      backend = ClientInfo::CAIRO_PIXMAN;
      break;
#endif
#ifndef COMPOSITING_TO_IMAGE
#if defined(OS_WIN)
    case CAIRO_SURFACE_TYPE_WIN32:
      backend = ClientInfo::CAIRO_WIN32;
      break;
#elif defined(OS_MACOSX)
    case CAIRO_SURFACE_TYPE_QUARTZ:
      backend = ClientInfo::CAIRO_QUARTZ;
      break;
#elif defined(OS_LINUX)
    case CAIRO_SURFACE_TYPE_XLIB:
      backend = ClientInfo::CAIRO_XLIB;
      break;
#endif
#endif
    default:
      // Shouldn't get here.
      DCHECK(false);
      backend = ClientInfo::UNKNOWN_BACKEND;
  }
  SetBackend(backend);
}

void RendererCairo::DestroyOffscreenSurface() {
  if (offscreen_surface_) {
    cairo_surface_destroy(offscreen_surface_);
    offscreen_surface_ = NULL;
  }
}

void RendererCairo::CreateDisplayContext() {
  DCHECK(!display_context_);

  if (!display_surface_) {
    DLOG(INFO) << "No display surface, cannot create display context";
    return;
  }
  display_context_ = cairo_create(display_surface_);
  if (CAIRO_STATUS_SUCCESS != cairo_status(display_context_)) {
    DLOG(ERROR) << "Failed to create display context";
    DestroyDisplayContext();
    return;
  }
#if defined(OS_MACOSX) && defined(COMPOSITING_TO_IMAGE)
  // If we created an image surface for a CGBitmapContext, we have to copy the
  // CG state to the Cairo state so that drawing with Cairo does the same thing
  // that CG would do.
  if (CAIRO_SURFACE_TYPE_IMAGE == cairo_surface_get_type(display_surface_)) {
    // First put the Cairo coordinate system into CG conventions.
    cairo_translate(display_context_,
                    0,
                    cairo_image_surface_get_height(display_surface_));
    cairo_scale(display_context_, 1, -1);
    // Now multiply with the CG transform.
    CGAffineTransform cg_matrix = CGContextGetCTM(mac_current_cg_context_);
    cairo_matrix_t cairo_matrix;
    cairo_matrix_init(&cairo_matrix,
                      cg_matrix.a,
                      cg_matrix.b,
                      cg_matrix.c,
                      cg_matrix.d,
                      cg_matrix.tx,
                      cg_matrix.ty);
    cairo_transform(display_context_, &cairo_matrix);
    // Now make a clip region out of the CG clip bounding box. If the CG clip
    // region is not a single box then we will end up updating some pixels of
    // the bitmap that CG wouldn't have updated, but that's fine. There is no
    // API to get the exact clip region.
    CGRect clip_box = CGContextGetClipBoundingBox(mac_current_cg_context_);
    cairo_rectangle(display_context_,
                    clip_box.origin.x,
                    clip_box.origin.y,
                    clip_box.size.width,
                    clip_box.size.height);
    cairo_clip(display_context_);
  }
#endif
}

void RendererCairo::DestroyDisplayContext() {
  if (display_context_) {
    cairo_destroy(display_context_);
    display_context_ = NULL;
  }
}

void RendererCairo::CreateOffscreenContext() {
  DCHECK(!offscreen_context_);

  if (!offscreen_surface_) {
    DLOG(INFO) << "No offscreen surface, cannot create offscreen context";
    return;
  }
  offscreen_context_ = cairo_create(offscreen_surface_);
  if (CAIRO_STATUS_SUCCESS != cairo_status(offscreen_context_)) {
    DLOG(ERROR) << "Failed to create offscreen context";
    DestroyOffscreenContext();
    return;
  }
}

void RendererCairo::DestroyOffscreenContext() {
  if (offscreen_context_) {
    cairo_destroy(offscreen_context_);
    offscreen_context_ = NULL;
  }
}

#if defined(COMPOSITING_TO_IMAGE) || defined(OS_MACOSX)
static cairo_format_t CairoFormatFromCairoContent(cairo_content_t content) {
  switch (content) {
    case CAIRO_CONTENT_COLOR:
      return CAIRO_FORMAT_RGB24;
    case CAIRO_CONTENT_ALPHA:
      return CAIRO_FORMAT_A8;
    case CAIRO_CONTENT_COLOR_ALPHA:
      return CAIRO_FORMAT_ARGB32;
    default:
      DCHECK(false);
      return CAIRO_FORMAT_ARGB32;
  }
}
#endif

cairo_surface_t* RendererCairo::CreateSimilarSurface(cairo_content_t content,
                                                     int width,
                                                     int height) {
  cairo_surface_t* similar;
#if defined(COMPOSITING_TO_IMAGE)
  similar = cairo_image_surface_create(CairoFormatFromCairoContent(content),
                                       width,
                                       height);
#elif defined(OS_LINUX) || defined(OS_WIN)
  if (!display_surface_) {
    DLOG(INFO) << "No display surface, cannot create similar surface";
    return NULL;
  }
  similar = cairo_surface_create_similar(display_surface_,
                                         content,
                                         width,
                                         height);
#else  // OS_MACOSX
  // On OSX we can't use cairo_surface_create_similar() because display_surface_
  // is only valid during Paint(), so instead hard-code what a
  // cairo_surface_create_similar() call would do. Conveniently the OSX
  // implementation doesn't actually make use of the surface argument.
  // (Note that this code path is not actually taken right now because we use
  // COMPOSITING_TO_IMAGE on OSX.)
  similar = cairo_quartz_surface_create(CairoFormatFromCairoContent(content),
                                        width,
                                        height);
#endif

  if (CAIRO_STATUS_SUCCESS != cairo_surface_status(similar)) {
    DLOG(ERROR) << "Failed to create similar surface";
    cairo_surface_destroy(similar);
    similar = NULL;
  }

  return similar;
}

void RendererCairo::AddDisplayRegion(cairo_t* cr) {
  cairo_rectangle(cr, 0, 0, display_width(), display_height());
}

void RendererCairo::AddRegion(cairo_t* cr, const Region& region) {
  if (region.everywhere) {
    AddDisplayRegion(cr);
  } else {
    AddRectangleRegion(cr, region);
  }
}

void RendererCairo::AddRegionToOffscreen(void* context, const Region& region) {
  DCHECK(context);
  RendererCairo* renderer = static_cast<RendererCairo*>(context);
  renderer->AddRegion(renderer->offscreen_context_, region);
}

// Overridden to no-ops since the base implementations are not applicable.
void RendererCairo::InitCommon() {
  NOTIMPLEMENTED();
}

void RendererCairo::UninitCommon() {
  NOTIMPLEMENTED();
}

Renderer::InitStatus RendererCairo::InitPlatformSpecific(
    const DisplayWindow& display_window,
    bool off_screen) {
#if defined(OS_LINUX)
  const DisplayWindowLinux &display_platform =
      static_cast<const DisplayWindowLinux&>(display_window);
  display_ = display_platform.display();
  window_ = display_platform.window();
#elif defined(OS_MACOSX)
  const DisplayWindowMac &display_platform =
      static_cast<const DisplayWindowMac&>(display_window);
  mac_cg_context_ = display_platform.cg_context();
#elif defined(OS_WIN)
  const DisplayWindowWindows &display_platform =
      static_cast<const DisplayWindowWindows&>(display_window);
  hwnd_ = display_platform.hwnd();
  if (NULL == hwnd_) {
    return INITIALIZATION_ERROR;
  }
#endif

  // There are two constraints on initialization order that we need to respect:
  // 1) On Mac, CreateDisplay() has to be done on every frame in Paint().
  // 2) On Linux when not using COMPOSITING_TO_IMAGE, CreateOffscreen() has to
  //    be deferred until the first Paint().
#if defined(OS_LINUX) || defined(OS_WIN)
  CreateDisplay();
#endif
#if defined(COMPOSITING_TO_IMAGE) || !defined(OS_LINUX)
  CreateOffscreen();
#endif

  // Set the maxinum texture size to 4096 so that Cairo renderer can work well
  // with a 2560x1600 monitor.
  static const int kMaxTexture2DCairoSize = 4096;
  set_max_texture_size(kMaxTexture2DCairoSize, kMaxTexture2DCairoSize);

  // Cairo always has native NPOT support. Even if is targeting a hardware
  // accelerated backend, texture coordinates will be managed internally, so
  // there shouldn't be any perf impact to using NPOT textures.
  SetSupportsNPOT(true);

  return SUCCESS;
}

#ifdef OS_MACOSX
bool RendererCairo::ChangeDisplayWindow(const DisplayWindow& display_window) {
  const DisplayWindowMac &display_platform =
      static_cast<const DisplayWindowMac&>(display_window);
  mac_window_ = display_platform.window();
  mac_cg_context_ = display_platform.cg_context();
}

static void AddRegionToRegionSet(void* context, const Region& region) {
  DCHECK(context);
  Region::RegionSet* regions = static_cast<Region::RegionSet*>(context);
  regions->insert(region);
}

bool RendererCairo::GetUpdatedRegions(Region::RegionSet* regions) {
  if (!regions) {
    return false;
  }

  regions->clear();

  UpdateRegions(&AddRegionToRegionSet, regions, NULL);

  return true;
}
#endif

// Handles the plugin resize event.
void RendererCairo::Resize(int width, int height) {
  DLOG(INFO) << "To Resize " << width << " x " << height;
  SetClientSize(width, height);

  DestroyOffscreen();
#if defined(OS_WIN)
  DestroyDisplay();
  CreateDisplay();
#elif defined(OS_LINUX)
  if (display_surface_) {
    cairo_xlib_surface_set_size(display_surface_, width, height);
  }
#endif
#if defined(COMPOSITING_TO_IMAGE) || !defined(OS_LINUX)
  CreateOffscreen();
#endif
}

// The platform specific part of BeginDraw.
bool RendererCairo::PlatformSpecificBeginDraw() {
  return true;
}

// Platform specific version of CreateTexture2D
Texture2D::Ref RendererCairo::CreatePlatformSpecificTexture2D(
    int width,
    int height,
    Texture::Format format,
    int levels,
    bool enable_render_surfaces) {
  return Texture2D::Ref(Texture2DCairo::Create(service_locator(),
                                               format,
                                               levels,
                                               width,
                                               height,
                                               enable_render_surfaces));
}

// The platform specific part of EndDraw.
void RendererCairo::PlatformSpecificEndDraw() {
  // Don't need to do anything.
}

// The platform specific part of StartRendering.
bool RendererCairo::PlatformSpecificStartRendering() {
  return true;
}

// The platform specific part of EndRendering.
void RendererCairo::PlatformSpecificFinishRendering() {
  Paint();
}

// The platform specific part of Present.
void RendererCairo::PlatformSpecificPresent() {
  // Don't need to do anything.
}

// TODO(fransiskusx): DO need to implement before shipped.
// Get a single fullscreen display mode by id.
// Returns true on success, false on error.
bool RendererCairo::GetDisplayMode(int id, DisplayMode* mode) {
  NOTIMPLEMENTED();
  return true;
}

// TODO(fransiskusx):  DO need to implement before shipped.
// Get a vector of the available fullscreen display modes.
// Clears *modes on error.
void RendererCairo::GetDisplayModes(std::vector<DisplayMode>* modes) {
  NOTIMPLEMENTED();
}

// TODO(fransiskusx): DO need to implement before shipped.
// The platform specific part of Clear.
void RendererCairo::PlatformSpecificClear(const Float4 &color,
                                          bool color_flag,
                                          float depth,
                                          bool depth_flag,
                                          int stencil,
                                          bool stencil_flag) {
  // TODO(tschmelcher): This spews useless logging if uncommented. We should
  // refactor the code so that this is never called. 
  //NOTIMPLEMENTED();
}

// TODO(fransiskusx): DO need to implement before shipped.
// Sets the viewport. This is the platform specific version.
void RendererCairo::SetViewportInPixels(int left,
                                        int top,
                                        int width,
                                        int height,
                                        float min_z,
                                        float max_z) {
  NOTIMPLEMENTED();
}

// Turns fullscreen display on.
// Parameters:
//  display: a platform-specific display identifier
//  mode_id: a mode returned by GetDisplayModes
// Returns true on success, false on failure.
bool RendererCairo::GoFullscreen(const DisplayWindow& display,
                                 int mode_id) {
  if (!fullscreen_) {
    DLOG(INFO) << "RendererCairo entering fullscreen";
#if defined(OS_LINUX)
    const DisplayWindowLinux &display_platform =
        static_cast<const DisplayWindowLinux&>(display);
    display_ = display_platform.display();
    window_ = display_platform.window();
    XWindowAttributes window_attributes;
    if (XGetWindowAttributes(display_, window_, &window_attributes)) {
      fullscreen_ = true;
      SetClientSize(window_attributes.width, window_attributes.height);
      DestroyOffscreen();
      DestroyDisplay();
      CreateDisplay();
#ifdef COMPOSITING_TO_IMAGE
      CreateOffscreen();
#endif
    }
#elif defined(OS_WIN)
    DEVMODE dev_mode;
    if (hwnd_ != NULL &&
        EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dev_mode)) {
      int width = dev_mode.dmPelsWidth;
      int height = dev_mode.dmPelsHeight;

      fullscreen_ = true;
      Resize(width, height);
    }
#endif
  }
  return fullscreen_;
}

// Cancels fullscreen display. Restores rendering to windowed mode
// with the given width and height.
// Parameters:
//  display: a platform-specific display identifier
//  width: the width to which to restore windowed rendering
//  height: the height to which to restore windowed rendering
// Returns true on success, false on failure.
bool RendererCairo::CancelFullscreen(const DisplayWindow& display,
                                     int width, int height) {
  if (fullscreen_) {
    DLOG(INFO) << "RendererCairo exiting fullscreen";
#if defined(OS_LINUX)
    // Sync to ensure any operations targeting the dying fullscreen window are
    // done. It's not clear why we need this, but we crash in nspluginwrapper
    // without it.
    XSync(display_, false);
    const DisplayWindowLinux &display_platform =
        static_cast<const DisplayWindowLinux&>(display);
    display_ = display_platform.display();
    window_ = display_platform.window();

    fullscreen_ = false;
    SetClientSize(width, height);
    DestroyOffscreen();
    DestroyDisplay();
    CreateDisplay();
#ifdef COMPOSITING_TO_IMAGE
    CreateOffscreen();
#endif
#elif defined(OS_WIN)
    if (hwnd_ == NULL) {
      // Not initialized.
      return false;
    }
    fullscreen_ = false;
    Resize(width, height);
#endif
  }
  return true;
}

// TODO(fransiskusx): Need to implement it later.
// Tells whether we're currently displayed fullscreen or not.
bool RendererCairo::fullscreen() const {
  return fullscreen_;
}

// Sets the client's size. Overridden from Renderer.
void RendererCairo::SetClientSize(int width, int height) {
  Renderer::SetClientSize(width, height);
}

Layer::Ref RendererCairo::CreateLayer() {
  return Layer::Ref(new Layer(service_locator()));
}

Pattern* RendererCairo::CreateTexturePattern(Pack* pack, Texture* texture) {
  Texture2DCairo* texture_cairo = down_cast<Texture2DCairo*>(texture);
  return Pattern::WrapCairoPattern(
      pack,
      cairo_pattern_create_for_surface(texture_cairo->surface()),
      texture_cairo);
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Sets rendering to the back buffer.
void RendererCairo::SetBackBufferPlatformSpecific() {
  // Don't need to do anything.
  NOTIMPLEMENTED();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Applies states that have been modified (marked dirty).
void RendererCairo::ApplyDirtyStates() {
  // Don't need to do anything.
  // TODO(tschmelcher): This spews useless logging if uncommented. We should
  // refactor the code so that this is never called. 
  //NOTIMPLEMENTED();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates a platform specific ParamCache.
ParamCache* RendererCairo::CreatePlatformSpecificParamCache() {
  NOTIMPLEMENTED();
  return NULL;
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Platform specific version of CreateTextureCUBE.
TextureCUBE::Ref RendererCairo::CreatePlatformSpecificTextureCUBE(
    int edge_length,
    Texture::Format format,
    int levels,
    bool enable_render_surfaces) {
  NOTIMPLEMENTED();
  return TextureCUBE::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
void RendererCairo::PushRenderStates(State* state) {
  // Don't need to do anything.;
  NOTIMPLEMENTED();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Sets the render surfaces on a specific platform.
void RendererCairo::SetRenderSurfacesPlatformSpecific(
    const RenderSurface* surface,
    const RenderDepthStencilSurface* depth_surface) {
  // Don't need to do anything.
  NOTIMPLEMENTED();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates a StreamBank, returning a platform specific implementation class.
StreamBank::Ref RendererCairo::CreateStreamBank() {
  NOTIMPLEMENTED();
  return StreamBank::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates a Primitive, returning a platform specific implementation class.
Primitive::Ref RendererCairo::CreatePrimitive() {
  NOTIMPLEMENTED();
  return Primitive::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates a DrawElement, returning a platform specific implementation
// class.
DrawElement::Ref RendererCairo::CreateDrawElement() {
  NOTIMPLEMENTED();
  return DrawElement::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates and returns a platform specific float buffer
VertexBuffer::Ref RendererCairo::CreateVertexBuffer() {
  NOTIMPLEMENTED();
  return VertexBuffer::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates and returns a platform specific integer buffer
IndexBuffer::Ref RendererCairo::CreateIndexBuffer() {
  NOTIMPLEMENTED();
  return IndexBuffer::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates and returns a platform specific effect object
Effect::Ref RendererCairo::CreateEffect() {
  NOTIMPLEMENTED();
  return Effect::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates and returns a platform specific Sampler object.
Sampler::Ref RendererCairo::CreateSampler() {
  NOTIMPLEMENTED();
  return Sampler::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Returns a platform specific 4 element swizzle table for RGBA UByteN fields.
// The should contain the index of R, G, B, and A in that order for the
// current platform.
const int* RendererCairo::GetRGBAUByteNSwizzleTable() {
  static int swizzle_table[] = { 0, 1, 2, 3, };
  NOTIMPLEMENTED();
  return swizzle_table;
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
// Creates and returns a platform-specific RenderDepthStencilSurface object
// for use as a depth-stencil render target.
RenderDepthStencilSurface::Ref RendererCairo::CreateDepthStencilSurface(
    int width,
    int height) {
  NOTIMPLEMENTED();
  return RenderDepthStencilSurface::Ref();
}

// TODO(fransiskusx): This function is not applicable to 2D rendering.
void RendererCairo::SetState(Renderer* renderer, Param* param) {
  // Don't need to do anything.
  NOTIMPLEMENTED();
}

void RendererCairo::PopRenderStates() {
  NOTIMPLEMENTED();
}

void RendererCairo::UpdateRegions(
    void (*add_region_callback)(void*, const Region&),
    void* context,
    bool* needs_sort) {

  if (needs_sort) {
    *needs_sort = false;
  }

  // Build an initial clip region for all drawing operations that restricts
  // drawing to only areas of the screen that have changed.
  for (LayerList::const_iterator i = layer_list_.begin();
       i != layer_list_.end(); i++) {
    Layer* layer = *i;

    // Aggregate all content dirtiness into the layer's dirtiness property.
    Pattern* pattern = layer->pattern();
    if (pattern) {
      if (pattern->content_dirty()) {
        layer->set_content_dirty(true);
      } else {
        Texture2DCairo* texture = pattern->texture();
        if (texture && texture->content_dirty()) {
          layer->set_content_dirty(true);
        }
      }
    }

    if (!layer->ShouldPaint() && !layer->GetSavedShouldPaint()) {
      // Won't be painted now and wasn't painted on the last frame either, so
      // doesn't need to be redrawn.
      continue;
    }

    if (layer->ShouldPaint() && layer->z_dirty() && needs_sort) {
      // If the z-value has changed and it is going to be painted then we need
      // to re-sort.
      *needs_sort = true;
    }

    if (layer->region_dirty()) {
      // Recompute clip regions.
      RoundRegion(layer->region(), &ceil, &floor, &layer->inner_clip_region());
      RoundRegion(layer->region(), &floor, &ceil, &layer->outer_clip_region());
    }

    // TODO(tschmelcher): This is actually only an upper bound of the minimal
    // clip region. In the case where region X is partially occluded by region Y
    // and the content of X has changed but the content of Y has not, this will
    // redraw all of X when in fact we only need to redraw the set-takeaway of
    // X minus Y.
    if (add_region_callback && layer->ShouldPaint() &&
        (layer->region_dirty() || layer->content_dirty() ||
         !layer->GetSavedShouldPaint())) {
      // If it is visible and the region, content, or visibility has changed
      // then the current region must be redrawn.
      (*add_region_callback)(context, layer->outer_clip_region());
    }

    if (add_region_callback && layer->GetSavedShouldPaint() &&
        (layer->region_dirty() || !layer->ShouldPaint())) {
      // If the region or visibility has changed and it was visible before then
      // the old region must be redrawn.
      (*add_region_callback)(context, layer->GetSavedOuterClipRegion());
    }
  }

  // Must also redraw the regions of any formerly visible layers.
  if (add_region_callback) {
    for (RegionList::const_iterator i = layer_ghost_list_.begin();
         i != layer_ghost_list_.end(); ++i) {
      (*add_region_callback)(context, *i);
    }
  }
}

}  // namespace o2d

}  // namespace o3d
