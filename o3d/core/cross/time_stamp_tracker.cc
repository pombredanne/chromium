/*
 * Copyright 2011, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include "core/cross/error.h"
#include "core/cross/time_stamp_tracker.h"

namespace o3d {

TimeStampTracker::TimeStampTracker(ServiceLocator* service_locator)
    : service_locator_(service_locator),
      time_stamp_pos_(0),
      estimated_frame_duration_(1/30.0F),
      last_time_stamp_(-1),
      num_out_of_order_pairs_(0),
      num_time_stamps_detected_(0) {
  for (int i = 0; i < kNumTimestamps; ++i) {
    time_stamps_[i] = 0;
  }
}

void TimeStampTracker::AddTimeStamp(int64 time_stamp) {
  static const int kTimeStampOutOfOrderDetectionInterval = 300;

  // Update out of order info.
  if (time_stamp < last_time_stamp_) {
    ++num_out_of_order_pairs_;
  }
  last_time_stamp_ = time_stamp;
  if (++num_time_stamps_detected_ == kTimeStampOutOfOrderDetectionInterval) {
    if (num_out_of_order_pairs_ > 0) {
      O3D_ERROR(service_locator_)
          << "O3D detected " << num_out_of_order_pairs_
          << " frames out of order in the past "
          << kTimeStampOutOfOrderDetectionInterval << " frames.";
    }
    num_out_of_order_pairs_ = 0;
    num_time_stamps_detected_ = 0;
  }

  // Update frame rate estimation.
  time_stamps_[time_stamp_pos_] = time_stamp;
  if (++time_stamp_pos_ == kNumTimestamps) {
    // Calculate the harmonic mean as an estimator for frame rate.
    float accumulated_frame_rate = 0;
    int num_accumulated = 0;
    for (int i = 1; i < kNumTimestamps; ++i) {
      const int64 time_delta_ns = time_stamps_[i] - time_stamps_[i - 1];
      if (time_delta_ns > 0) {
        accumulated_frame_rate += 1000000000.0F / time_delta_ns;
        ++num_accumulated;
      }
    }
    if (num_accumulated > 1) {
      estimated_frame_duration_ = num_accumulated / accumulated_frame_rate;
      if (estimated_frame_duration_ > 0.5F) {
        // We don't expect the frame rate is less than 2fps.
        estimated_frame_duration_ = 0.5F;
      }
    }

    // Put last time stamp in the first position for next round of estimation.
    time_stamps_[0] = time_stamps_[kNumTimestamps - 1];
    time_stamp_pos_ = 1;
  }
}

}  // namespace o3d
