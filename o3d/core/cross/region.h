/*
 * Copyright 2011, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifndef O3D_CORE_CROSS_REGION_H_
#define O3D_CORE_CROSS_REGION_H_

#include <set>

namespace o3d {

struct Region {
  struct Less {
    bool operator()(const Region& region0, const Region& region1) {
      if (!region0.everywhere && region1.everywhere) {
        return true;
      } else if (region0.everywhere && !region1.everywhere) {
        return false;
      }

      if (region0.everywhere && region1.everywhere) {
        // Other fields are ignored when "everywhere" is set.
        return false;
      }

      if (region0.x < region1.x) {
        return true;
      } else if (region0.x > region1.x) {
        return false;
      }

      if (region0.y < region1.y) {
        return true;
      } else if (region0.y > region1.y) {
        return false;
      }

      if (region0.width < region1.width) {
        return true;
      } else if (region0.width > region1.width) {
        return false;
      }

      if (region0.height < region1.height) {
        return true;
      } else if (region0.height > region1.height) {
        return false;
      }

      return false;
    }
  };

  typedef std::set<Region, Less> RegionSet;

  Region(bool everywhere, double x, double y, double width, double height)
      : everywhere(everywhere),
        x(x),
        y(y),
        width(width),
        height(height) {
  }

  bool everywhere;
  double x;
  double y;
  double width;
  double height;
};

}  // namespace o3d

#endif  // O3D_CORE_CROSS_REGION_H_
