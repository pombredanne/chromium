/*
 * Copyright 2009, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <fstream>
#include <iostream>
#include <limits>

#include "plugin/cross/config.h"
#include "base/logging.h"

namespace o3d {

static bool IsValidGuid(const std::string &guid) {
  const size_t kGuidLength = 36u;
  if (guid.length() != kGuidLength) {
    return false;
  }

  const std::string guid_chars = "0123456789abcdefABCDEF";
  for (size_t i = 0; i < guid.length(); ++i) {
    char c = guid[i];
    if (i == 8 || i == 13 || i == 18 || i == 23) {
      if (c != '-') {
        return false;
      }
    } else if (guid_chars.find(c) == std::string::npos) {
      return false;
    }
  }

  return true;
}

// Checks the driver GUID against the blacklist file.  Returns true if there's a
// match [this driver is blacklisted].  If the file doens't contain valid GUID,
// log an error and only return true if an exact match is found.
bool IsDriverBlacklisted(std::ifstream *input_file, const std::string &guid) {
  if (guid.empty()) {
    return false;
  }
  std::string guid_lowercase = guid;
  std::transform(guid_lowercase.begin(), guid_lowercase.end(),
                 guid_lowercase.begin(), ::tolower);
  *input_file >> std::ws;
  while (input_file->good()) {
    if (input_file->peek() == '#') {
      input_file->ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    } else {
      std::string id;
      *input_file >> id;
      if (!IsValidGuid(id)) {
        LOG(ERROR) << "Invalid GUID in blacklist file: " << id;
      }
      std::transform(id.begin(), id.end(), id.begin(), ::tolower);
      if (id == guid_lowercase) {
        return true;
      }
    }
    *input_file >> std::ws; // Skip whitespace here, to catch EOF cleanly.
  }

  if (input_file->fail() && (!input_file->eof())) {
    // failbit will be set when the end-of-file is reached.
    LOG(ERROR) << "Failed to read the blacklisted driver file completely.";
    return false;
  }
  CHECK(input_file->eof());
  return false;
}
}  // namespace o3d
