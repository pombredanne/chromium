/*
 * Copyright 2011, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#import "plugin/mac/o2d_layer.h"

#include "core/mac/display_window_mac.h"
#import "plugin/cross/o3d_glue.h"
#import "plugin/mac/plugin_mac.h"

@implementation O2DLayer

- (id)init {
  self = [super init];

  if (self != nil) {
    self.opaque = YES;
    self.sublayerTransform = CATransform3DMakeScale(1.0, -1.0, 1.0);
    self.transform = CATransform3DMakeScale(1.0, -1.0, 1.0);
    layer_content_ = [[OXDLayerContent alloc] init];
  }

  return self;
}

- (void)dealloc {
  [layer_content_ release];
  [super dealloc];
}

- (void)drawInContext:(CGContextRef)ctx {
  if (![layer_content_ canDraw]) {
    return;
  }

  glue::_o3d::PluginObject* obj = layer_content_.obj;

  if (obj->renderer_init_status() != o3d::Renderer::SUCCESS &&
      obj->renderer_init_status() != o3d::Renderer::UNINITIALIZED) {
    return;
  }

  if (ctx != obj->mac_cg_context_) {
    obj->mac_cg_context_ = ctx;
    o3d::DisplayWindowMac display;
    display.set_agl_context(obj->mac_agl_context_);
    display.set_cgl_context(obj->mac_cgl_context_);
    display.set_cg_context(obj->mac_cg_context_);
    if (obj->renderer()) {
      obj->renderer()->ChangeDisplayWindow(display);
    } else {
      obj->CreateRenderer(display);
      if (!obj->renderer()) {
        return;
      }
      obj->client()->Init();
    }
  }

  [layer_content_ draw];

  [super drawInContext:ctx];
}

- (void)setPluginObject:(glue::_o3d::PluginObject *)obj {
  [layer_content_ setPluginObject:obj];
}

- (void)setWidth:(int)width height:(int)height {
  [layer_content_ setWidth:width height:height];
}

@end
