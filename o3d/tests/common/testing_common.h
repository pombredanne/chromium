/*
 * Copyright 2009, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


// Set of external declarations for global objects required for O3D
// testing.

#ifndef O3D_TESTS_COMMON_TESTING_COMMON_H_
#define O3D_TESTS_COMMON_TESTING_COMMON_H_

#include "core/cross/types.h"
#include "gtest/gtest.h"

namespace o3d {
class DisplayWindow;
class Renderer;
class ServiceLocator;
}

// The ServiceLocator to use for testing.
extern o3d::ServiceLocator* g_service_locator;

// The Renderer to use for testing.
extern o3d::Renderer* g_renderer;

// Whether or not g_renderer is the 2D renderer.
extern bool g_render2d;

// The DisplayWindow used by the renderer.
extern o3d::DisplayWindow* g_display_window;

// Path to the executable, used to load files relative to it.
extern o3d::String *g_program_path;

// Un-qualified name of the executable, stripped of all path information.
// Note that the executable extension is included in this string.
extern o3d::String* g_program_name;

#endif  // O3D_TESTS_COMMON_TESTING_COMMON_H_
