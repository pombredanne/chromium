# Copyright (c) 2010 The Chromium Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

{
  'variables': {
    'variables': {
      # Override to select one of the predefined brandings below.
      'plugin_branding%': '',
    },
    'conditions': [
      ['"<(plugin_branding)" == ""',
        {
          # Default identity. Can be overridden to allow making custom
          # plugins based on O3D, but they will not work with regular O3D apps.
          'plugin_name%': 'O3D Plugin',
          'plugin_npapi_filename%': 'npo3dautoplugin',
          # For historical reasons, the plugin filename is different on Mac.
          'plugin_mac_npapi_filename%': 'O3D',
          'plugin_npapi_mimetype%': 'application/vnd.o3d.auto',
          'plugin_activex_hostcontrol_clsid%': '9666A772-407E-4F90-BC37-982E8160EB2D',
          'plugin_activex_typelib_clsid%': 'D4F6E31C-E952-48FE-9833-6AE308BD79C6',
          'plugin_activex_hostcontrol_name%': 'o3d_host',
          'plugin_activex_typelib_name%': 'npapi_host2',
          'plugin_installdir_csidl%': 'CSIDL_APPDATA',
          'plugin_vendor_directory%': 'Google',
          'plugin_product_directory%': 'O3D',
          'plugin_extras_directory%': 'O3DExtras',
          # You must set this to 1 if changing any of the above to suppress building
          # the installer (since it hard-codes the default identity).
          'plugin_rebranded%': '0',
          # Whether to enable the English-only, Win/Mac-only fullscreen message.
          'plugin_enable_fullscreen_msg%': '1',
          # A comma-separated list of strings, each double-quoted. empty => all domains permitted
          'plugin_domain_whitelist%': '',
          # Linux rpath setting pointing to the libCg install path. empty => none
          'plugin_rpath%' : '/opt/google/o3d/lib',
          # Linux path to optional environment variables file. empty => none
          'plugin_env_vars_file%' : '/opt/google/o3d/envvars',
        },
      ],
      ['"<(plugin_branding)" == "gtpo3d"',
        {
          # Google Talk Plugin branding.
          'plugin_name': 'Google Talk Plugin Video Accelerator',
          'plugin_npapi_filename': 'npgtpo3dautoplugin',
          'plugin_mac_npapi_filename': 'npgtpo3dautoplugin',
          'plugin_npapi_mimetype': 'application/vnd.gtpo3d.auto',
          'plugin_activex_hostcontrol_clsid': '9793fbbf-e9db-3b01-b322-3430cbcf3cd5',
          'plugin_activex_typelib_clsid': '0488b244-bb29-32a9-a0dc-dec6f27229a4',
          'plugin_activex_hostcontrol_name': 'gtpo3d_host',
          'plugin_activex_typelib_name': 'gtpo3d_npapi_host2',
          'plugin_installdir_csidl': 'CSIDL_LOCAL_APPDATA',
          'plugin_vendor_directory': 'Google',
          'plugin_product_directory': 'Google Talk Plugin',
          'plugin_extras_directory': 'Google Talk Plugin Extras',
          'plugin_rebranded': '1',
          'plugin_enable_fullscreen_msg': '0',
          'plugin_domain_whitelist': ('".corp.google.com", '
                                      '".googleplex.com", '
                                      '"hostedtalkgadget.google.com", '
                                      '"mail.google.com", '
                                      '"plus.google.com", '
                                      '"plus.sandbox.google.com", '
                                      '"talk.google.com", '
                                      '"talkgadget.google.com"'),
          'plugin_rpath': '/opt/google/talkplugin/lib',
          'plugin_env_vars_file': '/opt/google/talkplugin/envvars',
        },
      ],
    ],
  },
}
